local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local AutoUpdateInBackground = AutoUpdateInBackground
function GameObjectBattleMapBG:OnInitGame()
  self:Unload()
end
function GameObjectBattleMapBG:ResetBGPos()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("resetBGPos")
  end
end
function GameObjectBattleMapBG:OffsetBGPos(bgX, bgY, offsetX, offsetY)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("offsetBGPos", bgX, bgY, offsetX, offsetY)
  end
end
function GameObjectBattleMapBG:Unload()
  if self.m_currentBG then
    DebugOut("GameObjectBattleMapBG:Unload")
    self.m_currentBG:GetFlashObject():RejectLazyLoadTexture()
    self.m_currentBG:UnloadFlashObject()
    collectgarbage("collect")
  end
  if self:GetFlashObject() then
    self:GetFlashObject():RejectLazyLoadTexture()
  end
  self:UnloadFlashObject()
  self.m_currentBGAreaID = -1
  self.m_currentBGSectionID = -1
end
function GameObjectBattleMapBG:OnAddToGameState()
  if not self:GetFlashObject() and self.m_currentBGAreaID and self.m_currentBGSectionID and self.m_currentBGAreaID ~= -1 and self.m_currentBGSectionID ~= -1 then
    self:LoadBGMap(self.m_currentBGAreaID, self.m_currentBGSectionID)
  end
end
function GameObjectBattleMapBG:OnEraseFromGameState()
  if self.m_currentBG then
    DebugOut("GameObjectBattleMapBG:OnEraseFromGameState")
    self.m_currentBG:GetFlashObject():RejectLazyLoadTexture()
    self.m_currentBG:UnloadFlashObject()
    collectgarbage("collect")
  end
  if self:GetFlashObject() then
    self:GetFlashObject():RejectLazyLoadTexture()
  end
  self:UnloadFlashObject()
  self.m_currentBG = nil
end
function GameObjectBattleMapBG:LoadBGMap(areaID, sectionID)
  DebugOut("Try to LoadBGMap " .. areaID .. " " .. sectionID)
  if areaID > 1 then
    local textureResName = string.format("data2/BG_DOWN_enemy_menubg%d.png", areaID)
    local textureAlphaResName = string.format("data2/BG_DOWN_enemy_menubg%d_alpha.pak", areaID)
    local textureResOk = true
    local textureAlphaResOk = true
    if ext.crc32.crc32(textureResName) == "" or ext.crc32.crc32(textureResName) == nil then
      textureResOk = AutoUpdateInBackground:IsFileUpdatedToServer(textureResName)
    end
    if ext.crc32.crc32(textureAlphaResName) == "" or ext.crc32.crc32(textureAlphaResName) == nil then
      textureAlphaResOk = AutoUpdateInBackground:IsFileUpdatedToServer(textureAlphaResName)
    end
    if not textureResOk or AutoUpdate.isAndroidDevice and not textureAlphaResOk then
      areaID = 1
      sectionID = 1
    end
  end
  DebugOut("LoadBGMap " .. areaID .. " " .. sectionID)
  if self.m_currentBGAreaID == areaID and self.m_currentBGSectionID == sectionID and self.m_currentBG then
    return self.m_currentBG
  end
  self:Unload()
  local filename = "enemy_scene_bg_1_1.tfs"
  if areaID ~= 1 or sectionID ~= 1 then
    local resolution = ""
    if ext.is16x9() then
      resolution = "_16x9"
    elseif ext.is4x3() then
      resolution = "_4x3"
    elseif ext.is2x1 and ext.is2x1() then
      resolution = "_16x9"
    end
    local bgFilename = ""
    local bgCheckName = ""
    if AutoUpdate.isLzma3to1Support then
      bgCheckName = string.format("BG_DOWN_enemy_scene_bg_%d_%d_lzma_3to1.tfs", areaID, sectionID)
      bgFilename = string.format("BG_DOWN_enemy_scene_bg_%d_%d%s.tfs", areaID, sectionID, resolution)
    elseif AutoUpdate.isLzmaSupport then
      bgCheckName = string.format("BG_DOWN_enemy_scene_bg_%d_%d%s_lzma.tfs", areaID, sectionID, resolution)
      bgFilename = string.format("BG_DOWN_enemy_scene_bg_%d_%d%s.tfs", areaID, sectionID, resolution)
    else
      bgCheckName = string.format("BG_DOWN_enemy_scene_bg_%d_%d%s.tfs", areaID, sectionID, resolution)
      bgFilename = string.format("BG_DOWN_enemy_scene_bg_%d_%d%s.tfs", areaID, sectionID, resolution)
    end
    DebugOut("BG NAME = ", bgFilename)
    local bgCheckOk = false
    if ext.crc32.crc32("data2/" .. bgCheckName) == "" or ext.crc32.crc32("data2/" .. bgCheckName) == nil then
      bgCheckOk = AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/" .. bgCheckName)
    else
      bgCheckOk = true
    end
    if bgCheckOk then
      filename = bgCheckName
    else
      areaID = 1
      sectionID = 1
    end
  end
  self.m_flashFileName = filename
  self.m_currentBGAreaID = areaID
  self.m_currentBGSectionID = sectionID
  self:LoadFlashObject()
end
