WVEEnemy = luaClass(WVEPlayer)
function WVEEnemy:ctor(id, startPoint, endPoint, speed, movedDistance, playerStatus, nextPointArriveTime, finallPointArriveTime)
  self.mStartPoint = startPoint
  self.mEndPoint = endPoint
  self.mCurrentPathID = nil
  self.mCurrentPathDistance = nil
  self.mCurrentMovedDistance = movedDistance
  self.mSpeed = speed
  self.mType = EWVEPlayerType.TYPE_ENEMY
  self.mTypeFrame = EWVEPlayerTypeFrame[self.mType]
  self.mStatus = playerStatus
  self.mNextPointTime = nextPointArriveTime
  self.mFinallPointTime = finallPointArriveTime
  self.mUpdateTime = os.time()
end
function WVEEnemy:Refresh(startPoint, endPoint, speed, movedDistance, playerStatus, nextPointArriveTime, finallPointArriveTime)
  self.mStartPoint = startPoint
  self.mEndPoint = endPoint
  self.mCurrentPathID = nil
  self.mCurrentPathDistance = nil
  self.mCurrentMovedDistance = movedDistance
  self.mSpeed = speed
  self.mType = EWVEPlayerType.TYPE_ENEMY
  self.mTypeFrame = EWVEPlayerTypeFrame[self.mType]
  self.mStatus = playerStatus
  self.mNextPointTime = nextPointArriveTime
  self.mFinallPointTime = finallPointArriveTime
  self.mUpdateTime = os.time()
end
function WVEEnemy:SetDetailInfo(bigBossCount, smallBossCount)
  self.mBigBossCount = bigBossCount
  self.mSmallBossCount = smallBossCount
end
function WVEEnemy:OnUpdate(dt)
  local nextPointTime = self.mNextPointTime - (os.time() - self.mUpdateTime)
  local finallPointTime = self.mFinallPointTime - (os.time() - self.mUpdateTime)
  if nextPointTime < 0 then
    nextPointTime = 0
  end
  if finallPointTime < 0 then
    finallPointTime = 0
  end
end
