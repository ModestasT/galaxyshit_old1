local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateFormation = GameStateManager.GameStateFormation
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
GameTip._TipStack = {}
GameTip._ActiveTip = nil
local k_TipNormal = 0
local k_TipBattleResuilt = 1
function GameTip:OnInitGame()
  function GameTip.Updator()
    if GameTip:GetFlashObject() then
      GameTip:CheckDisplayStatus()
    end
    return 1
  end
  GameTimer:Add(GameTip.Updator, 1)
end
function GameTip:OnAddToGameState(TheGameStateAddTo)
  self.LastGameState = TheGameStateAddTo
end
function GameTip:OnEraseFromGameState()
  self.LastGameState = nil
end
function GameTip:OnFSCommand(cmd, arg)
  if cmd == "animation_movein_over" then
    self._ActiveTip._showing = true
    return
  end
  if cmd == "animation_moveout_over" then
    local overCallback = self._ActiveTip.on_over
    if overCallback then
      if overCallback.args and #overCallback.args > 0 then
        overCallback.func(unpack(overCallback.args))
      else
        overCallback.func()
      end
    end
    self._ActiveTip = nil
    table.remove(self._TipStack, 1)
    return
  end
  if cmd == "clicked" then
    self:MoveOutActiveTip()
    return
  end
end
function GameTip:MoveOutActiveTip()
  local activeTip = self._ActiveTip
  if activeTip and activeTip._showing then
    activeTip._showing = false
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_moveOut")
  end
end
function GameTip:Show(info, time, callback, args, checkDuplicate)
  time = time or 3000
  local on_tip_over
  if callback then
    on_tip_over = {}
    on_tip_over.func = callback
    on_tip_over.args = args
  end
  if checkDuplicate then
    for i, v in ipairs(GameTip._TipStack) do
      DebugOut("v.content ", v.content)
      if info == v.content then
        return
      end
    end
  end
  DebugOut("GameTip_Show " .. debug.traceback())
  table.insert(GameTip._TipStack, {
    content = info,
    last_time = time,
    on_over = on_tip_over,
    tipType = k_TipNormal
  })
end
function GameTip:Update(dt)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local active_tip = self._ActiveTip
  if active_tip and active_tip._showing then
    active_tip.last_time = active_tip.last_time - dt
    if active_tip.last_time <= 0 then
      self:MoveOutActiveTip()
    end
  end
  flash_obj:Update(dt)
end
function GameTip:Render()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Render()
  end
end
function GameTip:CheckDisplayStatus()
  local flash_obj = self:GetFlashObject()
  if #self._TipStack > 0 and not self._ActiveTip then
    DebugOut("GameTip:CheckDisplayStatus")
    local popIndex
    for i, tip in ipairs(GameTip._TipStack) do
      if tip.tipType == k_TipBattleResuilt and not self:CheckDisplay() then
        DebugOut("Delay to show tip.")
        DebugTable(tip)
      else
        popIndex = i
        break
      end
    end
    if popIndex == nil then
      DebugOut("No tip shown fined")
      return
    end
    if popIndex ~= 1 then
      local popTip = GameTip._TipStack[popIndex]
      table.remove(GameTip._TipStack, popIndex)
      table.insert(GameTip._TipStack, 1, popTip)
      DebugOut("Show Find Tip")
    end
    self._ActiveTip = GameTip._TipStack[1]
    local keybroadUsed = GameTextEdit.isInUse or false
    local offy = self._ActiveTip.offy or 0
    if not self.LastGameState then
      GameStateGlobalState:AddObject(self)
    end
    flash_obj:InvokeASCallback("_root", "lua2fs_setContentAndMoveIn", self._ActiveTip.content, keybroadUsed)
    self._ActiveTip._showing = false
  end
  if #self._TipStack == 0 and not self._ActiveTip and self.LastGameState then
    self.LastGameState:EraseObject(self)
  end
end
function GameTip:ShowBattleResultNotify(errorCode, time, callback, args)
  time = time or 3000
  local on_tip_over
  if callback then
    on_tip_over = {}
    on_tip_over.func = callback
    on_tip_over.args = args
  end
  local textId = AlertDataList:GetTextIdFromCode(errorCode)
  local info = GameLoader:GetGameText(textId)
  table.insert(GameTip._TipStack, {
    content = info,
    last_time = time,
    on_over = on_tip_over,
    tipType = k_TipBattleResuilt
  })
end
function GameTip:CheckDisplay()
  local currentState = GameStateManager:GetCurrentGameState()
  local skipList = {
    "m_currentGameState"
  }
  local stateName = LuaUtils:getKeyFromValue(GameStateManager, currentState, skipList)
  DebugState("GameTip:CheckDisplay Currant State " .. stateName)
  local ActiveState = GameStateManager:GetCurrentGameState()
  if ActiveState == GameStateBattlePlay then
    return false
  elseif ActiveState == GameStateFormation then
    return false
  elseif ActiveState == GameStateBattleMap then
    if GameStateBattleMap:IsObjectInState(GameUIEvent) then
      return false
    end
  elseif ActiveState == GameStateMainPlanet then
    if GameStateMainPlanet:IsObjectInState(GameVip) or GameStateMainPlanet:IsObjectInState(GameUIFirstCharge) then
      return false
    end
  elseif GameStateGlobalState:IsObjectInState(GameUIPrestigeRankUp) then
    return false
  end
  return true
end
if AutoUpdate.isAndroidDevice then
  function GameTip.OnAndroidBack()
    GameTip:GetFlashObject():InvokeASCallback("_root", "lua2fs_moveOut")
  end
end
