local Monsters = GameData.ChapterMonsterAct3.Monsters
Monsters[301001] = {
  ID = 301001,
  durability = 5626,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301002] = {
  ID = 301002,
  durability = 4560,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301003] = {
  ID = 301003,
  durability = 1010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301004] = {
  ID = 301004,
  durability = 2431,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301005] = {
  ID = 301005,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301006] = {
  ID = 301006,
  durability = 5628,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301007] = {
  ID = 301007,
  durability = 4560,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301008] = {
  ID = 301008,
  durability = 1010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301009] = {
  ID = 301009,
  durability = 2431,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301010] = {
  ID = 301010,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301011] = {
  ID = 301011,
  durability = 5626,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301012] = {
  ID = 301012,
  durability = 4561,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301013] = {
  ID = 301013,
  durability = 1010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301014] = {
  ID = 301014,
  durability = 2431,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301015] = {
  ID = 301015,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301016] = {
  ID = 301016,
  durability = 8287,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[301017] = {
  ID = 301017,
  durability = 4561,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301018] = {
  ID = 301018,
  durability = 1010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301019] = {
  ID = 301019,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301020] = {
  ID = 301020,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301021] = {
  ID = 301021,
  durability = 5628,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301022] = {
  ID = 301022,
  durability = 4562,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301023] = {
  ID = 301023,
  durability = 1010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301024] = {
  ID = 301024,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301025] = {
  ID = 301025,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301026] = {
  ID = 301026,
  durability = 5628,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301027] = {
  ID = 301027,
  durability = 4560,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301028] = {
  ID = 301028,
  durability = 1010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301029] = {
  ID = 301029,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301030] = {
  ID = 301030,
  durability = 2430,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301031] = {
  ID = 301031,
  durability = 6010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301032] = {
  ID = 301032,
  durability = 4869,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301033] = {
  ID = 301033,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301034] = {
  ID = 301034,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301035] = {
  ID = 301035,
  durability = 2583,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301036] = {
  ID = 301036,
  durability = 8865,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[301037] = {
  ID = 301037,
  durability = 4867,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301038] = {
  ID = 301038,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[301039] = {
  ID = 301039,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[301040] = {
  ID = 301040,
  durability = 2583,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302001] = {
  ID = 302001,
  durability = 6009,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302002] = {
  ID = 302002,
  durability = 4869,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302003] = {
  ID = 302003,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302004] = {
  ID = 302004,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302005] = {
  ID = 302005,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302006] = {
  ID = 302006,
  durability = 6009,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302007] = {
  ID = 302007,
  durability = 4869,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302008] = {
  ID = 302008,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302009] = {
  ID = 302009,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302010] = {
  ID = 302010,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302011] = {
  ID = 302011,
  durability = 6009,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302012] = {
  ID = 302012,
  durability = 4869,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302013] = {
  ID = 302013,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302014] = {
  ID = 302014,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302015] = {
  ID = 302015,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302016] = {
  ID = 302016,
  durability = 8868,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[302017] = {
  ID = 302017,
  durability = 4869,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302018] = {
  ID = 302018,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302019] = {
  ID = 302019,
  durability = 2583,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302020] = {
  ID = 302020,
  durability = 2583,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302021] = {
  ID = 302021,
  durability = 6010,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302022] = {
  ID = 302022,
  durability = 4868,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302023] = {
  ID = 302023,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302024] = {
  ID = 302024,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302025] = {
  ID = 302025,
  durability = 2583,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302026] = {
  ID = 302026,
  durability = 6009,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302027] = {
  ID = 302027,
  durability = 4867,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302028] = {
  ID = 302028,
  durability = 1061,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302029] = {
  ID = 302029,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302030] = {
  ID = 302030,
  durability = 2584,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302031] = {
  ID = 302031,
  durability = 6396,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302032] = {
  ID = 302032,
  durability = 5176,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302033] = {
  ID = 302033,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302034] = {
  ID = 302034,
  durability = 2737,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302035] = {
  ID = 302035,
  durability = 2738,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302036] = {
  ID = 302036,
  durability = 9441,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[302037] = {
  ID = 302037,
  durability = 5176,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302038] = {
  ID = 302038,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[302039] = {
  ID = 302039,
  durability = 2737,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[302040] = {
  ID = 302040,
  durability = 2738,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303001] = {
  ID = 303001,
  durability = 4158,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[303002] = {
  ID = 303002,
  durability = 3225,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303003] = {
  ID = 303003,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303004] = {
  ID = 303004,
  durability = 1518,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303005] = {
  ID = 303005,
  durability = 1925,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303006] = {
  ID = 303006,
  durability = 4158,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[303007] = {
  ID = 303007,
  durability = 3224,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303008] = {
  ID = 303008,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303009] = {
  ID = 303009,
  durability = 1518,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303010] = {
  ID = 303010,
  durability = 1924,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303011] = {
  ID = 303011,
  durability = 4159,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[303012] = {
  ID = 303012,
  durability = 3226,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303013] = {
  ID = 303013,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303014] = {
  ID = 303014,
  durability = 1518,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303015] = {
  ID = 303015,
  durability = 1925,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303016] = {
  ID = 303016,
  durability = 6091,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[303017] = {
  ID = 303017,
  durability = 3226,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303018] = {
  ID = 303018,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303019] = {
  ID = 303019,
  durability = 1518,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303020] = {
  ID = 303020,
  durability = 1925,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303021] = {
  ID = 303021,
  durability = 4160,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[303022] = {
  ID = 303022,
  durability = 3225,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303023] = {
  ID = 303023,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303024] = {
  ID = 303024,
  durability = 1518,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303025] = {
  ID = 303025,
  durability = 1925,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303026] = {
  ID = 303026,
  durability = 4159,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[303027] = {
  ID = 303027,
  durability = 3225,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303028] = {
  ID = 303028,
  durability = 1112,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303029] = {
  ID = 303029,
  durability = 1518,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303030] = {
  ID = 303030,
  durability = 1924,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303031] = {
  ID = 303031,
  durability = 4403,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[303032] = {
  ID = 303032,
  durability = 3409,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303033] = {
  ID = 303033,
  durability = 1163,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303034] = {
  ID = 303034,
  durability = 1595,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303035] = {
  ID = 303035,
  durability = 2027,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303036] = {
  ID = 303036,
  durability = 6454,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[303037] = {
  ID = 303037,
  durability = 3409,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303038] = {
  ID = 303038,
  durability = 1163,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[303039] = {
  ID = 303039,
  durability = 1595,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[303040] = {
  ID = 303040,
  durability = 2027,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304001] = {
  ID = 304001,
  durability = 4187,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[304002] = {
  ID = 304002,
  durability = 3237,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304003] = {
  ID = 304003,
  durability = 1163,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304004] = {
  ID = 304004,
  durability = 1596,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304005] = {
  ID = 304005,
  durability = 2027,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304006] = {
  ID = 304006,
  durability = 4188,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[304007] = {
  ID = 304007,
  durability = 3236,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304008] = {
  ID = 304008,
  durability = 1163,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304009] = {
  ID = 304009,
  durability = 1595,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304010] = {
  ID = 304010,
  durability = 2027,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304011] = {
  ID = 304011,
  durability = 4186,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[304012] = {
  ID = 304012,
  durability = 3236,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304013] = {
  ID = 304013,
  durability = 1164,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304014] = {
  ID = 304014,
  durability = 1596,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304015] = {
  ID = 304015,
  durability = 2027,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304016] = {
  ID = 304016,
  durability = 6130,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[304017] = {
  ID = 304017,
  durability = 3236,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304018] = {
  ID = 304018,
  durability = 1164,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304019] = {
  ID = 304019,
  durability = 1595,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304020] = {
  ID = 304020,
  durability = 2027,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304021] = {
  ID = 304021,
  durability = 4186,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[304022] = {
  ID = 304022,
  durability = 3237,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304023] = {
  ID = 304023,
  durability = 1163,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304024] = {
  ID = 304024,
  durability = 1596,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304025] = {
  ID = 304025,
  durability = 2028,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304026] = {
  ID = 304026,
  durability = 4188,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[304027] = {
  ID = 304027,
  durability = 3236,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304028] = {
  ID = 304028,
  durability = 1164,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304029] = {
  ID = 304029,
  durability = 1595,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304030] = {
  ID = 304030,
  durability = 2027,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304031] = {
  ID = 304031,
  durability = 4417,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[304032] = {
  ID = 304032,
  durability = 3411,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304033] = {
  ID = 304033,
  durability = 1215,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304034] = {
  ID = 304034,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304035] = {
  ID = 304035,
  durability = 2129,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304036] = {
  ID = 304036,
  durability = 6476,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[304037] = {
  ID = 304037,
  durability = 3411,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304038] = {
  ID = 304038,
  durability = 1214,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[304039] = {
  ID = 304039,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[304040] = {
  ID = 304040,
  durability = 2129,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305001] = {
  ID = 305001,
  durability = 4417,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[305002] = {
  ID = 305002,
  durability = 3410,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305003] = {
  ID = 305003,
  durability = 1215,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305004] = {
  ID = 305004,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305005] = {
  ID = 305005,
  durability = 2130,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305006] = {
  ID = 305006,
  durability = 4417,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[305007] = {
  ID = 305007,
  durability = 3411,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305008] = {
  ID = 305008,
  durability = 1215,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305009] = {
  ID = 305009,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305010] = {
  ID = 305010,
  durability = 2130,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305011] = {
  ID = 305011,
  durability = 4417,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[305012] = {
  ID = 305012,
  durability = 3411,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305013] = {
  ID = 305013,
  durability = 1215,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305014] = {
  ID = 305014,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305015] = {
  ID = 305015,
  durability = 2130,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305016] = {
  ID = 305016,
  durability = 6474,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[305017] = {
  ID = 305017,
  durability = 3411,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305018] = {
  ID = 305018,
  durability = 1215,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305019] = {
  ID = 305019,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305020] = {
  ID = 305020,
  durability = 2130,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305021] = {
  ID = 305021,
  durability = 4416,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[305022] = {
  ID = 305022,
  durability = 3411,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305023] = {
  ID = 305023,
  durability = 1214,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305024] = {
  ID = 305024,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305025] = {
  ID = 305025,
  durability = 2130,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305026] = {
  ID = 305026,
  durability = 4416,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[305027] = {
  ID = 305027,
  durability = 3411,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305028] = {
  ID = 305028,
  durability = 1214,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305029] = {
  ID = 305029,
  durability = 1672,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305030] = {
  ID = 305030,
  durability = 2130,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305031] = {
  ID = 305031,
  durability = 4647,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[305032] = {
  ID = 305032,
  durability = 3585,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305033] = {
  ID = 305033,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305034] = {
  ID = 305034,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305035] = {
  ID = 305035,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305036] = {
  ID = 305036,
  durability = 6823,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[305037] = {
  ID = 305037,
  durability = 3585,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305038] = {
  ID = 305038,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[305039] = {
  ID = 305039,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[305040] = {
  ID = 305040,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306001] = {
  ID = 306001,
  durability = 4648,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[306002] = {
  ID = 306002,
  durability = 3585,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306003] = {
  ID = 306003,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306004] = {
  ID = 306004,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306005] = {
  ID = 306005,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306006] = {
  ID = 306006,
  durability = 4647,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[306007] = {
  ID = 306007,
  durability = 3585,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306008] = {
  ID = 306008,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306009] = {
  ID = 306009,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306010] = {
  ID = 306010,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306011] = {
  ID = 306011,
  durability = 4647,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[306012] = {
  ID = 306012,
  durability = 3584,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306013] = {
  ID = 306013,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306014] = {
  ID = 306014,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306015] = {
  ID = 306015,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306016] = {
  ID = 306016,
  durability = 6820,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[306017] = {
  ID = 306017,
  durability = 3585,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306018] = {
  ID = 306018,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306019] = {
  ID = 306019,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306020] = {
  ID = 306020,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306021] = {
  ID = 306021,
  durability = 4647,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[306022] = {
  ID = 306022,
  durability = 3584,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306023] = {
  ID = 306023,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306024] = {
  ID = 306024,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306025] = {
  ID = 306025,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306026] = {
  ID = 306026,
  durability = 4647,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[306027] = {
  ID = 306027,
  durability = 3585,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306028] = {
  ID = 306028,
  durability = 1266,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306029] = {
  ID = 306029,
  durability = 1749,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306030] = {
  ID = 306030,
  durability = 2232,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306031] = {
  ID = 306031,
  durability = 4878,
  Avatar = "head35",
  Ship = "ship37"
}
Monsters[306032] = {
  ID = 306032,
  durability = 3758,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306033] = {
  ID = 306033,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306034] = {
  ID = 306034,
  durability = 1826,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306035] = {
  ID = 306035,
  durability = 2334,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306036] = {
  ID = 306036,
  durability = 7167,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[306037] = {
  ID = 306037,
  durability = 3759,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306038] = {
  ID = 306038,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[306039] = {
  ID = 306039,
  durability = 1826,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[306040] = {
  ID = 306040,
  durability = 2334,
  Avatar = "head35",
  Ship = "ship14"
}
Monsters[307001] = {
  ID = 307001,
  durability = 4879,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307002] = {
  ID = 307002,
  durability = 3759,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307003] = {
  ID = 307003,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307004] = {
  ID = 307004,
  durability = 1825,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307005] = {
  ID = 307005,
  durability = 2335,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307006] = {
  ID = 307006,
  durability = 4879,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307007] = {
  ID = 307007,
  durability = 3759,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307008] = {
  ID = 307008,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307009] = {
  ID = 307009,
  durability = 1826,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307010] = {
  ID = 307010,
  durability = 2334,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307011] = {
  ID = 307011,
  durability = 4877,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307012] = {
  ID = 307012,
  durability = 3758,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307013] = {
  ID = 307013,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307014] = {
  ID = 307014,
  durability = 1825,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307015] = {
  ID = 307015,
  durability = 2334,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307016] = {
  ID = 307016,
  durability = 7167,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[307017] = {
  ID = 307017,
  durability = 3759,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307018] = {
  ID = 307018,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307019] = {
  ID = 307019,
  durability = 1826,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307020] = {
  ID = 307020,
  durability = 2334,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307021] = {
  ID = 307021,
  durability = 4878,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307022] = {
  ID = 307022,
  durability = 3759,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307023] = {
  ID = 307023,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307024] = {
  ID = 307024,
  durability = 1826,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307025] = {
  ID = 307025,
  durability = 2334,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307026] = {
  ID = 307026,
  durability = 4879,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307027] = {
  ID = 307027,
  durability = 3759,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307028] = {
  ID = 307028,
  durability = 1317,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307029] = {
  ID = 307029,
  durability = 1825,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307030] = {
  ID = 307030,
  durability = 2334,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307031] = {
  ID = 307031,
  durability = 5339,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307032] = {
  ID = 307032,
  durability = 4107,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307033] = {
  ID = 307033,
  durability = 1420,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307034] = {
  ID = 307034,
  durability = 1979,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307035] = {
  ID = 307035,
  durability = 2539,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307036] = {
  ID = 307036,
  durability = 7858,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[307037] = {
  ID = 307037,
  durability = 4107,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307038] = {
  ID = 307038,
  durability = 1419,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[307039] = {
  ID = 307039,
  durability = 1979,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[307040] = {
  ID = 307040,
  durability = 2539,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308001] = {
  ID = 308001,
  durability = 5338,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308002] = {
  ID = 308002,
  durability = 4107,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308003] = {
  ID = 308003,
  durability = 1419,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308004] = {
  ID = 308004,
  durability = 1979,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308005] = {
  ID = 308005,
  durability = 2539,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308006] = {
  ID = 308006,
  durability = 5340,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308007] = {
  ID = 308007,
  durability = 4107,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308008] = {
  ID = 308008,
  durability = 1419,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308009] = {
  ID = 308009,
  durability = 1979,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308010] = {
  ID = 308010,
  durability = 2539,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308011] = {
  ID = 308011,
  durability = 5339,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308012] = {
  ID = 308012,
  durability = 4106,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308013] = {
  ID = 308013,
  durability = 1419,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308014] = {
  ID = 308014,
  durability = 1979,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308015] = {
  ID = 308015,
  durability = 2539,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308016] = {
  ID = 308016,
  durability = 7858,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[308017] = {
  ID = 308017,
  durability = 4106,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308018] = {
  ID = 308018,
  durability = 1420,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308019] = {
  ID = 308019,
  durability = 1979,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308020] = {
  ID = 308020,
  durability = 2539,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308021] = {
  ID = 308021,
  durability = 5338,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308022] = {
  ID = 308022,
  durability = 4106,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308023] = {
  ID = 308023,
  durability = 1419,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308024] = {
  ID = 308024,
  durability = 1980,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308025] = {
  ID = 308025,
  durability = 2539,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308026] = {
  ID = 308026,
  durability = 5339,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308027] = {
  ID = 308027,
  durability = 4108,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308028] = {
  ID = 308028,
  durability = 1419,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308029] = {
  ID = 308029,
  durability = 1979,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308030] = {
  ID = 308030,
  durability = 2540,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308031] = {
  ID = 308031,
  durability = 5799,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308032] = {
  ID = 308032,
  durability = 4455,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308033] = {
  ID = 308033,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308034] = {
  ID = 308034,
  durability = 2133,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308035] = {
  ID = 308035,
  durability = 2744,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308036] = {
  ID = 308036,
  durability = 8549,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[308037] = {
  ID = 308037,
  durability = 4455,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308038] = {
  ID = 308038,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[308039] = {
  ID = 308039,
  durability = 2133,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[308040] = {
  ID = 308040,
  durability = 2744,
  Avatar = "head35",
  Ship = "ship25"
}
