local GameStateMineMap = GameStateManager.GameStateMineMap
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameObjectMineMap = LuaObjectManager:GetLuaObject("GameObjectMineMap")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local QuestTutorialMine = TutorialQuestManager.QuestTutorialMine
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameStateKrypton = GameStateManager.GameStateKrypton
local localBoostInfo = {boostCountNow = 0, maxBoostCount = 0}
GameObjectMineMap.FORCE_INTERVAL = {
  [1] = {
    color = "#999999",
    text_id = "LC_MENU_MINING_IMPROVE_FORCE_LEVEL_1"
  },
  [2] = {
    color = "#33CC00",
    text_id = "LC_MENU_MINING_IMPROVE_FORCE_LEVEL_2"
  },
  [3] = {
    color = "#00BFFF",
    text_id = "LC_MENU_MINING_IMPROVE_FORCE_LEVEL_3"
  },
  [4] = {
    color = "#FF6600",
    text_id = "LC_MENU_MINING_IMPROVE_FORCE_LEVEL_4"
  },
  [5] = {
    color = "#FF0000",
    text_id = "LC_MENU_MINING_IMPROVE_FORCE_LEVEL_5"
  }
}
function GameObjectMineMap:OnAddToGameState(game_state)
  DebugOut("OnAddToGameState:GameObjectMineMap")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  NetMessageMgr:SendMsg(NetAPIList.mine_list_req.Code, nil, self.NetCallbackEnter, true, nil)
  NetMessageMgr:SendMsg(NetAPIList.mine_info_req.Code, nil, self.NetCallbackEnter, true, nil)
  self:GetFlashObject():InvokeASCallback("_root", "HideTipClose")
  if self.firstEnter == nil then
    self.firstEnter = true
  else
    self.firstEnter = false
  end
  GameObjectMineMap:CheckDownloadImage()
  self:UpdateOperationPrice("refresh_mine_normal")
  self:UpdateOperationPrice("refresh_mine_credit")
  self:UpdateOperationPrice("refresh_mine_credit_max")
  self:UpdateOperationPrice("boost_mine")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  self:GetFlashObject():InvokeASCallback("_root", "setVisible", "btnRefreshMineCredit", curLevel >= 2)
  self:GetFlashObject():InvokeASCallback("_root", "setVisible", "btnRefreshMineAdvanced", curLevel >= 4)
  self:GetFlashObject():InvokeASCallback("_root", "setVisible", "btnBoost", curLevel >= 4)
  self:RefreshResourceCount()
  if immanentversion170 == 4 or immanentversion170 == 5 then
    local unlock = true
    local kenergycenter = GameGlobalData:GetBuildingInfo("krypton_center")
    unlock = kenergycenter.level > 0
    self:GetFlashObject():InvokeASCallback("_root", "setKryptonBtnVisible", true, GameLoader:GetGameText("LC_MENU_UPGRADE_GALACTONITE"), unlock)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setKryptonBtnVisible", false, GameLoader:GetGameText("LC_MENU_UPGRADE_GALACTONITE"), false)
  end
  local resource = GameGlobalData:GetData("resource")
  self:GetFlashObject():InvokeASCallback("_root", "setKryptonInfo", GameUtils.numberConversion(resource.kenergy))
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResourceCount)
  GameGlobalData:RegisterDataChangeCallback("resource", function()
    local resource = GameGlobalData:GetData("resource")
    if GameObjectMineMap and GameObjectMineMap:GetFlashObject() then
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "setKryptonInfo", GameUtils.numberConversion(resource.kenergy))
    end
  end)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.mine_list_ntf.Code, GameObjectMineMap.MineListNtfHandeler)
  GameObjectMineMap:checkHelpTutorial()
end
function GameObjectMineMap:OnEraseFromGameState(game_state)
  self.AttackCDTimeUpdater = nil
  self.MineCDTimeUpdater = nil
  GameObjectMineMap.isOnShowRresh = nil
  GameObjectMineMap.isOnShowMeMineDetail = nil
  GameObjectMineMap.isOnShowOtherMineDetail = nil
  NetMessageMgr:RemoveMsgHandler(NetAPIList.mine_list_ntf.Code)
  self:UnloadFlashObject()
end
function GameObjectMineMap.MineListNtfHandeler(content)
  GameObjectMineMap.receiveTime = os.time()
  GameObjectMineMap.mine_pool = content.miners
  GameObjectMineMap:UpdateEnemyMiners(content.revenge_miners)
end
function GameObjectMineMap:CheckDownloadImage()
  DebugOut("!!!!what's wrong")
  if (ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_map_star_01.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_map_star_01.png") then
    DebugOut("what's wrong")
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_map_star_01.png", "territorial_map_bg.png")
  end
end
function GameObjectMineMap:IsFirstEnter()
  return self.firstEnter
end
function GameObjectMineMap:MoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "animationMoveIn")
end
function GameObjectMineMap:InitFlashObject()
end
function GameObjectMineMap:OnFSCommand(cmd, arg)
  DebugOut("\230\137\147\229\141\176------------->  ", cmd, arg)
  if cmd == "mine_info" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "getMoney" then
      self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorialMine", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorialMine", false)
    end
    if QuestTutorialMine:IsActive() then
      AddFlurryEvent("TutorialMine_ScanMine", {}, 2)
    end
    local function netCallProcess()
      NetMessageMgr:SendMsg(NetAPIList.mine_info_req.Code, nil, self.NetCallbackMineInfo, true, netCallProcess)
    end
    netCallProcess()
  elseif cmd == "mineable_map_move_out_over" then
    self.isOnShowRresh = nil
    if self.m_playSotryRobOther then
      self.m_playSotryRobOther = nil
      GameUICommonDialog:PlayStory({100051}, nil)
    end
  elseif cmd == "show_mineable_info" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "getMoney" then
      GameUtils:HideTutorialHelp()
    end
    self.isOnShowMeMineDetail = true
    self:ShowMineableInfo(tonumber(arg))
  elseif cmd == "miner_info_move_in" then
    self.isOnShowOtherMineDetail = true
  elseif cmd == "show_mining_info" then
    local mine_data = self.mine_pool[tonumber(arg)]
    self:ShowMiningInfo(mine_data)
  elseif cmd == "mine_req" then
    self.isOnShowRresh = nil
    self.isOnShowMeMineDetail = nil
    local content = {
      idx = tonumber(arg)
    }
    NetMessageMgr:SendMsg(NetAPIList.mine_digg_req.Code, content, self.NetCallbackMine, true)
    if QuestTutorialMine:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTipClose")
      self:GetFlashObject():InvokeASCallback("_root", "HideTipDig")
      QuestTutorialMine:SetFinish(true)
      self.m_playSotryRobOther = true
      if immanentversion == 2 then
        GameStateMainPlanet:SetStoryWhenFocusGain({100011, 51321})
      elseif immanentversion == 1 then
        GameStateMainPlanet:SetStoryWhenFocusGain({100011})
      end
    end
    if immanentversion170 == 4 or immanentversion170 == 5 then
      QuestTutorialMine:SetFinish(true)
    end
  elseif cmd == "refresh_mineable_mines" then
    if arg == "3" then
      local cost = GameGlobalData:GetGameConfig("refresh_mine_credit_max")
      local info = string.format(GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR"), tonumber(cost))
      if GameGlobalData:GetItemCount(2503) > 0 then
        info = string.format(GameLoader:GetGameText("LC_MENU_ITEM_USE_ALERT"), GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2503"))
      end
      GameUtils:CreditCostConfirm(info, function()
        local content = {
          refresh_type = tonumber(arg)
        }
        NetMessageMgr:SendMsg(NetAPIList.mine_refresh_req.Code, content, self.NetCallbackRefreshMines, true)
      end)
    elseif arg == "2" then
      local cost = GameGlobalData:GetGameConfig("refresh_mine_credit")
      local info = string.format(GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR"), tonumber(cost))
      if 0 < GameGlobalData:GetItemCount(2502) then
        info = string.format(GameLoader:GetGameText("LC_MENU_ITEM_USE_ALERT"), GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2502"))
      end
      GameUtils:CreditCostConfirm(info, function()
        local content = {
          refresh_type = tonumber(arg)
        }
        NetMessageMgr:SendMsg(NetAPIList.mine_refresh_req.Code, content, self.NetCallbackRefreshMines, true)
      end)
    elseif arg == "1" then
      local cost = GameGlobalData:GetGameConfig("refresh_mine_normal")
      local info = string.format(GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR"), tonumber(cost))
      if 0 < GameGlobalData:GetItemCount(2501) then
        info = string.format(GameLoader:GetGameText("LC_MENU_ITEM_USE_ALERT"), GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2501"))
      end
      GameUtils:CreditCostConfirm(info, function()
        local content = {
          refresh_type = tonumber(arg)
        }
        NetMessageMgr:SendMsg(NetAPIList.mine_refresh_req.Code, content, self.NetCallbackRefreshMines, true)
      end)
    else
      local content = {
        refresh_type = tonumber(arg)
      }
      NetMessageMgr:SendMsg(NetAPIList.mine_refresh_req.Code, content, self.NetCallbackRefreshMines, true)
    end
  elseif cmd == "mineInfoHideOver" then
    self.isOnShowMeMineDetail = nil
    if QuestTutorialMine:IsActive() then
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "ShowTutorialChooseMine")
    end
  elseif cmd == "minerInfoHideOver" then
    self.isOnShowOtherMineDetail = nil
  elseif cmd == "refresh_mines" then
    local function netCallProcess()
      NetMessageMgr:SendMsg(NetAPIList.mine_list_req.Code, nil, self.NetCallbackMineList, true, netCallProcess)
    end
    netCallProcess()
  elseif cmd == "enemy_miner" then
    local mine_data = self.enemy_miners[tonumber(arg)]
    self:ShowMiningInfo(mine_data)
  elseif cmd == "attack_miner" then
    AddFlurryEvent("TutorialMine_RobMine", {}, 2)
    if GameStateManager:GetCurrentGameState().fightRoundData then
      GameStateManager:GetCurrentGameState().fightRoundData = nil
    end
    self.isOnShowOtherMineDetail = nil
    self.lastAttackUser = arg
    local content = {user_id = arg}
    NetMessageMgr:SendMsg(NetAPIList.mine_atk_req.Code, content, self.NetCallbackAttack, true)
  elseif cmd == "mine_boost" then
    GameObjectMineMap:mineBoost()
  elseif cmd == "mine_speedup" then
    GameObjectMineMap:SpeedUp()
  elseif cmd == "clear_attack_cd" then
    GameObjectMineMap:ResetAttackCD()
  elseif cmd == "on_close" then
    GameStateMineMap:Quit()
  elseif cmd == "needUpdateMineableInfo" then
    GameObjectMineMap:UpdateMineablePool()
  elseif cmd == "needUpdateMineMap" then
    GameObjectMineMap:UpdateMinePool()
  elseif cmd == "mining_pos_action" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "getMoney" then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.left, "empty", GameUIMaskLayer.MaskStyle.small)
    end
  elseif cmd == "is_mining" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "getMoney" then
      local tipsText = GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_27")
      if immanentversion170 == 4 or immanentversion170 == 5 then
        tipsText = GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_27_NEW")
      end
      GameUtils:MaskLayerMidTutorial(tipsText)
    end
  elseif cmd == "is_helpTutorial_mine_pos" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "getMoney" then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.left, "empty", GameUIMaskLayer.MaskStyle.small)
    end
  elseif cmd == "touch_krypton" then
    local kryptonCenter = GameGlobalData:GetBuildingInfo("krypton_center")
    if 0 < kryptonCenter.level then
      GameStateKrypton:EnterKrypton(GameStateKrypton.KRYPTON)
    else
      local tipMsg = GameLoader:GetGameText("LC_MENU_UNLOCK_KRYPTON_UPDATE_TIP")
      local buildingNameText = GameLoader:GetGameText("LC_MENU_BUILDING_NAME_KRYPTON_CENTER")
      tipMsg = string.gsub(tipMsg, "<number1>", kryptonCenter.require_user_level)
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(tipMsg)
    end
  end
end
function GameObjectMineMap.mine_boostRequest()
  NetMessageMgr:SendMsg(NetAPIList.mine_boost_req.Code, nil, GameObjectMineMap.NetCallbackBoost, true)
end
function GameObjectMineMap.btn_refresh_advanced()
  local content = {refresh_type = 3}
  NetMessageMgr:SendMsg(NetAPIList.mine_refresh_req.Code, content, GameObjectMineMap.NetCallbackRefreshMines, true)
end
function GameObjectMineMap:GetDynamicItem(id)
  if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      return "item_" .. id
    else
      DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
      return "temp"
    end
  else
    return "item_" .. id
  end
end
function GameObjectMineMap:GetAwardText(rewards)
  local award_data = ""
  local award_item = rewards[1]
  local index = 0
  if award_item == nil then
    return "temp,0,temp,0"
  end
  if award_item.item_type == "item" or award_item.item_type == "krypton" then
    if 0 < award_item.no then
      award_data = GameObjectMineMap:GetDynamicItem(award_item.number) .. "," .. award_item.no
      index = 1
    end
  elseif 0 < award_item.number then
    award_data = award_item.item_type .. "," .. award_item.number
    index = 1
  end
  local award_item = rewards[2]
  if award_item ~= nil then
    if award_item.item_type == "item" or award_item.item_type == "krypton" then
      if 0 < award_item.no then
        if index > 0 then
          award_data = award_data .. ","
        end
        award_data = award_data .. GameObjectMineMap:GetDynamicItem(award_item.number) .. "," .. award_item.no
      end
    elseif 0 < award_item.number then
      if index > 0 then
        award_data = award_data .. ","
      end
      award_data = award_data .. award_item.item_type .. "," .. award_item.number
    end
  end
  DebugOut("award_data", award_data)
  return award_data
end
function GameObjectMineMap:ShowMineableInfo(index)
  local mine_data = self.mineable_pool["pool" .. index]
  local level = mine_data.level
  if DynamicResDownloader:IsDynamicStuff(level, DynamicResDownloader.resType.MINE_PIC) then
    local fullFileName = DynamicResDownloader:GetFullName("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
    else
      level = "temp"
    end
  end
  local award_data = GameObjectMineMap:GetAwardText(mine_data.awards)
  local award_item = mine_data.awards[1]
  local mine_time = GameLoader:GetGameText("LC_MENU_MINING_TIME_CHAR")
  mine_time = "<font color='" .. "#00A3D8" .. "'>" .. mine_time .. "</font>"
  mine_time = mine_time .. "<font color='" .. "#FFC926" .. "'>" .. GameUtils:formatTimeString(mine_data.time) .. "</font>"
  self:GetFlashObject():InvokeASCallback("_root", "showMineableMineInfo", index, level, GameLoader:GetGameText("LC_MENU_MINING_RANK_" .. tostring(mine_data.level)), mine_time, award_data, QuestTutorialMine:IsActive(), mine_data.level)
  DebugOut("ShowMineableInfo")
  DebugOut(mine_data.mine_level)
  DebugTable(mine_data)
  if QuestTutorialMine:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialChooseMine")
  end
end
function GameObjectMineMap:ShowMiningInfo(mine_data)
  local desc = GameLoader:GetGameText("LC_MENU_MINING_PLUNDERED_CHAR")
  desc = "<font color='" .. "#00A3D8" .. "'>" .. desc .. "</font>"
  desc = desc .. "<font color='" .. "#FFC926" .. "'>" .. mine_data.robbed_cnt .. "/" .. mine_data.robbed_max .. "</font>"
  self:GetFlashObject():InvokeASCallback("_root", "setMineBaseInfo", mine_data.mine_level, desc, tonumber(mine_data.robbed_cnt) < tonumber(mine_data.robbed_max))
  DebugTable(mine_data)
  local award_data = GameObjectMineMap:GetAwardText(mine_data.atk_award)
  local level = mine_data.mine_level
  DebugOut("minelevel", level)
  if DynamicResDownloader:IsDynamicStuff(level, DynamicResDownloader.resType.MINE_PIC) then
    local fullFileName = DynamicResDownloader:GetFullName("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
    else
      level = "temp"
    end
  end
  DebugOut("minelevel", level)
  self:GetFlashObject():InvokeASCallback("_root", "setMiningAttackAward", award_data, level)
  local atkCD = 0
  local flash_obj = self:GetFlashObject()
  if mine_data.atk_cd and tonumber(mine_data.robbed_cnt) < tonumber(mine_data.robbed_max) then
    atkCD = tonumber(mine_data.atk_cd)
  end
  DebugOut("ShowMiningInfo:", atkCD, ",", mine_data.atk_cd, ",", os.time(), ",", self.receiveTime)
  if atkCD <= 0 then
    self.AttackCDTimeUpdater = nil
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setMineOwnerAtkCD", -1, GameObjectMineMap.atk_cost or "nil")
    end
  else
    do
      local target_time = self.receiveTime + atkCD
      function self.AttackCDTimeUpdater()
        if not GameObjectMineMap.isOnShowOtherMineDetail then
          self.AttackCDTimeUpdater = nil
          return
        end
        local cd_time = target_time - os.time()
        local flash_obj = GameObjectMineMap:GetFlashObject()
        if not flash_obj then
          return
        end
        if cd_time >= 0 then
          local time_string = GameLoader:GetGameText("LC_MENU_MINING_PLUNDER_CD_CAHR_NEW") .. GameUtils:formatTimeString(cd_time)
          flash_obj:InvokeASCallback("_root", "setMineOwnerAtkCD", time_string, GameObjectMineMap.atk_cost or "nil")
        else
          flash_obj:InvokeASCallback("_root", "setMineOwnerAtkCD", -1, GameObjectMineMap.atk_cost or "nil")
          GameObjectMineMap.AttackCDTimeUpdater = nil
        end
      end
    end
  end
  if flash_obj then
    local forceData = self.FORCE_INTERVAL[mine_data.force_interval]
    forceData = forceData or self.FORCE_INTERVAL[1]
    local forceDesc = GameLoader:GetGameText(forceData.text_id)
    forceDesc = "<font color='" .. forceData.color .. "'>" .. forceDesc .. "</font>"
    local allicanceDesc = GameLoader:GetGameText("LC_MENU_ALLIANCE_TAB") .. ":" .. mine_data.alliance
    DebugOut("color:", forceDesc, ",", mine_data.alliance)
    flash_obj:InvokeASCallback("_root", "setMineOwnerInfo", GameUtils:GetUserDisplayName(mine_data.name), mine_data.level, forceDesc, allicanceDesc, GameLoader:GetGameText("LC_MENU_Level"))
    flash_obj:InvokeASCallback("_root", "showMiningInfo", mine_data.user_id)
  end
end
function GameObjectMineMap.RefreshMineablePool()
  GameObjectMineMap:UpdateMineablePool()
end
function GameObjectMineMap:UpdateMineablePool()
  local mine_pool = self.mineable_pool
  local mines_data = ""
  for i = 1, 4 do
    if mines_data ~= "" then
      mines_data = mines_data .. ","
    end
    local level = mine_pool["pool" .. tostring(i)].level
    if DynamicResDownloader:IsDynamicStuff(level, DynamicResDownloader.resType.MINE_PIC) then
      local fullFileName = DynamicResDownloader:GetFullName("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        mines_data = mines_data .. level
      else
        mines_data = mines_data .. "temp"
        DynamicResDownloader:AddDynamicRes("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC, nil, GameObjectMineMap.RefreshMineablePool)
      end
    else
      mines_data = mines_data .. level
    end
  end
  DebugOut("set mineable pool", mines_data)
  self:GetFlashObject():InvokeASCallback("_root", "setMineableMineData", mines_data)
end
function GameObjectMineMap.RefreshMinePool()
  GameObjectMineMap:UpdateMinePool()
end
function GameObjectMineMap:UpdateMinePool()
  local mine_pool = self.mine_pool
  local mines_data = ""
  local data_array = {}
  local user_info = GameGlobalData:GetData("userinfo")
  for _, miner in ipairs(mine_pool) do
    if miner.user_id ~= user_info.player_id then
      local level = miner.mine_level
      if DynamicResDownloader:IsDynamicStuff(level, DynamicResDownloader.resType.MINE_PIC) then
        local fullFileName = DynamicResDownloader:GetFullName("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          data_array[#data_array + 1] = tostring(level)
        else
          data_array[#data_array + 1] = "temp"
          DynamicResDownloader:AddDynamicRes("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC, nil, GameObjectMineMap.RefreshMinePool)
        end
      else
        data_array[#data_array + 1] = tostring(level)
      end
    else
      data_array[#data_array + 1] = tostring(-1)
    end
  end
  mines_data = table.concat(data_array, ",")
  DebugOut(mines_data)
  self:GetFlashObject():InvokeASCallback("_root", "setMineMapData", mines_data)
end
function GameObjectMineMap:UpdateEnemyMiners(enemy_miners)
  self.enemy_miners = enemy_miners
  for enemy_index = 1, 3 do
    local miner_data = self.enemy_miners[enemy_index]
    local user_id = -1
    local user_avatar = -1
    if miner_data then
      user_id = miner_data.user_id
      if miner_data.icon ~= 0 and miner_data.icon ~= 1 then
        user_avatar = GameDataAccessHelper:GetFleetAvatar(miner_data.icon, miner_data.main_fleet_level)
      else
        user_avatar = GameUtils:GetPlayerAvatarWithSex(miner_data.icon, miner_data.main_fleet_level)
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "setEnemyInfo", enemy_index, user_id, user_avatar)
  end
end
function GameObjectMineMap:UpdateAttackResource(curAtkRes, maxAtkRes)
  local descText = GameLoader:GetGameText("LC_MENU_MINING_IMPROVE_PLUNDER_TIME")
  local atkResText = tostring(curAtkRes) .. "/" .. tostring(maxAtkRes)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setAttackResource", descText, atkResText)
  end
end
function GameObjectMineMap:UpdateMineCDTime(mine_cd_time, mine_times)
  self.mine_times = mine_times
  if mine_cd_time <= 0 then
    local times_string = GameLoader:GetGameText("LC_MENU_MINING_RAMAINING_MINING_CHAR")
    times_string = string.format(times_string, GameObjectMineMap.mine_times)
    self:GetFlashObject():InvokeASCallback("_root", "setMineCDTime", -1, times_string)
    self.MineCDTimeUpdater = nil
  else
    do
      local target_time = os.time() + mine_cd_time
      function self.MineCDTimeUpdater()
        local cd_time = target_time - os.time()
        if cd_time >= 0 then
          local flash_obj = GameObjectMineMap:GetFlashObject()
          local time_string = GameUtils:formatTimeString(cd_time)
          local times_string = GameLoader:GetGameText("LC_MENU_MINING_RAMAINING_MINING_CHAR")
          times_string = string.format(times_string, GameObjectMineMap.mine_times)
          flash_obj:InvokeASCallback("_root", "setMineCDTime", time_string, times_string)
        else
          local flash_obj = GameObjectMineMap:GetFlashObject()
          local times_string = GameLoader:GetGameText("LC_MENU_MINING_RAMAINING_MINING_CHAR")
          times_string = string.format(times_string, GameObjectMineMap.mine_times)
          flash_obj:InvokeASCallback("_root", "setMineCDTime", -1, times_string)
          GameObjectMineMap.MineCDTimeUpdater = nil
        end
      end
    end
  end
end
function GameObjectMineMap:UpdateMineMining()
  local mines_data = ""
  local data_table = {}
  DebugOut("UpdateMineMining")
  DebugOut(self.AddMineNotification)
  DebugOut(self.mine_mining[1])
  if self.AddMineNotification and self.mine_mining[1] then
    DebugOut(self.mine_mining[1].end_time)
    self.AddMineNotification(self.mine_mining[1].end_time)
  end
  for mine_index = 1, 4 do
    local mining_data = self.mine_mining[mine_index]
    if mining_data then
      local level = mining_data.mine_level
      if DynamicResDownloader:IsDynamicStuff(level, DynamicResDownloader.resType.MINE_PIC) then
        local fullFileName = DynamicResDownloader:GetFullName("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          data_table[#data_table + 1] = tostring(level)
        else
          data_table[#data_table + 1] = "temp"
          DynamicResDownloader:AddDynamicRes("mine_" .. level .. ".png", DynamicResDownloader.resType.MINE_PIC, nil, GameObjectMineMap.RefreshMinePool)
        end
      else
        data_table[#data_table + 1] = tostring(level)
      end
    else
      data_table[#data_table + 1] = "-1"
    end
  end
  mines_data = table.concat(data_table, ",")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setMineMining", mines_data)
  end
end
function GameObjectMineMap:UpdateBoostInfo(boostInfo)
  if boostInfo.boostCountNow == 0 then
    self:GetFlashObject():InvokeASCallback("_root", "hideBoostInfo")
  else
    local boostPercent = boostInfo.boostCountNow / boostInfo.boostCountMax
    local boostPercentStr = string.format("%+.0f%s", boostPercent * 100, "%")
    DebugOut("boostPercentStr:" .. boostPercentStr)
    DebugOut("boostCountNow:" .. boostInfo.boostCountNow)
    self:GetFlashObject():InvokeASCallback("_root", "showBoostInfo")
    self:GetFlashObject():InvokeASCallback("_root", "updateBoostInfo", boostInfo.boostCountNow, boostPercentStr)
    self:UpdateBoostRemainTime(boostInfo.boostRemainTimeSecond)
  end
end
function GameObjectMineMap:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
    if self.AttackCDTimeUpdater then
      self.AttackCDTimeUpdater()
    end
    if self.MineCDTimeUpdater then
      self.MineCDTimeUpdater()
    end
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "onUpdateFrame", dt)
    end
  end
end
function GameObjectMineMap:GetMinerWithUserID(user_id)
  if self.mine_pool then
    for _, miner_data in ipairs(self.mine_pool) do
      if miner_data.user_id == user_id then
        return miner_data
      end
    end
  end
  return nil
end
function GameObjectMineMap:ShowRefreshMinesAnimation()
  self:GetFlashObject():InvokeASCallback("_root", "showRefreshMinesAnimation")
end
function GameObjectMineMap:ShowRefreshMineableMinesAnimation()
  self:GetFlashObject():InvokeASCallback("_root", "showRefreshMineableMinesAnimation")
end
function GameObjectMineMap:RandomMineableMines()
  self:GetFlashObject():InvokeASCallback("_root", "randomMineableMinesPos")
end
function GameObjectMineMap:UpdateOperationPrice(operation_type)
  local cost = GameGlobalData:GetGameConfig(operation_type)
  if cost then
    self:RefreshResourceCount()
  else
    local function netCallPrice()
      NetMessageMgr:SendMsg(NetAPIList.price_req.Code, {price_type = operation_type, type = 0}, self.NetCallbackPrice, true, netCallPrice)
    end
    netCallPrice()
  end
end
function GameObjectMineMap:SpeedUp()
  if not self:isVipLevelEnoughForMineSpeedUp() then
    self:tipVipNotEnoughForFunction(self:getMineSpeedUpVipLevelLimit())
    return
  end
  local cost = GameGlobalData:GetGameConfig("mine_speed_up")
  if cost and cost ~= -99 then
    local text_content = GameLoader:GetGameText("LC_MENU_RESET_CD_ASK")
    text_content = string.format(text_content, cost)
    local function callback()
      local content = {
        speedup_type = tonumber(arg)
      }
      NetMessageMgr:SendMsg(NetAPIList.mine_speedup_req.Code, content, self.NetCallbackSpeedup, true)
    end
    GameUtils:CreditCostConfirm(text_content, callback)
  else
    local function netCallPrice()
      NetMessageMgr:SendMsg(NetAPIList.price_req.Code, {
        price_type = "mine_speed_up",
        type = 0
      }, self.NetCallbackSpeedupPrice, false, netCallPrice)
    end
    netCallPrice()
  end
end
function GameObjectMineMap:ResetAttackCD()
  if not self:isVipLevelEnoughForClearMineAtk() then
    self:tipVipNotEnoughForFunction(self:getClearMineAtkCDVipLevelLimit())
    return
  end
  local cost = GameGlobalData:GetGameConfig("clear_mine_atk_cd")
  if cost and cost ~= -99 then
    local info_content = GameLoader:GetGameText("LC_MENU_RESET_CD_ASK")
    info_content = string.format(info_content, cost)
    local function callback()
      NetMessageMgr:SendMsg(NetAPIList.mine_reset_atk_cd_req.Code, nil, GameObjectMineMap.NetCallbackClearAttackCD, true)
    end
    GameUtils:CreditCostConfirm(info_content, callback)
  else
    local function netCallPrice()
      NetMessageMgr:SendMsg(NetAPIList.price_req.Code, {
        price_type = "clear_mine_atk_cd",
        type = 0
      }, self.NetCallbackClearAttackCDPrice, false, netCallPrice)
    end
    netCallPrice()
  end
end
function GameObjectMineMap:getMineSpeedUpVipLevelLimit()
  return GameDataAccessHelper:GetVIPLimit("mine_speed_up") or 6
end
function GameObjectMineMap:getClearMineAtkCDVipLevelLimit()
  return GameDataAccessHelper:GetVIPLimit("clear_mine_atk_cd") or 4
end
function GameObjectMineMap:isVipLevelEnoughForMineSpeedUp()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  return curLevel >= self:getMineSpeedUpVipLevelLimit()
end
function GameObjectMineMap:isVipLevelEnoughForClearMineAtk()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  return curLevel >= self:getClearMineAtkCDVipLevelLimit()
end
function GameObjectMineMap:tipVipNotEnoughForFunction(vipLevelNeed)
  local tipStr = GameLoader:GetGameText("LC_MENU_VIP_UNLOCK")
  if nil == tipStr or "" == tipStr then
    tipStr = "vip level%d need for this function"
  end
  tipStr = string.format(tipStr, vipLevelNeed)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
  local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
  local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
  GameUIMessageDialog:SetRightTextButton(cancel)
  GameUIMessageDialog:SetLeftGreenButton(affairs, GameObjectMineMap._goToPayment)
  GameUIMessageDialog:Display("", tipStr)
end
function GameObjectMineMap._goToPayment()
  local GameVip = LuaObjectManager:GetLuaObject("GameVip")
  GameVip.GotoPayment()
end
function GameObjectMineMap.NetCallbackMineList(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_atk_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.mine_list_ntf.Code then
    DebugOutPutTable(content, " fds mine_list_ntf")
    GameObjectMineMap.receiveTime = os.time()
    GameObjectMineMap.mine_pool = content.miners
    GameObjectMineMap:UpdateEnemyMiners(content.revenge_miners)
    GameObjectMineMap:ShowRefreshMinesAnimation()
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackRefreshMines(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_refresh_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.mine_refresh_ntf.Code then
    DebugOut("mine_refresh_ntf")
    DebugTable(content)
    GameObjectMineMap.mineable_pool = content
    GameObjectMineMap:ShowRefreshMineableMinesAnimation()
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackSpeedup(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_speedup_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    else
      DebugOut("GameObjectMineMap.NetCallbackSpeedup : succeed")
      GameGlobalData:UpdateGameConfig("mine_speed_up", -99)
      GameObjectMineMap.cleanMineMiningDataAndUpdateUI()
    end
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackBoost(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_boost_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.mine_info_ntf.Code then
    DebugOut("NetAPIList.mine_info_ntf.Code")
    DebugTable(content)
    GameObjectMineMap.atk_cost = content.atk_cost
    GameObjectMineMap.mineable_pool = content.mine_pool
    GameObjectMineMap.mine_mining = content.self_info
    GameObjectMineMap:UpdateMineablePool()
    GameObjectMineMap:UpdateMineMining()
    local mine_data = GameObjectMineMap.mine_mining[1]
    if mine_data then
      GameObjectMineMap:UpdateMineCDTime(mine_data.end_time, content.digg_cnt)
    else
      GameObjectMineMap:UpdateMineCDTime(-1, content.digg_cnt)
    end
    GameObjectMineMap:UpdateAttackResource(content.atk_now_resource, content.atk_max_resource)
    GameObjectMineMap.recordBoostInfo(content)
    GameObjectMineMap:UpdateBoostInfo(createBoostInfo(content.boost_cnt, content.boost_max_cnt, content.boost_rest_time))
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackAttack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_atk_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.mine_atk_ntf.Code then
    DebugOut("GameObjectMineMap.NetCallbackAttack")
    DebugTable(content)
    do
      local window_type = ""
      GameUIBattleResult:SetFightReport(content.reports[1], nil, nil)
      if content.result == 1 then
        local miner_data = GameObjectMineMap:GetMinerWithUserID(GameObjectMineMap.lastAttackUser)
        if miner_data then
          local minerOwnerName = GameUtils:GetUserDisplayName(miner_data.name)
          if miner_data then
            miner_data.robbed_cnt = miner_data.robbed_cnt + 1
          end
        end
        local index = 1
        GameUIBattleResult.minerOwnerName = minerOwnerName
        for i = 1, #content.atk_award do
          if content.atk_award[i].item_type == "prestige" and content.atk_award[i].number == 0 then
          else
            GameUIBattleResult:UpdateAwardItem3("challenge_win", index, content.atk_award[i].item_type, content.atk_award[i].no, content.atk_award[i].number)
            index = index + 1
          end
        end
        if index <= 2 then
          GameUIBattleResult:UpdateAwardItem("challenge_win", 2, -1, -1)
        end
        window_type = "challenge_win"
      else
        window_type = "challenge_lose"
      end
      local winText = GameLoader:GetGameText("LC_MENU_MINING_PLUNDER_SUCCESS_SHARE")
      winText = string.format(winText, GameUIBattleResult.minerOwnerName or "")
      local lastGameState = GameStateManager:GetCurrentGameState()
      if #content.reports[1].rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.reports[1].rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      GameStateBattlePlay.curBattleType = "MineMap"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.reports[1])
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(lastGameState)
        GameUIBattleResult:LoadFlashObject()
        GameUIBattleResult:AnimationMoveIn(window_type, false, winText, FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN)
        lastGameState:AddObject(GameUIBattleResult)
        GameObjectMineMap:OnFSCommand("refresh_mines")
      end)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      return true
    end
  end
  return false
end
function GameObjectMineMap.NetCallbackMineInfo(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.mine_info_ntf.Code then
    GameObjectMineMap.isOnShowRresh = true
    GameObjectMineMap.atk_cost = content.atk_cost
    GameObjectMineMap.mineable_pool = content.mine_pool
    GameObjectMineMap.mine_mining = content.self_info
    GameObjectMineMap:UpdateMineablePool()
    GameObjectMineMap:UpdateMineMining()
    if QuestTutorialMine:IsActive() then
      GameUICommonDialog:PlayStory({100010}, nil)
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "ShowTutorialChooseMine")
    end
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackMine(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_digg_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.mine_info_ntf.Code then
    GameObjectMineMap.atk_cost = content.atk_cost
    GameObjectMineMap.mineable_pool = content.mine_pool
    GameObjectMineMap.mine_mining = content.self_info
    GameObjectMineMap:UpdateMineablePool()
    GameObjectMineMap:UpdateMineMining()
    local mine_data = GameObjectMineMap.mine_mining[1]
    if mine_data then
      GameObjectMineMap:UpdateMineCDTime(mine_data.end_time, content.digg_cnt)
    else
      GameObjectMineMap:UpdateMineCDTime(-1, content.digg_cnt)
    end
    GameObjectMineMap:UpdateAttackResource(content.atk_now_resource, content.atk_max_resource)
    GameObjectMineMap.recordBoostInfo(content)
    GameObjectMineMap:UpdateBoostInfo(createBoostInfo(content.boost_cnt, content.boost_max_cnt, content.boost_rest_time))
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackEnter(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.mine_list_req.Code or content.api == NetAPIList.mine_info_req.Code) then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.mine_list_ntf.Code then
    DebugOutPutTable(content, " 3123 mine_list_ntf")
    GameObjectMineMap.receiveTime = os.time()
    GameObjectMineMap.mine_pool = content.miners
    GameObjectMineMap:UpdateMinePool()
    GameObjectMineMap:UpdateEnemyMiners(content.revenge_miners)
    return true
  end
  if msgtype == NetAPIList.mine_info_ntf.Code then
    GameObjectMineMap.atk_cost = content.atk_cost
    GameObjectMineMap.mineable_pool = content.mine_pool
    GameObjectMineMap.mine_mining = content.self_info
    if GameObjectMineMap:IsFirstEnter() then
      GameObjectMineMap:RandomMineableMines()
    end
    GameObjectMineMap:UpdateMineablePool()
    GameObjectMineMap:UpdateMineMining()
    local mine_data = GameObjectMineMap.mine_mining[1]
    if mine_data then
      GameObjectMineMap:UpdateMineCDTime(mine_data.end_time, content.digg_cnt)
    else
      GameObjectMineMap:UpdateMineCDTime(-1, content.digg_cnt)
    end
    GameObjectMineMap:UpdateAttackResource(content.atk_now_resource, content.atk_max_resource)
    GameObjectMineMap.recordBoostInfo(content)
    GameObjectMineMap:UpdateBoostInfo(createBoostInfo(content.boost_cnt, content.boost_max_cnt, content.boost_rest_time))
    GameObjectMineMap:MoveIn()
    if QuestTutorialMine:IsActive() then
      if GameObjectMineMap.mine_mining ~= {} and 0 < #GameObjectMineMap.mine_mining then
        QuestTutorialMine:SetFinish(true)
      else
        GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "ShowTipDig")
        GameUICommonDialog:PlayStory({100050}, nil)
      end
    else
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "HideTipDig")
    end
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackClearAttackCD(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mine_reset_atk_cd_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    else
      GameGlobalData:UpdateGameConfig("clear_mine_atk_cd", -99)
    end
    return true
  end
  return false
end
function GameObjectMineMap.MineInfoNotifyHandler(content)
  DebugOut("GameObjectMineMap.MineInfoNotifyHandler")
  GameObjectMineMap.mineable_pool = content.mine_pool
  GameObjectMineMap.mine_mining = content.self_info
  if GameObjectMineMap:GetFlashObject() then
    GameObjectMineMap:UpdateMineablePool()
    GameObjectMineMap:UpdateMineMining()
    local mine_data = GameObjectMineMap.mine_mining[1]
    if mine_data then
      GameObjectMineMap:UpdateMineCDTime(mine_data.end_time, content.digg_cnt)
    else
      GameObjectMineMap:UpdateMineCDTime(-1, content.digg_cnt)
    end
    GameObjectMineMap:UpdateAttackResource(content.atk_now_resource, content.atk_max_resource)
    GameObjectMineMap.recordBoostInfo(content)
    GameObjectMineMap:UpdateBoostInfo(createBoostInfo(content.boost_cnt, content.boost_max_cnt, content.boost_rest_time))
  end
end
function GameObjectMineMap.NetCallbackSpeedupPrice(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgType == NetAPIList.price_ack.Code then
    GameGlobalData:UpdateGameConfig("mine_speed_up", tonumber(content.price))
    GameObjectMineMap:SpeedUp()
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackClearAttackCDPrice(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgType == NetAPIList.price_ack.Code then
    GameGlobalData:UpdateGameConfig("clear_mine_atk_cd", tonumber(content.price))
    GameObjectMineMap:ResetAttackCD()
    return true
  end
  return false
end
function GameObjectMineMap.NetCallbackPrice(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  if msgType == NetAPIList.price_ack.Code then
    GameGlobalData:UpdateGameConfig(content.price_type, content.price)
    GameObjectMineMap:UpdateOperationPrice(content.price_type)
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameObjectMineMap.OnAndroidBack()
    if GameObjectMineMap.isOnShowMeMineDetail then
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "mineableInfoMoveOut")
    elseif GameObjectMineMap.isOnShowOtherMineDetail then
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "minerInfoMoveOut")
    elseif GameObjectMineMap.isOnShowRresh then
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "mineableMapMoveOut")
    else
      GameObjectMineMap:GetFlashObject():InvokeASCallback("_root", "mineMapMoveOut")
    end
  end
end
function GameObjectMineMap:RefreshResourceCount()
  local resource = GameGlobalData:GetData("item_count")
  local flash_obj = GameObjectMineMap:GetFlashObject()
  if flash_obj and resource then
    local item_2501_count = 0
    local item_2502_count = 0
    local item_2503_count = 0
    local item_2504_count = 0
    for i, v in ipairs(resource.items) do
      if v.item_id == 2501 then
        item_2501_count = v.item_no
      elseif v.item_id == 2502 then
        item_2502_count = v.item_no
      elseif v.item_id == 2503 then
        item_2503_count = v.item_no
      elseif v.item_id == 2504 then
        item_2504_count = v.item_no
      end
    end
    local curLevel = GameVipDetailInfoPanel:GetVipLevel()
    DebugOut("RefreshResourceCount")
    flash_obj:InvokeASCallback("_root", "RefreshResource", curLevel >= 4, item_2501_count, item_2502_count, item_2503_count, item_2504_count, GameGlobalData:GetGameConfig("refresh_mine_normal"), GameGlobalData:GetGameConfig("refresh_mine_credit"), GameGlobalData:GetGameConfig("refresh_mine_credit_max"), GameGlobalData:GetGameConfig("boost_mine"))
  end
end
function GameObjectMineMap:UpdateBoostRemainTime(timeNum)
  DebugOut("GameObjectMineMap:UpdateBoostRemainTime : " .. timeNum)
  self:GetFlashObject():InvokeASCallback("_root", "setRemainTime", timeNum)
end
function createBoostInfo(countNow, countMax, restTime)
  return {
    boostCountNow = countNow,
    boostCountMax = countMax,
    boostRemainTimeSecond = restTime
  }
end
function GameObjectMineMap.recordBoostInfo(content)
  localBoostInfo.boostCountNow = tonumber(content.boost_cnt)
  localBoostInfo.maxBoostCount = tonumber(content.boost_max_cnt)
  DebugOut("content.boost_cnt : " .. content.boost_cnt)
  DebugOut("content.boost_max_cnt : " .. content.boost_max_cnt)
end
function GameObjectMineMap.cleanMineMiningDataAndUpdateUI()
  GameObjectMineMap:UpdateMineCDTime(-1, GameObjectMineMap.mine_times)
  GameObjectMineMap.mine_mining = {}
  GameObjectMineMap:UpdateMineMining()
end
function GameObjectMineMap.miningComplete()
  GameObjectMineMap.mine_mining = {}
  GameObjectMineMap:UpdateMineMining()
end
function GameObjectMineMap:mineBoost()
  local boostCntNow = localBoostInfo.boostCountNow
  DebugOut("boostCntNow : " .. boostCntNow)
  local boostCntMax = localBoostInfo.maxBoostCount
  DebugOut("boostCntMax : " .. boostCntMax)
  local info = ""
  if boostCntNow < boostCntMax then
    local cost = GameGlobalData:GetGameConfig("boost_mine")
    local boostToPersent = (1 + (boostCntNow + 1) / boostCntMax) * 100
    DebugOut("boostToPersent" .. boostToPersent)
    info = GameLoader:GetGameText("LC_MENU_MINE_POWER_UP")
    info = string.gsub(info, "<number1>", tostring(cost))
    info = string.gsub(info, "<number2>", string.format("%.0f%%", boostToPersent))
    DebugOut(info)
  else
    GameObjectMineMap.mine_boostRequest()
    return
  end
  if GameGlobalData:GetItemCount(2504) > 0 then
    info = string.format(GameLoader:GetGameText("LC_MENU_ITEM_USE_ALERT"), GameLoader:GetGameText("LC_ITEM_ITEM_NAME_2504"))
  end
  GameUtils:CreditCostConfirm(info, function()
    GameObjectMineMap.mine_boostRequest()
  end)
end
function GameObjectMineMap:checkHelpTutorial(...)
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "getMoney" then
    self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorial", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorial", false)
  end
end
