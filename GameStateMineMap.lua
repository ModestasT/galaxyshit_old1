local GameStateMineMap = GameStateManager.GameStateMineMap
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameObjectMineMap = LuaObjectManager:GetLuaObject("GameObjectMineMap")
function GameStateMineMap:OnFocusGain(previous_state)
  if previous_state == GameStateBattleMap then
    function self.onQuitState()
      GameStateBattleMap:ReEnter(nil)
    end
  elseif previous_state == GameStateManager.GameStateLab then
    function self.onQuitState()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
    end
  elseif previous_state == GameStateManager.GameStateMainPlanet then
    function self.onQuitState()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  elseif previous_state == GameStateManager.GameStateDaily then
    function self.onQuitState()
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    end
  end
  self:AddObject(GameObjectMineMap)
end
function GameStateMineMap:OnFocusLost(new_state)
  self:EraseObject(GameObjectMineMap)
end
function GameStateMineMap:Quit()
  if self.onQuitState then
    self.onQuitState()
  end
end
