local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIStarSystemHandbook = LuaObjectManager:GetLuaObject("GameUIStarSystemHandbook")
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
local GameUIStarSystemFuben = LuaObjectManager:GetLuaObject("GameUIStarSystemFuben")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameStateStarSystem = GameStateManager.GameStateStarSystem
GameUIStarSystemHandbook.gloryColist = {}
GameUIStarSystemHandbook.MedalGrouplist = {}
GameUIStarSystemHandbook.CanActMedalGrouplist = {}
GameUIStarSystemHandbook.singleMedalGroupList = {}
GameUIStarSystemHandbook.total_attr = {}
GameUIStarSystemHandbook.total_glory = nil
GameUIStarSystemHandbook.isNeedShowTipMedal = false
GameUIStarSystemHandbook.fubenMedalGrouplist = {}
GameUIStarSystemHandbook.gotoPrev = false
function GameUIStarSystemHandbook:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function GameUIStarSystemHandbook:OnEraseFromGameState()
  GameUIStarSystemHandbook.isNeedShowTipMedal = false
  self:UnloadFlashObject()
end
function GameUIStarSystemHandbook:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "OnUpdate", dt)
end
function GameUIStarSystemHandbook:Show()
  GameStateStarSystem:AddObject(self)
  GameStateStarSystem:ForceCompleteCammandList()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    DebugOut("assert!")
  end
  DebugOut("tujianShow")
  local param = {}
  param.title = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_GLORY_WALL")
  param.Ranktitle = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART")
  param.Achititle = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_ACHIEVEMENT")
  param.attrTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SUM_PROPERTY")
  self:GetFlashObject():InvokeASCallback("_root", "HonorWallInit", param)
  self:RequestGloryInfo()
end
function GameUIStarSystemHandbook:showHonorWall(content)
  DebugOut("showHonorWall")
  DebugOut(debug.traceback())
  if content then
    DebugOut("gotohonorWall")
    local param = {}
    param.title = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_GLORY_WALL")
    param.Ranktitle = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART")
    param.Achititle = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_ACHIEVEMENT")
    param.attrTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SUM_PROPERTY")
    self.total_glory = content.total_glory
    self.total_attr = content.total_attr
    param.ifred = self.achiBtnIfred
    local attrStr = ""
    for _, v in pairs(self.total_attr) do
      attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
    end
    DebugOut("attrStr")
    DebugOut(attrStr)
    param.total_glory = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_VALUE") .. ":" .. self.total_glory
    param.total_attr = attrStr
    param.listlen = math.ceil(#self.total_attr / 4)
    if #self.total_attr == 0 then
      param.attrTi = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_WARNING")
    end
    if content.glory_infos then
      self.settingHonorInfos = nil
      self.settingHonorInfos = content.glory_infos
      DebugOut("settingHonorInfos")
      DebugTable(self.settingHonorInfos)
    end
    DebugOut(#self.gloryColist)
    self:GetFlashObject():InvokeASCallback("_root", "gotohonorWall", param, #self.gloryColist)
  else
    DebugOut("returnHonorwall")
    local attrStr = ""
    for _, v in pairs(self.total_attr) do
      attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
    end
    DebugOut("attrStr")
    DebugOut(attrStr)
    local param = {}
    param.total_glory = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_VALUE") .. ":" .. self.total_glory
    param.total_attr = attrStr
    param.ifred = self.achiBtnIfred
    param.listlen = math.ceil(#self.total_attr / 4)
    if #self.total_attr == 0 then
      param.attrTi = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_WARNING")
    end
    self:GetFlashObject():InvokeASCallback("_root", "refreshonorWall", param, #self.gloryColist)
  end
  if TutorialQuestManager.QuestTutorialStarSystemTujian:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowFirstTujianTip", true)
    local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
    GameUICommonDialog:PlayStory({9822})
  end
end
function GameUIStarSystemHandbook:refreshGloryColist(...)
  function BookinfoCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_collection_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_collection_ack.Code then
      DebugOut("glorylist__")
      DebugTable(self.gloryColist)
      self.medal_glory_collection_req_ing = false
      local attrStr = ""
      for _, v in pairs(self.total_attr) do
        attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
      end
      local param = {}
      param.total_glory = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_VALUE") .. ":" .. self.total_glory
      param.total_attr = attrStr
      param.ifred = self.achiBtnIfred
      param.listlen = math.ceil(#self.total_attr / 4)
      if #self.total_attr == 0 then
        param.attrTi = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_WARNING")
      end
      self:GetFlashObject():InvokeASCallback("_root", "refreshonorWall", param, #self.gloryColist)
      return true
    end
    return false
  end
  DebugOut("glorylistxxxxxx")
  DebugTable(self.gloryColist)
  self.gloryColist = {}
  self.medal_glory_collection_req_ing = true
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_collection_req.Code, {type = 0}, BookinfoCallback, true, nil)
end
function GameUIStarSystemHandbook:initChoice(...)
  self.choice = {}
  self.choice[1] = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SELECT_HP")
  self.choice[2] = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SELECT_ATK")
  self.choice[3] = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SELECT_DEF")
  self.choice[4] = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SELECT_SATK")
  self.choice[5] = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SELECT_SDEF")
  self.choice[6] = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SELECT_EATK")
end
function GameUIStarSystemHandbook:showMedalGroup(content)
  self:initChoice()
  self.choice_param = {
    [1] = false,
    [2] = false,
    [3] = false,
    [4] = false,
    [5] = false,
    [6] = false
  }
  local param = {}
  param.bookTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART_" .. self.gloryColist[self.curBookIndex].type)
  param.choiceTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_SELECT_PROPERTY")
  self.MedalGrouplist = nil
  self.MedalGrouplist = content.glory_collection.glories
  self.glory_medals = content.glory_collection.glory_medals
  self.fubenMedalGrouplist = self.MedalGrouplist
  self.filteredMedalGrouplist = self.MedalGrouplist
  self:GetFlashObject():InvokeASCallback("_root", "gotoMedalGroup", param, math.ceil(#self.MedalGrouplist / 2))
  self:GetFlashObject():InvokeASCallback("_root", "updateChoice")
  if TutorialQuestManager.QuestTutorialStarSystemTujian:IsActive() then
    TutorialQuestManager.QuestTutorialStarSystemTujian:SetFinish(true)
    do
      local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
      self:GetFlashObject():InvokeASCallback("_root", "ShowMedalActiveTip", true)
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowMedalActiveTip", false)
        self:GetFlashObject():InvokeASCallback("_root", "ShowMedalTip", true)
        GameUICommonDialog:PlayStory({9824})
      end
      GameUICommonDialog:PlayStory({9823}, callback)
      GameUIStarSystemHandbook.isNeedShowTipMedal = true
    end
  end
end
function GameUIStarSystemHandbook:activateMedalGroup(content)
  self.MedalGrouplist = nil
  if self.curBookIndex then
    DebugOut("curBookIndex")
    DebugOut(self.curBookIndex)
    self.MedalGrouplist = content.glory_collection.glories
    self.gloryColist[self.curBookIndex] = content.glory_collection
    self.glory_medals = content.glory_collection.glory_medals
  else
    DebugOut("self.curBookIndex not exist")
  end
  self:updateFubenMedalGrouplist()
  self.total_glory = content.total_glory
  self.total_attr = content.total_attr
  local attrStr = ""
  for _, v in pairs(self.total_attr) do
    attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
  end
  DebugOut("activateMedalGroup")
  DebugTable(self.fubenMedalGrouplist)
  local param = {}
  param.itemKey = self.curGloryIndex
  param.flag = 0
  self:GetFlashObject():InvokeASCallback("_root", "UpdateDataMedalGroupInfo", self.filteredMedalGrouplist, param)
end
function GameUIStarSystemHandbook:LvUpMedalGroup(content)
  self.MedalGrouplist = nil
  if self.curBookIndex then
    DebugOut("curBookIndex")
    DebugOut(self.curBookIndex)
    self.MedalGrouplist = content.glory_collection.glories
    self.gloryColist[self.curBookIndex] = content.glory_collection
    self.glory_medals = content.glory_collection.glory_medals
  else
    DebugOut("self.curBookIndex not exist")
  end
  self:updateFubenMedalGrouplist()
  self.total_glory = content.total_glory
  self.total_attr = content.total_attr
  local attrStr = ""
  for _, v in pairs(self.total_attr) do
    attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
  end
  DebugOut("LvUpMedalGroup")
  DebugTable(self.filteredMedalGrouplist)
  local param = {}
  param.itemKey = self.curGloryIndex
  param.rankAniFrame = self.rank + 1
  param.flag = 1
  self:GetFlashObject():InvokeASCallback("_root", "UpdateDataMedalGroupInfo", self.filteredMedalGrouplist, param)
end
function GameUIStarSystemHandbook:OnAchiRedNTF(content)
  self.achiBtnIfred = content.is_red == 1 and true or false
end
function GameUIStarSystemHandbook:OnHonorCollectionNTF(content)
  DebugOut("OnHonorCollectionNTF")
  if self.medal_glory_collection_req_ing then
    for _, v in pairs(content.glory_collections) do
      table.insert(self.gloryColist, v)
    end
  end
end
function GameUIStarSystemHandbook:RequestGloryInfo()
  function GloryInfoCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_collection_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_collection_ack.Code then
      self.medal_glory_collection_req_ing = false
      self:showHonorWall(content)
      return true
    end
    return false
  end
  self.gloryColist = {}
  self.medal_glory_collection_req_ing = true
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_collection_req.Code, {type = 0}, GloryInfoCallback, true, nil)
end
function GameUIStarSystemHandbook:RequestMedalGroupInfo(Id)
  function RequestMedalGroupCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_collection_enter_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_collection_enter_ack.Code then
      self:showMedalGroup(content)
      return true
    end
    return false
  end
  local itemKey = tonumber(Id)
  self.curBookIndex = itemKey
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_collection_enter_req.Code, {
    type = self.gloryColist[self.curBookIndex].type
  }, RequestMedalGroupCallback, true, nil)
end
function GameUIStarSystemHandbook:RequestActivate(Id)
  local itemKey = tonumber(Id)
  function self.RequestActivateCallback(msgType, content)
    DebugOut("RequestActivateCallback")
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_activate_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_activate_ack.Code then
      self:activateMedalGroup(content)
      return true
    end
    return false
  end
  self.req_type = {}
  self.req_type.type = self.filteredMedalGrouplist[itemKey].type
  self.curGloryIndex = itemKey
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_activate_req.Code, self.req_type, self.RequestActivateCallback, true, nil)
end
function GameUIStarSystemHandbook:RequestLvUp(Id)
  local itemKey = tonumber(Id)
  function RequestLvUpCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_rank_up_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_rank_up_ack.Code then
      self:LvUpMedalGroup(content)
      return true
    end
    return false
  end
  self.req_type = {}
  self.req_type.type = self.filteredMedalGrouplist[itemKey].type
  self.curGloryIndex = itemKey
  self.rank = self.filteredMedalGrouplist[itemKey].rank
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_rank_up_req.Code, self.req_type, RequestLvUpCallback, true, nil)
end
function GameUIStarSystemHandbook:findGloryMedalData(glory_medals)
  local foundGlory_medals = {}
  for m, v in pairs(glory_medals) do
    for k, w in pairs(self.glory_medals) do
      if v == w.type then
        foundGlory_medals[m] = w
      end
    end
  end
  DebugOut("glory_medals_puhy")
  DebugTable(foundGlory_medals)
  return foundGlory_medals
end
function GameUIStarSystemHandbook:getMedalInfo(arg)
  self.medalInfo = arg
  local glorylist
  glorylist = self.filteredMedalGrouplist
  local params = LuaUtils:string_split(arg, ":")
  self.singleMedalGroupList = nil
  DebugOut("getMedalInfo")
  DebugTable(glorylist)
  self.singleMedalGroupList = self:findGloryMedalData(glorylist[tonumber(params[1])].glory_medals)
  local medal = self.singleMedalGroupList[tonumber(params[2])]
  local param = {}
  param.itemKey = tonumber(params[2])
  param.name = GameLoader:GetGameText("LC_MENU_MEDAL_NAME_" .. medal.type)
  param.type = medal.type
  param.medalDesc = GameLoader:GetGameText("LC_MENU_MEDAL_DESC_" .. medal.type)
  param.num = medal.num
  param.attrTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_WEAR_PROPERTY")
  param.medalNumTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_NUM")
  local attrStr = ""
  for _, v in pairs(medal.attr) do
    attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
  end
  param.attr = attrStr
  param.GloryTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CAN_ACTIVATE")
  local CanActGloryContent = ""
  for _, v in pairs(medal.previews) do
    local gloryName = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_TITLE_NAME_" .. v.type.key .. "_" .. v.type.value)
    v.gloryName = gloryName
  end
  param.previews = medal.previews
  self.previews = nil
  self.previews = medal.previews
  DebugOut("self.previews")
  DebugOut(self.previews)
  param.gotoTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_HEAD_TO")
  param.nquality = GameHelper:GetMedalQuality("medal", medal.type)
  param.iconIndex = GameHelper:GetAwardTypeIconFrameName("medal_new_icon", medal.type)
  param.imgName = GameHelper:GetGameItemDownloadPng("medal_new_icon", medal.type)
  DebugOut("#####puhy")
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "gotoMedal", param)
  self:GetFlashObject():InvokeASCallback("_root", "ShowMedalTip", false)
  self:GetFlashObject():InvokeASCallback("_root", "ShowNaturalPopTip", self.isNeedShowTipMedal)
  if self.isNeedShowTipMedal then
    local function callback()
      GameUIStarSystemHandbook.isNeedShowTipMedal = false
      GameUIStarSystemHandbook:GetFlashObject():InvokeASCallback("_root", "ShowNaturalPopTip", false)
    end
    local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
    GameUICommonDialog:PlayStory({9825, 9826}, callback)
  end
end
function GameUIStarSystemHandbook:getCanActMedalGroupInfo(Id)
  local itemKey = tonumber(Id)
  DebugOut("CanActMedalGroupInfo")
  self:GetFlashObject():InvokeASCallback("_root", "gotoCanActGroup", #self.previews)
end
function GameUIStarSystemHandbook:MedalGetWaysInfo(arg)
  local param = {}
  param.titleName = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_ACCESS")
  param.one = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_BOSS")
  param.two = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_STORE")
  param.three = GameLoader:GetGameText("LC_MENU_NAME_MEDAL_GLORY_ROAD")
  self:GetFlashObject():InvokeASCallback("_root", "gotoMedalGetWays", param)
end
function GameUIStarSystemHandbook:UpdateSingleBookInfo(Id)
  local function DynamicBackgroundCallback(extinfo)
    DebugOut("DynamicBackgroundCallback")
    if self:GetFlashObject() then
      self:GetFlashObject():OpenTextureForLua(extinfo.img)
      self:GetFlashObject():InvokeASCallback("_root", "setBookImg_new", extinfo)
    end
  end
  local itemKey = tonumber(Id)
  local param = {}
  DebugOut("#self.UpdateSingleBookInfo")
  DebugOut(itemKey)
  local itemData = self.gloryColist[itemKey]
  param.name = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART_" .. itemData.type)
  param.itemKey = itemKey
  param.type = itemData.type
  param.locked = itemData.open
  param.activateNum = itemData.activate.key
  param.TotalNum = itemData.activate.value
  param.loadText = GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR")
  DebugOut("UpdateSingleBookInfo")
  param.img = itemData.img
  param.ifred = itemData.red_point == 1 and true or false
  local localPath = "data2/" .. DynamicResDownloader:GetFullName(itemData.img, DynamicResDownloader.resType.WELCOME_PIC)
  DebugOut("~~~~~")
  DebugOut(ext.crc32.crc32(localPath))
  if ext.crc32.crc32(localPath) ~= "" then
    param.imgExist = true
  else
    param.imgExist = false
    local extendInfo = {}
    extendInfo.img = itemData.img
    extendInfo.itemKey = itemKey
    DynamicResDownloader:AddDynamicRes(itemData.img, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, DynamicBackgroundCallback)
  end
  DebugOut("bookinfo")
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "setBook_new", param)
end
function GameUIStarSystemHandbook:UpdateSingleMedalGroupInfo(Id)
  local itemKey = tonumber(Id)
  local param1 = {}
  local param2 = {}
  local glorylist
  glorylist = self.filteredMedalGrouplist
  DebugOut("glorylist")
  DebugTable(itemKey)
  local itemKeyFirst = itemKey * 2 - 1
  local itemKeySecond = itemKey * 2
  param1.itemKey = itemKeyFirst
  param1.actTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_ACTIVATE")
  param1.lvupTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_UPGRADE")
  param1.UpMaxTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_UPGRADE_MAX")
  DebugOut("self.MedalGrouplist[itemKeyFirst].type")
  DebugOut(itemKeyFirst)
  DebugTable(glorylist[itemKeyFirst])
  param1.type = glorylist[itemKeyFirst].type
  param1.name = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_TITLE_NAME_" .. param1.type.key .. "_" .. param1.type.value)
  param1.status = glorylist[itemKeyFirst].status
  param1.rank = glorylist[itemKeyFirst].rank
  param1.MedalNum = #glorylist[itemKeyFirst].glory_medals
  param1.iconIndex = {}
  param1.imgName = {}
  param1.nquality = {}
  for k = 1, param1.MedalNum do
    param1.nquality[k] = GameHelper:GetMedalQuality("medal", glorylist[itemKeyFirst].glory_medals[k])
    param1.iconIndex[k] = GameHelper:GetAwardTypeIconFrameName("medal", glorylist[itemKeyFirst].glory_medals[k])
    param1.imgName[k] = GameHelper:GetGameItemDownloadPng("medal", glorylist[itemKeyFirst].glory_medals[k])
  end
  local attrs = glorylist[itemKeyFirst].attr
  local attrStr = ""
  for _, v in pairs(attrs) do
    attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
  end
  param1.attr = attrStr
  for _, v in pairs(self.settingHonorInfos) do
    if v.type.key == glorylist[itemKeyFirst].type.key and v.type.value == glorylist[itemKeyFirst].type.value then
      param1.rankMax = v.rank_max
      break
    end
  end
  if glorylist[itemKeySecond] then
    param2.itemKey = itemKeySecond
    param2.actTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_ACTIVATE")
    param2.lvupTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_UPGRADE")
    param2.UpMaxTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_UPGRADE_MAX")
    param2.type = glorylist[itemKeySecond].type
    param2.name = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_TITLE_NAME_" .. param2.type.key .. "_" .. param2.type.value)
    param2.status = glorylist[itemKeySecond].status
    param2.rank = glorylist[itemKeySecond].rank
    param2.MedalNum = #glorylist[itemKeySecond].glory_medals
    param2.iconIndex = {}
    param2.imgName = {}
    param2.nquality = {}
    for k = 1, param2.MedalNum do
      param2.nquality[k] = GameHelper:GetMedalQuality("medal", glorylist[itemKeySecond].glory_medals[k])
      param2.iconIndex[k] = GameHelper:GetAwardTypeIconFrameName("medal", glorylist[itemKeySecond].glory_medals[k])
      param2.imgName[k] = GameHelper:GetGameItemDownloadPng("medal", glorylist[itemKeySecond].glory_medals[k])
    end
    local attrs = glorylist[itemKeySecond].attr
    local attrStr = ""
    for _, v in pairs(attrs) do
      attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
    end
    param2.attr = attrStr
    for _, v in pairs(self.settingHonorInfos) do
      if v.type.key == glorylist[itemKeySecond].type.key and v.type.value == glorylist[itemKeySecond].type.value then
        param2.rankMax = v.rank_max
        break
      end
    end
    DebugOut("self.MedalGrouplist[itemKeyFirst].type")
    DebugOut(itemKeySecond)
    DebugTable(glorylist[itemKeySecond])
  else
    param2 = nil
  end
  DebugOut("medal_group")
  DebugTable(param1)
  DebugTable(param2)
  self:GetFlashObject():InvokeASCallback("_root", "setMedalGroup_new", param1, param2)
end
function GameUIStarSystemHandbook:UpdateSingleCanActMedalGroupInfo(Id)
  local itemKey = tonumber(Id)
  local param = {}
  param.itemKey = itemKey
  param.name = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_TITLE_NAME_" .. self.previews[itemKey].type.key .. "_" .. self.previews[itemKey].type.value)
  local attrs = self.previews[itemKey].attr
  local attrStr = ""
  for _, v in pairs(attrs) do
    attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\n"
  end
  param.attr = attrStr
  if self.previews[itemKey].status == 0 or self.previews[itemKey].status == 1 then
    param.locked = true
  elseif self.previews[itemKey].status > 1 then
    param.locked = false
  end
  for __, w in pairs(self.MedalGrouplist) do
    if self.previews[itemKey].type.key == w.type.key and self.previews[itemKey].type.value == w.type.value then
      param.glory_medals = w.glory_medals
      break
    end
  end
  param.MedalNum = #param.glory_medals
  param.iconIndex = {}
  param.imgName = {}
  param.nquality = {}
  for k = 1, param.MedalNum do
    param.nquality[k] = GameHelper:GetMedalQuality("medal", param.glory_medals[k])
    param.iconIndex[k] = GameHelper:GetAwardTypeIconFrameName("medal", param.glory_medals[k])
    param.imgName[k] = GameHelper:GetGameItemDownloadPng("medal", param.glory_medals[k])
  end
  DebugOut("UpdateSingleCanActMedalGroupInfo_attrStr")
  DebugOut("CanActMedalGroupinfo")
  self:GetFlashObject():InvokeASCallback("_root", "setCanActMedalGroup_new", param)
end
function GameUIStarSystemHandbook:UpdateSingleAttr(Id)
  local itemKey = tonumber(Id)
  local attrs = ""
  for i = 1, 4 do
    if self.total_attr[(itemKey - 1) * 4 + i] then
      local attr = self.total_attr[(itemKey - 1) * 4 + i]
      attrs = attrs .. GameUtils:GetAttrName(attr.attr_id) .. "+" .. GameUtils:GetAttrValue(attr.attr_id, attr.attr_value) .. "\001"
    end
  end
  local param = {}
  param.itemKey = itemKey
  param.total_attr = attrs
  DebugOut("SingleAttr")
  DebugTable(param.total_attr)
  self:GetFlashObject():InvokeASCallback("_root", "setAttr_new", param)
end
function GameUIStarSystemHandbook:UpdateSingleAchiAttr(Id)
  local itemKey = tonumber(Id)
  DebugOut("Achi_total_attr")
  DebugTable(self.Achi_total_attr)
  local attrs = ""
  for i = 1, 4 do
    if self.Achi_total_attr[(itemKey - 1) * 4 + i] then
      local attr = self.Achi_total_attr[(itemKey - 1) * 4 + i]
      attrs = attrs .. GameUtils:GetAttrName(attr.attr_id) .. "+" .. GameUtils:GetAttrValue(attr.attr_id, attr.attr_value) .. "\001"
    end
  end
  local param = {}
  param.itemKey = itemKey
  param.total_attr = attrs
  DebugOut("SingleAttr")
  DebugTable(param.total_attr)
  self:GetFlashObject():InvokeASCallback("_root", "setAchiAttr_new", param)
end
function GameUIStarSystemHandbook:UpdateSingleChoice(Id)
  local itemKey = tonumber(Id)
  DebugOut("puhyID")
  DebugOut(itemKey)
  local param = {}
  param.itemKey = itemKey
  param.choice = self.choice[itemKey]
  DebugOut("setChoice_new")
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "setChoice_new", param)
end
function GameUIStarSystemHandbook:OnFSCommand(cmd, arg)
  if GameUtils:OnFSCommand(cmd, arg, self) then
    return
  end
  if cmd == "UpdateSingleBookInfo" then
    self:UpdateSingleBookInfo(arg)
  elseif cmd == "UpdateSingleMedalGroupInfo" then
    self:UpdateSingleMedalGroupInfo(arg)
  elseif cmd == "UpdateSingleCanActMedalGroupInfo" then
    self:UpdateSingleCanActMedalGroupInfo(arg)
  elseif cmd == "updateSingleRank" then
    self:updateSingleRank(arg)
  elseif cmd == "onBookOpen" then
    self:RequestMedalGroupInfo(arg)
  elseif cmd == "onRankOpen" then
    self:RequestRankInfo(arg)
  elseif cmd == "onActivate" then
    self:RequestActivate(arg)
  elseif cmd == "onLvUp" then
    self:RequestLvUp(arg)
  elseif cmd == "onMedalOpen" then
    self:getMedalInfo(arg)
  elseif cmd == "onMedalGet" then
    self:MedalGetWaysInfo(arg)
  elseif cmd == "onCanActMedalGroup" then
    self:getCanActMedalGroupInfo(arg)
  elseif cmd == "onResetResource" then
    self:getResetResource(arg)
  elseif cmd == "onReset" then
    self:RequestReset(arg)
  elseif cmd == "onGloryPreviewOpen" then
    self:getGloryPreview(arg)
  elseif cmd == "onAchiOpen" then
    self:RequestAchievementInfo()
  elseif cmd == "FreeActAchieve" then
    self:FreeActAchieve()
  elseif cmd == "MoveOutToHonorWall" then
    self:showHonorWall()
  elseif cmd == "exitHonorWall" then
    self:exitHonorWall()
  elseif cmd == "getRankInfo" then
    self:getRankInfo(arg)
  elseif cmd == "openMain" then
  elseif cmd == "refreshMedalgroup" then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateDataMedalGroupInfo", self.filteredMedalGrouplist, nil)
  elseif cmd == "UpdateSingleAttr" then
    self:UpdateSingleAttr(arg)
  elseif cmd == "UpdateSingleAchiAttr" then
    self:UpdateSingleAchiAttr(arg)
  elseif cmd == "UpdateSingleChoice" then
    self:UpdateSingleChoice(arg)
  elseif cmd == "UpdateSinglePreviewInfo" then
    self:UpdateSinglePreviewInfo(arg)
  elseif cmd == "UpdateSingleResetReturnMedalInfo" then
    self:UpdateSingleResetReturnMedalInfo(arg)
  elseif cmd == "refreshGloryList" then
    self:refreshGloryList(arg)
  elseif cmd == "showAchiPointAttr" then
    self:showAchiPointAttr(arg)
  elseif cmd == "gotoOther" then
    self:gotoOther(arg)
  elseif cmd == "honorWallTip" then
    self:honorWallTip()
  elseif cmd == "medalGroupTip" then
    self:medalGroupTip()
  elseif cmd == "AchiTip" then
    self:AchiTip()
  elseif cmd == "showItemBox" then
    self:getMedalInfo(arg)
  elseif cmd == "refreshHonorWallFromAchi" then
    self:requestHonorWallAttrFromAchi(arg)
  end
end
function GameUIStarSystemHandbook:requestHonorWallAttrFromAchi(Id)
  function RefreshAttrCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_collection_total_attr_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_collection_total_attr_ack.Code then
      self:refreshHonorWallFromAchi(content)
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_collection_total_attr_req.Code, nil, RefreshAttrCallback, true, nil)
end
function GameUIStarSystemHandbook:refreshHonorWallFromAchi(content)
  self.total_attr = content.total_attr
  local attrStr = ""
  for _, v in pairs(self.total_attr) do
    attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
  end
  local param = {}
  param.total_attr = attrStr
  param.listlen = math.ceil(#self.total_attr / 4)
  self:GetFlashObject():InvokeASCallback("_root", "refreshonorWallTotalAttr", param)
end
function GameUIStarSystemHandbook:honorWallTip(...)
  GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
  GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_GLORY_1"))
end
function GameUIStarSystemHandbook:medalGroupTip(...)
  GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
  GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_GLORY_2"))
end
function GameUIStarSystemHandbook:AchiTip(...)
  GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
  GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_GLORY_ACHIEVEMENT"))
end
function GameUIStarSystemHandbook:RequestGloryInfoFromOthers()
  function GloryInfoCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_collection_enter_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_collection_enter_ack.Code then
      self:refreshGloryFromOthers(content)
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_collection_enter_req.Code, {
    type = self.gloryColist[self.curBookIndex].type
  }, GloryInfoCallback, true, nil)
  function BookInfoCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_collection_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_collection_ack.Code then
      self.medal_glory_collection_req_ing = false
      return true
    end
    return false
  end
  self.gloryColist = {}
  self.medal_glory_collection_req_ing = true
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_collection_req.Code, {type = 1}, BookInfoCallback, true, nil)
end
function GameUIStarSystemHandbook:refreshGloryFromOthers(content)
  local param = {}
  self.MedalGrouplist = nil
  if self.curBookIndex then
    DebugOut("curBookIndex")
    DebugOut(self.curBookIndex)
    self.MedalGrouplist = content.glory_collection.glories
    self.glory_medals = content.glory_collection.glory_medals
  else
    DebugOut("self.curBookIndex not exist")
  end
  self:updateFubenMedalGrouplist()
  local params = LuaUtils:string_split(self.medalInfo, ":")
  local glorylist
  glorylist = self.filteredMedalGrouplist
  self.singleMedalGroupList = self:findGloryMedalData(glorylist[tonumber(params[1])].glory_medals)
  local medal = self.singleMedalGroupList[tonumber(params[2])]
  local medalInfo = {}
  medalInfo.medalNumTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_NUM")
  medalInfo.num = medal.num
  self:GetFlashObject():InvokeASCallback("_root", "refreshNumFromOthers", medalInfo)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateDataMedalGroupInfo", self.filteredMedalGrouplist)
end
function GameUIStarSystemHandbook:ComeBack(...)
  GameStateStarSystem:EraseObject(GameUIStarSystemPort_SUB)
  GameStateStarSystem:ForceCompleteCammandList()
  self:RequestGloryInfoFromOthers()
end
function GameUIStarSystemHandbook:gotoOther(arg)
  DebugOut("gotoOther")
  DebugOut(arg)
  self.gotoPrev = true
  if arg == "gotoFuben" then
    GameUIStarSystemFuben:Show()
  elseif arg == "gotoChouka" then
    GameStateStarSystem:AddObject(GameUIStarSystemPort_SUB)
    GameStateStarSystem:ForceCompleteCammandList()
    GameUIStarSystemPort.gacha:Show()
  elseif arg == "gotoHeishi" then
    GameStateStarSystem:AddObject(GameUIStarSystemPort_SUB)
    GameStateStarSystem:ForceCompleteCammandList()
    GameUIStarSystemPort.uiStore:Show()
  end
end
function GameUIStarSystemHandbook:exitHonorWall(...)
  DebugOut("exitHonorWall")
  GameStateStarSystem:EraseObject(self)
  GameStateStarSystem:AddObject(GameUIStarSystemPort)
  GameStateStarSystem:ForceCompleteCammandList()
  GameUIStarSystemPort:Show("onBtn_tujian")
end
function GameUIStarSystemHandbook:UpdateSingleResetReturnMedalInfo(Id)
  local itemKey = tonumber(Id)
  local param = {}
  local glorylist
  glorylist = self.filteredMedalGrouplist
  DebugOut("back num")
  DebugOut(self.curGloryIndex)
  DebugOut(self.MedalGrouplist[self.curGloryIndex].status)
  DebugOut(self.MedalGrouplist[self.curGloryIndex].rank)
  param.itemKey = itemKey
  local medal = self.singleMedalGroupList[itemKey]
  param.name = GameLoader:GetGameText("LC_MENU_MEDAL_NAME_" .. medal.type)
  param.nquality = GameHelper:GetMedalQuality("medal", medal.type)
  param.iconIndex = GameHelper:GetAwardTypeIconFrameName("medal", medal.type)
  param.imgName = GameHelper:GetGameItemDownloadPng("medal", medal.type)
  param.returnNum = glorylist[self.curGloryIndex].rank + 1
  param.medal_type = self.curGloryIndex .. ":" .. itemKey
  param.listlen = #self.singleMedalGroupList
  DebugOut("param.medal_type ")
  DebugOut(param.medal_type)
  self:GetFlashObject():InvokeASCallback("_root", "setResetReturnMedal_new", param)
end
function GameUIStarSystemHandbook:updateFubenMedalGrouplist(...)
  DebugOut("FubenMedalGrouplist")
  DebugTable(self.fubenMedalGrouplist)
  DebugOut("MedalGrouplist")
  DebugTable(self.MedalGrouplist)
  for _, v in pairs(self.MedalGrouplist) do
    for n, w in pairs(self.fubenMedalGrouplist) do
      if v.type.key == w.type.key and v.type.value == w.type.value then
        DebugOut("fuzhi1")
        self.fubenMedalGrouplist[n] = v
        break
      end
    end
  end
  DebugOut("sourceFubenMedalGrouplist")
  DebugTable(self.filteredMedalGrouplist)
  for _, v in pairs(self.MedalGrouplist) do
    for n, w in pairs(self.filteredMedalGrouplist) do
      if v.type.key == w.type.key and v.type.value == w.type.value then
        DebugOut("fuzhi2")
        self.filteredMedalGrouplist[n] = v
        break
      end
    end
  end
  DebugOut("fuzhiFubenMedalGrouplist")
  DebugTable(self.filteredMedalGrouplist)
end
function GameUIStarSystemHandbook:ResetMedalGroup(content)
  self.MedalGrouplist = nil
  if self.curBookIndex then
    self.MedalGrouplist = content.glory_collection.glories
    self.gloryColist[self.curBookIndex] = content.glory_collection
    self.glory_medals = content.glory_collection.glory_medals
  else
    DebugOut("self.curBookIndex not exist")
  end
  self:updateFubenMedalGrouplist()
  self.total_glory = content.total_glory
  self.total_attr = content.total_attr
  local attrStr = ""
  for _, v in pairs(self.total_attr) do
    attrStr = attrStr .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "\001"
  end
  DebugOut("ResetMedalGroup")
end
function GameUIStarSystemHandbook:getResetResource(Id)
  local itemKey = tonumber(Id)
  local param = {}
  self.singleMedalGroupList = nil
  local glorylist
  glorylist = self.filteredMedalGrouplist
  self.singleMedalGroupList = self:findGloryMedalData(glorylist[itemKey].glory_medals)
  self.curGloryIndex = itemKey
  param.itemKey = itemKey
  param.listlen = #glorylist[itemKey].glory_medals
  param.resetTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_RESET_DESC")
  param.resetedTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_RESET_SUCCESS") .. "," .. GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_RESET_SUCCESS_DESC")
  DebugOut("param.listlen ")
  DebugOut(param.listlen)
  self:GetFlashObject():InvokeASCallback("_root", "gotoReset", param)
end
function GameUIStarSystemHandbook:RequestReset(Id)
  function RequestResetCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_reset_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_reset_ack.Code then
      self:ResetMedalGroup(content)
      return true
    end
    return false
  end
  local itemKey = tonumber(Id)
  self.req_type = {}
  self.req_type.type = self.filteredMedalGrouplist[itemKey].type
  self.curGloryIndex = itemKey
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_reset_req.Code, self.req_type, RequestResetCallback, true, nil)
end
function GameUIStarSystemHandbook:getGloryPreview(Id)
  local itemKey = tonumber(Id)
  local m = {}
  local glorylist
  glorylist = self.filteredMedalGrouplist
  for _, w in pairs(self.settingHonorInfos) do
    if glorylist[itemKey].type.key == w.type.key and glorylist[itemKey].type.value == w.type.value then
      m[w.rank + 1] = w.attr
    end
  end
  DebugOut("previews info")
  DebugTable(m)
  self.Enhance = {}
  for k, v in pairs(m) do
    self.Enhance[k] = {}
    self.Enhance[k].EnhanceTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_PROPERTY_" .. k - 1)
    local attrs1 = ""
    local attrs2 = ""
    local i = 0
    for _, w in pairs(v) do
      if i >= 2 then
        attrs2 = attrs2 .. GameUtils:GetAttrName(w.attr_id) .. "+" .. GameUtils:GetAttrValue(w.attr_id, w.attr_value) .. "\n"
      else
        attrs1 = attrs1 .. GameUtils:GetAttrName(w.attr_id) .. "+" .. GameUtils:GetAttrValue(w.attr_id, w.attr_value) .. "\n"
      end
      i = i + 1
    end
    self.Enhance[k].EnhanceAttrs1 = attrs1
    self.Enhance[k].EnhanceAttrs2 = attrs2
  end
  DebugOut("self.Enhance")
  DebugOut(#self.Enhance)
  DebugTable(self.Enhance)
  local titleName = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_PREVIEW")
  self:GetFlashObject():InvokeASCallback("_root", "gotoGloryPreview", titleName, #self.Enhance)
end
function GameUIStarSystemHandbook:UpdateSinglePreviewInfo(Id)
  DebugOut("wwwww")
  local itemKey = tonumber(Id)
  local param = {}
  DebugOut(itemKey)
  param.itemKey = itemKey
  param.EnhanceTxt = self.Enhance[itemKey].EnhanceTxt
  param.EnhanceAttrs1 = self.Enhance[itemKey].EnhanceAttrs1
  param.EnhanceAttrs2 = self.Enhance[itemKey].EnhanceAttrs2
  DebugOut("UpdateSinglePreviewInfo")
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "setGloryPreview_new", param)
end
function GameUIStarSystemHandbook:getRankInfo(Id)
  local itemKey = tonumber(Id)
  self.curRankBookIndex = itemKey
  for _, v in pairs(self.RankInfo) do
    if v.type == self.curRankBookIndex then
      self.curRankinfo = v
      break
    end
  end
  local param = {}
  self.curRanklist = nil
  self.curRanklist = self.curRankinfo.tops
  param.listLen = #self.curRanklist
  param.playerName = self.curRankinfo.self.user_name
  param.playerRank = self.curRankinfo.self.rank
  param.playerGlory = self.curRankinfo.self.glory
  self:GetFlashObject():InvokeASCallback("_root", "RefreshRank", param)
end
function GameUIStarSystemHandbook:showRank(content)
  self.curRankBookIndex = 0
  self.RankInfo = content.glory_collection_ranks
  for _, v in pairs(self.RankInfo) do
    if v.type == self.curRankBookIndex then
      self.curRankinfo = v
      break
    end
  end
  local param = {}
  param.playerName = self.curRankinfo.self.user_name
  param.playerRank = self.curRankinfo.self.rank
  param.playerGlory = self.curRankinfo.self.glory
  param.Ranktitle = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART")
  param.allTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART_ALL")
  param.Book1Txt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART_1")
  param.Book2Txt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART_2")
  param.Book3Txt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART_3")
  param.Book4Txt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_CHART_4")
  param.RankTxt = GameLoader:GetGameText("LC_MENU_ALLIANCE_INFO_RANK")
  param.NameTxt = GameLoader:GetGameText("LC_MENU_ARENA_CAPTION_NAME")
  param.GloryTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_VALUE")
  param.listLen = #self.curRankinfo.tops
  self.curRanklist = self.curRankinfo.tops
  self:GetFlashObject():InvokeASCallback("_root", "gotoRank", param)
end
function GameUIStarSystemHandbook:RequestRankInfo()
  function RequestRankCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_collection_rank_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_collection_rank_ack.Code then
      self:showRank(content)
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_collection_rank_req.Code, nil, RequestRankCallback, true, nil)
end
function GameUIStarSystemHandbook:updateSingleRank(Id)
  local itemKey = tonumber(Id)
  local param = {}
  DebugOut("updateSingleRank")
  DebugTable(self.curRanklist[itemKey])
  param.itemKey = itemKey
  param.rank = self.curRanklist[itemKey].rank
  param.name = self.curRanklist[itemKey].user_name
  param.gloryNum = self.curRanklist[itemKey].glory
  self:GetFlashObject():InvokeASCallback("_root", "setRank_new", param)
end
function GameUIStarSystemHandbook:showAchievement(content)
  local param = {}
  DebugTable("content")
  DebugOut("puhypuhy")
  DebugTable(content)
  param.activated = content.activated
  param.can_activate = content.can_activate
  self.settingAchievement = content.achievements
  param.achievements = content.achievements
  param.gloryTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_VALUE") .. ":" .. self.total_glory
  param.attrTxt = GameLoader:GetGameText("LC_MENU_MEDAL_ACHIEVEMENT_PROPERTY")
  param.achiTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_ACHIEVEMENT")
  param.btnachiTxt = GameLoader:GetGameText("LC_MENU_MEDAL_GLORY_ACTIVATE")
  param.noAttrTxt = GameLoader:GetGameText("LC_MENU_MEDAL_ACHIEVEMENT_WARNING")
  param.titleTxtList = {}
  param.titleTxtList.page1 = GameLoader:GetGameText("LC_MENU_MEDAL_ACHIEVEMENT_PAGE_1")
  param.titleTxtList.page2 = GameLoader:GetGameText("LC_MENU_MEDAL_ACHIEVEMENT_PAGE_2")
  param.titleTxtList.page3 = GameLoader:GetGameText("LC_MENU_MEDAL_ACHIEVEMENT_PAGE_3")
  param.titleTxtList.page4 = GameLoader:GetGameText("LC_MENU_MEDAL_ACHIEVEMENT_PAGE_4")
  param.titleTxtList.page5 = GameLoader:GetGameText("LC_MENU_MEDAL_ACHIEVEMENT_PAGE_5")
  self.Achi_total_attr = content.total_attr
  param.listlen = math.ceil(#self.Achi_total_attr / 4)
  param.ifCanActActivate = content.activated < content.can_activate and true or false
  DebugOut("gotoAchievement")
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "gotoAchievement", param)
end
function GameUIStarSystemHandbook:RequestAchievementInfo()
  function RequestAchiCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_achievement_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_achievement_ack.Code then
      self:showAchievement(content)
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_achievement_req.Code, nil, RequestAchiCallback, true, nil)
end
function GameUIStarSystemHandbook:showAchiPointAttr(Id)
  local itemKey = tonumber(Id)
  local attrs = ""
  DebugOut("showAchiPointAttr")
  DebugOut(itemKey)
  DebugTable(self.settingAchievement[itemKey])
  for _, v in pairs(self.settingAchievement[itemKey].attr) do
    attrs = attrs .. GameUtils:GetAttrName(v.attr_id) .. "+" .. GameUtils:GetAttrValue(v.attr_id, v.attr_value) .. "    "
  end
  self:GetFlashObject():InvokeASCallback("_root", "showAchiPointAttr", attrs)
end
function GameUIStarSystemHandbook:updateAchievementInfo(content)
  local param = {}
  param.activated = content.activated
  param.can_activate = content.can_activate
  self.Achi_total_attr = content.total_attr
  param.listlen = math.ceil(#self.Achi_total_attr / 4)
  param.ifCanActActivate = content.activated < content.can_activate and true or false
  DebugOut("updateAchievementInfo")
  self.achiBtnIfred = false
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "refreshGloryNumAndTotalAttr", param)
end
function GameUIStarSystemHandbook:FreeActAchieve()
  function RequestAchiCallback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_glory_achievement_activate_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.medal_glory_achievement_activate_ack.Code then
      self:updateAchievementInfo(content)
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.medal_glory_achievement_activate_req.Code, nil, RequestAchiCallback, true, nil)
end
function GameUIStarSystemHandbook:DeepCopy(object)
  local SearchTable = {}
  local function Func(object)
    if type(object) ~= "table" then
      return object
    end
    local NewTable = {}
    SearchTable[object] = NewTable
    for k, v in pairs(object) do
      NewTable[Func(k)] = Func(v)
    end
    return ...
  end
  return (...), object
end
function GameUIStarSystemHandbook:refreshGloryList(Id)
  local itemKey = tonumber(Id)
  local prevLen = #self.fubenMedalGrouplist
  self.choice_param[itemKey] = not self.choice_param[itemKey]
  local param = self.choice_param
  self.filteredMedalGrouplist = {}
  local isAll = true
  local temp = self.choice_param[1]
  for _, v in pairs(self.choice_param) do
    if temp ~= v then
      isAll = false
      break
    end
  end
  if isAll then
    self.filteredMedalGrouplist = self.fubenMedalGrouplist
    self:GetFlashObject():InvokeASCallback("_root", "UpdatemedalGroupInfo", math.ceil(#self.filteredMedalGrouplist / 2))
    return
  end
  local tlist = self:DeepCopy(self.fubenMedalGrouplist)
  local map = {}
  map[1] = 1
  map[2] = 3
  map[3] = 4
  map[4] = 5
  map[5] = 6
  map[6] = 7
  local len = #tlist
  local filter = {}
  local i = 1
  for k = 1, 6 do
    if param[k] then
      filter[i] = k
      i = i + 1
    end
    DebugTable(filter)
  end
  local j = 1
  for n = 1, #filter do
    for k = 1, len do
      if tlist[k] then
        for _, w in pairs(tlist[k].attr) do
          if w.attr_id == map[filter[n]] then
            self.filteredMedalGrouplist[j] = tlist[k]
            j = j + 1
            tlist[k] = nil
            break
          end
        end
      end
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdatemedalGroupInfo", math.ceil(#self.filteredMedalGrouplist / 2))
end
print("ddddddwwwwww")
