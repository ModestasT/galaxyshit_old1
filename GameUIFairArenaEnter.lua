local GameUIFairArenaEnter = LuaObjectManager:GetLuaObject("GameUIFairArenaEnter")
local GameUIFairArenaMain = LuaObjectManager:GetLuaObject("GameUIFairArenaMain")
local GameStateFairArena = GameStateManager.GameStateFairArena
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIFairArenaFormation = LuaObjectManager:GetLuaObject("GameUIFairArenaFormation")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
GameUIFairArenaEnter.baseData = nil
function GameUIFairArenaEnter:OnInitGame()
end
function GameUIFairArenaEnter:Close()
  if GameUIFairArenaEnter:GetFlashObject() then
    GameUIFairArenaEnter:GetFlashObject():InvokeASCallback("_root", "MoveOut")
  end
end
function GameUIFairArenaEnter:OnAddToGameState()
  DebugOut("GameUIFairArenaEnter:OnAddToGameState")
  self:LoadFlashObject()
  GameUIFairArenaEnter:InitBaseUI()
  GameTimer:Add(self._OnTimerTick, 1000)
end
function GameUIFairArenaEnter:InitBaseUI()
  local data = {}
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if leaderlist and GameGlobalData.GlobalData and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex], player_main_fleet.level)
  elseif GameGlobalData:GetUserInfo().icon ~= 0 and GameGlobalData:GetUserInfo().icon ~= 1 then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(GameGlobalData:GetUserInfo().icon, player_main_fleet.level)
  end
  DebugOut("GameUIFairArenaEnter:InitBaseUI:", player_avatar)
  data.avatar = player_avatar
  local userInfo = GameGlobalData:GetData("userinfo")
  DebugTable(userinfo)
  local name_display = GameUtils:GetUserDisplayName(userInfo.name)
  local _, serverName = unpack(LuaUtils:string_split(userInfo.name, "."))
  data.name = name_display
  data.serverName = serverName
  data.battleText = GameLoader:GetGameText("LC_MENU_NOTICT_EMPIRE_COLOSSEUM_BATTLE_COUNTDOWN")
  self:GetFlashObject():InvokeASCallback("_root", "initBaseUI", data)
  GameUIFairArenaEnter:RefreshRedPoint()
end
function GameUIFairArenaEnter:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameUIFairArenaEnter.baseData = nil
  collectgarbage("collect")
end
function GameUIFairArenaEnter:SetBasicData()
  local function callBack(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fair_arena_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    elseif msgType == NetAPIList.fair_arena_ack.Code then
      content.baseTime = os.time()
      GameStateFairArena.baseData = content
      if content.is_submit then
        GameStateFairArena.baseData.subText = GameLoader:GetGameText("LC_MENU_BUTTON_NAME_2")
      else
        GameStateFairArena.baseData.subText = GameLoader:GetGameText("LC_MENU_BUTTON_NAME_1")
      end
      if GameUIFairArenaEnter:GetFlashObject() then
        GameUIFairArenaEnter:GetFlashObject():InvokeASCallback("_root", "setSubmitState", GameStateFairArena.baseData)
      end
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.fair_arena_req.Code, nil, callBack, true, nil)
end
function GameUIFairArenaEnter:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameStateFairArena:ExitState()
    return
  end
  if cmd == "setBasicData" then
    GameUIFairArenaEnter:SetBasicData()
  elseif cmd == "closeBtn" then
    GameUIFairArenaMain:Close()
    GameUIFairArenaEnter:Close()
  elseif cmd == "report" then
    if GameStateFairArena.baseData.state == 1 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NOTICE_EMPIRE_COLOSSEUM_CANNOTIN"))
    else
      GameStateFairArena:SwitchState(GameStateFairArena.RankState, 4)
    end
  elseif cmd == "ranklist" then
    if GameStateFairArena.baseData.state == 1 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NOTICE_EMPIRE_COLOSSEUM_CANNOTIN"))
    else
      GameStateFairArena:SwitchState(GameStateFairArena.RankState, 3)
    end
  elseif cmd == "award" then
    if GameStateFairArena.baseData.state == 1 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NOTICE_EMPIRE_COLOSSEUM_CANNOTIN"))
    else
      GameStateFairArena:SwitchState(GameStateFairArena.RankState, 2)
    end
  elseif cmd == "introduce" then
    GameStateFairArena:SwitchState(GameStateFairArena.RankState, 1)
  elseif cmd == "submit" then
    GameStateFairArena:SwitchState(GameStateFairArena.SelectState, 0)
  end
end
function GameUIFairArenaEnter._OnTimerTick()
  local flashObj = GameUIFairArenaEnter:GetFlashObject()
  if flashObj and GameStateFairArena.baseData then
    local left = GameStateFairArena.baseData.left_time - (os.time() - GameStateFairArena.baseData.baseTime)
    local left2 = GameStateFairArena.baseData.season_left_time - (os.time() - GameStateFairArena.baseData.baseTime)
    local left3 = GameStateFairArena.baseData.battle_left_time - (os.time() - GameStateFairArena.baseData.baseTime)
    if left < 0 then
      left = 0
    end
    if left2 < 0 then
      left2 = 0
    end
    if left3 < 0 then
      left3 = 0
    end
    local tiemStr = ""
    if GameStateFairArena.baseData.left_time < 0 then
      tiemStr = ""
    elseif GameStateFairArena.baseData.left_time >= 0 and left > 0 then
      tiemStr = GameLoader:GetGameText("LC_MENU_NOTICE_TEXT_FORMATION_OVER") .. GameUtils:formatTimeStringAsPartion2(left)
      if GameStateFairArena.baseData.state == 2 then
        tiemStr = GameLoader:GetGameText("LC_MENU_NOTICE_TEXT_FORMATION_START") .. GameUtils:formatTimeStringAsPartion2(left)
      end
    elseif GameStateFairArena.baseData.left_time >= 0 and left == 0 then
      tiemStr = GameLoader:GetGameText("LC_MENU_NOTICE_FORMATION_END")
      if GameStateFairArena.baseData.state == 2 then
        tiemStr = ""
      end
      GameUIFairArenaEnter.needUpdateData = true
    end
    local tiemStr2 = ""
    if GameStateFairArena.baseData.season_left_time < 0 then
      tiemStr2 = ""
    elseif GameStateFairArena.baseData.season_left_time >= 0 and left2 > 0 then
      tiemStr2 = GameLoader:GetGameText("LC_MENU_LEAGUE_SEASON_TIME_CHAR") .. GameUtils:formatTimeStringAsPartion2(left2)
    elseif GameStateFairArena.baseData.season_left_time >= 0 and left2 == 0 then
      tiemStr2 = GameLoader:GetGameText("LC_MENU_NOTICE_SEASON_END")
    end
    local tiemStr3 = ""
    if GameStateFairArena.baseData.battle_left_time < 0 then
      tiemStr3 = ""
    elseif GameStateFairArena.baseData.battle_left_time >= 0 and left3 > 0 then
      tiemStr3 = GameUtils:formatTimeString(left3)
    elseif GameStateFairArena.baseData.battle_left_time >= 0 and left3 == 0 then
      tiemStr3 = "\230\136\152\230\150\151\231\187\147\230\157\159"
    end
    if GameStateFairArena.baseData.state == 2 then
      tiemStr2 = ""
    end
    if GameUIFairArenaEnter:GetFlashObject() then
      GameUIFairArenaEnter:GetFlashObject():InvokeASCallback("_root", "SetInfoText", tiemStr)
    end
    if GameUIFairArenaEnter:GetFlashObject() then
      GameUIFairArenaEnter:GetFlashObject():InvokeASCallback("_root", "SetSeasonInfoText", tiemStr2)
    end
    if GameUIFairArenaEnter:GetFlashObject() then
      GameUIFairArenaEnter:GetFlashObject():InvokeASCallback("_root", "SetBattleInfoText", tiemStr3)
    end
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIFairArenaEnter) then
    return 1000
  end
  return nil
end
function GameUIFairArenaEnter:RefreshRedPoint()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setBtnRewardRedPoint", false)
    if GameGlobalData.redPointStates then
      for k, v in pairs(GameGlobalData.redPointStates.states) do
        if v.key == 100000003 and v.value == 1 then
          flashObj:InvokeASCallback("_root", "setBtnRewardRedPoint", true)
          break
        end
      end
    end
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIFairArenaEnter.OnAndroidBack()
    GameUIFairArenaEnter:OnFSCommand("close")
  end
end
