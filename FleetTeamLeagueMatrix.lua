local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
FleetTeamLeagueMatrix = {}
FleetTeamLeagueMatrix.matrixs = nil
FleetTeamLeagueMatrix.leaderFleet = 1
TEAMLEAGUE_MATRIX_ALL = 0
TEAMLEAGUE_MATRIX_ATTACK = 1
TEAMLEAGUE_MATRIX_DEFENCE = 2
function FleetTeamLeagueMatrix:Init(matrixInfo)
  if matrixInfo == nil then
    return
  end
  self.type = matrixInfo.type
  self.leaderFleet = matrixInfo.leader_fleet
  self.matrixs = matrixInfo.matrixs
  table.sort(self.matrixs, function(a, b)
    return a.id < b.id
  end)
  table.sort(self.matrixs[1].cells, function(a, b)
    return a.cell_id < b.cell_id
  end)
  table.sort(self.matrixs[2].cells, function(a, b)
    return a.cell_id < b.cell_id
  end)
  table.sort(self.matrixs[3].cells, function(a, b)
    return a.cell_id < b.cell_id
  end)
end
function FleetTeamLeagueMatrix:IsValidIndex(index)
  if index >= 1 and index <= 3 then
    return true
  end
  return false
end
function FleetTeamLeagueMatrix:GetMatrixByIndex(index)
  if self.matrixs ~= nil and self:IsValidIndex(index) then
    return self.matrixs[index]
  end
end
function FleetTeamLeagueMatrix:IsEmptyTeam(index)
  for i, fleet in pairs(self:GetMatrixByIndex(index).cells) do
    if fleet.fleet_identity ~= 0 and fleet.fleet_identity ~= -1 then
      return false
    end
  end
  return true
end
function FleetTeamLeagueMatrix:GetMatrixsReq(req_type, callback)
  FleetTeamLeagueMatrix.matrixGetCallback = callback
  local requestFailedCallback
  function requestFailedCallback()
    local req_param = {type = req_type}
    NetMessageMgr:SendMsg(NetAPIList.teamleague_matrix_req.Code, req_param, FleetTeamLeagueMatrix.GetMatrixsAck, true, requestFailedCallback)
  end
  requestFailedCallback()
end
function FleetTeamLeagueMatrix.GetMatrixsAck(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.teamleague_matrix_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.teamleague_matrix_ack.Code then
    DebugOutPutTable(content, "teamleague_matrix_ack==")
    local FleetTeamLeagueAtkMatrix = FleetTeamLeagueMatrix:GetAttackInstance()
    local FleetTeamLeagueDefMatrix = FleetTeamLeagueMatrix:GetDefenceInstance()
    for k, v in pairs(content.teamleague_matrix) do
      if v.type == TEAMLEAGUE_MATRIX_ATTACK then
        FleetTeamLeagueAtkMatrix:Init(v)
      elseif v.type == TEAMLEAGUE_MATRIX_DEFENCE then
        FleetTeamLeagueDefMatrix:Init(v)
      end
    end
    if FleetTeamLeagueMatrix.matrixGetCallback then
      FleetTeamLeagueMatrix.matrixGetCallback()
      FleetTeamLeagueMatrix.matrixGetCallback = nil
    end
    return true
  end
  return false
end
function FleetTeamLeagueMatrix:IsInMatrix(fleet_id)
  if not self.matrixs then
    return false
  end
  for _, v in ipairs(self.matrixs) do
    for i, fleet in pairs(v.cells) do
      if fleet.fleet_identity == fleet_id then
        return true
      end
    end
  end
  return false
end
function FleetTeamLeagueMatrix:GetMatrixFleets()
  local matrixFleets = {}
  local fleets = GameGlobalData.GlobalData.fleetinfo.fleets
  if fleets then
    local fleetsInMatrix = {}
    for _, fleet in ipairs(fleets) do
      if self:IsInMatrix(fleet.identity) then
        table.insert(fleetsInMatrix, fleet.identity)
      end
    end
    table.sort(fleetsInMatrix, function(a, b)
      return a < b
    end)
    matrixFleets = LuaUtils:table_copy(fleetsInMatrix)
  end
  return matrixFleets
end
function FleetTeamLeagueMatrix:GetFreeFleets()
  local freeFleets = {}
  local fleets = GameGlobalData.GlobalData.fleetinfo.fleets
  if fleets then
    local fleetsNotInMatrix = {}
    for _, fleet in ipairs(fleets) do
      if not self:IsInMatrix(fleet.identity) then
        table.insert(fleetsNotInMatrix, fleet.identity)
      end
    end
    table.sort(fleetsNotInMatrix, function(a, b)
      return a < b
    end)
    freeFleets = LuaUtils:table_copy(fleetsNotInMatrix)
  end
  return freeFleets
end
function FleetTeamLeagueMatrix:GetActiviteSpell(commander_id)
  for _, v in ipairs(self.matrixs) do
    for i, fleet in pairs(v.cells) do
      if fleet.fleet_identity == commander_id then
        return fleet.fleet_spell
      end
    end
  end
  return nil
end
function FleetTeamLeagueMatrix:SetCurrentSkill(commander_id, skill_id)
  for _, v in ipairs(self.matrixs) do
    for i, fleet in pairs(v.cells) do
      if fleet.fleet_identity == commander_id then
        fleet.fleet_spell = skill_id
        self.isChange = true
      end
    end
  end
end
function FleetTeamLeagueMatrix:SaveMatrixChanged(callback)
  self.matrixSaveCallback = callback
  if self.isChange then
    self.isChange = false
    local req_param = {}
    req_param.teamleague_matrix = {}
    table.insert(req_param.teamleague_matrix, {
      type = self.type,
      leader_fleet = self.leaderFleet,
      matrixs = self.matrixs
    })
    NetMessageMgr:SendMsg(NetAPIList.teamleague_matrix_save_req.Code, req_param, FleetTeamLeagueMatrix.SaveMatrixAck)
  elseif callback then
    callback()
  end
end
function FleetTeamLeagueMatrix.SaveMatrixAck(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.teamleague_matrix_save_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    elseif FleetTeamLeagueMatrix.matrixSaveCallback then
      FleetTeamLeagueMatrix.matrixSaveCallback()
      FleetTeamLeagueMatrix.matrixSaveCallback = nil
    end
    return true
  end
  return false
end
function FleetTeamLeagueMatrix:SetMatrix(teamIndex, position, cell)
  local cells = self.matrixs[teamIndex].cells
  cells[position].fleet_identity = cell
  self.isChange = true
end
function FleetTeamLeagueMatrix:SwapMatrix(teamIndex, oldPosition, newPosition)
  local cells = self.matrixs[teamIndex].cells
  local tmp_fleet_identity = cells[oldPosition].fleet_identity
  local tmp_fleet_spell = cells[oldPosition].fleet_spell
  cells[oldPosition].fleet_identity = cells[newPosition].fleet_identity
  cells[oldPosition].fleet_spell = cells[newPosition].fleet_spell
  cells[newPosition].fleet_identity = tmp_fleet_identity
  cells[newPosition].fleet_spell = tmp_fleet_spell
  self.isChange = true
end
function FleetTeamLeagueMatrix:SetLeaderFleet(id)
  self.leaderFleet = id
  self.isChange = true
end
function FleetTeamLeagueMatrix:GetLeaderFleet()
  return self.leaderFleet
end
function FleetTeamLeagueMatrix:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end
function FleetTeamLeagueMatrix:HasInit()
  if self.matrixs == nil then
    return false
  end
  return true
end
function FleetTeamLeagueMatrix:GetAttackInstance()
  if self.atkInstance == nil then
    self.atkInstance = self:new()
  end
  return self.atkInstance
end
function FleetTeamLeagueMatrix:GetDefenceInstance()
  if self.defInstance == nil then
    self.defInstance = self:new()
  end
  return self.defInstance
end
function FleetTeamLeagueMatrix:SwitchTeamReq(teamIndex)
  DebugOut("send tlc_mulmatrix_switch_req!!!")
  NetMessageMgr:SendMsg(NetAPIList.tlc_mulmatrix_switch_req.Code, {
    id = self.matrixs[teamIndex].id,
    switch = true
  }, nil, false, nil)
end
function FleetTeamLeagueMatrix.UpdateMatrix(content)
  local FleetTeamLeagueAtkMatrix = FleetTeamLeagueMatrix:GetAttackInstance()
  local FleetTeamLeagueDefMatrix = FleetTeamLeagueMatrix:GetDefenceInstance()
  for k, v in pairs(content.teamleague_matrix) do
    if v.type == TEAMLEAGUE_MATRIX_ATTACK then
      FleetTeamLeagueAtkMatrix:Init(v)
    elseif v.type == TEAMLEAGUE_MATRIX_DEFENCE then
      FleetTeamLeagueDefMatrix:Init(v)
    end
  end
end
