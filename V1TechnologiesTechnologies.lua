local Technologies = GameData.Technologies.Technologies
Technologies[1] = {
  TechID = 1,
  TechName = "Basic durability tech",
  IconName = "1.0",
  TechDesc = "1.0"
}
Technologies[2] = {
  TechID = 2,
  TechName = "Basic Physic Weapon",
  IconName = "2.0",
  TechDesc = "2.0"
}
Technologies[3] = {
  TechID = 3,
  TechName = "Basic Physic Armour",
  IconName = "3.0",
  TechDesc = "3.0"
}
Technologies[4] = {
  TechID = 4,
  TechName = "Basic SK Weapon",
  IconName = "4.0",
  TechDesc = "4.0"
}
Technologies[5] = {
  TechID = 5,
  TechName = "Basic SK Armour",
  IconName = "5.0",
  TechDesc = "5.0"
}
Technologies[6] = {
  TechID = 6,
  TechName = "Basic EN Weapon",
  IconName = "6.0",
  TechDesc = "6.0"
}
Technologies[7] = {
  TechID = 7,
  TechName = "Basic ENp Armour",
  IconName = "7.0",
  TechDesc = "7.0"
}
Technologies[8] = {
  TechID = 8,
  TechName = "basic Engine Speedup",
  IconName = "8.0",
  TechDesc = "8.0"
}
Technologies[101] = {
  TechID = 101,
  TechName = "Intermediate durability tech",
  IconName = "101.0",
  TechDesc = "101.0"
}
Technologies[102] = {
  TechID = 102,
  TechName = "Intermediate Physic Weapon",
  IconName = "102.0",
  TechDesc = "102.0"
}
Technologies[103] = {
  TechID = 103,
  TechName = "Intermediate Physic Armour",
  IconName = "103.0",
  TechDesc = "103.0"
}
Technologies[104] = {
  TechID = 104,
  TechName = "Intermediate SK Weapon",
  IconName = "104.0",
  TechDesc = "104.0"
}
Technologies[105] = {
  TechID = 105,
  TechName = "Intermediate SK Armour",
  IconName = "105.0",
  TechDesc = "105.0"
}
Technologies[106] = {
  TechID = 106,
  TechName = "Intermediate EN Weapon",
  IconName = "106.0",
  TechDesc = "106.0"
}
Technologies[107] = {
  TechID = 107,
  TechName = "Intermediate EN Armour",
  IconName = "107.0",
  TechDesc = "107.0"
}
Technologies[108] = {
  TechID = 108,
  TechName = "Intermediate Engine Speedup",
  IconName = "108.0",
  TechDesc = "108.0"
}
Technologies[201] = {
  TechID = 201,
  TechName = "Advanced durability tech",
  IconName = "201.0",
  TechDesc = "201.0"
}
Technologies[202] = {
  TechID = 202,
  TechName = "Advanced Physic Weapon",
  IconName = "202.0",
  TechDesc = "202.0"
}
Technologies[203] = {
  TechID = 203,
  TechName = "Advanced Physic Armour",
  IconName = "203.0",
  TechDesc = "203.0"
}
Technologies[204] = {
  TechID = 204,
  TechName = "Advanced SK Weapon",
  IconName = "204.0",
  TechDesc = "204.0"
}
Technologies[205] = {
  TechID = 205,
  TechName = "Advanced SK Armour",
  IconName = "205.0",
  TechDesc = "205.0"
}
Technologies[206] = {
  TechID = 206,
  TechName = "Advanced EN Weapon",
  IconName = "206.0",
  TechDesc = "206.0"
}
Technologies[207] = {
  TechID = 207,
  TechName = "Advanced EN Armour",
  IconName = "207.0",
  TechDesc = "207.0"
}
Technologies[208] = {
  TechID = 208,
  TechName = "Advanced Engine Speedup",
  IconName = "208.0",
  TechDesc = "208.0"
}
