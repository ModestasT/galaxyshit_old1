WVEInfoPanelLeaderboard = luaClass(nil)
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function WVEInfoPanelLeaderboard:ctor(wveGameManeger)
  self.mGameManger = wveGameManeger
  self.TAB_STATE = {
    MYSELF = 1,
    DAMAGE = 2,
    KILL = 3
  }
  self.mCurTab = self.TAB_STATE.MYSELF
  self.primeRankData = {}
  self.fightHistory = {}
  self.insertItemIndex = nil
end
function WVEInfoPanelLeaderboard:OnFSCommand(cmd, arg)
  local flashObj = self.mGameManger:GetFlashObject()
  if cmd == "updateLeaderboardItemInfo" then
    local itemIndex = tonumber(arg)
    DebugOut("itemIndex", itemIndex)
    if self.mCurTab ~= self.TAB_STATE.MYSELF then
      self:SetLeaderboardTitleItemInfo(itemIndex)
    else
      self:SetLeaderboardReportItemInfo(0, itemIndex)
    end
  elseif cmd == "LeaderboardItemLeave" then
  elseif cmd == "ShowLeaderboardPanel" then
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowLeaderboardPanel")
      self.IsShowLeaderboardWindow = true
    end
    self:GetPrimeRankData()
    self.mCurTab = self.TAB_STATE.MYSELF
    self:InitLeaderboardList()
    self:RequestFightHistory()
  elseif cmd == "ShowLeaderboardMyReportTab" then
    DebugOut("ShowLeaderboardMyReportTab")
    if self.mCurTab == self.TAB_STATE.MYSELF then
      return
    end
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowLeaderboardMyReportTab")
    end
    self:GetPrimeRankData()
    self.mCurTab = self.TAB_STATE.MYSELF
    self:InitLeaderboardList()
    self:RequestFightHistory()
  elseif cmd == "ShowLeaderboardKillTab" then
    DebugOut("ShowLeaderboardKillTab", self.mCurTab)
    if self.mCurTab == self.TAB_STATE.KILL then
      DebugOut("1")
      return
    end
    if flashObj then
      DebugOut("2")
      flashObj:InvokeASCallback("_root", "ShowLeaderboardKillTab")
    end
    self:GetPrimeRankData()
    self.mCurTab = self.TAB_STATE.KILL
    self:InitLeaderboardList()
  elseif cmd == "ShowLeaderboardDamageTab" then
    DebugOut("ShowLeaderboardDamageTab")
    if self.mCurTab == self.TAB_STATE.DAMAGE then
      return
    end
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowLeaderboardDamageTab")
    end
    self:GetPrimeRankData()
    self.mCurTab = self.TAB_STATE.DAMAGE
    self:InitLeaderboardList()
  elseif cmd == "CloseLeaderboardPanel" then
    if flashObj then
      flashObj:InvokeASCallback("_root", "CloseLeaderboardPanel")
      self.IsShowLeaderboardWindow = false
    end
  elseif cmd == "ClickLeaderboardItem" then
    local args = LuaUtils:string_split(arg, ",")
    local itemIndex = tonumber(args[1])
    local childrenCount = tonumber(args[2])
    if childrenCount and childrenCount > 0 then
      if flashObj then
        flashObj:InvokeASCallback("_root", "InitLeaderboardFightItemList", itemIndex, 0)
      end
    else
      self:RequestFightHistory(itemIndex)
    end
  elseif cmd == "ClickLeaderboardReplayBtn" then
    if self.mCurTab == self.TAB_STATE.MYSELF then
      local itemIndex = tonumber(arg)
      if self.fightHistory and self.fightHistory[1] and self.fightHistory[1].datas[itemIndex] then
        NetMessageMgr:SendMsg(NetAPIList.prime_fight_report_req.Code, self.fightHistory[1].datas[itemIndex], self.RequestFightReportCallback, true, nil)
      end
    else
      local args = LuaUtils:string_split(arg, ",")
      local parentIndex = tonumber(args[1])
      local childrenIndex = tonumber(args[2])
      DebugOut("ClickLeaderboardReplayBtn", parentIndex, ",", childrenIndex)
      self:RequestFightReport(parentIndex, childrenIndex)
    end
  else
    return false
  end
  return true
end
function WVEInfoPanelLeaderboard:BackToLeaderboardPanel(preTab)
  DebugOut("BackToLeaderboardPanel")
  local flashObj = self.mGameManger:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowLeaderboardPanel")
    self.IsShowLeaderboardWindow = true
  end
  self:GetPrimeRankData()
  self:InitLeaderboardList()
  if preTab == self.TAB_STATE.MYSELF then
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowLeaderboardMyReportTab")
    end
    self.mCurTab = self.TAB_STATE.MYSELF
    self:RequestFightHistory()
  elseif preTab == self.TAB_STATE.DAMAGE then
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowLeaderboardDamageTab")
    end
    self.mCurTab = self.TAB_STATE.DAMAGE
  elseif preTab == self.TAB_STATE.KILL then
    if flashObj then
      flashObj:InvokeASCallback("_root", "ShowLeaderboardKillTab")
    end
    self.mCurTab = self.TAB_STATE.KILL
  end
end
function WVEInfoPanelLeaderboard:SetLeaderboardTitleItemInfo(itemIndex)
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  if not self.primeRankData then
    return
  end
  local data = self.primeRankData.damage_ranks
  local name = ""
  if self.mCurTab == self.TAB_STATE.KILL then
    data = self.primeRankData.kill_ranks
  end
  if data and data[itemIndex] then
    name = data[itemIndex].user_name
  end
  DebugOut("SetLeaderboardTitleItemInfo:", name, data[itemIndex].point)
  local point = GameUtils.numberConversionEnglish(tonumber(data[itemIndex].point))
  flashObj:InvokeASCallback("_root", "SetLeaderboardItemInfo", itemIndex, 0, itemIndex, name, point)
end
function WVEInfoPanelLeaderboard:SetLeaderboardReportItemInfo(parentIndex, childrenIndex)
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  if not self.fightHistory then
    return
  end
  local time, desc
  local reportData = {}
  for k, v in pairs(self.fightHistory) do
    if v.parent == parentIndex then
      reportData = v.datas[childrenIndex]
      break
    end
  end
  if not reportData then
    return
  end
  time = GameUtils:formatTimeStringAsPartion(os.time() - reportData.time)
  if reportData.is_win then
    desc = string.format(GameLoader:GetGameText("LC_MENU_WVE_FIGHT_WIN"), GameLoader:GetGameText("LC_BATTLE_PRIME_MOBS_" .. tostring(reportData.defender)))
  else
    desc = string.format(GameLoader:GetGameText("LC_MENU_WVE_FIGHT_FAILED"), GameLoader:GetGameText("LC_BATTLE_PRIME_MOBS_" .. tostring(reportData.defender)))
  end
  DebugOut("SetLeaderboardReportItemInfo", time, desc)
  flashObj:InvokeASCallback("_root", "SetLeaderboardItemInfo", childrenIndex, 1, time, desc, 0)
end
function WVEInfoPanelLeaderboard:RequestFightReport(titleIndex, reportIndex)
  DebugOut("RequestFightReport:", titleIndex, ",", reportIndex)
  DebugTable(self.fightHistory)
  if not self.fightHistory then
    return
  end
  self.leaderboard_titleIndex = titleIndex
  for k, v in pairs(self.fightHistory) do
    if v.parent == titleIndex then
      if v.datas[reportIndex] then
        DebugOut("RequestFightReport:", v.datas[reportIndex].report_id)
        NetMessageMgr:SendMsg(NetAPIList.prime_fight_report_req.Code, v.datas[reportIndex], self.RequestFightReportCallback, true, nil)
      end
      break
    end
  end
end
function WVEInfoPanelLeaderboard:ShowLeaderboardItem(itemIndex)
  DebugOut("ShowLeaderboardItem:", itemIndex)
  if not itemIndex then
    return
  end
  local flashObj = self.mGameManger:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowLeaderboardItem", itemIndex)
  end
  WVEGameManeger:OnFSCommand("ClickLeaderboardItem", itemIndex)
end
function WVEInfoPanelLeaderboard.RequestFightReportCallback(msgType, content)
  DebugOut("RequestFightReportCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.battle_fight_report_ack.Code then
    if content and content.histories and content.histories[1] then
      GameStateBattlePlay.curBattleType = "WVELeaderboard"
      GameStateManager.GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.histories[1].report, nil, nil)
      do
        local preTab = WVEGameManeger.mWVEInfoPanelLeaderboard.mCurTab
        local titleIndex = WVEGameManeger.mWVEInfoPanelLeaderboard.leaderboard_titleIndex
        GameStateBattlePlay:RegisterOverCallback(function()
          WVEGameManeger:BackToGame()
          DebugOut("RegisterOverCallback:", preTab, ",", titleIndex)
          WVEGameManeger.mWVEInfoPanelLeaderboard:BackToLeaderboardPanel(preTab)
          if preTab ~= WVEGameManeger.mWVEInfoPanelLeaderboard.TAB_STATE.MYSELF then
            WVEGameManeger.mWVEInfoPanelLeaderboard:ShowLeaderboardItem(titleIndex)
          end
        end, nil)
        GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      end
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_REPLAY_NOT_EXIST"))
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.battle_fight_report_ack.Code then
    return true
  end
  return false
end
function WVEInfoPanelLeaderboard:InitLeaderboardList()
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  self.fightHistory = {}
  local listCount = 0
  if self.mCurTab == self.TAB_STATE.MYSELF then
  elseif self.mCurTab == self.TAB_STATE.DAMAGE then
    if self.primeRankData.damage_ranks then
      listCount = #self.primeRankData.damage_ranks
    end
  elseif self.mCurTab == self.TAB_STATE.KILL and self.primeRankData.kill_ranks then
    listCount = #self.primeRankData.kill_ranks
  end
  DebugOut("InitLeaderboardList:", listCount)
  flashObj:InvokeASCallback("_root", "initLeaderboardListBox", listCount)
end
function WVEInfoPanelLeaderboard:GetPrimeRankData()
  DebugOut("GetPrimeRankData:")
  self.primeRankData = {}
  self.primeRankData.damage_self_rank = {}
  self.primeRankData.damage_ranks = {}
  self.primeRankData.kill_self_rank = {}
  self.primeRankData.kill_ranks = {}
  local rankData = GameGlobalData:GetData("prime_rank_info")
  if not rankData then
    DebugOut("nil value")
    return
  end
  DebugTable(rankData)
  for k, v in pairs(rankData) do
    if tonumber(v.type) == 2 then
      DebugOut("2")
      self.primeRankData.damage_self_rank = v.self_rank
      self.primeRankData.damage_ranks = v.players
    elseif tonumber(v.type) == 1 then
      DebugOut("1")
      self.primeRankData.kill_self_rank = v.self_rank
      self.primeRankData.kill_ranks = v.players
    end
  end
  DebugTable(self.primeRankData)
end
function WVEInfoPanelLeaderboard:RequestFightHistory(itemIndex)
  if not self.primeRankData then
    DebugOut("ShowLeaderboardPanel:rankData is nil")
    return
  end
  DebugOut("RequestFightHistory:")
  DebugOut(self.mCurTab)
  local requestContent = {}
  if self.mCurTab == self.TAB_STATE.MYSELF then
    if self.primeRankData.damage_self_rank then
      requestContent.server_id = self.primeRankData.damage_self_rank.server_id
      requestContent.user_id = self.primeRankData.damage_self_rank.user_id
    elseif self.primeRankData.kill_self_rank then
      requestContent.server_id = self.primeRankData.kill_self_rank.server_id
      requestContent.user_id = self.primeRankData.kill_self_rank.user_id
    end
  elseif self.mCurTab == self.TAB_STATE.DAMAGE then
    if self.primeRankData.damage_ranks and self.primeRankData.damage_ranks[itemIndex] then
      requestContent.server_id = self.primeRankData.damage_ranks[itemIndex].server_id
      requestContent.user_id = self.primeRankData.damage_ranks[itemIndex].user_id
    end
  elseif self.mCurTab == self.TAB_STATE.KILL and self.primeRankData.kill_ranks and self.primeRankData.kill_ranks[itemIndex] then
    requestContent.server_id = self.primeRankData.kill_ranks[itemIndex].server_id
    requestContent.user_id = self.primeRankData.kill_ranks[itemIndex].user_id
  end
  DebugOut("RequestFightHistory")
  DebugTable(requestContent)
  if not requestContent.server_id or not requestContent.user_id then
    return
  end
  self.insertItemIndex = itemIndex
  NetMessageMgr:SendMsg(NetAPIList.prime_fight_history_req.Code, requestContent, self.RequestFightHistoryCallback, true, nil)
end
function WVEInfoPanelLeaderboard:GenerateFightHistoryData()
  local data = {}
  data.fight_history = {}
  for i = 1, 5 do
    local item = {}
    item.report_id = 1
    item.atker = "atker" .. tostring(self.insertItemIndex)
    item.defender = "defender"
    item.is_win = i % 2 == 0
    item.time = os.time() - 9999
    table.insert(data.fight_history, item)
  end
  return data
end
function WVEInfoPanelLeaderboard:GenerateRankData()
  local data = {}
  data.type = 2
  data.self_rank = {}
  data.players = {}
  local totalPlayer = 120
  for i = 1, totalPlayer do
    local player = {}
    player.server_id = 1
    player.user_id = 2
    player.user_name = "name" .. i
    player.rank = i
    player.point = 100 + i
    table.insert(data.players, player)
  end
  local rankData = {}
  table.insert(rankData, data)
  return rankData
end
function WVEInfoPanelLeaderboard.RequestFightHistoryCallback(msgType, content)
  DebugOut("RequestFightHistoryCallback:", msgType)
  DebugTable(content)
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    DebugOut("flashObj is nil")
    return
  end
  if not content then
    return
  end
  local panel = WVEGameManeger.mWVEInfoPanelLeaderboard
  local insertItem = {}
  if msgType == NetAPIList.prime_fight_history_ack.Code then
    if panel.mCurTab == panel.TAB_STATE.MYSELF then
      insertItem.parent = 0
      insertItem.datas = content.fight_history
      if insertItem.datas and 0 < #insertItem.datas then
        table.insert(panel.fightHistory, insertItem)
      end
      flashObj:InvokeASCallback("_root", "initLeaderboardListBox", #content.fight_history)
    elseif panel.mCurTab == panel.TAB_STATE.DAMAGE then
      DebugOut("Damage:", panel.insertItemIndex)
      insertItem.parent = panel.insertItemIndex
      insertItem.datas = content.fight_history
      if insertItem.datas and 0 < #insertItem.datas then
        table.insert(panel.fightHistory, insertItem)
      end
      local datas = panel:GenerateFightInfo(content.fight_history)
      flashObj:InvokeASCallback("_root", "InitLeaderboardFightItemList", panel.insertItemIndex, #content.fight_history, datas)
    elseif panel.mCurTab == panel.TAB_STATE.KILL then
      DebugOut("Kill:", panel.insertItemIndex)
      insertItem.parent = panel.insertItemIndex
      insertItem.datas = content.fight_history
      if insertItem.datas and 0 < #insertItem.datas then
        table.insert(panel.fightHistory, insertItem)
      end
      local datas = panel:GenerateFightInfo(content.fight_history)
      flashObj:InvokeASCallback("_root", "InitLeaderboardFightItemList", panel.insertItemIndex, #content.fight_history, datas)
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.prime_fight_history_req.Code then
    return true
  end
  return false
end
function WVEInfoPanelLeaderboard:Test(itemIndex)
  local fight_history = {
    [1] = {
      report_id = 1,
      atker = "atker1",
      defender = "defender",
      is_win = true,
      time = 1000000
    },
    [2] = {
      report_id = 1,
      atker = "atker2",
      defender = "defender",
      is_win = true,
      time = 1000000
    },
    [3] = {
      report_id = 1,
      atker = "atker3",
      defender = "defender",
      is_win = true,
      time = 1000000
    }
  }
  local panel = WVEGameManeger.mWVEInfoPanelLeaderboard
  local datas = panel:GenerateFightInfo(fight_history)
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    DebugOut("flashObj is nil")
    return
  end
  flashObj:InvokeASCallback("_root", "InitLeaderboardFightItemList", itemIndex, #fight_history, datas)
end
function WVEInfoPanelLeaderboard:GenerateFightInfo(fightHistory)
  if not fightHistory then
    return
  end
  local datas = {}
  for k, v in pairs(fightHistory) do
    local item = {}
    item.text1 = GameUtils:formatTimeStringAsPartion(os.time() - v.time)
    if v.is_win then
      item.text2 = string.format(GameLoader:GetGameText("LC_MENU_WVE_FIGHT_WIN"), GameLoader:GetGameText("LC_BATTLE_PRIME_MOBS_" .. tostring(v.defender)))
    else
      item.text2 = string.format(GameLoader:GetGameText("LC_MENU_WVE_FIGHT_FAILED"), GameLoader:GetGameText("LC_BATTLE_PRIME_MOBS_" .. tostring(v.defender)))
    end
    table.insert(datas, item)
  end
  return datas
end
