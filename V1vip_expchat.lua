local chat = GameData.vip_exp.chat
chat[0] = {
  vip_level = 0,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[1] = {
  vip_level = 1,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[2] = {
  vip_level = 2,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[3] = {
  vip_level = 3,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[4] = {
  vip_level = 4,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[5] = {
  vip_level = 5,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[6] = {
  vip_level = 6,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[7] = {
  vip_level = 7,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[8] = {
  vip_level = 8,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[9] = {
  vip_level = 9,
  name_color = "#00CCFF",
  chat_color = "#FFFFFF",
  show = 0
}
chat[10] = {
  vip_level = 10,
  name_color = "#9966FF",
  chat_color = "#9999FF",
  show = 1
}
chat[11] = {
  vip_level = 11,
  name_color = "#FF9900",
  chat_color = "#FFCC00",
  show = 1
}
chat[12] = {
  vip_level = 12,
  name_color = "#FFFF00",
  chat_color = "#FFFF99",
  show = 1
}
