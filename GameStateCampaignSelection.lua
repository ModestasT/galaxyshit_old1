local NetApiList = NetApiList
local NetMessageMgr = NetMessageMgr
local GameStateCampaignSelection = GameStateManager.GameStateCampaignSelection
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameSectionSelect = LuaObjectManager:GetLuaObject("GameSectionSelect")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
function GameStateCampaignSelection:InitGameState()
end
function GameStateCampaignSelection:EnterArea(area_id)
  DebugOut("GameStateCampaignSelection:EnterArea ", area_id)
  GameSectionSelect:SetCurrentArea(area_id)
  self:AddObject(GameUISection)
  self:AddObject(GameSectionSelect)
  GameSectionSelect:RequestAllActStatus()
  local area_name = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. tostring(GameSectionSelect.currentareaId))
  GameUISection:SetTitle(area_name)
end
function GameStateCampaignSelection:OnFocusGain(previousState)
  DebugOut("GameStateCampaignSelection FocusGain")
  GameUtils:PlayMusic("GE2_Map.mp3")
  if QuestTutorialBattleMap:IsActive() then
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51115})
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({100054})
    end
  elseif TutorialQuestManager.QuestTutorialFirstChapter:IsActive() and (immanentversion170 == 4 or immanentversion170 == 5) then
    GameUICommonDialog:PlayStory({51115})
  end
end
function GameStateCampaignSelection:OnFocusLost(newState)
  DebugOut("GameStateCampaignSelection FocusLost")
  self:EraseObject(GameSectionSelect)
  self:EraseObject(GameUISection)
end
function GameStateCampaignSelection:UpdateBattleStatus()
  table.sort(self.m_battleStatus, function(a, b)
    return a.battle_id < b.battle_id
  end)
  self.m_numSectionBattles = #self.m_battleStatus
  self._lastSectionBattleID = self.m_battleStatus[#self.m_battleStatus].battle_id
  local index = 0
  for index = 1, self.m_numSectionBattles do
    local battle_info = self.m_battleStatus[index]
    if battle_info.status == 0 then
      local area_id, battle_id = GameUtils:ResolveBattleID(battle_info.battle_id)
      self._activeBattleID = battle_id
      self._activeBattleIndex = index
      self._activeScene = GameDataAccessHelper:GetBattleInfo(self.m_currentACT, self._activeBattleID).SceneShot
      self._sectionProgress = math.floor((index - 1) / self.m_numSectionBattles * 100)
      return
    end
  end
  self._activeBattleID = nil
  self._activeBattleIndex = self.m_numSectionBattles
  self._activeScene = nil
  self._sectionProgress = 100
end
function GameStateCampaignSelection:GetLastBattleID()
  return self._lastSectionBattleID
end
function GameStateCampaignSelection:GetSectionProgress()
  return self._sectionProgress
end
function GameStateCampaignSelection:GetActiveBattleID()
  return self._activeBattleID
end
function GameStateCampaignSelection:GetActiveBattleIndex()
  return self._activeBattleIndex
end
function GameStateCampaignSelection:GetActiveScene()
  return self._activeScene
end
