local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIAchievement = LuaObjectManager:GetLuaObject("GameUIAchievement")
function GameUIAchievement:Clear()
  GameUIAchievement:ClearLocalData()
end
function GameUIAchievement:OnAddToGameState(parent_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:UpdateAchievementDataRequest()
end
function GameUIAchievement:OnEraseFromState()
  self:Clear()
  self:UnloadFlashObject()
end
function GameUIAchievement:OnFSCommand(cmd, arg)
  if cmd == "update_classify_item" then
    self:UpdateClassifyItem(tonumber(arg))
    return
  elseif cmd == "update_overview_item" then
    self:UpdateOverviewItem(tonumber(arg))
    return
  elseif cmd == "update_achievement_item" then
    self:UpdateAchievementItem(tonumber(arg))
    return
  elseif cmd == "close" then
    return
  elseif cmd == "classify_item_clicked" then
    local indexItem = tonumber(arg)
    self:UpdateAchievementList(indexItem)
    return
  end
end
function GameUIAchievement._NetMessageCallback(msgtype, content)
  if msgtype == NetAPIList.achievement_list_ack.Code then
    GameUIAchievement._AchievementInfoList = content.achievement_list
    GameUIAchievement:GenerateAchievement(content.achievement_list)
    GameUIAchievement:UpdateClassifyList()
    GameUIAchievement:UpdateAchievementList(1)
    return true
  end
  return false
end
function GameUIAchievement:ClearLocalData()
  self._AchievementType = nil
  self._AchievementTypeList = nil
  self._AchievementInfoList = nil
  self._AchievementOverviewList = nil
end
function GameUIAchievement:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "lua2fs_UpdateFrame", dt)
end
function GameUIAchievement:UpdateClassifyList()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_updateClassifyList", #self._AchievementClassifyTable)
end
function GameUIAchievement:UpdateClassifyItem(index)
  local classify_info = self._AchievementClassifyTable[index]
  local classify_local_name = self:GetClassifyLocalText(classify_info._name)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateClassifyItem", index, classify_local_name, index)
end
function GameUIAchievement:UpdateAchievementList(index_classify)
  self._activeClassifyIndex = index_classify
  local classify_info = self._AchievementClassifyTable[index_classify]
  local flash_obj = self:GetFlashObject()
  if index_classify == 1 then
    local description = GameLoader:GetGameText("LC_MENU_ACHIEVEMENT_EXPLAN_CHAR")
    flash_obj:InvokeASCallback("_root", "lua2fs_updateOverviewList", #classify_info._items, description)
  else
    flash_obj:InvokeASCallback("_root", "lua2fs_updateAchievementList", #classify_info._items, index_classify)
  end
end
function GameUIAchievement:UpdateOverviewItem(index)
  local flash_obj = self:GetFlashObject()
  local classify_info = self._AchievementClassifyTable[1]
  local item_info = classify_info._items[index]
  flash_obj:InvokeASCallback("_root", "lua2fs_updateOverviewItem", index, item_info._level, self:GetLevelLocalText(item_info._level), item_info._complete, item_info._total)
end
function GameUIAchievement:UpdateAchievementItem(index)
  local flash_obj = self:GetFlashObject()
  local classify_info = self._AchievementClassifyTable[self._activeClassifyIndex]
  local item_info = classify_info._items[index]
  if self._activeClassifyIndex == 1 then
    flash_obj:InvokeASCallback("_root", "lua2fs_updateOverviewItem", index, item_info._level, item_info._complete, item_info._total)
  else
    local item_info_type = type(item_info)
    if item_info_type == "string" then
      flash_obj:InvokeASCallback("_root", "lua2fs_updateSeparaterItem", index, self:GetGroupLocalText(item_info))
    elseif item_info_type == "table" then
      flash_obj:InvokeASCallback("_root", "lua2fs_updateAchievementItem", index, item_info.level, self:GetAchievementTitle(item_info.id), self:GetAchievementDesc(item_info.id), item_info.finish_count, item_info.number, GameUtils.numberConversion(item_info.credit))
    else
      assert(false)
    end
  end
end
function GameUIAchievement:UpdateAchievementData(index)
  local data_type = self._AchievementTypeList[index]
  self._AchievementType = data_type
  local packet = {classify = data_type}
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.achievement_list_req.Code, packet, self._NetMessageCallback, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.achievement_list_req.Code, packet, self._NetMessageCallback, true, netFailedCallback)
end
function GameUIAchievement:UpdateAchievementDataRequest()
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.achievement_list_req.Code, nil, self._NetMessageCallback, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.achievement_list_req.Code, nil, self._NetMessageCallback, true, netFailedCallback)
end
function GameUIAchievement:UpdateAchievementInfoList()
  local indexType = 1
  for i, v in ipairs(self._AchievementTypeList) do
    if v == self._AchievementType then
      indexType = i + 1
      break
    end
  end
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_InitAchievementList", #self._AchievementInfoList, indexType)
end
function GameUIAchievement:UpdateOverviewData()
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.achievement_overview_req.Code, nil, self._NetMessageCallback, true, netFailedCallback)
  end
  NetMessageMgr:SendMsg(NetAPIList.achievement_overview_req.Code, nil, self._NetMessageCallback, true, netFailedCallback)
end
function GameUIAchievement:UpdateAchievementOverviewList()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_InitAchievementList", #self._AchievementOverviewList + 1, 1)
end
function GameUIAchievement:UpdateAchievementInfoItem(index)
  local flash_obj = self:GetFlashObject()
  local data_achievement = self._AchievementInfoList[index]
  if data_achievement.stype then
    DebugOut(data_achievement.stype)
    flash_obj:InvokeASCallback("_root", "lua2fs_updateSeparaterItem", index, data_achievement.stype)
    return
  end
  local text_title = GameDataAccessHelper:GetAchievementTitle(data_achievement.id)
  text_title = GameLoader:GetGameText("LC_MENU_" .. text_title)
  local text_desc = GameDataAccessHelper:GetAchievementDesc(data_achievement.id)
  text_desc = GameLoader:GetGameText("LC_MENU_" .. text_desc)
  text_desc = string.format(text_desc, data_achievement.number)
  flash_obj:InvokeASCallback("_root", "lua2fs_updateAchievementItem", index, GameDataAccessHelper:GetAchievementLevel(data_achievement.id), text_title, text_desc, data_achievement.finish_count, data_achievement.number, GameUtils.numberConversion(data_achievement.credit))
end
function GameUIAchievement:GenerateAchievement(achievement_list)
  local SortComp = function(a, b)
    return a.level * 1000 + a.id < b.level * 1000 + b.id
  end
  table.sort(achievement_list, SortComp)
  self._AchievementTable = achievement_list
  local classify_table = {}
  self._AchievementClassifyTable = classify_table
  local classify_item_overview = {}
  classify_item_overview._name = "overview"
  classify_item_overview._items = {}
  local complete_count = {}
  for level = 1, 4 do
    complete_count[level] = {complete = 0, total = 0}
  end
  for _, achievement in ipairs(self._AchievementTable) do
    local level = achievement.level
    if 0 < achievement.is_finished then
      complete_count[level].complete = complete_count[level].complete + 1
    end
    complete_count[level].total = complete_count[level].total + 1
  end
  table.insert(classify_item_overview._items, {
    _level = 4,
    _complete = complete_count[4].complete,
    _total = complete_count[4].total
  })
  table.insert(classify_item_overview._items, {
    _level = 3,
    _complete = complete_count[3].complete,
    _total = complete_count[3].total
  })
  table.insert(classify_item_overview._items, {
    _level = 2,
    _complete = complete_count[2].complete,
    _total = complete_count[2].total
  })
  table.insert(classify_item_overview._items, {
    _level = 1,
    _complete = complete_count[1].complete,
    _total = complete_count[1].total
  })
  table.insert(classify_table, classify_item_overview)
  self:SetLocalText("classify_overview", self:GetClassifyLocalText("overview"))
  local classify_new_item = {
    _name = "new",
    _items = {}
  }
  classify_new_item._items = LuaUtils:array_filter(self._AchievementTable, function(k, v)
    return v.is_finished ~= 0
  end)
  table.sort(classify_new_item._items, function(a, b)
    return a.finish_time > b.finish_time
  end)
  while table.getn(classify_new_item._items) > 5 do
    table.remove(classify_new_item._items)
  end
  table.insert(classify_table, classify_new_item)
  for _, v in ipairs(self._AchievementTable) do
    local find_classify = false
    for index_classify, classify_item in ipairs(classify_table) do
      if classify_item._name == v.classify then
        find_classify = true
        table.insert(classify_item._items, v)
        break
      end
    end
    if not find_classify then
      local new_classify_item = {
        _name = v.classify,
        _items = {}
      }
      table.insert(new_classify_item._items, v)
      table.insert(classify_table, new_classify_item)
      self:SetLocalText("classify_" .. new_classify_item._name, self:GetClassifyLocalText(new_classify_item._name))
    end
  end
  local num_classify = #classify_table
  for index_classify = 2, num_classify do
    local classify_info = classify_table[index_classify]
    local classify_name = classify_info._name
    local group_list = {}
    for _, achievement_info in ipairs(classify_info._items) do
      local find_group = false
      for igroup, group_info in ipairs(group_list) do
        if group_info == achievement_info.group then
          find_group = true
          break
        end
      end
      if not find_group and achievement_info.group ~= "empty" then
        table.insert(group_list, achievement_info.group)
        self:SetLocalText("group_" .. achievement_info.group, self:GetGroupLocalText(achievement_info.group))
      end
    end
    local groupListCounts = {empty = 0}
    for _, groupName in ipairs(group_list) do
      groupListCounts[groupName] = 0
    end
    if #group_list > 0 and index_classify > 2 then
      for _, achievement_info in ipairs(classify_info._items) do
        if achievement_info.group == "empty" then
          groupListCounts.empty = groupListCounts.empty + 1
          table.insert(group_list, groupListCounts.empty, achievement_info)
        else
          local is_insert = false
          for index_item, item_info in pairs(group_list) do
            if type(item_info) == "string" and item_info == achievement_info.group then
              groupListCounts[achievement_info.group] = groupListCounts[achievement_info.group] + 1
              table.insert(group_list, index_item + groupListCounts[achievement_info.group], achievement_info)
              is_insert = true
            end
          end
          assert(is_insert)
        end
      end
      classify_info._items = group_list
    end
  end
  DebugTable(self._AchievementClassifyTable)
end
function GameUIAchievement:Visible(is_visible)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_setVisible", "", is_visible)
end
function GameUIAchievement:GetClassifyLocalText(classify_name)
  return ...
end
function GameUIAchievement:GetGroupLocalText(group_name)
  return ...
end
function GameUIAchievement:GetLevelLocalText(level)
  return ...
end
function GameUIAchievement:GetAchievementTitle(achievement_id)
  return ...
end
function GameUIAchievement:GetAchievementDesc(achievement_id)
  return ...
end
function GameUIAchievement:SetLocalText(text_key, text_content)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_setLocalText", text_key, text_content)
end
