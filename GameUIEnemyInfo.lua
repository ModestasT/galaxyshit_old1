local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameObjectBattleMap = LuaObjectManager:GetLuaObject("GameObjectBattleMap")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialBuildTechLab = TutorialQuestManager.QuestTutorialBuildTechLab
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialBuildAffairs = TutorialQuestManager.QuestTutorialBuildAffairs
local QuestTutorialMine = TutorialQuestManager.QuestTutorialMine
local QuestTutorialSkill = TutorialQuestManager.QuestTutorialSkill
local QuestTutorialSlot = TutorialQuestManager.QuestTutorialSlot
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialBattleFailed = TutorialQuestManager.QuestTutorialBattleFailed
local QuestTutorialRush = TutorialQuestManager.QuestTutorialRush
local QuestTutorialArena = TutorialQuestManager.QuestTutorialArena
local GameRush = LuaObjectManager:GetLuaObject("GameRush")
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUISupplyDialog = LuaObjectManager:GetLuaObject("GameUISupplyDialog")
local QuestTutorialBattleRush = TutorialQuestManager.QuestTutorialBattleRush
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local QuestTutorialPveMapPoint = TutorialQuestManager.QuestTutorialPveMapPoint
local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local QuestTutorialMainTask = TutorialQuestManager.QuestTutorialMainTask
local GameUtils = GameUtils
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local historyFirst = {}
local historyLowest = {}
local historyNews = {}
local rewradsList = {}
local m_accomplish_level = 0
GameUIEnemyInfo.isShowRushDialogFlag = false
GameUIEnemyInfo.isElite = false
GameUIEnemyInfo.consumeElite = 0
GameUIEnemyInfo.priceElite = 0
GameUIEnemyInfo.leftEliteCount = 0
GameUIEnemyInfo.mapType = "normal"
function GameUIEnemyInfo.GetBattleStatusCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.battle_status_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.battle_status_ack.Code then
    GameUIEnemyInfo:UpdateBattleStatus(content)
    GameStateManager:GetCurrentGameState():AddObject(GameUIEnemyInfo)
    return true
  end
  return false
end
function GameUIEnemyInfo:GetLastEnemy()
  local lastEnemyIndex = 0
  if self.m_enemyIndex > 1 then
    lastEnemyIndex = self.m_enemyIndex
  else
    return nil
  end
  local areaID, battleID = 0, 0
  repeat
    lastEnemyIndex = lastEnemyIndex - 1
    if lastEnemyIndex == 0 then
      break
    end
    local combinedBattleID = GameObjectBattleMap.m_battleStatus[lastEnemyIndex].battle_id
    areaID, battleID = GameUtils:ResolveBattleID(combinedBattleID)
    local battleInfo = GameDataAccessHelper:GetBattleInfo(areaID, battleID)
    local event_type = battleInfo.EVENT_TYPE
  until event_type == 1
  if lastEnemyIndex > 0 then
    return areaID, battleID, lastEnemyIndex
  else
    return nil
  end
end
function GameUIEnemyInfo:ShowEnemyInfo(actID, battleID, enemyIndex)
  DebugOut("ShowEnemyInfo " .. actID .. " " .. battleID .. " " .. enemyIndex)
  if QuestTutorialRush:IsActive() and GameUIEnemyInfo.isShowRushDialogFlag then
    GameUIEnemyInfo.isShowRushDialogFlag = false
    DebugOut("QuestTutorialRush:")
    QuestTutorialRush:SetFinish(true)
    TutorialQuestManager:Save()
  end
  self:LoadFlashObject()
  self.m_enemyIndex = enemyIndex
  self.m_currentBattleID = battleID
  self.m_combinedBattleID = GameUtils:CombineBattleID(actID, self.m_currentBattleID)
  local battle_status_req = {
    battle_id = self.m_combinedBattleID
  }
  NetMessageMgr:SendMsg(NetAPIList.battle_status_req.Code, battle_status_req, self.GetBattleStatusCallback, true, nil)
  local battleInfo = GameObjectBattleMap:GetBattleInfo(GameStateBattleMap.m_currentAreaID, self.m_currentBattleID)
  if battleInfo.status == -1 then
    self.m_renderFx:SetBtnEnable("_root.mc_1.button.btn_battlenow", false)
  else
    self.m_renderFx:SetBtnEnable("_root.mc_1.button.btn_battlenow", true)
  end
  self.isElite = false
  local is170 = immanentversion170 == 4 or immanentversion170 == 5
  self:GetFlashObject():InvokeASCallback("_root", "InitUI", is170, false, self.mapType)
  local openLevel = 16
  local openRushNotice = string.gsub(GameLoader:GetGameText("LC_MENU_RUSH_OPEN_LEVEL_TIP"), "<Level_num>", openLevel)
  local enable_rush = GameGlobalData:GetModuleStatus("rush")
  self:GetFlashObject():InvokeASCallback("_root", "enableBtnRush", enable_rush, openRushNotice)
  if immanentversion == 1 then
    if QuestTutorialBattleMap:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "showTutorialAnimTip")
    else
      self:GetFlashObject():InvokeASCallback("_root", "hideTutorialAnimTip")
    end
    if not QuestTutorialSkill:IsFinished() and not QuestTutorialSkill:IsActive() and self.m_combinedBattleID == 1002012 then
      QuestTutorialSkill:SetActive(true)
    end
  elseif immanentversion == 2 then
    self:GetFlashObject():InvokeASCallback("_root", "enableBtnRush", enable_rush, openRushNotice)
    self:GetFlashObject():InvokeASCallback("_root", "hideTutorialAnimTip")
  end
end
function GameUIEnemyInfo:ShowEliteEnemyInfo(battleId, status)
  self:LoadFlashObject()
  self.m_combinedBattleID = battleId
  DebugOut("hajksdhasdj:", battleId)
  local battle_status_req = {
    battle_id = self.m_combinedBattleID
  }
  NetMessageMgr:SendMsg(NetAPIList.battle_status_req.Code, battle_status_req, self.GetBattleStatusCallback, true, nil)
  if status == 4 then
    self.m_renderFx:SetBtnEnable("_root.mc_1.button.btn_battlenow", false)
  else
    self.m_renderFx:SetBtnEnable("_root.mc_1.button.btn_battlenow", true)
  end
  self.isElite = true
  local is170 = immanentversion170 == 4 or immanentversion170 == 5
  self:GetFlashObject():InvokeASCallback("_root", "InitUI", is170, true, self.mapType)
  local openLevel = 16
  local openRushNotice = string.gsub(GameLoader:GetGameText("LC_MENU_RUSH_OPEN_LEVEL_TIP"), "<Level_num>", openLevel)
  local enable_rush = GameGlobalData:GetModuleStatus("rush")
  self:GetFlashObject():InvokeASCallback("_root", "enableBtnRush", enable_rush, openRushNotice)
  self:GetFlashObject():InvokeASCallback("_root", "hideTutorialAnimTip")
end
function GameUIEnemyInfo:OnEraseFromGameState()
  self:UnloadFlashObject()
  self.isOnBattleHistory = nil
  self.mapType = "normal"
  GameUIEnemyInfo.canFinishRushTutorial = nil
end
function GameUIEnemyInfo:OnAddToGameState(state)
  self:LoadFlashObject()
  if self.m_renderFx ~= nil then
    self.m_renderFx:InvokeASCallback("_root", "beginEnemyInfoShow")
  end
  if GameUIEnemyInfo.canFinishRushTutorial and immanentversion == 2 and QuestTutorialBattleRush:IsActive() then
    QuestTutorialBattleRush:SetFinish(true)
  end
end
function GameUIEnemyInfo:UpdateBattleHistoryList()
  local flash_obj = self:GetFlashObject()
  local user_name
  user_name = self._firstHistory.username
  user_name = user_name and GameUtils:GetUserDisplayName(self._firstHistory.username)
  flash_obj:InvokeASCallback("_root", "lua2fs_updateHistoryItemData", 1, self._firstHistory.report_id, user_name, self._firstHistory.level, GameLoader:GetGameText("LC_MENU_Level"))
  user_name = self._lowestHistory.username
  user_name = user_name and GameUtils:GetUserDisplayName(self._lowestHistory.username)
  flash_obj:InvokeASCallback("_root", "lua2fs_updateHistoryItemData", 2, self._lowestHistory.report_id, user_name, self._lowestHistory.level, GameLoader:GetGameText("LC_MENU_Level"))
  for i = 1, 3 do
    local report_id, user_name, user_level
    local battle_history = self._recentHistories[i]
    if battle_history then
      report_id = battle_history.report_id
      user_name = GameUtils:GetUserDisplayName(battle_history.username)
      user_level = battle_history.level
    end
    flash_obj:InvokeASCallback("_root", "lua2fs_updateHistoryItemData", i + 2, report_id, user_name, user_level, GameLoader:GetGameText("LC_MENU_Level"))
  end
end
function GameUIEnemyInfo:SetHistoryItem(item_key, history_data)
  local report_id = -1
  local player_name = -1
  local player_level = -1
  local time_ago = -1
  if history_data then
    report_id = history_data.report_id
    player_name = GameUtils:GetUserDisplayName(history_data.username)
    player_level = history_data.level
    time_ago = GameUtils:formatTimeString(history_data.left_time)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setHistoryItem", item_key, report_id, player_name, player_level, time_ago, GameLoader:GetGameText("LC_MENU_Level"))
end
function GameUIEnemyInfo:UpdateBattleRewardsList()
  if rewradsList ~= nil and #rewradsList > 0 then
    for i = 1, 5 do
      local text = -1
      local icon = -1
      local v = rewradsList[i]
      local itemType = ""
      if v then
        itemType = v.base.item_type
        text, icon = GameHelper:GetAwardTypeTextAndIcon(v.base.item_type, v.base.number)
      else
        text = GameLoader:GetGameText("LC_MENU_EMPTY")
        icon = -1
      end
      self:GetFlashObject():InvokeASCallback("_root", "SetReward", tostring(icon), tostring(text), i, itemType)
    end
  end
end
function GameUIEnemyInfo:UpdatePassCondition(conditionStr)
  DebugOut("UpdatePassCondition", conditionStr)
  local first, second, third
  local config_round_String = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COMPLETION_ROUND")
  local config_hp_String = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COMPLETION_LOST_HP")
  local config_fleet_String = GameLoader:GetGameText("LC_MENU_DUNGEON_SPACE_COMPLETION_LOST_FLEET")
  local roundNum = conditionStr:match("{round,(%d+)}")
  if roundNum then
    DebugOut(roundNum)
    roundNum = tonumber(roundNum)
    if type(roundNum) == "number" then
      first = string.format(config_round_String, roundNum)
    end
  end
  local fleetNum = conditionStr:match("{lost_fleet,(%d+)}")
  if fleetNum then
    DebugOut(fleetNum)
    fleetNum = tonumber(fleetNum)
    if type(fleetNum) == "number" then
      second = string.format(config_fleet_String, fleetNum)
    end
  end
  local hpNum = conditionStr:match("{lost_hp,(%d+)}")
  if hpNum then
    DebugOut(hpNum)
    hpNum = hpNum .. "%"
    third = string.format(config_hp_String, hpNum)
  end
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_updateRankCondition", first, second, third)
end
function GameUIEnemyInfo:SortAwardList(awardDataTable)
  local itemPriority = {
    credit = 1,
    prestige = 2,
    money = 3,
    exp = 4,
    item = 5
  }
  DebugOut("GameUIEnemyInfo:SortAwardList")
  DebugTable(awardDataTable)
  local function compFun(v1, v2)
    local prioV1 = itemPriority[v1.base.item_type]
    local prioV2 = itemPriority[v2.base.item_type]
    if prioV1 ~= nil and prioV2 ~= nil then
      return prioV1 < prioV2
    elseif prioV2 == nil then
      return true
    else
      return false
    end
  end
  table.sort(awardDataTable, compFun)
  return awardDataTable
end
function GameUIEnemyInfo:SetAccomplisthLevel(level)
  m_accomplish_level = level
end
function GameUIEnemyInfo:UpdateBattleStatus(content)
  self._firstHistory = nil
  self._lowestHistory = nil
  if content.first[1] then
    self._firstHistory = content.first[1]
  end
  if content.lowest[1] then
    self._lowestHistory = content.lowest[1]
  end
  self._recentHistories = content.news
  local sortedAwardDataTable = self:SortAwardList(content.rewards)
  rewradsList = sortedAwardDataTable
  m_accomplish_level = content.accomplish_level
  if immanentversion == 2 then
    if tonumber(GameStateBattleMap.m_currentAreaID) == 60 then
      self:GetFlashObject():InvokeASCallback("_root", "enableBtnBattle", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "enableBtnBattle", true)
    end
  end
  local enemyInfo = {}
  local enemyName = ""
  local enemyDesc = ""
  if self.isElite then
    local name, avatar, level = GameObjectAdventure:GetCurNameFleetHead()
    enemyInfo._avatar = avatar
    enemyInfo._level = level
    enemyName = name
  else
    enemyInfo = GameDataAccessHelper:GetBattleMonsterInfo(GameStateBattleMap.m_currentAreaID, self.m_currentBattleID)
    enemyName = GameDataAccessHelper:GetAreaNameText(GameStateBattleMap.m_currentAreaID, self.m_currentBattleID)
    enemyDesc = GameDataAccessHelper:GetAreaDesText(GameStateBattleMap.m_currentAreaID, self.m_currentBattleID)
  end
  self.isCanBattle = true
  local need_level = ""
  local userinfo = GameGlobalData:GetData("levelinfo")
  if content.user_level > userinfo.level then
    DebugOut("------------2------------")
    self.isCanBattle = false
    need_level = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), content.user_level)
  end
  self.m_renderFx:InvokeASCallback("_root", "setEnemyInfo", enemyName, enemyInfo._avatar, enemyInfo._level, enemyDesc, self.isCanBattle, need_level)
  self.m_renderFx:InvokeASCallback("_root", "showStarLevel", m_accomplish_level, m_accomplish_level > 0)
  if not self.isCanBattle and not QuestTutorialRush:IsFinished() then
    local left_times, total_times = 0, 0
    local battle_supply = GameGlobalData:GetData("battle_supply")
    if battle_supply then
      left_times = battle_supply.current
      thenotal_times = battle_supply.max
    end
    local areaID, battleID, lastEnemyIndex = self:GetLastEnemy()
    local enable_rush = GameGlobalData:GetModuleStatus("rush")
    if enable_rush and areaID ~= nil and left_times > 0 then
      DebugOut("QuestTutorialRush: the first rush tutorial")
      QuestTutorialRush:SetActive(true)
      self:GetFlashObject():InvokeASCallback("_root", "showTutorialAnimTip")
    end
  end
  if immanentversion == 2 then
    local enable_rush = GameGlobalData:GetModuleStatus("rush")
    if enable_rush and not QuestTutorialBattleRush:IsFinished() then
      QuestTutorialBattleRush:SetActive(true)
    end
    DebugOut("fuck QuestTutorialBattleRush", QuestTutorialBattleRush:IsActive())
    if m_accomplish_level > 0 and QuestTutorialBattleRush:IsActive() then
      GameUIEnemyInfo:GetFlashObject():InvokeASCallback("_root", "showRushAnim")
    else
      GameUIEnemyInfo:GetFlashObject():InvokeASCallback("_root", "hideRushAnim")
    end
  end
  if self.isElite then
    self.priceElite = content.price
    self.consumeElite = content.consume_num
    self.leftEliteCount = content.left_count
    local consume = content.consume_num
    local leftCount = content.left_count
    if leftCount <= 0 then
      leftCount = "<font color='#FE0000'>" .. leftCount .. "</font>"
    end
    local consumeStr = GameLoader:GetGameText("LC_MENU_ELITE_SUPPLY_CONSUME") .. consume
    local leftCountStr = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. leftCount
    local tipStr = consumeStr .. "     " .. leftCountStr
    self:GetFlashObject():InvokeASCallback("_root", "SetEliteTip", tipStr)
  end
  GameUIEnemyInfo:UpdateBattleRewardsList()
  if not self.isElite then
    local battleInfo = GameDataAccessHelper:GetBattleRank(GameStateBattleMap.m_currentAreaID, self.m_currentBattleID)
    GameUIEnemyInfo:UpdatePassCondition(battleInfo)
  end
end
function GameUIEnemyInfo.userBattleResultCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pve_battle_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.battle_result_ack.Code then
    DebugOut("GameUIEnemyInfo.userBattleResultCallback")
    GameGlobalData:UpdateFleetInfoAfterBattle(content.fleets)
    GameGlobalData:RefreshData("fleetinfo")
    do
      local isFinishBattle = false
      if 0 < content.result.accomplish_level then
        if m_accomplish_level == 0 then
          isFinishBattle = true
        end
        if content.result.accomplish_level > m_accomplish_level then
          m_accomplish_level = content.result.accomplish_level
        end
      end
      GameUIBattleResult:LoadFlashObject()
      GameUIBattleResult.m_combinedBattleID = GameUIEnemyInfo.m_combinedBattleID
      GameUIBattleResult.m_battleID = GameUIEnemyInfo.m_currentBattleID
      if immanentversion == 2 and content.result.accomplish_level < 1 and GameUIBattleResult.m_combinedBattleID == 60001009 then
        QuestTutorialBattleFailed:SetActive(true)
      end
      local function callback()
        local window_type = ""
        GameUIBattleResult:SetFightReport(content.report, GameStateBattleMap.m_currentAreaID, GameUIEnemyInfo.m_currentBattleID)
        if content.result.accomplish_level > 0 then
          GameUIBattleResult:UpdateExperiencePercent()
          GameUIBattleResult:UpdateAccomplishLevel(content.result.accomplish_level)
          local awardDataTable = {}
          for _, award_data in ipairs(content.result.rewards) do
            local itemName, icon = GameHelper:GetAwardTypeTextAndIcon(award_data.base.item_type, award_data.base.number)
            if award_data.base.item_type ~= "exp" then
              table.insert(awardDataTable, {
                itemType = award_data.base.item_type,
                itemDesc = itemName,
                itemNumber = award_data.base.number
              })
            end
          end
          GameUIBattleResult:UpdateAwardList("battle_win", awardDataTable)
          window_type = "battle_win"
        else
          window_type = "battle_lose"
        end
        GameStateBattleMap:ReEnter(function()
          DebugOut("  \230\137\147\229\141\176 ---> QuestTutorialBattleFailed:IsActive()", QuestTutorialBattleFailed:IsActive())
          if QuestTutorialBattleFailed:IsActive() and immanentversion == 2 then
            DebugOut("go 1")
            local GameStateFormation = GameStateManager.GameStateFormation
            local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
            GameStateFormation:SetBattleID(GameStateBattleMap.m_currentAreaID, GameUIEnemyInfo.m_currentBattleID)
            GameStateManager:SetCurrentGameState(GameStateFormation)
          else
            DebugOut("go 2")
            GameUIBattleResult:AnimationMoveIn(window_type, isFinishBattle)
            GameStateBattleMap:AddObject(GameUIBattleResult)
          end
        end)
      end
      DebugOut("\230\137\147\229\141\176_2", GameUIEnemyInfo.m_combinedBattleID, QuestTutorialSkill:IsActive(), QuestTutorialSkill:IsFinished())
      if isFinishBattle then
        local battleInfo = GameDataAccessHelper:GetBattleInfo(GameStateBattleMap.m_currentAreaID, GameUIEnemyInfo.m_currentBattleID)
        DebugOut("\230\137\147\229\141\176_2 ", GameUIEnemyInfo.m_combinedBattleID)
        if GameUIEnemyInfo.m_combinedBattleID == 1004009 then
          if not QuestTutorialBuildTechLab:IsFinished() and immanentversion == 1 then
            if immanentversion170 == nil then
              QuestTutorialBuildTechLab:SetActive(true)
            end
            local techlab = GameGlobalData:GetBuildingInfo("tech_lab")
            if 0 < techlab.level then
              QuestTutorialBuildTechLab:SetFinish(true)
              if not QuestTutorialUseTechLab:IsFinished() then
                QuestTutorialUseTechLab:SetActive(true)
              end
            end
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1003012 then
          if immanentversion == 1 then
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1002005 then
          if not QuestTutorialSlot:IsActive() and not QuestTutorialSlot:IsFinished() then
            QuestTutorialSlot:SetActive(true)
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1004004 then
          if immanentversion == 1 and not QuestTutorialBuildAffairs:IsActive() and not QuestTutorialBuildAffairs:IsFinished() then
            QuestTutorialBuildAffairs:SetActive(true)
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1002011 then
          if immanentversion == 2 and not QuestTutorialArena:IsActive() and not QuestTutorialArena:IsFinished() then
            QuestTutorialArena:SetActive(true)
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1003011 then
          if immanentversion == 2 and not QuestTutorialMine:IsActive() and not QuestTutorialMine:IsFinished() then
            QuestTutorialMine:SetActive(true)
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1005004 then
          if immanentversion == 1 and not QuestTutorialMine:IsActive() and not QuestTutorialMine:IsFinished() then
            QuestTutorialMine:SetActive(true)
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1001011 then
          if QuestTutorialPveMapPoint:IsActive() then
            QuestTutorialPveMapPoint:SetFinish(true, false)
          end
        elseif GameUIEnemyInfo.m_combinedBattleID == 1007005 and not QuestTutorialBuildKrypton:IsActive() and not QuestTutorialBuildKrypton:IsFinished() then
          if immanentversion170 == nil then
            QuestTutorialBuildKrypton:SetActive(true)
          end
          local krypton = GameGlobalData:GetBuildingInfo("krypton_center")
          if 0 < krypton.level then
            QuestTutorialBuildKrypton:SetFinish(true)
            local fleetkryptons = GameGlobalData:GetData("fleetkryptons")
            local ishavekrypton = false
            for k, v in pairs(fleetkryptons) do
              if v.kryptons and 0 < #v.kryptons then
                ishavekrypton = true
              end
            end
            if ishavekrypton then
              QuestTutorialUseKrypton:SetFinish(true)
            end
          end
        end
      end
      GameStateBattlePlay:RegisterOverCallback(callback, nil)
      GameStateBattlePlay.curBattleType = "UIEnemyInfo"
      GameStateManager.GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.report, GameStateBattleMap.m_currentAreaID, GameUIEnemyInfo.m_currentBattleID)
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateBattlePlay)
      if DebugConfig.isDebugTip then
        GameTip:Show("tip 1")
        NetMessageMgr.CommonNtfHandler({code = 110912})
        GameTip:Show("tip 2")
        GameTip:Show("tip 3")
      end
      return true
    end
  elseif msgType == NetAPIList.battle_fight_report_ack.Code then
    if content.code ~= 0 then
      return true
    end
    local fight_report = content.histories[1].report
    GameStateBattlePlay.curBattleType = "UIEnemyInfo"
    GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, fight_report, GameStateBattleMap.m_currentAreaID, GameUIEnemyInfo.m_currentBattleID)
    GameStateBattlePlay:RegisterOverCallback(function()
      GameStateBattleMap:ReEnter(function()
        GameStateBattleMap:AddObject(GameUIEnemyInfo)
      end)
    end, nil)
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    return true
  end
  return false
end
function GameUIEnemyInfo:BattleNowClicked()
  local battle_request = {
    battle_id = self.m_combinedBattleID,
    action = 0
  }
  local progress = GameGlobalData:GetData("progress")
  DebugOut("GameUIEnemyInfo:BattleNowClicked", self.m_combinedBattleID)
  NetMessageMgr:SendMsg(NetAPIList.pve_battle_req.Code, battle_request, self.userBattleResultCallback, true)
end
function GameUIEnemyInfo:MoveInBattleHistory()
  if self.m_renderFx ~= nil then
    GameUIEnemyInfo.isOnBattleHistory = true
    self.m_renderFx:InvokeASCallback("_root", "showHistoryBox")
  end
end
function GameUIEnemyInfo:MoveOutBattleHistory()
  if self.m_renderFx ~= nil then
    self.m_renderFx:InvokeASCallback("_root", "moveOutBattleHistory")
  end
end
function GameUIEnemyInfo:CloseBattleHistory()
  if self.m_renderFx ~= nil then
    self.m_renderFx:InvokeASCallback("_root", "closeBattleHistory")
  end
end
function GameUIEnemyInfo:MoveOutEnemyInfo()
  if self.m_renderFx ~= nil then
    self.m_renderFx:InvokeASCallback("_root", "endEnemyInfoShow")
  end
end
function GameUIEnemyInfo:PlayHistoryBattleReport(report_id)
  local packet = {battle_report_id = report_id}
  GameStateBattlePlay.report_id = report_id
  NetMessageMgr:SendMsg(NetAPIList.battle_fight_report_req.Code, packet, self.userBattleResultCallback, true, nil)
end
function GameUIEnemyInfo.ReqEliteCountCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.adventure_add_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    else
    end
    return true
  elseif msgType == NetAPIList.adventure_add_ack.Code then
    GameUIEnemyInfo.priceElite = content.next_price
    GameUIEnemyInfo.leftEliteCount = content.left_count
    local consume = GameUIEnemyInfo.consumeElite
    local leftCount = content.left_count
    if leftCount <= 0 then
      leftCount = "<font color=#FE0000>" .. leftCount .. "</font>"
    end
    local consumeStr = GameLoader:GetGameText("LC_MENU_ELITE_SUPPLY_CONSUME") .. consume
    local leftCountStr = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. leftCount
    local tipStr = consumeStr .. "     " .. leftCountStr
    GameUIEnemyInfo:GetFlashObject():InvokeASCallback("_root", "SetEliteTip", tipStr)
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectAdventure) then
      GameObjectAdventure:InitAdventureData()
    end
    return true
  end
  return false
end
function GameUIEnemyInfo:ReqEliteCount()
  local param = {}
  param.battle_id = self.m_combinedBattleID
  NetMessageMgr:SendMsg(NetAPIList.adventure_add_req.Code, param, self.ReqEliteCountCallBack, true, nil)
end
function GameUIEnemyInfo:OnFSCommand(cmd, arg)
  if cmd == "MoveIn_Battle_History" then
    if not GameGlobalData:FTPVersion() then
      GameUIEnemyInfo:MoveInBattleHistory()
    end
  elseif cmd == "Enemy_Info_Rush" then
    if not GameGlobalData:FTPVersion() then
      if self.isElite then
        if self.leftEliteCount > 0 then
          GameStateManager:GetCurrentGameState():EraseObject(GameUIEnemyInfo)
          GameRush:ShowRush(true, self.leftEliteCount, self.consumeElite)
        else
          local cost = self.priceElite
          local info = GameLoader:GetGameText("LC_MENU_ELITE_CHALLENGE_BUY_TICKETS")
          info = string.gsub(info, "<number1>", tostring(cost))
          info = string.gsub(info, "<number2>", 3)
          GameUtils:CreditCostConfirm(info, function()
            GameUIEnemyInfo:ReqEliteCount()
          end)
          return
        end
      else
        GameStateManager:GetCurrentGameState():EraseObject(GameUIEnemyInfo)
        GameRush:ShowRush()
      end
      DebugOut("Enemy_Info_Rush")
      self:GetFlashObject():InvokeASCallback("_root", "hideRushAnim")
    end
  elseif cmd == "Add_Elite_Count" then
    local cost = self.priceElite
    local info = GameLoader:GetGameText("LC_MENU_ELITE_CHALLENGE_BUY_TICKETS")
    info = string.gsub(info, "<number1>", tostring(cost))
    info = string.gsub(info, "<number2>", 3)
    GameUtils:CreditCostConfirm(info, function()
      GameUIEnemyInfo:ReqEliteCount()
    end)
  elseif cmd == "history_box_move_out" then
    self.isOnBattleHistory = nil
  elseif cmd == "MoveOut_Battle_History" then
    GameUIEnemyInfo:MoveOutBattleHistory()
  elseif cmd == "Close_Battle_History" then
    GameUIEnemyInfo:CloseBattleHistory()
  elseif cmd == "Enemy_Info_BattleNow" then
    if self.isElite then
      if self.leftEliteCount > 0 then
        GameStateManager:GetCurrentGameState():EraseObject(GameUIEnemyInfo)
        local battle_id = GameObjectAdventure.currentBattleIdTable[GameObjectAdventure.boss_index]
        local GameStateFormation = GameStateManager.GameStateFormation
        GameStateFormation:SetBattleID(51, battle_id - 51000000)
        GameObjectAdventure.m_currentBattleID = battle_id - 51000000
        GameStateManager:SetCurrentGameState(GameStateFormation)
      else
        local cost = self.priceElite
        local info = GameLoader:GetGameText("LC_MENU_ELITE_CHALLENGE_BUY_TICKETS")
        info = string.gsub(info, "<number1>", tostring(cost))
        info = string.gsub(info, "<number2>", 3)
        GameUtils:CreditCostConfirm(info, function()
          GameUIEnemyInfo:ReqEliteCount()
        end)
      end
    elseif GameUIEnemyInfo:CheckIsCanBattle() then
      DebugOut("LOG_1,", GameStateBattleMap.m_currentAreaID, self.m_currentBattleID)
      if tonumber(GameStateBattleMap.m_currentAreaID) == 60 and immanentversion == 2 and (tonumber(self.m_currentBattleID) == 1002 or tonumber(self.m_currentBattleID) == 1007 or tonumber(self.m_currentBattleID) == 1013) then
        GameUIEnemyInfo:BattleNowClicked()
      else
        GameStateManager:GetCurrentGameState():EraseObject(GameUIEnemyInfo)
        local GameStateFormation = GameStateManager.GameStateFormation
        GameStateFormation:SetBattleID(GameStateBattleMap.m_currentAreaID, self.m_currentBattleID)
        GameStateManager:SetCurrentGameState(GameStateFormation)
        if QuestTutorialBattleMap:IsActive() then
          QuestTutorialBattleMap:SetFinish(true)
          if not QuestTutorialMainTask:IsFinished() and not QuestTutorialMainTask:IsActive() then
            QuestTutorialMainTask:SetActive(true)
          end
          AddFlurryEvent("ClickBattleInEnemyInfoUI", {}, 1)
        end
      end
    end
  elseif cmd == "Close_Enemy_Info" then
    GameUIEnemyInfo:MoveOutEnemyInfo()
  elseif cmd == "Remove_Enemy_Info" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUIEnemyInfo)
  elseif cmd == "select_report" then
    GameUIEnemyInfo:PlayHistoryBattleReport(arg)
  elseif cmd == "update_history_item" then
    if arg == "first" then
      self:SetHistoryItem("first", self._firstHistory)
    elseif arg == "lowlevel" then
      self:SetHistoryItem("lowlevel", self._lowestHistory)
    else
      local index = tonumber(arg)
      self:SetHistoryItem(index, self._recentHistories[index])
    end
  elseif cmd == "ShowItemDetail" then
    local item = rewradsList[tonumber(arg)].base
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
end
function GameUIEnemyInfo:CheckIsCanBattle()
  DebugOut("GameUIEnemyInfo:CheckIsCanBattle__", GameUIEnemyInfo.m_combinedBattleID)
  if GameUIEnemyInfo.m_combinedBattleID == 60001003 and immanentversion == 2 and not QuestTutorialSkill:IsActive() and not QuestTutorialSkill:IsFinished() then
    QuestTutorialSkill:SetActive(true)
  end
  local left_times, total_times = 0, 0
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if battle_supply then
    left_times = battle_supply.current
    thenotal_times = battle_supply.max
  else
    return false
  end
  if self.isCanBattle and left_times > 0 then
    return true
  elseif left_times <= 0 then
    if Facebook:IsFacebookEnabled() then
      if not Facebook:IsBindFacebook() then
        if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_SUPPLY] == 1 then
          FacebookPopUI.mManullyCloseCallback = GameUIEnemyInfo.netCallSupplyInfo
          FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
          FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_PVESUPPLY)
          if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
            local countryStr = "unknow"
          end
        else
          GameUIEnemyInfo.netCallSupplyInfo()
        end
      else
        GameUIEnemyInfo.netCallSupplyInfo()
      end
    else
      GameUIEnemyInfo.netCallSupplyInfo()
    end
  else
    local areaID, battleID, lastEnemyIndex = self:GetLastEnemy()
    local enable_rush = GameGlobalData:GetModuleStatus("rush")
    if not enable_rush then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_TECHUP_REQUIRE_PLAYER_LEVEL"))
    elseif areaID == nil then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_GO_RUSH_ALERT"))
    else
      if QuestTutorialRush:IsActive() then
        DebugOut("QuestTutorialRush:SetActive")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.RushTutorial)
      else
        DebugOut("QuestTutorialRush:SetActive -- else")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
      end
      GameUIEnemyInfo.isShowRushDialogFlag = true
      local title = ""
      local info = GameLoader:GetGameText("LC_MENU_GO_RUSH_ALERT")
      GameUIMessageDialog:SetLeftTextButton(GameLoader:GetGameText("LC_MENU_RUSH_BUTTON"), GameUIEnemyInfo.ShowEnemyInfo, {
        GameUIEnemyInfo,
        areaID,
        battleID,
        lastEnemyIndex
      })
      GameUIMessageDialog:SetRightTextButton(GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CONFIRM"), GameUIEnemyInfo.RushDialogBackToUIEnemyInfo, nil)
      GameUIMessageDialog:Display(title, info)
    end
  end
  return false
end
function GameUIEnemyInfo:RushDialogBackToUIEnemyInfo()
  GameUIEnemyInfo.isShowRushDialogFlag = false
end
function GameUIEnemyInfo:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "onUpdateFrame", dt)
    self:GetFlashObject():Update(dt)
  end
end
function GameUIEnemyInfo:InitFlashObject()
end
function GameUIEnemyInfo.netCallSupplyInfo()
  local packet = {
    type = "battle_supply"
  }
  NetMessageMgr:SendMsg(NetAPIList.supply_info_req.Code, packet, GameUIEnemyInfo.NetCallbackSupplyInfo, true, nil)
end
function GameUIEnemyInfo.NetCallbackSupplyInfo(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
    end
    return true
  end
  if msgType == NetAPIList.supply_info_ack.Code then
    GameUISupplyDialog:Show()
    return true
  end
  return false
end
function GameUIEnemyInfo.NetCallbackBuySupply(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_exchange_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    else
    end
    return true
  end
  if msgType == NetAPIList.supply_info_ack.Code then
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIEnemyInfo.OnAndroidBack()
    if GameUIEnemyInfo.isOnBattleHistory then
      GameUIEnemyInfo.isOnBattleHistory = nil
      GameUIEnemyInfo:GetFlashObject():InvokeASCallback("_root", "historyBoxMoveOut")
    else
      GameUIEnemyInfo:MoveOutEnemyInfo()
    end
  end
end
