local fakeBattleResult0 = {
  player1 = "vddfv. vbb.s991",
  player1_identity = 102,
  player1_pos = 8,
  player2_identity = 6010001,
  player2_pos = 8,
  player2_avatar = "head24",
  player2 = "battle",
  player1_avatar = "head17",
  result = 1,
  rounds = {
    [1] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 1,
          shield = 0,
          atk_acc_change = 25,
          acc = 100,
          intercept = false,
          atk_acc = 100,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 241,
          durability = 559,
          atk_pos = 1,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 1,
      round_cnt = 1
    },
    [2] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 1,
          sp_id = 417,
          shield = 0,
          buff_effect = 0,
          acc = 125,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 817,
          round_cnt = 0,
          atk_pos = 1,
          crit = false
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 1,
      round_cnt = 2
    },
    [3] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 3,
          shield = 0,
          atk_acc_change = 25,
          acc = 100,
          intercept = false,
          atk_acc = 100,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 239,
          durability = 561,
          atk_pos = 3,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 3,
      round_cnt = 3
    },
    [4] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 393,
          def_pos = 5,
          sp_id = 418,
          shield = 0,
          buff_effect = 0,
          acc = 150,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 607,
          round_cnt = 0,
          atk_pos = 2,
          crit = false
        },
        [2] = {
          durability = 0,
          def_pos = 4,
          sp_id = 418,
          shield = 0,
          buff_effect = 0,
          acc = 150,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 1037,
          round_cnt = 0,
          atk_pos = 2,
          crit = true
        },
        [3] = {
          durability = 0,
          def_pos = 6,
          sp_id = 418,
          shield = 0,
          buff_effect = 0,
          acc = 150,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 1028,
          round_cnt = 0,
          atk_pos = 2,
          crit = true
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 2,
      round_cnt = 4
    },
    [5] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 1363,
          def_pos = 2,
          sp_id = 211,
          shield = 0,
          buff_effect = 0,
          acc = 25,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 136,
          round_cnt = 0,
          atk_pos = 5,
          crit = true
        },
        [2] = {
          durability = 4948,
          def_pos = 8,
          sp_id = 211,
          shield = 0,
          buff_effect = 0,
          acc = 100,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 52,
          round_cnt = 0,
          atk_pos = 5,
          crit = false
        }
      },
      buff = {},
      player1_action = false,
      dot = {},
      pos = 5,
      round_cnt = 5
    },
    [6] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 3,
          sp_id = 417,
          shield = 0,
          buff_effect = 0,
          acc = 175,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 1683,
          round_cnt = 0,
          atk_pos = 3,
          crit = true
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 3,
      round_cnt = 6
    },
    [7] = {
      attacks = {
        [1] = {
          attack_type = 2,
          atk_effect = "laser",
          def_pos = 1,
          shield = 0,
          atk_acc_change = 25,
          acc = 200,
          intercept = false,
          atk_acc = 25,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = false,
          dur_damage = 0,
          durability = 559,
          atk_pos = 7,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 7,
      round_cnt = 7
    },
    [8] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 7,
          sp_id = 417,
          shield = 0,
          buff_effect = 0,
          acc = 50,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 863,
          round_cnt = 0,
          atk_pos = 4,
          crit = true
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 4,
      round_cnt = 8
    },
    [9] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 1363,
          def_pos = 2,
          sp_id = 219,
          shield = 0,
          buff_effect = 0,
          acc = 50,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 0,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = false,
          dur_damage = 0,
          round_cnt = 0,
          atk_pos = 8,
          crit = false
        }
      },
      buff = {},
      player1_action = false,
      dot = {},
      pos = 8,
      round_cnt = 9
    },
    [10] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 9,
          sp_id = 417,
          shield = 0,
          buff_effect = 0,
          acc = 50,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 863,
          round_cnt = 0,
          atk_pos = 6,
          crit = true
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 6,
      round_cnt = 10
    },
    [11] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 5,
          sp_id = 419,
          shield = 0,
          buff_effect = 0,
          acc = 150,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 6850,
          round_cnt = 0,
          atk_pos = 8,
          crit = false
        },
        [2] = {
          durability = 0,
          def_pos = 8,
          sp_id = 419,
          shield = 0,
          buff_effect = 0,
          acc = 150,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 9600,
          round_cnt = 0,
          atk_pos = 8,
          crit = true
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 8,
      round_cnt = 11
    }
  },
  player1_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 800,
      cur_accumulator = 75,
      max_durability = 800,
      identity = 105,
      pos = 1
    },
    [2] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 1500,
      cur_accumulator = 75,
      max_durability = 1500,
      identity = 108,
      pos = 2
    },
    [3] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 800,
      cur_accumulator = 75,
      max_durability = 800,
      identity = 105,
      pos = 3
    },
    [4] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 700,
      cur_accumulator = 75,
      max_durability = 700,
      identity = 109,
      pos = 4
    },
    [5] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 700,
      cur_accumulator = 75,
      max_durability = 700,
      identity = 109,
      pos = 6
    },
    [6] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 5000,
      cur_accumulator = 75,
      max_durability = 5000,
      identity = 102,
      pos = 8
    }
  },
  player2_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 2000,
      cur_accumulator = 50,
      max_durability = 2000,
      identity = 6010001,
      pos = 8
    },
    [2] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 1500,
      cur_accumulator = 50,
      max_durability = 1500,
      identity = 6010002,
      pos = 5
    },
    [3] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 600,
      cur_accumulator = 50,
      max_durability = 600,
      identity = 6010003,
      pos = 1
    },
    [4] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 600,
      cur_accumulator = 50,
      max_durability = 600,
      identity = 6010003,
      pos = 3
    },
    [5] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 700,
      cur_accumulator = 50,
      max_durability = 700,
      identity = 6010004,
      pos = 4
    },
    [6] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 700,
      cur_accumulator = 50,
      max_durability = 700,
      identity = 6010004,
      pos = 6
    },
    [7] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 700,
      cur_accumulator = 50,
      max_durability = 700,
      identity = 6010004,
      pos = 7
    },
    [8] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 700,
      cur_accumulator = 50,
      max_durability = 700,
      identity = 6010004,
      pos = 9
    }
  }
}
local fakeBattleAnim0 = {
  [1] = {
    round_index = 1,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {1100001},
    befor_dialog_anim = nil,
    hide_anim_in_dialog = true,
    after_dialog_anim = nil
  },
  [2] = {
    round_index = 2,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {11000011},
    befor_dialog_anim = nil,
    hide_anim_in_dialog = true,
    after_dialog_anim = nil
  },
  [3] = {
    round_index = 11,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {1100002},
    befor_dialog_anim = nil,
    hide_anim_in_dialog = true,
    after_dialog_anim = nil
  }
}
local fakeBattleCommand0 = {
  {
    "SetPlayer1Avatar",
    "LC_NPC_NPC_1",
    "head17"
  },
  {
    "SetPlayer2Avatar",
    "LC_NPC_NPC_47",
    "head24"
  }
}
GameData.fakeBattleAnim0 = fakeBattleAnim0
GameData.fakeBattleResult0 = fakeBattleResult0
GameData.fakeBattleCommand0 = fakeBattleCommand0
