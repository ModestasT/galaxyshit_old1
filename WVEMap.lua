WVEMap = luaClass(nil)
local GameStateWVE = GameStateManager.GameStateWVE
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
WVEMap.updateStep = 1
WVEMap.updateDT = 0
local updateMarchTimeFrame = 0
local updateReviveTimeFrame = 0
local UPDATE_TIME_FRAME = 15
local currentMarchLeftTime = 0
local currentReviveLeftTime = 0
local currentStarcraftLeftTime = 0
function WVEMap:ctor(wveGameManager)
  self.mPlayerList = {}
  self.mEnemyList = {}
  self.mPlayerMyself = nil
  self.mGameManger = wveGameManager
  self.updateStep = 1
  self.updateDT = 0
  self.mEnemyDotStayTime = 0
end
function WVEMap:ClearMap()
  self.mMapPoints = {}
  self.mMapPaths = {}
  self.MapPlayerList = {}
  self.MapBuffList = {}
end
function WVEMap:GenerateMapData(MapContent)
  if MapContent then
    self.mMapPoints = self.mMapPoints or {}
    for i, v in ipairs(MapContent.dots) do
      local mapPoint = WVEMapPoint.new(v.id, v.dot_type, v.ui_type, v.dot_coordinates, v.dot_name)
      self.mMapPoints[v.id] = mapPoint
    end
    self.mMapPaths = self.mMapPaths or {}
    for i, v in ipairs(MapContent.lines) do
      local pathPoint1 = self.mMapPoints[v.dot_to_dot[1]]:GetCoords()
      local pathPoint2 = self.mMapPoints[v.dot_to_dot[2]]:GetCoords()
      local mapPath = WVEMapPath.new(v.id, v.dot_to_dot[1], v.dot_to_dot[2], pathPoint1, pathPoint2, v.distance)
      self.mMapPaths[v.id] = mapPath
    end
    self:PrepareMapForDisplay()
  end
end
function WVEMap:PrepareMapForDisplay()
  DebugOut("display map   111!!")
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  DebugOut("display!!  22")
  flash_obj:InvokeASCallback("_root", "WVEMAP_preparePaths", self.mMapPaths)
  flash_obj:InvokeASCallback("_root", "WVEMAP_preparePoints", self.mMapPoints)
end
function WVEMap:parsePointOccupyFrame(pointID, pointOccupy)
  local playerMyself = self.mGameManger.mWVEPlayerManager:GetPlayerMyself()
  if pointID == playerMyself:GetPointID() and pointOccupy == EWVEPointOccupyStatus.OCCUPY_STATE_PLAYERS and self:GetPointTypeByID(pointID) ~= EWVEPointType.TYPE_PLAYER_BASE then
    return EWVEPointOccupyStatusFrame[EWVEPointOccupyStatus.OCCUPY_STATE_MYSELF]
  end
  return EWVEPointOccupyStatusFrame[pointOccupy]
end
function WVEMap:parsePointOccupyState(pointID, pointOccupy)
  local playerMyself = self.mGameManger.mWVEPlayerManager:GetPlayerMyself()
  if pointID == playerMyself:GetPointID() and pointOccupy == EWVEPointOccupyStatus.OCCUPY_STATE_PLAYERS then
    return EWVEPointOccupyStatus.OCCUPY_STATE_MYSELF
  end
  return pointOccupy
end
function WVEMap:IsEnemyPoint(pointID)
  if self.mMapPoints[pointID].mOccupyState == EWVEPointOccupyStatus.OCCUPY_STATE_ENEMY then
    return true
  end
  return false
end
function WVEMap:GetPointOccupyState(pointID)
  return self.mMapPoints[pointID].mOccupyState
end
function WVEMap:UpdateEnemyDotStayTime(time)
  self.mEnemyDotStayTime = time
end
function WVEMap:UpdateMapPointsStatus(pointContent)
  local needUpdatePoints = {}
  for i, v in ipairs(pointContent) do
    self.mMapPoints[v.id].mOccupyStateFrame = self:parsePointOccupyFrame(v.id, v.occupy)
    self.mMapPoints[v.id].mOccupyState = self:parsePointOccupyState(v.id, v.occupy)
    self.mMapPoints[v.id].mBattleState = v.status
    self.mMapPoints[v.id].mOccupyNum = v.occupy_no
    needUpdatePoints[v.id] = self.mMapPoints[v.id]
  end
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "WVEMAP_updatePointsStatus", needUpdatePoints)
end
function WVEMap:UpdateMapSinglePointOccupyState(pointContent)
  self.mMapPoints[pointContent.id].mOccupyState = self:parsePointOccupyState(pointContent.id, pointContent.value)
  self.mMapPoints[pointContent.id].mOccupyStateFrame = self:parsePointOccupyFrame(pointContent.id, pointContent.value)
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "WVEMAP_updateSinglePointOccupyState", self.mMapPoints[pointContent.id])
end
function WVEMap:UpdateMapSinglePointOccupyNumber(pointContent)
  self.mMapPoints[pointContent.id].mOccupyNum = pointContent.value
  DebugOut("self.mMapPoints[pointContent.id].mOccupyNum = ", self.mMapPoints[pointContent.id].mOccupyNum)
  if self.mMapPoints[pointContent.id].mOccupyState then
    local flash_obj = self.mGameManger:GetFlashObject()
    if not flash_obj then
      return
    end
    flash_obj:InvokeASCallback("_root", "WVEMAP_updateSinglePointOccupyState", self.mMapPoints[pointContent.id])
  end
end
function WVEMap:UpdateMapSinglePointBattleState(pointContent)
  self.mMapPoints[pointContent.id].mBattleState = pointContent.value
  DebugOut("self.mMapPoints[pointContent.id].mBattleState = ", self.mMapPoints[pointContent.id].mBattleState)
  local flash_obj = self.mGameManger:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "WVEMAP_updateSinglePointBattleState", self.mMapPoints[pointContent.id])
end
function WVEMap:PlayerMyselfLeavePoint(pointID)
  local mapPoint = self.mMapPoints[pointID]
  if mapPoint.mOccupyState == EWVEPointOccupyStatus.OCCUPY_STATE_MYSELF then
    mapPoint.mOccupyState = EWVEPointOccupyStatus.OCCUPY_STATE_PLAYERS
    mapPoint.mOccupyStateFrame = EWVEPointOccupyStatusFrame[mapPoint.mOccupyState]
    local flash_obj = self.mGameManger:GetFlashObject()
    if not flash_obj then
      return
    end
    flash_obj:InvokeASCallback("_root", "WVEMAP_updateSinglePointOccupyState", mapPoint)
  end
end
function WVEMap:PlayerMyselfEnterPoint(pointID)
  local mapPoint = self.mMapPoints[pointID]
  if mapPoint.mOccupyState == EWVEPointOccupyStatus.OCCUPY_STATE_PLAYERS or mapPoint.mOccupyState == EWVEPointOccupyStatus.OCCUPY_STATE_FREE then
    mapPoint.mOccupyStateFrame = self:parsePointOccupyFrame(pointID, EWVEPointOccupyStatus.OCCUPY_STATE_PLAYERS)
    mapPoint.mOccupyState = EWVEPointOccupyStatus.OCCUPY_STATE_MYSELF
    local flash_obj = self.mGameManger:GetFlashObject()
    if not flash_obj then
      return
    end
    flash_obj:InvokeASCallback("_root", "WVEMAP_updateSinglePointOccupyState", mapPoint)
  end
end
function WVEMap.TeleportNtfHandler(content)
  if content ~= nil and tonumber(content.user_id) ~= tonumber(MapPlayerMySelf.mID) then
    local flash_obj = GameUIStarCraftMap:GetFlashObject()
    GameUIStarCraftMap.MapPlayerList[content.user_id] = nil
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "killPlayer", content.user_id)
    end
  end
  return true
end
function WVEMap:UpdateMyMarchTime()
  local flash_obj = self:GetFlashObject()
  local tmpLeftTime = 0
  if 0 < MapPlayerMySelf.mLeftTime then
    tmpLeftTime = MapPlayerMySelf.mLeftTime - (os.time() - MapPlayerMySelf.mNotifyGetTime)
    if tmpLeftTime < 0 then
      tmpLeftTime = 0
    end
  else
    tmpLeftTime = 0
  end
  if currentMarchLeftTime == tmpLeftTime then
    return
  end
  currentMarchLeftTime = tmpLeftTime
  local timeStr = GameUtils:formatTimeString(tmpLeftTime)
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "updatePlayerState", true, false, timeStr)
    flash_obj:InvokeASCallback("_root", "updatePlayerFleetTime", timeStr)
  end
end
function WVEMap:Update(dt)
  local flash_obj = self.mGameManger:GetFlashObject()
  if flash_obj then
    if self.updateStep == 8 then
      flash_obj:InvokeASCallback("_root", "WVEMAP_updatePlayerPos", self.updateDT)
      self.updateStep = 0
      self.updateDT = 0
    else
      self.updateStep = self.updateStep + 1
      self.updateDT = self.updateDT + dt
    end
    flash_obj:Update(dt)
  end
end
function WVEMap:ForceOut()
  NetMessageMgr:RegisterReconnectHandler(nil)
  GameUIStarCraftMap:OnRemoveAvatarInfoChangeHandler()
  GameUIStarCraftMap:RemoveMessageHandler()
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
end
function WVEMap:getPathByStartEnd(startPoint, endPoint)
  for k, v in pairs(self.mMapPaths) do
    if v.mPointID_1 == startPoint and v.mPointID_2 == endPoint or v.mPointID_1 == endPoint and v.mPointID_2 == startPoint then
      return v
    end
  end
  return nil
end
function WVEMap:getPathByID(pathID)
  return self.mMapPaths[pathID]
end
function WVEMap:GetPointTypeByID(pointID)
  if self.mMapPoints[pointID] then
    return self.mMapPoints[pointID].mType
  end
  return nil
end
function WVEMap:GetPointNameByID(pointID)
  if self.mMapPoints[pointID] then
    return self.mMapPoints[pointID].mName
  end
  return nil
end
local MAX_INT = -10000
function WVEMap:CalculaeMarchPath(startPoint, endPoint)
  if not startPoint or not endPoint or startPoint <= 0 or endPoint <= 0 then
    return nil
  end
  local pathList = {}
  if startPoint == endPoint then
    return pathList
  end
  local sQueue = {}
  local uQueue = {}
  local pathNodeStart = {
    point = startPoint,
    parentNode = nil,
    distanceToStart = 0
  }
  sQueue[pathNodeStart.point] = pathNodeStart
  for k, v in pairs(self.mMapPoints) do
    if v.mID ~= startPoint then
      local pathNode = {
        point = v.mID,
        parentNode = pathNodeStart,
        distanceToStart = 0
      }
      if self:GetPointTypeByID(v.mID) == EWVEPointType.TYPE_ENEMY_BASE then
        pathNode.distanceToStart = MAX_INT
      else
        local path = self:getPathByStartEnd(startPoint, v.mID)
        if path then
          pathNode.distanceToStart = path.mPathLength
        else
          pathNode.distanceToStart = MAX_INT
        end
      end
      table.insert(uQueue, pathNode)
    end
  end
  local finding = true
  local curParentNode = pathNodeStart
  while finding do
    local minDistanceNode
    local index = -1
    for i, v in ipairs(uQueue) do
      if not minDistanceNode then
        minDistanceNode = v
        index = i
      elseif minDistanceNode.distanceToStart == MAX_INT and v.distanceToStart ~= MAX_INT then
        minDistanceNode = v
        index = i
      elseif minDistanceNode.distanceToStart ~= MAX_INT and v.distanceToStart ~= MAX_INT and minDistanceNode.distanceToStart > v.distanceToStart then
        minDistanceNode = v
        index = i
      end
    end
    if not minDistanceNode then
      finding = false
    else
      table.remove(uQueue, index)
      sQueue[minDistanceNode.point] = minDistanceNode
      for i, v in ipairs(uQueue) do
        if self:GetPointTypeByID(v.point) ~= EWVEPointType.TYPE_ENEMY_BASE then
          local tmpPath = self:getPathByStartEnd(minDistanceNode.point, v.point)
          if tmpPath and minDistanceNode.distanceToStart ~= MAX_INT and (tmpPath.mPathLength + minDistanceNode.distanceToStart < v.distanceToStart or v.distanceToStart == MAX_INT) then
            v.distanceToStart = tmpPath.mPathLength + minDistanceNode.distanceToStart
            v.parentNode = minDistanceNode
          end
        end
      end
    end
  end
  DebugOut("sQueue = ")
  DebugTable(sQueue)
  local parent = sQueue[endPoint].parentNode
  local current = sQueue[endPoint]
  if current.distanceToStart == MAX_INT then
    return pathList
  end
  while current.point ~= startPoint do
    DebugOut("current.point = ", current.point)
    DebugOut("parent.point = ", parent.point)
    table.insert(pathList, 1, current.point)
    current = parent
    if current.parentNode then
      parent = sQueue[parent.parentNode.point]
    end
  end
  DebugOut("we find a path form ", startPoint .. " to " .. endPoint)
  DebugTable(pathList)
  return pathList
end
function WVEMap:CalculateMarchTime(startPoint, endPoint, movedDistance, speed)
  local pointsList = self:CalculaeMarchPath(startPoint, endPoint)
  table.insert(pointsList, 1, startPoint)
  local totalLength = self:CalculateTotalLengthOfPoints(pointsList)
  if totalLength and totalLength > 0 and speed and speed ~= 0 then
    return (totalLength - movedDistance) / speed + (#pointsList - 2) * self.mEnemyDotStayTime
  end
  return 0
end
function WVEMap:CalculateTotalLengthOfPoints(pointsList)
  if pointsList then
    local pointsCount = #pointsList
    local index = pointsCount
    local totalLength = 0
    while index > 1 do
      local path = self:getPathByStartEnd(pointsList[index - 1], pointsList[index])
      totalLength = totalLength + path.mPathLength
      index = index - 1
    end
    DebugOut("totalLength = ", totalLength)
    return totalLength
  end
  return 0
end
function WVEMap:GetPlyaerBasePointID()
  for k, v in pairs(self.mMapPoints) do
    if v.mType == EWVEPointType.TYPE_PLAYER_BASE then
      return v.mID
    end
  end
  return nil
end
function WVEMap:IsMySelfOnPath()
  return MapPlayerMySelf.mIsOnPath
end
function WVEMap:IsMySelfDie()
  return MapPlayerMySelf.mCurrentHP <= 0
end
function WVEMap:GetMyPointID()
  return MapPlayerMySelf.mCurrentKeyPointID
end
function WVEMap:GetMySpeed()
  return MapPlayerMySelf.mSpeed
end
function WVEMap:GetKeyPointName(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    return GameUIStarCraftMap.MapKeyPointList[pointID].mPointName
  end
end
function WVEMap:GetMyStartPointID()
  if MapPlayerMySelf.mIsOnPath then
    return MapPlayerMySelf.mCurrentStartPointID
  else
    return MapPlayerMySelf.mCurrentKeyPointID
  end
end
function WVEMap:GetMyCurrentPathEndPoint()
  if GameUIStarCraftMap.MapPlayerList[MapPlayerMySelf.mID] then
    return GameUIStarCraftMap.MapPlayerList[MapPlayerMySelf.mID].mEndPoint
  end
  return nil
end
function WVEMap:GetServerDisplayNameByServerID(serverID)
  if serverID then
    return GameUIStarCraftMap.ServerName[serverID]
  end
  return nil
end
function WVEMap:GetServerDisplayNameByPointID(pointID)
  if GameUIStarCraftMap.MapKeyPointList[pointID] then
    serverID = GameUIStarCraftMap.MapKeyPointList[pointID].mServerID
    return GameUIStarCraftMap.ServerName[serverID]
  end
  return nil
end
function WVEMap:GetSelfServerName()
  return GameUIStarCraftMap.ServerName[MapPlayerMySelf.mServerID]
end
function WVEMap:showCostConfirmPanel(costPrice, strID, messageboxHandler)
  local info_content = GameLoader:GetGameText(strID)
  info_content = string.format(info_content, costPrice)
  GameUtils:CreditCostConfirm(info_content, messageboxHandler)
end
function WVEMap:KillMySelf()
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "killPlayer", MapPlayerMySelf.mID)
  end
end
function WVEMap:removeTeleportAnim()
  local flash_obj = GameUIStarCraftMap:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "animationTeleportStop")
  end
end
