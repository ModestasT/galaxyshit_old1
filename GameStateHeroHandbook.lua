local GameStateHeroHandbook = GameStateManager.GameStateHeroHandbook
local GameUIHeroHandbook = LuaObjectManager:GetLuaObject("GameUIHeroHandbook")
function GameStateHeroHandbook:OnFocusGain(previousState)
  DebugOut("GameStateHeroHandbook:OnFocusGain")
  if previousState ~= GameStateManager.GameStateBattlePlay then
    self.lastState = previousState
  end
  self:AddObject(GameUIHeroHandbook)
end
function GameStateHeroHandbook:OnFocusLost(newState)
  DebugOut("GameStateHeroHandbook:OnFocusLost")
  self:EraseObject(GameUIHeroHandbook)
end
function GameStateHeroHandbook:Exit()
  if nil == self.lastState then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  else
    GameStateManager:SetCurrentGameState(self.lastState)
  end
end
