local section = GameData.section.section
section[1] = {
  areaId = 1,
  sectionType = 1,
  sectionCount = 8,
  bossHead = {pos8 = "head7"}
}
section[2] = {
  areaId = 2,
  sectionType = 2,
  sectionCount = 8,
  bossHead = {pos8 = "head9"}
}
section[3] = {
  areaId = 3,
  sectionType = 1,
  sectionCount = 8,
  bossHead = {pos8 = "head36"}
}
section[4] = {
  areaId = 4,
  sectionType = 2,
  sectionCount = 8,
  bossHead = {pos8 = "head36"}
}
section[5] = {
  areaId = 5,
  sectionType = 1,
  sectionCount = 8,
  bossHead = {pos8 = "head47"}
}
section[6] = {
  areaId = 6,
  sectionType = 2,
  sectionCount = 8,
  bossHead = {pos8 = "head4"}
}
section[7] = {
  areaId = 7,
  sectionType = 1,
  sectionCount = 8,
  bossHead = {pos8 = "head38"}
}
section[8] = {
  areaId = 8,
  sectionType = 2,
  sectionCount = 8,
  bossHead = {pos8 = "head43"}
}
section[9] = {
  areaId = 9,
  sectionType = 1,
  sectionCount = 8,
  bossHead = {pos8 = "head43"}
}
section[10] = {
  areaId = 10,
  sectionType = 2,
  sectionCount = 8,
  bossHead = {pos8 = "head43"}
}
