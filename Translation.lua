ext.dofile("data1/Json4Lua.tfl")
local Translation = {
  abroadMode = true,
  free_addr = "",
  free_function = nil,
  free_enable = true,
  free_error = {},
  free_ok = 0,
  free_bytes = 0,
  pay_error = {},
  pay_ok = 0,
  pay_bytes = 0,
  pay_addr = "",
  pay_function = nil,
  pay_appid = "",
  pay_key = "",
  plat_token = "",
  stat_token = "",
  stat_url = "",
  udid = "",
  version = "1.3.16"
}
require("Emoji.tfl")
Translation.EmojiFilter = EmojiFilter or function(str, repl)
  return str
end
Translation.EmojiFilterAddSpace = EmojiFilterAddSpace or function(str, repl)
  return str
end
function GetTranslation()
  return Translation
end
function Translation:string_combine(tb, char)
  local str = ""
  for k, v in ipairs(tb) do
    str = str .. v
    if char and k ~= #tb then
      str = str .. char
    end
  end
  return str
end
function Translation:setConfig(appid, key, platformToken, platformUrl, statToken, statUrl, udid, abroadMode)
  Translation.pay_appid = appid
  Translation.pay_key = key
  print(platformToken)
  Translation.plat_token = platformToken
  if statUrl ~= nil and statUrl ~= "" then
    Translation.stat_url = statUrl .. "/record_sum?"
    Translation.stat_token = statToken
  end
  Translation.udid = udid
  Translation.abroadMode = abroadMode
  Translation.trans_url = platformUrl .. "?"
  Translation.access_token = platformToken
  function Translation.trans_function(sl, dl, text, cb)
    if Translation.abroadMode then
      Translation:abroadTrans(sl, dl, text, cb)
    else
      Translation:unAbroadTrans(sl, dl, text, cb)
    end
  end
end
function Translation:abroadTrans(sl, dl, text, cb)
  if self.abroadMode then
    dl = self:convertBaiduToGoogleCode(dl)
  end
  local params = {
    access_token = Translation.access_token,
    q = text,
    target = dl
  }
  local header = {
    ["User-Agent"] = "Tap4fun client"
  }
  DebugOut("Translation:abroadTrans:", Translation.trans_url .. self:combineUrlParamTable(params))
  ext.http.requestBasic({
    Translation.trans_url,
    param = self:combineUrlParamTable(params),
    mode = "notencrypt",
    header = header,
    method = "GET",
    callback = function(statusCode, content, err)
      if statusCode == 200 and content then
        DebugTable(content)
        if content.data and content.data.translations then
          local trans = ""
          for k, v in ipairs(content.data.translations) do
            trans = trans .. v.translatedText
          end
          cb(true, "", trans, dl)
        else
          cb(false, tostring(content.error_msg), "", "")
        end
      else
        cb(false, tostring(statusCode), "", "")
      end
    end
  })
end
function Translation:unAbroadTrans(sl, dl, text, cb)
  if not self.abroadMode then
    dl = self:convertGoogleCodeToBaidu(dl)
  end
  local params = {
    access_token = Translation.access_token,
    q = text,
    target = dl
  }
  local header = {
    ["User-Agent"] = "Tap4fun client"
  }
  ext.http.requestBasic({
    Translation.trans_url,
    param = self:combineUrlParamTable(params),
    mode = "notencrypt",
    header = header,
    method = "GET",
    callback = function(statusCode, content, err)
      if statusCode == 200 and content then
        DebugTable(content)
        if content == nil or content.trans_result == nil then
          cb(false, tostring(statusCode), "", "")
        elseif content.trans_result then
          local trans = ""
          for k, v in pairs(content.trans_result) do
            trans = trans .. v.dst
          end
          cb(true, "", trans, Translation:convertBaiduToGoogleCode(dl))
        else
          cb(false, tostring(content.error_msg), "", "")
        end
      else
        cb(false, tostring(statusCode), "", "")
      end
    end
  })
end
function Translation:urlencode(str)
  if str then
    str = string.gsub(str, "\n", "\r\n")
    str = string.gsub(str, "([^%w ])", function(c)
      return ...
    end)
    str = string.gsub(str, " ", "+")
  end
  return str
end
function Translation:combineUrlParamTable(t)
  local r = {}
  for k, v in pairs(t) do
    table.insert(r, tostring(k) .. "=" .. self:urlencode(v))
  end
  return ...
end
function Translation:translateText(sl, dl, text, cb)
  local function payCallback(isSuccess, code, content, dtlang)
    if isSuccess then
      Translation.pay_ok = Translation.pay_ok + 1
      local r = {translatedText = content, sourceLanguage = dtlang}
      GameUtils:printTable(r)
      cb(true, r)
    else
      count = Translation.pay_error[code]
      if count == nil then
        Translation.pay_error[code] = 1
      else
        Translation.pay_error[code] = count + 1
      end
      cb(false, nil)
    end
  end
  local function freeCallback(isSuccess, code, content, dtlang)
    if isSuccess then
      Translation.free_ok = Translation.free_ok + 1
      local r = {translatedText = content, sourceLanguage = dtlang}
      GameUtils:printTable(r)
      cb(true, r)
    else
      count = Translation.free_error[code]
      if count == nil then
        Translation.free_error[code] = 1
      else
        Translation.free_error[code] = count + 1
      end
      Translation.free_enable = false
      Translation.pay_function(sl, dl, text, payCallback)
    end
  end
  if Translation.free_enable == true then
    Translation.free_function(sl, dl, text, freeCallback)
  else
    Translation.pay_function(sl, dl, text, payCallback)
  end
end
function Translation:translate(sl, dl, text, cb)
  local function Callback(isSuccess, code, content, dtlang)
    if isSuccess then
      Translation.pay_ok = Translation.pay_ok + 1
      local r = {translatedText = content, sourceLanguage = dtlang}
      GameUtils:printTable(r)
      cb(true, r)
    else
      count = Translation.pay_error[code]
      if count == nil then
        Translation.pay_error[code] = 1
      else
        Translation.pay_error[code] = count + 1
      end
      cb(false, nil)
    end
  end
  Translation.trans_function(sl, dl, text, Callback)
end
function Translation:freebaiduTranslate(slanguage, dlanguage, text, cb)
  local params = {
    from = slanguage,
    to = dlanguage,
    query = text,
    transtype = "trans",
    simple_means_flag = 3
  }
  Translation.free_bytes = Translation.free_bytes + string.len(text)
  local header = {
    ["User-Agent"] = "Tap4fun client",
    ["Accept-Encoding"] = "gzip"
  }
  ext.http.requestBasic({
    Translation.free_addr,
    param = self:combineUrlParamTable(params),
    mode = "notencrypt",
    header = header,
    format = "text",
    method = "post",
    callback = function(statusCode, content, err)
      if statusCode == 200 and content then
        local r = ext.json.parse(content)
        if r == nil or r.trans_result == nil or r.trans_result.data[1] == nil then
          cb(false, tostring(statusCode), "", "")
        else
          local trans = ""
          for k, v in pairs(r.trans_result.data) do
            trans = trans .. v.dst
          end
          if r.trans_result.to ~= dlanguage then
            trans = text
          end
          cb(true, "", trans, self:convertBaiduToGoogleCode(r.trans_result.from))
        end
      else
        cb(false, tostring(statusCode), "", "")
      end
    end
  })
end
function Translation:paybaiduTranslate(slanguage, dlanguage, text, cb)
  print("paybaiduTranslatepaybaiduTranslate---")
  saltNum = os.time()
  local params = {
    appid = Translation.pay_appid,
    salt = tostring(saltNum),
    from = slanguage,
    to = dlanguage,
    sign = string.lower(ext.ENC1(Translation.pay_appid .. text .. tostring(saltNum) .. Translation.pay_key)),
    q = text
  }
  Translation.pay_bytes = Translation.pay_bytes + string.len(text)
  local header = {
    ["User-Agent"] = "Tap4fun client",
    ["Accept-Encoding"] = "gzip"
  }
  ext.http.requestBasic({
    Translation.pay_addr,
    param = self:combineUrlParamTable(params),
    mode = "notencrypt",
    header = header,
    format = "text",
    method = "get",
    callback = function(statusCode, content, err)
      if statusCode == 200 and content then
        local r = ext.json.parse(content)
        if not r.trans_result then
          cb(false, "tranlation failed", "", "")
          return
        end
        local trans = ""
        for k, v in pairs(r.trans_result) do
          trans = trans .. v.dst
        end
        cb(true, "", trans, r.from)
      else
        cb(false, tostring(statusCode), "", "")
      end
    end
  })
end
function Translation:freeTranslate(slanguage, dlanguage, text, cb)
  text = self.EmojiFilterAddSpace(text)
  print("Translation:freeTranslate")
  local params = {
    client = "t",
    sl = slanguage,
    tl = dlanguage,
    ie = "UTF-8",
    oe = "UTF-8",
    dt = "t",
    tk = "570059.956274",
    q = text
  }
  Translation.free_bytes = Translation.free_bytes + string.len(text)
  local header = {
    ["User-Agent"] = "Tap4fun client",
    ["Accept-Encoding"] = "gzip"
  }
  ext.http.requestBasic({
    Translation.free_addr,
    param = self:combineUrlParamTable(params),
    mode = "notencrypt",
    header = header,
    format = "text",
    method = "get",
    callback = function(statusCode, content, err)
      if statusCode == 200 and content then
        local i = 0
        local j = 0
        local text = ""
        while true do
          i, j, txt = string.find(content, "%[\"(.-)\",\".-\",,,", j + 1)
          if i == nil then
            break
          end
          text = text .. txt
        end
        _, _, lang = string.find(content, ",,\"(.-)\"")
        if text == nil or text == "" or lang == nil then
          cb(false, tostring(statusCode), "", "")
        else
          cb(true, "", text, lang)
        end
      else
        cb(false, tostring(statusCode), "", "")
      end
    end
  })
end
function Translation:platformTranslate(slanguage, dlanguage, text, cb)
  local params = {
    access_token = Translation.plat_token,
    target = dlanguage,
    q = text
  }
  Translation.pay_bytes = Translation.pay_bytes + string.len(text)
  local header = {
    ["User-Agent"] = "Tap4fun client",
    ["Accept-Encoding"] = "gzip"
  }
  ext.http.requestBasic({
    Translation.pay_addr,
    param = self:combineUrlParamTable(params),
    mode = "notencrypt",
    header = header,
    format = "text",
    method = "get",
    callback = function(statusCode, content, err)
      if statusCode == 200 and content then
        local r = tjson.decode(content)
        if r.error_code == nil then
          local trans = ""
          for k, v in pairs(r.data.translations) do
            trans = trans .. v.translatedText
          end
          cb(true, "", trans, r.data.translations[1].detectedSourceLanguage)
        else
          cb(false, tostring(r.error_code), "", "")
        end
      else
        cb(false, tostring(statusCode), "", "")
      end
    end
  })
end
function Translation:payTranslate(slanguage, dlanguage, text, cb)
  print("Translation:payTranslate")
  local params = {
    key = Translation.pay_key,
    target = dlanguage,
    q = text
  }
  Translation.pay_bytes = Translation.pay_bytes + string.len(text)
  local header = {
    ["User-Agent"] = "Tap4fun client",
    ["Accept-Encoding"] = "gzip"
  }
  ext.http.requestBasic({
    Translation.pay_addr,
    param = self:combineUrlParamTable(params),
    mode = "notencrypt",
    header = header,
    format = "text",
    method = "get",
    callback = function(statusCode, content, err)
      if statusCode == 200 and content then
        local r = ext.json.parse(content)
        local trans = ""
        for k, v in pairs(r.data.translations) do
          trans = trans .. v.translatedText
        end
        cb(true, "", trans, r.data.translations[1].detectedSourceLanguage)
      else
        cb(false, tostring(statusCode), "", "")
      end
    end
  })
end
function Translation:Tablecount(ht)
  local n = 0
  for _, v in pairs(ht) do
    n = n + 1
  end
  return n
end
function Translation:onPause()
  if Translation.stat_url == "" or Translation.stat_url == nil then
    return
  end
  data = ""
  codeHeader = "1"
  if Translation.abroadMode == false then
    codeHeader = "2"
  end
  if self:Tablecount(Translation.free_error) > 0 then
    data = data .. codeHeader .. "3,"
    for k, v in pairs(Translation.free_error) do
      v = self:urlencode(v)
      data = data .. self:urlencode(tostring(k)) .. "," .. tostring(v)
    end
    data = data .. "," .. Translation.free_bytes .. "|"
  end
  data = data .. codeHeader .. "1," .. "0," .. tostring(Translation.free_ok) .. "," .. Translation.free_bytes .. "|"
  if 0 < self:Tablecount(Translation.pay_error) then
    data = data .. "|" .. codeHeader .. "4,"
    for k, v in pairs(Translation.pay_error) do
      data = data .. tostring(k) .. "," .. tostring(v)
    end
    data = data .. "," .. Translation.pay_bytes .. "|"
  end
  data = data .. codeHeader .. "2," .. "0," .. tostring(Translation.pay_ok) .. "," .. Translation.pay_bytes .. "|"
  data = "data=" .. self:urlencode(data)
  data = data .. "&access_token=" .. self:urlencode(Translation.stat_token)
  data = data .. "&udid=" .. self:urlencode(Translation.udid)
  data = data .. "&ts=" .. os.time()
  data = data .. "&version=" .. self:urlencode(Translation.version)
  Translation.free_error = {}
  Translation.free_ok = 0
  Translation.free_bytes = 0
  Translation.pay_error = {}
  Translation.pay_ok = 0
  Translation.pay_bytes = 0
  local header = {
    ["User-Agent"] = "Tap4fun client",
    ["Accept-Encoding"] = "gzip"
  }
  ext.http.requestBasic({
    Translation.stat_url,
    param = data,
    mode = "notencrypt",
    header = header,
    format = "text",
    method = "post",
    callback = function(...)
    end
  })
end
function Translation:convertBaiduToGoogleCode(lo)
  if lo == "zh" then
    return "zh-CN"
  elseif lo == "cht" then
    return "zh-TW"
  elseif lo == "jp" then
    return "ja"
  elseif lo == "spa" then
    return "es"
  elseif lo == "kor" then
    return "ko"
  elseif lo == "fra" then
    return "fr"
  else
    return lo
  end
end
function Translation:convertGoogleCodeToBaidu(lo)
  if lo == "zh-CN" then
    return "zh"
  elseif lo == "zh-TW" then
    return "cht"
  elseif lo == "ja" then
    return "jp"
  elseif lo == "es" then
    return "spa"
  elseif lo == "ko" then
    return "kor"
  elseif lo == "fr" then
    return "fra"
  else
    return lo
  end
end
