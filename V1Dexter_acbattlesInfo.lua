local battlesInfo = GameData.Dexter_ac.battlesInfo
battlesInfo[1001] = {
  id = 1001,
  level_req = 40,
  MONSTERS = {
    5200014,
    0,
    0,
    5200014,
    5200014,
    5200016,
    5200014,
    0,
    0
  },
  money = 10000,
  supply = 1000,
  exp = 1260,
  captain = 5200016,
  force = 386024,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[1002] = {
  id = 1002,
  level_req = 40,
  MONSTERS = {
    5200014,
    5200014,
    5200016,
    0,
    0,
    5200014,
    5200014,
    0,
    0
  },
  money = 10000,
  supply = 1000,
  exp = 1260,
  captain = 5200016,
  force = 386024,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[1003] = {
  id = 1003,
  level_req = 40,
  MONSTERS = {
    5200012,
    0,
    0,
    5200012,
    5200012,
    5200016,
    5200012,
    0,
    0
  },
  money = 10000,
  supply = 1000,
  exp = 1260,
  captain = 5200016,
  force = 424960,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[1004] = {
  id = 1004,
  level_req = 40,
  MONSTERS = {
    0,
    5200012,
    0,
    0,
    5200012,
    5200012,
    0,
    5200016,
    5200012
  },
  money = 10000,
  supply = 1000,
  exp = 1260,
  captain = 5200016,
  force = 424960,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[1005] = {
  id = 1005,
  level_req = 40,
  MONSTERS = {
    0,
    5200012,
    5200012,
    5200012,
    0,
    0,
    5200016,
    5200012,
    0
  },
  money = 10000,
  supply = 1000,
  exp = 1260,
  captain = 5200016,
  force = 424960,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[2001] = {
  id = 2001,
  level_req = 45,
  MONSTERS = {
    5200012,
    5200016,
    0,
    5200012,
    5200012,
    0,
    0,
    5200012,
    0
  },
  money = 10000,
  supply = 2000,
  exp = 1540,
  captain = 5200016,
  force = 424960,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[2002] = {
  id = 2002,
  level_req = 45,
  MONSTERS = {
    5200012,
    5200012,
    5200016,
    5200012,
    0,
    0,
    0,
    0,
    5200012
  },
  money = 10000,
  supply = 2000,
  exp = 1540,
  captain = 5200016,
  force = 424960,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[2003] = {
  id = 2003,
  level_req = 45,
  MONSTERS = {
    5200026,
    5200022,
    5200022,
    0,
    0,
    0,
    0,
    5200022,
    5200022
  },
  money = 10000,
  supply = 2000,
  exp = 1540,
  captain = 5200026,
  force = 532284,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[2004] = {
  id = 2004,
  level_req = 45,
  MONSTERS = {
    5200022,
    5200022,
    0,
    0,
    0,
    5200022,
    5200026,
    0,
    5200022
  },
  money = 10000,
  supply = 2000,
  exp = 1540,
  captain = 5200026,
  force = 532284,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[2005] = {
  id = 2005,
  level_req = 45,
  MONSTERS = {
    5200022,
    5200022,
    5200022,
    5200026,
    0,
    0,
    0,
    5200022,
    0
  },
  money = 10000,
  supply = 2000,
  exp = 1540,
  captain = 5200026,
  force = 532284,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[3001] = {
  id = 3001,
  level_req = 50,
  MONSTERS = {
    0,
    5200022,
    0,
    5200022,
    0,
    5200026,
    5200022,
    0,
    5200022
  },
  money = 10000,
  supply = 3000,
  exp = 1819,
  captain = 5200026,
  force = 532284,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[3002] = {
  id = 3002,
  level_req = 50,
  MONSTERS = {
    5200026,
    5200022,
    5200022,
    5200022,
    0,
    5200022,
    0,
    0,
    0
  },
  money = 10000,
  supply = 3000,
  exp = 1819,
  captain = 5200026,
  force = 532284,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[3003] = {
  id = 3003,
  level_req = 50,
  MONSTERS = {
    5200032,
    0,
    0,
    0,
    5200032,
    5200036,
    5200032,
    5200032,
    0
  },
  money = 10000,
  supply = 3000,
  exp = 1819,
  captain = 5200036,
  force = 905438,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[3004] = {
  id = 3004,
  level_req = 50,
  MONSTERS = {
    5200036,
    5200032,
    5200032,
    5200032,
    5200032,
    0,
    0,
    0,
    0
  },
  money = 10000,
  supply = 3000,
  exp = 1819,
  captain = 5200036,
  force = 905438,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[3005] = {
  id = 3005,
  level_req = 50,
  MONSTERS = {
    5200036,
    5200032,
    5200032,
    0,
    5200032,
    5200032,
    0,
    0,
    0
  },
  money = 10000,
  supply = 3000,
  exp = 1819,
  captain = 5200036,
  force = 905438,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[4001] = {
  id = 4001,
  level_req = 55,
  MONSTERS = {
    0,
    5200032,
    5200032,
    5200036,
    0,
    5200032,
    0,
    5200032,
    0
  },
  money = 10000,
  supply = 4000,
  exp = 2100,
  captain = 5200036,
  force = 905438,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[4002] = {
  id = 4002,
  level_req = 55,
  MONSTERS = {
    5200032,
    5200032,
    0,
    0,
    0,
    5200032,
    5200036,
    0,
    5200032
  },
  money = 10000,
  supply = 4000,
  exp = 2100,
  captain = 5200036,
  force = 905438,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[4003] = {
  id = 4003,
  level_req = 55,
  MONSTERS = {
    5200042,
    5200042,
    0,
    5200046,
    0,
    5200042,
    0,
    0,
    5200042
  },
  money = 10000,
  supply = 4000,
  exp = 2100,
  captain = 5200046,
  force = 1052036,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[4004] = {
  id = 4004,
  level_req = 55,
  MONSTERS = {
    0,
    5200042,
    0,
    5200046,
    0,
    5200042,
    5200042,
    0,
    5200042
  },
  money = 10000,
  supply = 4000,
  exp = 2100,
  captain = 5200046,
  force = 1052036,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[4005] = {
  id = 4005,
  level_req = 55,
  MONSTERS = {
    0,
    5200042,
    5200042,
    0,
    0,
    5200046,
    5200042,
    5200042,
    0
  },
  money = 10000,
  supply = 4000,
  exp = 2100,
  captain = 5200046,
  force = 1052036,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[5001] = {
  id = 5001,
  level_req = 60,
  MONSTERS = {
    5200042,
    5200042,
    0,
    5200042,
    0,
    0,
    5200042,
    5200046,
    0
  },
  money = 10000,
  supply = 5000,
  exp = 2380,
  captain = 5200046,
  force = 1052036,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[5002] = {
  id = 5002,
  level_req = 60,
  MONSTERS = {
    5200042,
    5200042,
    5200042,
    0,
    0,
    0,
    5200046,
    0,
    5200042
  },
  money = 10000,
  supply = 5000,
  exp = 2380,
  captain = 5200046,
  force = 1052036,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[5003] = {
  id = 5003,
  level_req = 60,
  MONSTERS = {
    5200052,
    0,
    0,
    5200052,
    5200052,
    0,
    5200056,
    0,
    5200052
  },
  money = 10000,
  supply = 5000,
  exp = 2380,
  captain = 5200056,
  force = 1636281,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[5004] = {
  id = 5004,
  level_req = 60,
  MONSTERS = {
    0,
    5200052,
    5200052,
    5200052,
    5200056,
    0,
    0,
    0,
    5200052
  },
  money = 10000,
  supply = 5000,
  exp = 2380,
  captain = 5200056,
  force = 1636281,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[5005] = {
  id = 5005,
  level_req = 60,
  MONSTERS = {
    5200052,
    0,
    0,
    0,
    5200052,
    5200056,
    5200052,
    5200052,
    0
  },
  money = 10000,
  supply = 5000,
  exp = 2380,
  captain = 5200056,
  force = 1636281,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[6001] = {
  id = 6001,
  level_req = 65,
  MONSTERS = {
    5200052,
    5200052,
    5200052,
    0,
    5200052,
    0,
    0,
    5200056,
    0
  },
  money = 10000,
  supply = 6000,
  exp = 2660,
  captain = 5200056,
  force = 1636281,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[6002] = {
  id = 6002,
  level_req = 65,
  MONSTERS = {
    5200052,
    0,
    5200052,
    5200052,
    5200052,
    5200056,
    0,
    0,
    0
  },
  money = 10000,
  supply = 6000,
  exp = 2660,
  captain = 5200056,
  force = 1636281,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[6003] = {
  id = 6003,
  level_req = 65,
  MONSTERS = {
    5200062,
    5200062,
    5200066,
    5200062,
    0,
    5200062,
    0,
    0,
    0
  },
  money = 10000,
  supply = 6000,
  exp = 2660,
  captain = 5200066,
  force = 1868623,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[6004] = {
  id = 6004,
  level_req = 65,
  MONSTERS = {
    5200066,
    0,
    0,
    5200062,
    0,
    5200062,
    5200062,
    0,
    5200062
  },
  money = 10000,
  supply = 6000,
  exp = 2660,
  captain = 5200066,
  force = 1868623,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[6005] = {
  id = 6005,
  level_req = 65,
  MONSTERS = {
    0,
    5200062,
    0,
    5200066,
    0,
    5200062,
    5200062,
    0,
    5200062
  },
  money = 10000,
  supply = 6000,
  exp = 2660,
  captain = 5200066,
  force = 1868623,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[7001] = {
  id = 7001,
  level_req = 70,
  MONSTERS = {
    0,
    0,
    5200062,
    5200066,
    0,
    5200062,
    5200062,
    0,
    5200062
  },
  money = 10000,
  supply = 7000,
  exp = 2940,
  captain = 5200066,
  force = 1868623,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[7002] = {
  id = 7002,
  level_req = 70,
  MONSTERS = {
    5200062,
    0,
    5200062,
    5200062,
    0,
    5200062,
    5200066,
    0,
    0
  },
  money = 10000,
  supply = 7000,
  exp = 2940,
  captain = 5200066,
  force = 1868623,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[7003] = {
  id = 7003,
  level_req = 70,
  MONSTERS = {
    5200072,
    0,
    0,
    5200072,
    0,
    5200072,
    5200072,
    0,
    5200076
  },
  money = 10000,
  supply = 7000,
  exp = 2940,
  captain = 5200076,
  force = 3018600,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[7004] = {
  id = 7004,
  level_req = 70,
  MONSTERS = {
    5200072,
    0,
    5200072,
    5200072,
    0,
    5200072,
    0,
    0,
    5200076
  },
  money = 10000,
  supply = 7000,
  exp = 2940,
  captain = 5200076,
  force = 3018600,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[7005] = {
  id = 7005,
  level_req = 70,
  MONSTERS = {
    5200076,
    0,
    0,
    5200072,
    5200072,
    5200072,
    0,
    5200072,
    0
  },
  money = 10000,
  supply = 7000,
  exp = 2940,
  captain = 5200076,
  force = 3018600,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[8001] = {
  id = 8001,
  level_req = 75,
  MONSTERS = {
    5200076,
    5200072,
    0,
    5200072,
    0,
    5200072,
    5200072,
    0,
    0
  },
  money = 10000,
  supply = 8000,
  exp = 3220,
  captain = 5200076,
  force = 3018600,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[8002] = {
  id = 8002,
  level_req = 75,
  MONSTERS = {
    5200072,
    5200072,
    5200072,
    0,
    5200072,
    5200076,
    0,
    0,
    0
  },
  money = 10000,
  supply = 8000,
  exp = 3220,
  captain = 5200076,
  force = 3018600,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[8003] = {
  id = 8003,
  level_req = 75,
  MONSTERS = {
    5200086,
    0,
    5200082,
    5200082,
    5200082,
    5200082,
    0,
    0,
    0
  },
  money = 10000,
  supply = 8000,
  exp = 3220,
  captain = 5200086,
  force = 3464012,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[8004] = {
  id = 8004,
  level_req = 75,
  MONSTERS = {
    5200082,
    0,
    0,
    5200082,
    0,
    5200086,
    5200082,
    0,
    5200082
  },
  money = 10000,
  supply = 8000,
  exp = 3220,
  captain = 5200086,
  force = 3464012,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[8005] = {
  id = 8005,
  level_req = 75,
  MONSTERS = {
    5200082,
    5200086,
    0,
    0,
    0,
    5200082,
    5200082,
    0,
    5200082
  },
  money = 10000,
  supply = 8000,
  exp = 3220,
  captain = 5200086,
  force = 3464012,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[9001] = {
  id = 9001,
  level_req = 80,
  MONSTERS = {
    0,
    5200082,
    0,
    5200082,
    0,
    5200082,
    5200082,
    5200086,
    0
  },
  money = 10000,
  supply = 9000,
  exp = 3500,
  captain = 5200086,
  force = 3464012,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[9002] = {
  id = 9002,
  level_req = 80,
  MONSTERS = {
    5200082,
    5200082,
    5200082,
    0,
    5200082,
    0,
    5200086,
    0,
    0
  },
  money = 10000,
  supply = 9000,
  exp = 3500,
  captain = 5200086,
  force = 3464012,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[9003] = {
  id = 9003,
  level_req = 80,
  MONSTERS = {
    0,
    5200092,
    0,
    5200092,
    5200092,
    5200092,
    0,
    0,
    5200096
  },
  money = 10000,
  supply = 9000,
  exp = 3500,
  captain = 5200096,
  force = 5758513,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[9004] = {
  id = 9004,
  level_req = 80,
  MONSTERS = {
    5200096,
    5200092,
    5200092,
    5200092,
    5200092,
    0,
    0,
    0,
    0
  },
  money = 10000,
  supply = 9000,
  exp = 3500,
  captain = 5200096,
  force = 5758513,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[9005] = {
  id = 9005,
  level_req = 80,
  MONSTERS = {
    5200092,
    0,
    0,
    0,
    5200096,
    0,
    5200092,
    5200092,
    5200092
  },
  money = 10000,
  supply = 9000,
  exp = 3500,
  captain = 5200096,
  force = 5758513,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[10001] = {
  id = 10001,
  level_req = 85,
  MONSTERS = {
    5200092,
    5200092,
    5200092,
    5200096,
    5200092,
    0,
    0,
    0,
    0
  },
  money = 10000,
  supply = 10000,
  exp = 3779,
  captain = 5200096,
  force = 5758513,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[10002] = {
  id = 10002,
  level_req = 85,
  MONSTERS = {
    0,
    0,
    5200096,
    5200092,
    5200092,
    0,
    0,
    5200092,
    5200092
  },
  money = 10000,
  supply = 10000,
  exp = 3779,
  captain = 5200096,
  force = 5758513,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[10003] = {
  id = 10003,
  level_req = 85,
  MONSTERS = {
    5200102,
    0,
    5200106,
    0,
    5200102,
    0,
    5200102,
    5200102,
    0
  },
  money = 10000,
  supply = 10000,
  exp = 3779,
  captain = 5200106,
  force = 6522610,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[10004] = {
  id = 10004,
  level_req = 85,
  MONSTERS = {
    0,
    0,
    5200102,
    5200102,
    5200102,
    5200106,
    5200102,
    0,
    0
  },
  money = 10000,
  supply = 10000,
  exp = 3779,
  captain = 5200106,
  force = 6522610,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[10005] = {
  id = 10005,
  level_req = 85,
  MONSTERS = {
    5200102,
    0,
    5200102,
    0,
    5200102,
    5200102,
    0,
    0,
    5200106
  },
  money = 10000,
  supply = 10000,
  exp = 3779,
  captain = 5200106,
  force = 6522610,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[11001] = {
  id = 11001,
  level_req = 90,
  MONSTERS = {
    5200102,
    0,
    5200102,
    0,
    5200106,
    0,
    5200102,
    5200102,
    0
  },
  money = 10000,
  supply = 11000,
  exp = 4059,
  captain = 5200106,
  force = 6522610,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[11002] = {
  id = 11002,
  level_req = 90,
  MONSTERS = {
    5200102,
    5200102,
    0,
    5200102,
    5200102,
    0,
    0,
    5200106,
    0
  },
  money = 10000,
  supply = 11000,
  exp = 4059,
  captain = 5200106,
  force = 6522610,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[11003] = {
  id = 11003,
  level_req = 90,
  MONSTERS = {
    5200112,
    0,
    0,
    0,
    5200116,
    0,
    5200112,
    5200112,
    5200112
  },
  money = 10000,
  supply = 11000,
  exp = 4059,
  captain = 5200116,
  force = 10652278,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[11004] = {
  id = 11004,
  level_req = 90,
  MONSTERS = {
    5200112,
    0,
    0,
    0,
    5200112,
    5200112,
    5200116,
    5200112,
    0
  },
  money = 10000,
  supply = 11000,
  exp = 4059,
  captain = 5200116,
  force = 10652278,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[11005] = {
  id = 11005,
  level_req = 90,
  MONSTERS = {
    5200112,
    0,
    0,
    5200112,
    5200112,
    5200116,
    5200112,
    0,
    0
  },
  money = 10000,
  supply = 11000,
  exp = 4059,
  captain = 5200116,
  force = 10652278,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[12001] = {
  id = 12001,
  level_req = 95,
  MONSTERS = {
    5200122,
    5200122,
    0,
    5200126,
    5200122,
    5200122,
    0,
    0,
    0
  },
  money = 10000,
  supply = 12000,
  exp = 4340,
  captain = 5200126,
  force = 20000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[12002] = {
  id = 12002,
  level_req = 95,
  MONSTERS = {
    5200126,
    5200122,
    0,
    0,
    5200122,
    5200122,
    0,
    0,
    5200122
  },
  money = 10000,
  supply = 12000,
  exp = 4340,
  captain = 5200126,
  force = 20000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[12003] = {
  id = 12003,
  level_req = 95,
  MONSTERS = {
    5200122,
    5200122,
    0,
    5200122,
    0,
    5200122,
    0,
    5200126,
    0
  },
  money = 10000,
  supply = 12000,
  exp = 4340,
  captain = 5200126,
  force = 20000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[12004] = {
  id = 12004,
  level_req = 95,
  MONSTERS = {
    5200122,
    5200122,
    5200122,
    0,
    5200122,
    0,
    5200126,
    0,
    0
  },
  money = 10000,
  supply = 12000,
  exp = 4340,
  captain = 5200126,
  force = 20000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[12005] = {
  id = 12005,
  level_req = 95,
  MONSTERS = {
    5200126,
    5200122,
    0,
    0,
    5200122,
    5200122,
    0,
    5200122,
    0
  },
  money = 10000,
  supply = 12000,
  exp = 4340,
  captain = 5200126,
  force = 20000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[13001] = {
  id = 13001,
  level_req = 100,
  MONSTERS = {
    0,
    5200132,
    0,
    5200136,
    5200132,
    0,
    5200132,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 4620,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[13002] = {
  id = 13002,
  level_req = 100,
  MONSTERS = {
    0,
    5200132,
    5200132,
    0,
    0,
    5200132,
    0,
    5200136,
    5200132
  },
  money = 10000,
  supply = 13000,
  exp = 4620,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[13003] = {
  id = 13003,
  level_req = 100,
  MONSTERS = {
    0,
    5200136,
    0,
    5200132,
    5200132,
    5200132,
    5200132,
    0,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 4620,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[13004] = {
  id = 13004,
  level_req = 100,
  MONSTERS = {
    5200136,
    5200132,
    0,
    5200132,
    5200132,
    0,
    0,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 4620,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[13005] = {
  id = 13005,
  level_req = 100,
  MONSTERS = {
    5200132,
    5200136,
    5200132,
    0,
    0,
    0,
    5200132,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 4620,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[14001] = {
  id = 14001,
  level_req = 105,
  MONSTERS = {
    0,
    5200132,
    0,
    5200136,
    5200132,
    0,
    5200132,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5120,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[14002] = {
  id = 14002,
  level_req = 105,
  MONSTERS = {
    0,
    5200132,
    5200132,
    0,
    0,
    5200132,
    0,
    5200136,
    5200132
  },
  money = 10000,
  supply = 13000,
  exp = 5120,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[14003] = {
  id = 14003,
  level_req = 105,
  MONSTERS = {
    0,
    5200136,
    0,
    5200132,
    5200132,
    5200132,
    5200132,
    0,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5120,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[14004] = {
  id = 14004,
  level_req = 105,
  MONSTERS = {
    5200136,
    5200132,
    0,
    5200132,
    5200132,
    0,
    0,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5120,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[14005] = {
  id = 14005,
  level_req = 105,
  MONSTERS = {
    5200132,
    5200136,
    5200132,
    0,
    0,
    0,
    5200132,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5120,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[15001] = {
  id = 15001,
  level_req = 110,
  MONSTERS = {
    0,
    5200132,
    0,
    5200136,
    5200132,
    0,
    5200132,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5500,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[15002] = {
  id = 15002,
  level_req = 110,
  MONSTERS = {
    0,
    5200132,
    5200132,
    0,
    0,
    5200132,
    0,
    5200136,
    5200132
  },
  money = 10000,
  supply = 13000,
  exp = 5500,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[15003] = {
  id = 15003,
  level_req = 110,
  MONSTERS = {
    0,
    5200136,
    0,
    5200132,
    5200132,
    5200132,
    5200132,
    0,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5500,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[15004] = {
  id = 15004,
  level_req = 110,
  MONSTERS = {
    5200136,
    5200132,
    0,
    5200132,
    5200132,
    0,
    0,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5500,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
battlesInfo[15005] = {
  id = 15005,
  level_req = 110,
  MONSTERS = {
    5200132,
    5200136,
    5200132,
    0,
    0,
    0,
    5200132,
    5200132,
    0
  },
  money = 10000,
  supply = 13000,
  exp = 5500,
  captain = 5200136,
  force = 30000000,
  Head = "head36",
  MonsterImage = 1
}
