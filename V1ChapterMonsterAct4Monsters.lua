local Monsters = GameData.ChapterMonsterAct4.Monsters
Monsters[401001] = {
  ID = 401001,
  durability = 9466,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401002] = {
  ID = 401002,
  durability = 7633,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401003] = {
  ID = 401003,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401004] = {
  ID = 401004,
  durability = 3967,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401005] = {
  ID = 401005,
  durability = 3966,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401006] = {
  ID = 401006,
  durability = 9466,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401007] = {
  ID = 401007,
  durability = 7634,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401008] = {
  ID = 401008,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401009] = {
  ID = 401009,
  durability = 3967,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401010] = {
  ID = 401010,
  durability = 3966,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401011] = {
  ID = 401011,
  durability = 9465,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401012] = {
  ID = 401012,
  durability = 7633,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401013] = {
  ID = 401013,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401014] = {
  ID = 401014,
  durability = 3966,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401015] = {
  ID = 401015,
  durability = 3966,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401016] = {
  ID = 401016,
  durability = 14047,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[401017] = {
  ID = 401017,
  durability = 7633,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401018] = {
  ID = 401018,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401019] = {
  ID = 401019,
  durability = 3967,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401020] = {
  ID = 401020,
  durability = 3967,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401021] = {
  ID = 401021,
  durability = 9468,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401022] = {
  ID = 401022,
  durability = 7632,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401023] = {
  ID = 401023,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401024] = {
  ID = 401024,
  durability = 3967,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401025] = {
  ID = 401025,
  durability = 3966,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401026] = {
  ID = 401026,
  durability = 9465,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401027] = {
  ID = 401027,
  durability = 7633,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401028] = {
  ID = 401028,
  durability = 1522,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401029] = {
  ID = 401029,
  durability = 3966,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401030] = {
  ID = 401030,
  durability = 3966,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401031] = {
  ID = 401031,
  durability = 10233,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401032] = {
  ID = 401032,
  durability = 8246,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401033] = {
  ID = 401033,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401034] = {
  ID = 401034,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401035] = {
  ID = 401035,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401036] = {
  ID = 401036,
  durability = 15199,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[401037] = {
  ID = 401037,
  durability = 8246,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401038] = {
  ID = 401038,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[401039] = {
  ID = 401039,
  durability = 4274,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[401040] = {
  ID = 401040,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402001] = {
  ID = 402001,
  durability = 10234,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402002] = {
  ID = 402002,
  durability = 8248,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402003] = {
  ID = 402003,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402004] = {
  ID = 402004,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402005] = {
  ID = 402005,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402006] = {
  ID = 402006,
  durability = 10233,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402007] = {
  ID = 402007,
  durability = 8247,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402008] = {
  ID = 402008,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402009] = {
  ID = 402009,
  durability = 4274,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402010] = {
  ID = 402010,
  durability = 4274,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402011] = {
  ID = 402011,
  durability = 10233,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402012] = {
  ID = 402012,
  durability = 8248,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402013] = {
  ID = 402013,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402014] = {
  ID = 402014,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402015] = {
  ID = 402015,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402016] = {
  ID = 402016,
  durability = 15201,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[402017] = {
  ID = 402017,
  durability = 8247,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402018] = {
  ID = 402018,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402019] = {
  ID = 402019,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402020] = {
  ID = 402020,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402021] = {
  ID = 402021,
  durability = 10236,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402022] = {
  ID = 402022,
  durability = 8247,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402023] = {
  ID = 402023,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402024] = {
  ID = 402024,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402025] = {
  ID = 402025,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402026] = {
  ID = 402026,
  durability = 10236,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402027] = {
  ID = 402027,
  durability = 8247,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402028] = {
  ID = 402028,
  durability = 1624,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402029] = {
  ID = 402029,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402030] = {
  ID = 402030,
  durability = 4273,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402031] = {
  ID = 402031,
  durability = 11004,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402032] = {
  ID = 402032,
  durability = 8862,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402033] = {
  ID = 402033,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402034] = {
  ID = 402034,
  durability = 4580,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402035] = {
  ID = 402035,
  durability = 4580,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402036] = {
  ID = 402036,
  durability = 16356,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[402037] = {
  ID = 402037,
  durability = 8862,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402038] = {
  ID = 402038,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[402039] = {
  ID = 402039,
  durability = 4580,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[402040] = {
  ID = 402040,
  durability = 4581,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403001] = {
  ID = 403001,
  durability = 7077,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[403002] = {
  ID = 403002,
  durability = 5437,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403003] = {
  ID = 403003,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403004] = {
  ID = 403004,
  durability = 2440,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403005] = {
  ID = 403005,
  durability = 3154,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403006] = {
  ID = 403006,
  durability = 7078,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[403007] = {
  ID = 403007,
  durability = 5437,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403008] = {
  ID = 403008,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403009] = {
  ID = 403009,
  durability = 2440,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403010] = {
  ID = 403010,
  durability = 3153,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403011] = {
  ID = 403011,
  durability = 7078,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[403012] = {
  ID = 403012,
  durability = 5437,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403013] = {
  ID = 403013,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403014] = {
  ID = 403014,
  durability = 2440,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403015] = {
  ID = 403015,
  durability = 3154,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403016] = {
  ID = 403016,
  durability = 10468,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[403017] = {
  ID = 403017,
  durability = 5437,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403018] = {
  ID = 403018,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403019] = {
  ID = 403019,
  durability = 2440,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403020] = {
  ID = 403020,
  durability = 3154,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403021] = {
  ID = 403021,
  durability = 7078,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[403022] = {
  ID = 403022,
  durability = 5437,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403023] = {
  ID = 403023,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403024] = {
  ID = 403024,
  durability = 2440,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403025] = {
  ID = 403025,
  durability = 3154,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403026] = {
  ID = 403026,
  durability = 7078,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[403027] = {
  ID = 403027,
  durability = 5437,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403028] = {
  ID = 403028,
  durability = 1727,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403029] = {
  ID = 403029,
  durability = 2440,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403030] = {
  ID = 403030,
  durability = 3154,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403031] = {
  ID = 403031,
  durability = 7564,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[403032] = {
  ID = 403032,
  durability = 5805,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403033] = {
  ID = 403033,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403034] = {
  ID = 403034,
  durability = 2593,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403035] = {
  ID = 403035,
  durability = 3359,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403036] = {
  ID = 403036,
  durability = 11196,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[403037] = {
  ID = 403037,
  durability = 5805,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[403038] = {
  ID = 403038,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[403039] = {
  ID = 403039,
  durability = 2594,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[403040] = {
  ID = 403040,
  durability = 3358,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404001] = {
  ID = 404001,
  durability = 7183,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[404002] = {
  ID = 404002,
  durability = 5499,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404003] = {
  ID = 404003,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404004] = {
  ID = 404004,
  durability = 2594,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404005] = {
  ID = 404005,
  durability = 3359,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404006] = {
  ID = 404006,
  durability = 7182,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[404007] = {
  ID = 404007,
  durability = 5500,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404008] = {
  ID = 404008,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404009] = {
  ID = 404009,
  durability = 2593,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404010] = {
  ID = 404010,
  durability = 3358,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404011] = {
  ID = 404011,
  durability = 7181,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[404012] = {
  ID = 404012,
  durability = 5499,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404013] = {
  ID = 404013,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404014] = {
  ID = 404014,
  durability = 2593,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404015] = {
  ID = 404015,
  durability = 3358,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404016] = {
  ID = 404016,
  durability = 10623,
  Avatar = "head36",
  Ship = "ship25"
}
Monsters[404017] = {
  ID = 404017,
  durability = 5499,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404018] = {
  ID = 404018,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404019] = {
  ID = 404019,
  durability = 2593,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404020] = {
  ID = 404020,
  durability = 3358,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404021] = {
  ID = 404021,
  durability = 7181,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[404022] = {
  ID = 404022,
  durability = 5499,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404023] = {
  ID = 404023,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404024] = {
  ID = 404024,
  durability = 2594,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404025] = {
  ID = 404025,
  durability = 3358,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404026] = {
  ID = 404026,
  durability = 7181,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[404027] = {
  ID = 404027,
  durability = 5499,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404028] = {
  ID = 404028,
  durability = 1829,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404029] = {
  ID = 404029,
  durability = 2594,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404030] = {
  ID = 404030,
  durability = 3359,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404031] = {
  ID = 404031,
  durability = 7643,
  Avatar = "head35",
  Ship = "ship11"
}
Monsters[404032] = {
  ID = 404032,
  durability = 5848,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404033] = {
  ID = 404033,
  durability = 1931,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404034] = {
  ID = 404034,
  durability = 2748,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404035] = {
  ID = 404035,
  durability = 3564,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404036] = {
  ID = 404036,
  durability = 11314,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[404037] = {
  ID = 404037,
  durability = 5847,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[404038] = {
  ID = 404038,
  durability = 1931,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[404039] = {
  ID = 404039,
  durability = 2748,
  Avatar = "head35",
  Ship = "ship47"
}
Monsters[404040] = {
  ID = 404040,
  durability = 3564,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[405001] = {
  ID = 405001,
  durability = 7643,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[405002] = {
  ID = 405002,
  durability = 5847,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405003] = {
  ID = 405003,
  durability = 1931,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405004] = {
  ID = 405004,
  durability = 2747,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405005] = {
  ID = 405005,
  durability = 3563,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405006] = {
  ID = 405006,
  durability = 7642,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[405007] = {
  ID = 405007,
  durability = 5848,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405008] = {
  ID = 405008,
  durability = 1932,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405009] = {
  ID = 405009,
  durability = 2748,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405010] = {
  ID = 405010,
  durability = 3564,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405011] = {
  ID = 405011,
  durability = 7643,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[405012] = {
  ID = 405012,
  durability = 5847,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405013] = {
  ID = 405013,
  durability = 1931,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405014] = {
  ID = 405014,
  durability = 2747,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405015] = {
  ID = 405015,
  durability = 3564,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405016] = {
  ID = 405016,
  durability = 11313,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[405017] = {
  ID = 405017,
  durability = 5848,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405018] = {
  ID = 405018,
  durability = 1932,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405019] = {
  ID = 405019,
  durability = 2747,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405020] = {
  ID = 405020,
  durability = 3563,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405021] = {
  ID = 405021,
  durability = 7643,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[405022] = {
  ID = 405022,
  durability = 5848,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405023] = {
  ID = 405023,
  durability = 1932,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405024] = {
  ID = 405024,
  durability = 2748,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405025] = {
  ID = 405025,
  durability = 3563,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405026] = {
  ID = 405026,
  durability = 7643,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[405027] = {
  ID = 405027,
  durability = 5848,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405028] = {
  ID = 405028,
  durability = 1931,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405029] = {
  ID = 405029,
  durability = 2747,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405030] = {
  ID = 405030,
  durability = 3563,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405031] = {
  ID = 405031,
  durability = 8104,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[405032] = {
  ID = 405032,
  durability = 6196,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405033] = {
  ID = 405033,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405034] = {
  ID = 405034,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405035] = {
  ID = 405035,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405036] = {
  ID = 405036,
  durability = 12004,
  Avatar = "head36",
  Ship = "ship24"
}
Monsters[405037] = {
  ID = 405037,
  durability = 6195,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405038] = {
  ID = 405038,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[405039] = {
  ID = 405039,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[405040] = {
  ID = 405040,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406001] = {
  ID = 406001,
  durability = 8103,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[406002] = {
  ID = 406002,
  durability = 6196,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406003] = {
  ID = 406003,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406004] = {
  ID = 406004,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406005] = {
  ID = 406005,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406006] = {
  ID = 406006,
  durability = 8103,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[406007] = {
  ID = 406007,
  durability = 6195,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406008] = {
  ID = 406008,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406009] = {
  ID = 406009,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406010] = {
  ID = 406010,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406011] = {
  ID = 406011,
  durability = 8103,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[406012] = {
  ID = 406012,
  durability = 6196,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406013] = {
  ID = 406013,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406014] = {
  ID = 406014,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406015] = {
  ID = 406015,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406016] = {
  ID = 406016,
  durability = 12005,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[406017] = {
  ID = 406017,
  durability = 6196,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406018] = {
  ID = 406018,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406019] = {
  ID = 406019,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406020] = {
  ID = 406020,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406021] = {
  ID = 406021,
  durability = 8103,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[406022] = {
  ID = 406022,
  durability = 6195,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406023] = {
  ID = 406023,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406024] = {
  ID = 406024,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406025] = {
  ID = 406025,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406026] = {
  ID = 406026,
  durability = 8103,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[406027] = {
  ID = 406027,
  durability = 6196,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406028] = {
  ID = 406028,
  durability = 2034,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406029] = {
  ID = 406029,
  durability = 2901,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406030] = {
  ID = 406030,
  durability = 3768,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406031] = {
  ID = 406031,
  durability = 8564,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[406032] = {
  ID = 406032,
  durability = 6544,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406033] = {
  ID = 406033,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406034] = {
  ID = 406034,
  durability = 3054,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406035] = {
  ID = 406035,
  durability = 3973,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406036] = {
  ID = 406036,
  durability = 12697,
  Avatar = "head36",
  Ship = "ship24"
}
Monsters[406037] = {
  ID = 406037,
  durability = 6544,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406038] = {
  ID = 406038,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[406039] = {
  ID = 406039,
  durability = 3054,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[406040] = {
  ID = 406040,
  durability = 3972,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407001] = {
  ID = 407001,
  durability = 8563,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[407002] = {
  ID = 407002,
  durability = 6543,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407003] = {
  ID = 407003,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407004] = {
  ID = 407004,
  durability = 3054,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407005] = {
  ID = 407005,
  durability = 3973,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407006] = {
  ID = 407006,
  durability = 8564,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[407007] = {
  ID = 407007,
  durability = 6545,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407008] = {
  ID = 407008,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407009] = {
  ID = 407009,
  durability = 3055,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407010] = {
  ID = 407010,
  durability = 3973,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407011] = {
  ID = 407011,
  durability = 8564,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[407012] = {
  ID = 407012,
  durability = 6543,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407013] = {
  ID = 407013,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407014] = {
  ID = 407014,
  durability = 3055,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407015] = {
  ID = 407015,
  durability = 3973,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407016] = {
  ID = 407016,
  durability = 12697,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[407017] = {
  ID = 407017,
  durability = 6543,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407018] = {
  ID = 407018,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407019] = {
  ID = 407019,
  durability = 3055,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407020] = {
  ID = 407020,
  durability = 3973,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407021] = {
  ID = 407021,
  durability = 8565,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[407022] = {
  ID = 407022,
  durability = 6543,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407023] = {
  ID = 407023,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407024] = {
  ID = 407024,
  durability = 3055,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407025] = {
  ID = 407025,
  durability = 3972,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407026] = {
  ID = 407026,
  durability = 8565,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[407027] = {
  ID = 407027,
  durability = 6543,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407028] = {
  ID = 407028,
  durability = 2136,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407029] = {
  ID = 407029,
  durability = 3054,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407030] = {
  ID = 407030,
  durability = 3972,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407031] = {
  ID = 407031,
  durability = 9025,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[407032] = {
  ID = 407032,
  durability = 6892,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407033] = {
  ID = 407033,
  durability = 2239,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407034] = {
  ID = 407034,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407035] = {
  ID = 407035,
  durability = 4177,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407036] = {
  ID = 407036,
  durability = 13386,
  Avatar = "head36",
  Ship = "ship24"
}
Monsters[407037] = {
  ID = 407037,
  durability = 6893,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407038] = {
  ID = 407038,
  durability = 2239,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[407039] = {
  ID = 407039,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[407040] = {
  ID = 407040,
  durability = 4177,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408001] = {
  ID = 408001,
  durability = 9026,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[408002] = {
  ID = 408002,
  durability = 6892,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408003] = {
  ID = 408003,
  durability = 2238,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408004] = {
  ID = 408004,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408005] = {
  ID = 408005,
  durability = 4178,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408006] = {
  ID = 408006,
  durability = 9025,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[408007] = {
  ID = 408007,
  durability = 6892,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408008] = {
  ID = 408008,
  durability = 2239,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408009] = {
  ID = 408009,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408010] = {
  ID = 408010,
  durability = 4178,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408011] = {
  ID = 408011,
  durability = 9024,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[408012] = {
  ID = 408012,
  durability = 6892,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408013] = {
  ID = 408013,
  durability = 2239,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408014] = {
  ID = 408014,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408015] = {
  ID = 408015,
  durability = 4178,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408016] = {
  ID = 408016,
  durability = 13386,
  Avatar = "head36",
  Ship = "ship12"
}
Monsters[408017] = {
  ID = 408017,
  durability = 6892,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408018] = {
  ID = 408018,
  durability = 2239,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408019] = {
  ID = 408019,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408020] = {
  ID = 408020,
  durability = 4178,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408021] = {
  ID = 408021,
  durability = 9025,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[408022] = {
  ID = 408022,
  durability = 6892,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408023] = {
  ID = 408023,
  durability = 2239,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408024] = {
  ID = 408024,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408025] = {
  ID = 408025,
  durability = 4178,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408026] = {
  ID = 408026,
  durability = 9025,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[408027] = {
  ID = 408027,
  durability = 6892,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408028] = {
  ID = 408028,
  durability = 2239,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408029] = {
  ID = 408029,
  durability = 3208,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408030] = {
  ID = 408030,
  durability = 4177,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408031] = {
  ID = 408031,
  durability = 9486,
  Avatar = "head35",
  Ship = "ship25"
}
Monsters[408032] = {
  ID = 408032,
  durability = 7240,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408033] = {
  ID = 408033,
  durability = 2341,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408034] = {
  ID = 408034,
  durability = 3362,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408035] = {
  ID = 408035,
  durability = 4382,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408036] = {
  ID = 408036,
  durability = 14078,
  Avatar = "head36",
  Ship = "ship24"
}
Monsters[408037] = {
  ID = 408037,
  durability = 7240,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408038] = {
  ID = 408038,
  durability = 2341,
  Avatar = "head35",
  Ship = "ship43"
}
Monsters[408039] = {
  ID = 408039,
  durability = 3362,
  Avatar = "head35",
  Ship = "ship12"
}
Monsters[408040] = {
  ID = 408040,
  durability = 4382,
  Avatar = "head35",
  Ship = "ship12"
}
