local GameSectionSelect = LuaObjectManager:GetLuaObject("GameSectionSelect")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameGlobalData = GameGlobalData
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialFirstChapter = TutorialQuestManager.QuestTutorialFirstChapter
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local GameStateCampaignSelection = GameStateManager.GameStateCampaignSelection
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local QuestTutorialFirstBattle = TutorialQuestManager.QuestTutorialFirstBattle
local GameObjectBattleMap = LuaObjectManager:GetLuaObject("GameObjectBattleMap")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
GameSectionSelect.scaleStart = -200
GameSectionSelect.scaleStep = 200
GameSectionSelect.scalePreId = 2
function GameSectionSelect:OnInitGame()
  self.currentareaId = -1
  self.enterAreaId = -1
  self.enterSectionId = -1
  self.sectionStatus = {}
  self.enterSection = false
  GameGlobalData:RegisterDataChangeCallback("progress", self.OnProgressChange)
end
function GameSectionSelect.OnProgressChange()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameSectionSelect) then
    GameSectionSelect:RefreshBackground()
  end
  GameSectionSelect.maxareaId = GameGlobalData:GetData("progress").act
end
function GameSectionSelect:OnParentStateGetFocus()
  if QuestTutorialBattleMap:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    if immanentversion170 == 4 or immanentversion170 == 5 then
      GameUICommonDialog:PlayStory({1100092}, function()
      end)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
  end
  GameSectionSelect:CheckFirstChapterTutorial()
end
function GameSectionSelect:CheckFirstChapterTutorial()
  if immanentversion170 == 4 or immanentversion170 == 5 then
    DebugOut("GameSectionSelect:CheckFirstChapterTutorial:", QuestTutorialFirstChapter:IsActive(), self.currentareaId)
    if QuestTutorialFirstChapter:IsActive() and self.currentareaId == 1 then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    elseif QuestTutorialFirstChapter:IsFinished() then
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
    end
  end
end
function GameSectionSelect:OnAddToGameState()
  DebugOut("GameSectionSelect:OnAddToGameState")
  GameUtils:PlayMusic("GE2_Map.mp3")
  local progress = GameGlobalData:GetData("progress")
  self.enterAreaId = progress.act
  self.enterSectionId = progress.chapter
  self.maxareaId = progress.act
  self:LoadFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "showSection")
  self:RefreshBackground()
end
function GameSectionSelect:RequestAllActStatus()
  self.maxareaId = GameDataAccessHelper:GetMaxArea()
  local param = {
    act_no = self.maxareaId
  }
  DebugOut("----------------------self.maxareaId: ", self.maxareaId)
  NetMessageMgr:SendMsg(NetAPIList.all_act_status_req.Code, param, self.ActStatusCallback, true)
end
function GameSectionSelect.ActStatusCallback(msgType, content)
  if msgType == NetAPIList.all_act_status_ack.Code then
    GameSectionSelect.sectionStatus = content.acts
    local count = 0
    for k, v in pairs(GameSectionSelect.sectionStatus) do
      count = count + 1
    end
    GameStateManager:SetCurrentGameState(GameStateCampaignSelection)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.all_act_status_req.Code then
    return true
  end
  return false
end
function GameSectionSelect:OnEraseFromGameState()
  self.currentareaId = -1
  self.enterAreaId = -1
  self.enterSectionId = -1
  self.sectionStatus = {}
  self.enterSection = false
  self:UnloadFlashObject()
end
function GameSectionSelect:SetCurrentArea(areaId)
  self.currentareaId = areaId
end
function GameSectionSelect:Update(dt)
  self:GetFlashObject():Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt, self.currentareaId)
end
function GameSectionSelect:OnFSCommand(cmd, arg)
  if cmd == "eraseSectionMenu" and self.enterSection then
    self.enterSection = false
    GameStateBattleMap:EnterSection(self.currentareaId, self.enterSectionId, nil)
  end
  if cmd == "nextAnimOver" or cmd == "prevAnimOver" then
    local progress = GameGlobalData:GetData("progress")
    if cmd == "nextAnimOver" and self.nextAnimStart then
      self.nextAnimStart = false
      self.currentareaId = self.currentareaId + 1
    elseif cmd == "prevAnimOver" and self.prevAnimStart then
      self.prevAnimStart = false
      self.currentareaId = self.currentareaId - 1
    end
    self:RefreshBackground()
    local area_name = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. tostring(self.currentareaId))
    GameUISection:SetTitle(area_name)
  end
  if cmd == "showNextPart" or cmd == "showPrevPart" then
    if self.nextAnimStart then
      self.currentareaId = self.currentareaId + 1
      self.nextAnimStart = false
      self:RefreshBackground()
    elseif self.prevAnimStart then
      self.currentareaId = self.currentareaId - 1
      self.prevAnimStart = false
      self:RefreshBackground()
    end
    if cmd == "showNextPart" then
      self.nextAnimStart = true
      if self.currentareaId == self.maxareaId then
        self.nextAnimStart = false
        self:GetFlashObject():InvokeASCallback("_root", "CancelPartsAnim")
      end
    elseif cmd == "showPrevPart" then
      self.prevAnimStart = true
      if self.currentareaId == 1 then
        self.prevAnimStart = false
        self:GetFlashObject():InvokeASCallback("_root", "CancelPartsAnim")
      end
    end
  end
  if cmd == "selectSection" then
    local sectionId = tonumber(arg)
    if self.enterSection then
      return
    end
    if self.enterAreaId == self.currentareaId and self.enterSectionId == sectionId then
      self.enterSection = true
      if QuestTutorialBattleMap:IsActive() then
        AddFlurryEvent("EnterBattleMapFirstTime", {}, 2)
        if QuestTutorialFirstBattle:IsActive() then
          AddFlurryEvent("EnterSection_1_1", {}, 1)
        end
      elseif self.enterAreaId == 1 and self.enterSectionId == 2 and self.enteredSection2 == nil then
        AddFlurryEvent("EnterSection_1_2", {}, 1)
        self.enteredSection2 = true
      end
      self:GetFlashObject():InvokeASCallback("_root", "hideSection")
      GameUtils:PlaySound("holographic_map_click.mp3")
      return
    end
    if self:CanEnterSection(self.currentareaId, sectionId) then
      self.enterAreaId = self.currentareaId
      self.enterSectionId = sectionId
      self:RefreshBackground()
      GameUtils:PlaySound("holographic_map_switch.mp3")
    end
  end
  if cmd == "receiveReward" then
    local sectionId = tonumber(arg)
    self.rewardSectionId = sectionId
    local param = {
      charpter_id = self.currentareaId * 1000 + sectionId
    }
    NetMessageMgr:SendMsg(NetAPIList.allstars_reward_req.Code, param, GameSectionSelect.ReceiveRewardCallback, true)
  end
  if cmd == "MoveOutRightMenu" then
    GameUIBarRight:MoveOutRightMenu()
  end
  if cmd == "explorePlanetary_pos" and GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "explorePlanetary" then
    local param = LuaUtils:string_split(arg, "\001")
    GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.left, "empty", GameUIMaskLayer.MaskStyle.small)
  end
end
function GameSectionSelect.ReceiveRewardCallback(msgType, content)
  DebugOut("GameSectionSelect.ReceiveRewardCallback   msgType=" .. msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.allstars_reward_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
    end
    return true
  elseif msgType == NetAPIList.allstars_reward_ack.Code then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    for key, v in pairs(content.rewards) do
      local eventText = GameLoader:GetGameText("LC_MENU_STAR_REWARDS_TYPE")
      local numberText = tostring(v.number)
      local nameText = GameHelper:GetAwardText(v.item_type, v.number, v.no)
      local tipText = GameHelper:GetRewardInfoAndEvent("", nameText, eventText)
      GameTip:Show(tipText)
    end
    local statusArray = GameSectionSelect.sectionStatus[GameSectionSelect.currentareaId].status
    for k, v in pairs(statusArray) do
      if v.id == GameSectionSelect.currentareaId * 1000 + GameSectionSelect.rewardSectionId then
        v.level = 5
      end
    end
    GameSectionSelect:RefreshBackground()
    if (immanentversion170 == 4 or immanentversion170 == 5) and QuestTutorialFirstChapter:IsActive() then
      QuestTutorialFirstChapter:SetFinish(true, false)
      GameUICommonDialog:PlayStory({1100093}, function()
        TutorialQuestManager.QuestTutorialFirstReform:SetActive(true, false)
        GameUISection:OnFSCommand("close", "")
      end)
    end
    return true
  end
  return false
end
function GameSectionSelect:RefreshBackground()
  DebugOut("RefreshBackground: ", self.currentareaId, self.maxareaId)
  self.currentareaId = math.max(1, self.currentareaId)
  self.currentareaId = math.min(self.maxareaId, self.currentareaId)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshSection", self.currentareaId, self.maxareaId)
  self:RefreshTopInfo(self.currentareaId)
  self:RefreshProgress(self.currentareaId)
  self:checkHelpTutorial()
  local rewardType = 1
  if (immanentversion170 == 4 or immanentversion170 == 5) and self.currentareaId == 1 then
    rewardType = 2
  end
  local index, contents, boss, name, rewards, status, current = 1, "", "", "", "", "", nil
  local _, _, finish_count, all_count = GameSectionSelect:GetCompletedId()
  local completeText = GameLoader:GetGameText("LC_MENU_SECTION_CHAR_COMPLET") .. " " .. math.floor(finish_count * 100 / all_count) .. "%"
  if self.currentareaId > 1 then
    index, contents, boss, name, status, rewards, current = self:GetSectionContent(self.currentareaId - 1)
    self:GetFlashObject():InvokeASCallback("_root", "RefreshSectionContent", 0, index, contents, boss, name, status, rewards, completeText, current, rewardType)
  end
  if self.currentareaId < self.maxareaId then
    index, contents, boss, name, status, rewards, current = self:GetSectionContent(self.currentareaId + 1)
    self:GetFlashObject():InvokeASCallback("_root", "RefreshSectionContent", 2, index, contents, boss, name, status, rewards, completeText, current, rewardType)
  end
  index, contents, boss, name, status, rewards, current = self:GetSectionContent(self.currentareaId)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshSectionContent", 1, index, contents, boss, name, status, rewards, completeText, current, rewardType)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshScale", self.currentareaId, self.scaleStart, self.scaleStep, self.scalePreId)
end
function GameSectionSelect:checkHelpTutorial(...)
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "explorePlanetary" then
    self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorial", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setHelpTutorial", false)
  end
end
function GameSectionSelect:RefreshTopInfo(areaId)
  local briefingText, bossText, head = "", "", ""
  local compArea, compSection = self:GetCompletedId()
  briefingText = GameLoader:GetGameText("LC_MENU_AREA_BRIEFING_" .. areaId)
  if areaId < compArea then
    bossText = GameLoader:GetGameText("LC_MENU_AREA_BOSS_DES_" .. areaId)
  end
  for i = 1, GameDataAccessHelper:GetMaxSection(areaId) do
    local tempHead = self:GetBossHead(areaId, i)
    if tempHead ~= "" then
      head = tempHead
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "RefreshTopInfo", briefingText, bossText, head)
end
function GameSectionSelect:RefreshProgress(areaId)
  local progressText = ""
  local compArea, compSection = self:GetCompletedId()
  progressText = GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. areaId)
  if areaId < compArea then
    progressText = progressText .. ":100%"
  elseif areaId > compArea then
    progressText = progressText .. ":0%"
  else
    progressText = progressText .. ":" .. math.floor(compSection * 100 / GameDataAccessHelper:GetMaxSection(areaId)) .. "%"
  end
  self:GetFlashObject():InvokeASCallback("_root", "RefreshProgress", progressText)
end
function GameSectionSelect:CanEnterSection(areaId, sectionId)
  local completedareaId, completedSectionId = self:GetCompletedId()
  if areaId < completedareaId or areaId == completedareaId and sectionId <= completedSectionId then
    return true
  end
  return false
end
function GameSectionSelect:GetSectionContent(areaId)
  local completedareaId, completedSectionId = self:GetCompletedId()
  local sectionContent, boss, name, status, rewards, current = "", "", "", "", "", ""
  if areaId < completedareaId then
    for i = 1, GameDataAccessHelper:GetMaxSection(areaId) do
      current = current .. tostring(areaId == self.enterAreaId and i == self.enterSectionId) .. "\001"
      sectionContent = sectionContent .. "pass" .. "\001"
    end
  elseif areaId > completedareaId then
    for i = 1, GameDataAccessHelper:GetMaxSection(areaId) do
      current = current .. tostring(areaId == self.enterAreaId and i == self.enterSectionId) .. "\001"
      sectionContent = sectionContent .. "lock" .. "\001"
    end
  else
    for i = 1, GameDataAccessHelper:GetMaxSection(areaId) do
      current = current .. tostring(areaId == self.enterAreaId and i == self.enterSectionId) .. "\001"
      if completedSectionId > i then
        sectionContent = sectionContent .. "pass" .. "\001"
      elseif completedSectionId < i then
        sectionContent = sectionContent .. "lock" .. "\001"
      else
        sectionContent = sectionContent .. "normal" .. "\001"
      end
    end
  end
  for i = 1, GameDataAccessHelper:GetMaxSection(areaId) do
    boss = boss .. self:GetBossHead(areaId, i) .. "\001"
    name = name .. GameLoader:GetGameText("LC_MENU_SECTION_NAME_" .. areaId .. "_" .. i) .. "\001"
    local level, haveReward = self:GetSectionStatusLevel(areaId, i)
    status = status .. level .. "\001"
    rewards = rewards .. haveReward .. "\001"
  end
  return GameDataAccessHelper:GetSectionType(areaId), sectionContent, boss, name, status, rewards, current
end
function GameSectionSelect:GetSectionStatusLevel(areaId, sectionId)
  if not self.sectionStatus[areaId] then
    return 0
  end
  local statusArray = self.sectionStatus[areaId].status
  for k, v in pairs(statusArray) do
    if v.id == areaId * 1000 + sectionId then
      local level = v.level > 3 and 3 or v.level
      local haveReward = tostring(v.level == 4)
      return level, haveReward
    end
  end
  return 0
end
function GameSectionSelect:GetBossHead(areaId, sectionId)
  local bossHeadArray = GameDataAccessHelper:GetBossHead(areaId)
  local head = bossHeadArray["pos" .. sectionId] or ""
  if head ~= "" and GameDataAccessHelper:IsHeadResNeedDownload(head) then
    if GameDataAccessHelper:CheckFleetHasAvataImage(head) then
      return head
    else
      return "head9001"
    end
  end
  return head
end
function GameSectionSelect:GetCompletedId()
  local progress = GameGlobalData:GetData("progress")
  return progress.act, progress.chapter, progress.finish_count, progress.all_count
end
