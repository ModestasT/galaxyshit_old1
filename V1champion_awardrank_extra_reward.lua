local rank_extra_reward = GameData.champion_award.rank_extra_reward
rank_extra_reward[1] = {
  start_pos = 1,
  end_pos = 1,
  k2_money_basic = 200,
  k2_money_add = 0,
  k2_prestige_basic = 20,
  k2_prestige_add = 0
}
rank_extra_reward[2] = {
  start_pos = 2,
  end_pos = 2,
  k2_money_basic = 170,
  k2_money_add = 0,
  k2_prestige_basic = 19,
  k2_prestige_add = 0
}
rank_extra_reward[3] = {
  start_pos = 3,
  end_pos = 3,
  k2_money_basic = 140,
  k2_money_add = 0,
  k2_prestige_basic = 18,
  k2_prestige_add = 0
}
rank_extra_reward[4] = {
  start_pos = 4,
  end_pos = 4,
  k2_money_basic = 110,
  k2_money_add = 0,
  k2_prestige_basic = 17,
  k2_prestige_add = 0
}
rank_extra_reward[5] = {
  start_pos = 5,
  end_pos = 5,
  k2_money_basic = 100,
  k2_money_add = 0,
  k2_prestige_basic = 17,
  k2_prestige_add = 0
}
rank_extra_reward[9] = {
  start_pos = 9,
  end_pos = 6,
  k2_money_basic = 80,
  k2_money_add = 1250,
  k2_prestige_basic = 16,
  k2_prestige_add = 1500
}
rank_extra_reward[19] = {
  start_pos = 19,
  end_pos = 10,
  k2_money_basic = 70,
  k2_money_add = 500,
  k2_prestige_basic = 15,
  k2_prestige_add = 500
}
rank_extra_reward[49] = {
  start_pos = 49,
  end_pos = 20,
  k2_money_basic = 55,
  k2_money_add = 250,
  k2_prestige_basic = 14,
  k2_prestige_add = 150
}
rank_extra_reward[99] = {
  start_pos = 99,
  end_pos = 50,
  k2_money_basic = 45,
  k2_money_add = 100,
  k2_prestige_basic = 13,
  k2_prestige_add = 90
}
rank_extra_reward[199] = {
  start_pos = 199,
  end_pos = 100,
  k2_money_basic = 35,
  k2_money_add = 50,
  k2_prestige_basic = 12,
  k2_prestige_add = 45
}
rank_extra_reward[499] = {
  start_pos = 499,
  end_pos = 200,
  k2_money_basic = 20,
  k2_money_add = 25,
  k2_prestige_basic = 11,
  k2_prestige_add = 15
}
rank_extra_reward[999] = {
  start_pos = 999,
  end_pos = 500,
  k2_money_basic = 10,
  k2_money_add = 10,
  k2_prestige_basic = 10,
  k2_prestige_add = 9
}
rank_extra_reward[4999] = {
  start_pos = 4999,
  end_pos = 1000,
  k2_money_basic = 2,
  k2_money_add = 1,
  k2_prestige_basic = 2,
  k2_prestige_add = 8
}
rank_extra_reward[999999] = {
  start_pos = 999999,
  end_pos = 5000,
  k2_money_basic = 1,
  k2_money_add = 0,
  k2_prestige_basic = 1,
  k2_prestige_add = 0
}
