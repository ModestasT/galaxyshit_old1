local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStateStarCraft = GameStateManager.GameStateStarCraft
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GASSIONS_PER_PAGE = 5
GameUIStarCraftDetail = {}
GameUIStarCraftDetail.flash_obj = nil
GameUIStarCraftDetail.mDetailInfo = nil
GameUIStarCraftDetail.mOccupierList = nil
GameUIStarCraftDetail.mCurPointID = nil
GameUIStarCraftDetail.mCurrentOccupierIndex = -1
GameUIStarCraftDetail.mPlayerInfoPanelOpened = false
GameUIStarCraftDetail.mCurrentDisplayIndex = 1
GameUIStarCraftDetail.mCurrentPageIndex = 1
GameUIStarCraftDetail.mTotalPages = 1
local OcuppieMCIndex = {
  Index_Left = 0,
  Index_Mid = 1,
  Index_Right = 2
}
GameUIStarCraftDetail.mTotalNum = 0
GameUIStarCraftDetail.mBattleList = nil
GameUIStarCraftDetail.mCurTeleportPrice = 0
GameUIStarCraftDetail.mRank = {}
GameUIStarCraftDetail.mRank.CurrentRank = 1
GameUIStarCraftDetail.mRank.WorldRank = {}
GameUIStarCraftDetail.mRank.ServerRank = {}
GameUIStarCraftDetail.mRank.ownWorldRank = {}
GameUIStarCraftDetail.mRank.ownServerRank = {}
GameUIStarCraftDetail.mReward = {}
GameUIStarCraftDetail.mReward.CurrentReward = 1
GameUIStarCraftDetail.mReward.RankReward = {}
GameUIStarCraftDetail.mReward.KingReward = {}
GameUIStarCraftDetail.mHistory = {}
GameUIStarCraftDetail.mHistory.top3Player = {}
GameUIStarCraftDetail.mHistory.top3Team = {}
GameUIStarCraftDetail.mHistory.inHistory = false
GameUIStarCraftDetail.mKingRewardLeftTime = -1
GameUIStarCraftDetail.mKingRewardLeftTimeFetchTime = -1
GameUIStarCraftDetail.mCurKingRewardLeftTime = -1
GameUIStarCraftDetail.mIsRewardPanelShow = false
GameUIStarCraftDetail.buffListData = nil
local BATTLE_REPORT_TYPE = {
  BATTLE_LOG_TYPE_LOST_KEYPOINT = 1,
  BATTLE_LOG_TYPE_ATTACK_KEYPOINT = 2,
  BATTLE_LOG_TYPE_OCCUPY_KEYPOINT = 3,
  BATTLE_LOG_TYPE_ATTACK_KEYPOINT_MY = 4,
  BATTLE_LOG_TYPE_OCCUPY_KEYPOINT_MY = 5,
  BATTLE_LOG_TYPE_BATTLE_GREAT_WIN = 6,
  BATTLE_LOG_TYPE_BATTLE_GOOD_WIN = 7,
  BATTLE_LOG_TYPE_BATTLE_WIN = 8,
  BATTLE_LOG_TYPE_BATTLE_LOSE = 9
}
local LocalizationReplaceOrder = {
  [100] = {"<name>"},
  [111] = {"<name>", "<hp_num>"},
  [112] = {"<name>", "<hp_num>"},
  [113] = {"<name>", "<hp_num>"},
  [200] = {
    "<judian>",
    "<shili>",
    "<name>"
  },
  [210] = {"<name>", "<judian>"},
  [211] = {
    "<name>",
    "<shili>",
    "<judian>"
  },
  [220] = {"<judian>"},
  [221] = {"<shili>", "<judian>"},
  [1] = {"<player>", "<nothing>"},
  [2] = {
    "<end_player>",
    "<kill_player>",
    "<nothing>"
  },
  [3] = {"<player>", "<nothing>"},
  [4] = {"<player>", "<nothing>"}
}
function GameUIStarCraftDetail:Init(flash_obj)
  self.flash_obj = flash_obj
end
function GameUIStarCraftDetail:RequestKeyPointDetailData(keyPointID)
  GameUIStarCraftDetail.mCurPointID = keyPointID
  local content = {dot_id = keyPointID, page = 1}
  GameUIStarCraftDetail.mCurrentPageIndex = 1
  NetMessageMgr:SendMsg(NetAPIList.contention_stop_req.Code, content, GameUIStarCraftDetail.RequestKeyPointDetailDataCallback, true, nil)
end
function GameUIStarCraftDetail:RequestNextOcuppoers(keyPointID, pageIndex)
  GameUIStarCraftDetail.mCurPointID = keyPointID
  local content = {
    dot_id = GameUIStarCraftDetail.mCurPointID,
    page = pageIndex
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_stop_req.Code, content, GameUIStarCraftDetail.RequestNextOcuppoersCallback, true, nil)
end
function GameUIStarCraftDetail.RequestNextOcuppoersCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.contention_stop_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.contention_stop_ack.Code then
    if content ~= nil then
      local kingId = tostring(content.king_id)
      for i, v in ipairs(content.occupiers) do
        local occupier = {}
        occupier.mID = v.user_id
        occupier.mName = v.user_name
        occupier.sexFrame = GameDataAccessHelper:GetFleetAvatar(tonumber(v.icon), v.main_fleet_level, v.sex)
        if occupier.mID == kingId then
          occupier.mIsKing = true
        else
          occupier.mIsKing = false
        end
        table.insert(GameUIStarCraftDetail.mOccupierList, occupier)
      end
      GameUIStarCraftDetail:NextGarrisons()
    end
    return true
  end
  return false
end
function GameUIStarCraftDetail.RequestKeyPointDetailDataCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.contention_stop_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.contention_stop_ack.Code then
    if content ~= nil then
      GameUIStarCraftDetail.mOccupierList = nil
      GameUIStarCraftDetail.mTotalNum = content.max_num
      GameUIStarCraftDetail.mOccupierList = {}
      local kingId = tostring(content.king_id)
      for i, v in ipairs(content.occupiers) do
        local occupier = {}
        occupier.mID = v.user_id
        occupier.mName = v.user_name
        occupier.sexFrame = GameDataAccessHelper:GetFleetAvatar(tonumber(v.icon), v.main_fleet_level, v.sex)
        occupier.mIsKing = false
        if occupier.mID == kingId then
          occupier.mIsKing = true
        else
          occupier.mIsKing = false
        end
        table.insert(GameUIStarCraftDetail.mOccupierList, occupier)
      end
      local flash = GameUIStarCraftDetail.flash_obj
      if flash then
        flash:InvokeASCallback("_root", "showKeyPointDetail", true, GameUIStarCraftDetail.mCurPointID)
        local colorFrame = GameUIStarCraftMap:GetDetailInfoColorFrameByPointID(GameUIStarCraftDetail.mCurPointID)
        local txt = ""
        if colorFrame == "red" then
          txt = GameLoader:GetGameText("LC_MENU_TC_ATTACK_BUTTON")
        elseif colorFrame == "green" then
          txt = GameLoader:GetGameText("LC_MENU_TC_MOVE_BUTTON")
        elseif colorFrame == "blue" then
          txt = GameLoader:GetGameText("LC_MENU_TC_OCCUPY_BUTTON")
        end
        local curEvent = GameUIStarCraftMap:GetEventByPointID(GameUIStarCraftDetail.mCurPointID)
        local eventState = 0
        if curEvent then
          eventState = 1
        end
        flash:InvokeASCallback("_root", "setUpKeyPointDetailBG", colorFrame, txt)
        flash:InvokeASCallback("_root", "setKeyPointDetailName", GameUIStarCraftMap:GetPointNameByPointID(GameUIStarCraftDetail.mCurPointID))
        flash:InvokeASCallback("_root", "setKeyPointDetailInfoIcon", GameUIStarCraftMap:GetPointTypeByID(GameUIStarCraftDetail.mCurPointID), GameUIStarCraftMap:GetServerDisplayNameByPointID(GameUIStarCraftDetail.mCurPointID), GameUIStarCraftMap:GetColorFrameByPointID(GameUIStarCraftDetail.mCurPointID), eventState)
        flash:InvokeASCallback("_root", "SetKeyPointAllPower", GameUtils.numberAddComma(content.all_power))
        flash:InvokeASCallback("_root", "setKeyPointDetailOccupierNum", content.max_num)
        local teleportText = GameLoader:GetGameText("LC_MENU_TC_TELEPORT_BUTTON")
        DebugOut("teleportText", teleportText)
        local isEnemyCamp = GameUIStarCraftMap:IsEnemyCamp(GameUIStarCraftDetail.mCurPointID)
        if isEnemyCamp then
          flash:InvokeASCallback("_root", "setKeyPointMoveButton", "", true)
          flash:InvokeASCallback("_root", "setKeyPointTeleportButton", true, teleportText)
        else
          flash:InvokeASCallback("_root", "setKeyPointTeleportButton", false, teleportText)
          if GameUIStarCraftMap:IsMySelfOnPath() then
            flash:InvokeASCallback("_root", "setKeyPointMoveButton", "", true)
          else
            local startID = GameUIStarCraftMap:GetMyPointID()
            DebugOut("startID = ", startID)
            DebugOut("GameUIStarCraftDetail.mCurPointID = ", GameUIStarCraftDetail.mCurPointID)
            if startID == GameUIStarCraftDetail.mCurPointID then
              flash:InvokeASCallback("_root", "setKeyPointMoveButton", "", true)
              flash:InvokeASCallback("_root", "setKeyPointTeleportButton", true, teleportText)
            else
              local pathList = GameUIStarCraftMap:CalculaeMarchPath(startID, GameUIStarCraftDetail.mCurPointID, false)
              DebugOut("pathList = ")
              DebugTable(pathList)
              if pathList then
                local totalLen = 0
                for k, v in pairs(pathList) do
                  local path = GameUIStarCraftMap.MapPathList[v.line_id]
                  totalLen = totalLen + path.mDistance
                end
                DebugOut("totalLen = ", totalLen)
                local speed = GameUIStarCraftMap:GetMySpeed()
                local time = math.floor(totalLen / speed)
                time = GameUtils:formatTimeString(time)
                flash:InvokeASCallback("_root", "setKeyPointMoveButton", time, false)
              end
            end
          end
        end
        local event = GameUIStarCraftMap:GetEventByPointID(GameUIStarCraftDetail.mCurPointID)
        if event and event.mActive then
          DebugOut("event = ")
          DebugTable(event)
          GameUIStarCraftDetail:SetEventKeyPointData(event)
        else
          GameUIStarCraftDetail:SetKeyPointBuffList()
        end
        GameUIStarCraftDetail:SetKeyPointTitleByType(GameUIStarCraftMap:GetPointTypeByID(GameUIStarCraftDetail.mCurPointID))
      end
      GameUIStarCraftDetail.mTotalPages = math.ceil(GameUIStarCraftDetail.mTotalNum / GASSIONS_PER_PAGE)
      GameUIStarCraftDetail:RefreshGarrisons(GameUIStarCraftDetail.mCurrentPageIndex, OcuppieMCIndex.Index_Mid)
      GameUIStarCraftDetail:SetOcuppierArrowVisible()
    end
    return true
  end
  return false
end
function GameUIStarCraftDetail:SetKeyPointTitleByType(pointType)
  local text = ""
  if pointType == EKeyPointType.Type_White_Hole or pointType == EKeyPointType.Type_Boat_Yard then
    text = GameLoader:GetGameText("LC_MENU_TC_BONUS_DESC_3")
  elseif pointType == EKeyPointType.Type_Missile then
    local event = GameUIStarCraftMap:GetEventByPointID(GameUIStarCraftDetail.mCurPointID)
    if event and event.mActive then
      local target = event:GetEventTarget()
      local time = event:GetMissileTime() .. "S"
      local name = GameUIStarCraftMap:GetPointNameByPointID(tonumber(target))
      text = GameLoader:GetGameText("LC_MENU_TC_BONUS_DESC_5_1")
      text = string.gsub(text, "<point>", name)
      text = string.gsub(text, "<time>", time)
    else
      text = GameLoader:GetGameText("LC_MENU_TC_BONUS_DESC_5_2")
    end
  else
    text = GameLoader:GetGameText("LC_MENU_TC_BONUS_DESC_1")
  end
  DebugOut("point title text = ", text)
  local flash = GameUIStarCraftDetail.flash_obj
  if flash then
    flash:InvokeASCallback("_root", "setKeyPointTitle", text)
  end
end
function GameUIStarCraftDetail:CalculateTeleportPrice()
  local startID = GameUIStarCraftMap:GetMyStartPointID()
  if GameUIStarCraftMap:IsMySelfOnPath() and startID == GameUIStarCraftDetail.mCurPointID then
    startID = GameUIStarCraftMap:GetMyCurrentPathEndPoint()
  end
  local pathList = GameUIStarCraftMap:CalculaeMarchPath(startID, GameUIStarCraftDetail.mCurPointID, true)
  if pathList then
    local totalLen = 0
    for k, v in pairs(pathList) do
      local path = GameUIStarCraftMap.MapPathList[v.line_id]
      totalLen = totalLen + path.mDistance
    end
    local speed = GameUIStarCraftMap:GetMySpeed()
    local time = totalLen / speed
    time = math.ceil(time / 60)
    GameUIStarCraftDetail.mCurTeleportPrice = time * GameStateStarCraft.mTeleportPrice
  else
    GameUIStarCraftDetail.mCurTeleportPrice = 0
  end
end
function GameUIStarCraftDetail:OnFSCommand(cmd, arg)
  if cmd == "needUpdateGarrisonItem" then
    GameUIStarCraftDetail:UpdateGarrisonItem(tonumber(arg))
  elseif cmd == "GarrisonItemReleased" then
    local ocuppoerIndex = GameUIStarCraftDetail.mCurrentPageIndex * GASSIONS_PER_PAGE - GASSIONS_PER_PAGE + tonumber(arg)
    if ocuppoerIndex <= #GameUIStarCraftDetail.mOccupierList then
      GameUIStarCraftDetail.mCurrentOccupierIndex = ocuppoerIndex
      GameUIStarCraftDetail:RequestPlayerInfo()
    end
  elseif cmd == "closePlayerInfo" then
    GameUIStarCraftDetail:ClosePlayerInfo()
    GameUIStarCraftDetail.mPlayerInfoPanelOpened = false
    GameUIStarCraftDetail.mCurrentOccupierIndex = -1
  elseif cmd == "playerinfoCloseFinish" then
  elseif cmd == "nextPlayer" then
    GameUIStarCraftDetail:NextPlayer()
  elseif cmd == "prePlayer" then
    GameUIStarCraftDetail:PrePlayer()
  elseif cmd == "needUpdateBattleItem" then
    GameUIStarCraftDetail:UpdateBattleItem(tonumber(arg))
  elseif cmd == "battleReport" then
    GameUIStarCraftDetail:RequestBattleReport()
    local flash = GameUIStarCraftDetail.flash_obj
    if flash then
      flash:InvokeASCallback("_root", "showBattleReportPanel")
      GameUIStarCraftDetail.IsShowBattleReportWindow = true
    end
  elseif cmd == "wantTeleport" then
    if GameUIStarCraftMap:IsMySelfDie() then
      local errorContent = GameLoader:GetGameText("LC_ALERT_tc_player_already_die")
      GameTip:Show(errorContent)
    else
      GameUIStarCraftDetail:CalculateTeleportPrice()
      if GameUIStarCraftDetail.mCurTeleportPrice == 0 then
        local errorContent = GameLoader:GetGameText("LC_ALERT_tc_jump_path_error")
        GameTip:Show(errorContent)
      else
        GameUIStarCraftMap:showCostConfirmPanel(GameUIStarCraftDetail.mCurTeleportPrice, "LC_MENU_TC_TELEPORT_CONFIRM_ALERT", GameUIStarCraftDetail.teleportHandler)
      end
    end
  elseif cmd == "BattleReplay" then
    DebugOut("arg ", arg)
    GameUIStarCraftDetail:BattleReplay(tonumber(arg))
  elseif cmd == "prevGarrisons" then
    GameUIStarCraftDetail:PreGarrisons()
  elseif cmd == "nextGarrisons" then
    GameUIStarCraftDetail:NextGarrisons()
  elseif cmd == "Rank" then
    GameUIStarCraftDetail:RequestRank()
    local flash = GameUIStarCraftDetail.flash_obj
    if flash then
      flash:InvokeASCallback("_root", "ShowRankPanel")
      GameUIStarCraftDetail.IsShowRankWindow = true
    end
  elseif cmd == "Rewards" then
    GameUIStarCraftDetail.mReward.CurrentReward = 1
    GameUIStarCraftDetail.mKingRewardLeftTimeFetchTime = -1
    GameUIStarCraftDetail:RequestReward()
    GameUIStarCraftDetail:RequestKingReward()
    local flash = GameUIStarCraftDetail.flash_obj
    if flash then
      flash:InvokeASCallback("_root", "ShowRewardPanel")
      GameUIStarCraftDetail.mIsRewardPanelShow = true
    end
  elseif cmd == "needUpdateRankItem" then
    GameUIStarCraftDetail:UpdateRankItem(tonumber(arg))
  elseif cmd == "NeedUpdateRewardItem" then
    GameUIStarCraftDetail:UpdateRewardItem(tonumber(arg))
  elseif cmd == "chooseTab" then
    GameUIStarCraftDetail.mRank.CurrentRank = tonumber(arg)
    DebugOut("come in chooseTab ")
    GameUIStarCraftDetail:SetOwnRank()
    GameUIStarCraftDetail:RefreshRank()
  elseif cmd == "History" then
    GameUIStarCraftDetail:RequestHistory()
  elseif cmd == "LeaveHistory" then
    GameUIStarCraftDetail.mHistory.inHistory = false
  elseif cmd == "ReplaceSeason" then
    DebugOut("ReplaceSeason")
    GameUIStarCraftDetail:SetSeasons(false, false)
    GameUIStarCraftDetail:RequestHistory()
  elseif cmd == "ReplaceRightSeason" then
    DebugOut("ReplaceRightSeason")
    GameUIStarCraftDetail:SetSeasons(false, false)
    GameUIStarCraftDetail:RequestHistory()
  elseif cmd == "ReplaceSeasonlm" then
    GameUIStarCraftMap:NextSeason()
    GameUIStarCraftDetail:SetSeasons(true, false)
  elseif cmd == "ReplaceSeasonmr" then
    GameUIStarCraftMap:BeforeSeason()
    GameUIStarCraftDetail:SetSeasons(false, true)
  elseif cmd == "awardItemReleased" then
    local awardItemIndex, awardItemSubIndex = arg:match("(%d+)\001(%d+)")
    DebugOut("item index = awardItemIndex, awardItemSubItemIndex", awardItemIndex, awardItemSubIndex)
    local clickedItem = GameUIStarCraftDetail:GetSubRewardItem(tonumber(awardItemIndex), tonumber(awardItemSubIndex))
    GameUIStarCraftDetail:ShowItemDetil(clickedItem, clickedItem.item_type)
  elseif cmd == "chooseRewardTab" then
    GameUIStarCraftDetail.mReward.CurrentReward = tonumber(arg)
    GameUIStarCraftDetail:RefreshReward()
  elseif cmd == "RewardPanelClose" then
    GameUIStarCraftDetail.mIsRewardPanelShow = false
  elseif cmd == "nextAnimWouldOver" then
    GameUIStarCraftDetail:RefreshGarrisons(GameUIStarCraftDetail.mCurrentPageIndex, OcuppieMCIndex.Index_Mid)
  elseif cmd == "prevAnimWouldOver" then
    GameUIStarCraftDetail:RefreshGarrisons(GameUIStarCraftDetail.mCurrentPageIndex, OcuppieMCIndex.Index_Mid)
  elseif cmd == "KingRewardItemReleased" then
    local clickedItem = GameUIStarCraftDetail:GetKingRewardItem(tonumber(arg))
    if clickedItem then
      GameUIStarCraftDetail:ShowItemDetil(clickedItem, clickedItem.item_type)
    end
  elseif cmd == "BonusSumItemReleased" then
    local itemKey = tonumber(arg)
    local itemInfo = GameUIStarCraftDetail.mCollectResList[itemKey]
    local gameItem = GameUIStarCraftDetail:MakeGameItemFormCollectResItem(itemInfo)
    if gameItem then
      GameUIStarCraftDetail:ShowItemDetil(gameItem, gameItem.item_type)
    end
  elseif cmd == "needUpdateCollectResItem" then
    GameUIStarCraftDetail:UpdateCollectResItem(tonumber(arg))
  elseif cmd == "hideCollectResMenu" then
    GameUIStarCraftDetail.IsShowCollectMenu = false
  elseif cmd == "HideRankWindow" then
    GameUIStarCraftDetail.IsShowRankWindow = false
  elseif cmd == "HideBattleReportWindow" then
    GameUIStarCraftDetail.IsShowBattleReportWindow = false
  elseif cmd == "update_buff_item" then
    GameUIStarCraftDetail:UpdateBuffPoint(tonumber(arg))
  end
end
function GameUIStarCraftDetail:UpdateBuffPoint(itemIndex)
  if GameUIStarCraftDetail.buffListData then
    local data = GameUIStarCraftDetail.buffListData[itemIndex]
    if GameUIStarCraftDetail.flash_obj then
      GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setKeyPointDetailInfoBuffItem", data, itemIndex)
    end
  end
end
function GameUIStarCraftDetail:SetSeasons(isLeftTwo, isRightTwo)
  local currentSeasonIndex = 1
  local flash = GameUIStarCraftDetail.flash_obj
  local seasonLeftTimeBegin = ""
  local seasonLeftTimeEnd = ""
  local seasonLeftTime = ""
  local seasonMiddleTimeBegin = ""
  local seasonMiddleTimeEnd = ""
  local seasonMiddleTime = ""
  local seasonRightTimeBegin = ""
  local seasonRightTimeEnd = ""
  local seasonRightTime = ""
  local seasonLeftTitle = ""
  local seasonMiddleTitle = ""
  local seasonRightTitle = ""
  for i = 1, #GameUIStarCraftMap.allSeason do
    if GameUIStarCraftMap.allSeason[i].id == GameUIStarCraftMap.currentShowSeason.id then
      currentSeasonIndex = i
      GameUIStarCraftMap.currentSeasonIndex = currentSeasonIndex
      DebugOut("GameUIStarCraftMap.currentSeasonIndex", currentSeasonIndex)
    end
  end
  if currentSeasonIndex - 1 > 0 then
    seasonLeftTimeBegin = os.date("%Y.%m.%d", GameUIStarCraftMap.allSeason[currentSeasonIndex - 1].start_time)
    seasonLeftTimeEnd = os.date("%Y.%m.%d", GameUIStarCraftMap.allSeason[currentSeasonIndex - 1].end_time)
    seasonLeftTime = seasonLeftTimeBegin .. "-" .. seasonLeftTimeEnd
    seasonLeftTitle = GameLoader:GetGameText("LC_MENU_TC_SEASON_LABEL")
    seasonLeftTitle = string.format(seasonLeftTitle, GameUIStarCraftMap.allSeason[currentSeasonIndex - 1].id)
  end
  seasonMiddleTimeBegin = os.date("%Y.%m.%d", GameUIStarCraftMap.allSeason[currentSeasonIndex].start_time)
  seasonMiddleTimeEnd = os.date("%Y.%m.%d", GameUIStarCraftMap.allSeason[currentSeasonIndex].end_time)
  seasonMiddleTime = seasonMiddleTimeBegin .. "-" .. seasonMiddleTimeEnd
  seasonMiddleTitle = GameLoader:GetGameText("LC_MENU_TC_SEASON_LABEL")
  seasonMiddleTitle = string.format(seasonMiddleTitle, GameUIStarCraftMap.allSeason[currentSeasonIndex].id)
  DebugOut("seasonMiddleTitle id = ", GameUIStarCraftMap.allSeason[currentSeasonIndex].id)
  if currentSeasonIndex + 1 <= #GameUIStarCraftMap.allSeason then
    seasonRightTimeBegin = os.date("%Y.%m.%d", GameUIStarCraftMap.allSeason[currentSeasonIndex + 1].start_time)
    seasonRightTimeEnd = os.date("%Y.%m.%d", GameUIStarCraftMap.allSeason[currentSeasonIndex + 1].end_time)
    seasonRightTime = seasonRightTimeBegin .. "-" .. seasonRightTimeEnd
    seasonRightTitle = GameLoader:GetGameText("LC_MENU_TC_SEASON_LABEL")
    seasonRightTitle = string.format(seasonRightTitle, GameUIStarCraftMap.allSeason[currentSeasonIndex + 1].id)
  end
  if currentSeasonIndex == 1 then
    flash:InvokeASCallback("_root", "HideHistoryLeftArrow")
  end
  if currentSeasonIndex == #GameUIStarCraftMap.allSeason then
    flash:InvokeASCallback("_root", "HideHistoryRightArrow")
  end
  if currentSeasonIndex > 1 then
    flash:InvokeASCallback("_root", "ShowHistoryLeftArrow")
  end
  if currentSeasonIndex < #GameUIStarCraftMap.allSeason then
    flash:InvokeASCallback("_root", "ShowHistoryRightArrow")
  end
  if flash then
    flash:InvokeASCallback("_root", "SetMiddleSeason", seasonMiddleTime, seasonMiddleTitle)
    if not isRightTwo then
      flash:InvokeASCallback("_root", "SetLeftSeason", seasonLeftTime, seasonLeftTitle)
    end
    if not isLeftTwo then
      flash:InvokeASCallback("_root", "SetRightSeason", seasonRightTime, seasonRightTitle)
    end
  end
end
function GameUIStarCraftDetail:RequestHistory()
  local content = {
    season = GameUIStarCraftMap.currentShowSeason.id
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_history_req.Code, content, GameUIStarCraftDetail.RequestHistoryCallback, true, nil)
end
function GameUIStarCraftDetail.RequestHistoryCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_history_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_history_ack.Code then
    if content ~= nil then
      if content.season == 0 then
        local errorContent = GameLoader:GetGameText("LC_ALERT_TC_HISTORY_NO_RECORD_ALERT")
        GameTip:Show(errorContent)
        return true
      end
      GameUIStarCraftDetail.mHistory.top3Player = content.top3player
      GameUIStarCraftDetail.mHistory.top3Team = content.top3team
    end
    local flash = GameUIStarCraftDetail.flash_obj
    if flash and GameUIStarCraftDetail.mHistory.inHistory == false then
      flash:InvokeASCallback("_root", "ShowHistoryPanel", GameGlobalData.starCraftData.type)
      GameUIStarCraftDetail.mHistory.inHistory = true
    end
    GameUIStarCraftDetail:SetSeasons(false, false)
    GameUIStarCraftDetail:RefreshHistory()
    return true
  end
  return false
end
function GameUIStarCraftDetail:GetColorFrameByServerID(serverID)
  local ownServerID = tonumber(GameUIStarCraftMap:GetMyselfServerID())
  local ownServerIDIndex = 1
  local maxServerID = 1
  local minServerID = 1
  for i = 1, 3 do
    if ownServerID == tonumber(GameUIStarCraftDetail.mHistory.top3Team[i].server_id) then
      ownServerIDIndex = i
    end
  end
  local remainOneIndex = (ownServerIDIndex + 1) % 3
  if remainOneIndex == 0 then
    remainOneIndex = 3
  end
  local remainTwoIndex = (ownServerIDIndex + 2) % 3
  if remainTwoIndex == 0 then
    remainTwoIndex = 3
  end
  if GameUIStarCraftDetail.mHistory.top3Team[remainOneIndex].server_id > GameUIStarCraftDetail.mHistory.top3Team[remainTwoIndex].server_id then
    maxServerID = GameUIStarCraftDetail.mHistory.top3Team[remainOneIndex].server_id
    minServerID = GameUIStarCraftDetail.mHistory.top3Team[remainTwoIndex].server_id
  else
    minServerID = GameUIStarCraftDetail.mHistory.top3Team[remainOneIndex].server_id
    maxServerID = GameUIStarCraftDetail.mHistory.top3Team[remainTwoIndex].server_id
  end
  if serverID == ownServerID then
    return "green"
  elseif serverID == maxServerID then
    return "purple"
  else
    return "red"
  end
end
function GameUIStarCraftDetail:RefreshHistory()
  local top1Frame = GameUIStarCraftDetail:GetColorFrameByServerID(GameUIStarCraftDetail.mHistory.top3Team[1].server_id)
  local top2Frame = GameUIStarCraftDetail:GetColorFrameByServerID(GameUIStarCraftDetail.mHistory.top3Team[2].server_id)
  local top3Frame = GameUIStarCraftDetail:GetColorFrameByServerID(GameUIStarCraftDetail.mHistory.top3Team[3].server_id)
  local top1Name = GameUIStarCraftDetail.mHistory.top3Team[1].server
  local top2Name = GameUIStarCraftDetail.mHistory.top3Team[2].server
  local top3Name = GameUIStarCraftDetail.mHistory.top3Team[3].server
  local top1_txt = "TOP1"
  local top2_txt = "TOP2"
  local top3_txt = "TOP3"
  local flash = GameUIStarCraftDetail.flash_obj
  if flash then
    flash:InvokeASCallback("_root", "SetTop3Team", top1_txt, top2_txt, top3_txt, top1Frame, top2Frame, top3Frame, top1Name, top2Name, top3Name)
  end
  if GameUIStarCraftDetail.mHistory.top3player == nil then
    local name1 = ""
    local name2 = ""
    local name3 = ""
    local name4 = ""
    local server1 = ""
    local server2 = ""
    local server3 = ""
    local server4 = ""
    local layer1 = ""
    local layer2 = ""
    local layer3 = ""
    local layer4 = ""
    if GameGlobalData.starCraftData.type == 1 then
      flash:InvokeASCallback("_root", "SetTop4Player", name1, name2, name3, name4, server1, server2, server3, server4, layer1, layer2, layer3, layer4)
    else
      flash:InvokeASCallback("_root", "SetTop4Player2", name1, name2, name3, name4, server1, server2, server3, server4, layer1, layer2, layer3, layer4)
    end
  end
  if #GameUIStarCraftDetail.mHistory.top3Player == 4 then
    local name1 = GameUIStarCraftDetail.mHistory.top3Player[1].name
    local name2 = GameUIStarCraftDetail.mHistory.top3Player[2].name
    local name3 = GameUIStarCraftDetail.mHistory.top3Player[3].name
    local name4 = GameUIStarCraftDetail.mHistory.top3Player[4].name
    local server1 = GameUIStarCraftDetail.mHistory.top3Player[1].server
    local server2 = GameUIStarCraftDetail.mHistory.top3Player[2].server
    local server3 = GameUIStarCraftDetail.mHistory.top3Player[3].server
    local server4 = GameUIStarCraftDetail.mHistory.top3Player[4].server
    local layer1 = GameUIStarCraftDetail.mHistory.top3Player[1].point
    local layer2 = GameUIStarCraftDetail.mHistory.top3Player[2].point
    local layer3 = GameUIStarCraftDetail.mHistory.top3Player[3].point
    local layer4 = GameUIStarCraftDetail.mHistory.top3Player[4].point
    if GameGlobalData.starCraftData.type == 1 then
      flash:InvokeASCallback("_root", "SetTop4Player", name1, name2, name3, name4, server1, server2, server3, server4, layer1, layer2, layer3, layer4)
    else
      flash:InvokeASCallback("_root", "SetTop4Player2", name1, name2, name3, name4, server1, server2, server3, server4, layer1, layer2, layer3, layer4)
    end
  end
  if #GameUIStarCraftDetail.mHistory.top3Player == 3 then
    local name1 = GameUIStarCraftDetail.mHistory.top3Player[1].name
    local name2 = GameUIStarCraftDetail.mHistory.top3Player[2].name
    local name3 = GameUIStarCraftDetail.mHistory.top3Player[3].name
    local name4 = ""
    local server1 = GameUIStarCraftDetail.mHistory.top3Player[1].server
    local server2 = GameUIStarCraftDetail.mHistory.top3Player[2].server
    local server3 = GameUIStarCraftDetail.mHistory.top3Player[3].server
    local server4 = ""
    local layer1 = GameUIStarCraftDetail.mHistory.top3Player[1].point
    local layer2 = GameUIStarCraftDetail.mHistory.top3Player[2].point
    local layer3 = GameUIStarCraftDetail.mHistory.top3Player[3].point
    local layer4 = ""
    if GameGlobalData.starCraftData.type == 1 then
      flash:InvokeASCallback("_root", "SetTop4Player", name1, name2, name3, name4, server1, server2, server3, server4, layer1, layer2, layer3, layer4)
    else
      flash:InvokeASCallback("_root", "SetTop4Player2", name1, name2, name3, name4, server1, server2, server3, server4, layer1, layer2, layer3, layer4)
    end
  end
end
function GameUIStarCraftDetail:UpdateRewardItem(id)
  local itemKey = tonumber(id)
  local rewardRow
  if GameUIStarCraftDetail.mReward.CurrentReward == 1 then
    if itemKey > #GameUIStarCraftDetail.mReward.RankReward then
      return
    else
      rewardRow = GameUIStarCraftDetail.mReward.RankReward[itemKey]
      local rankTxt = GameLoader:GetGameText("LC_MENU_TC_REWARD_PERSONAL")
      rankTxt = string.format(rankTxt, rewardRow.max)
      local iconArray = ""
      local nameArray = ""
      local numberArray = ""
      for i = 1, #rewardRow.awards do
        local frame = GameHelper:GetCommonIconFrame(rewardRow.awards[i], GameUIStarCraftDetail.ReplaceCommonIcon)
        iconArray = iconArray .. frame .. "\001"
        local nametxt = GameHelper:GetAwardTypeText(rewardRow.awards[i].item_type, rewardRow.awards[i].number)
        nameArray = nameArray .. nametxt .. "\001"
        if rewardRow.awards[i].item_type == "krypton" or rewardRow.awards[i].item_type == "item" then
          numberArray = numberArray .. "x" .. rewardRow.awards[i].no .. "\001"
        else
          numberArray = numberArray .. "x" .. rewardRow.awards[i].number .. "\001"
        end
      end
      GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setRewardItem", itemKey, rankTxt, iconArray, nameArray, numberArray)
    end
  elseif GameUIStarCraftDetail.mReward.CurrentReward == 2 then
    DebugTable(GameUIStarCraftDetail.mReward.KingReward)
    if itemKey > #GameUIStarCraftDetail.mReward.KingReward[1].awards then
      return
    else
      rewardRow = GameUIStarCraftDetail.mReward.KingReward[1]
      local iconFrame = GameHelper:GetCommonIconFrame(rewardRow.awards[itemKey], GameUIStarCraftDetail.ReplaceCommonIcon)
      local itemName = GameHelper:GetAwardTypeText(rewardRow.awards[itemKey].item_type, rewardRow.awards[itemKey].number)
      local itemNum = ""
      if rewardRow.awards[itemKey].item_type == "krypton" or rewardRow.awards[itemKey].item_type == "item" then
        itemNum = rewardRow.awards[itemKey].no
      else
        itemNum = rewardRow.awards[itemKey].number
      end
      GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setKingRewardItem", itemKey, itemName, itemNum, iconFrame)
    end
  end
end
function GameUIStarCraftDetail.ReplaceCommonIcon()
  if GameUIStarCraftDetail.mReward.CurrentReward == 1 then
    GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "UpdateRewardList")
  elseif GameUIStarCraftDetail.mReward.CurrentReward == 2 then
    GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "UpdateKingRewardList")
  end
end
function GameUIStarCraftDetail:UpdateRankItem(id)
  local itemKey = tonumber(id)
  local rankRow
  if GameUIStarCraftDetail.mRank.CurrentRank == 1 then
    if itemKey > #GameUIStarCraftDetail.mRank.WorldRank then
      return
    else
      rankRow = GameUIStarCraftDetail.mRank.WorldRank[itemKey]
    end
  elseif GameUIStarCraftDetail.mRank.CurrentRank == 2 then
    if itemKey > #GameUIStarCraftDetail.mRank.ServerRank then
      return
    else
      rankRow = GameUIStarCraftDetail.mRank.ServerRank[itemKey]
    end
  end
  local isKing = false
  if nil ~= rankRow.king and 1 == tonumber(rankRow.king) then
    isKing = true
  end
  GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setRankItem", isKing, itemKey, tonumber(rankRow.rank), GameUtils:CutOutUsernameByLastDot(rankRow.user_name), tostring(rankRow.team), tostring(rankRow.point))
end
function GameUIStarCraftDetail:BattleReplay(battleIndex)
  GameUIStarCraftDetail.mBattleList.ClickedIndex = battleIndex
  local battleID = GameUIStarCraftDetail.mBattleList[battleIndex].ext
  if GameUIStarCraftDetail.mBattleList[battleIndex].mType >= 200 then
    return
  end
  if GameStateManager:GetCurrentGameState().fightRoundData then
    GameStateManager:GetCurrentGameState().fightRoundData = nil
  end
  DebugOut("BattleReplay ", battleIndex)
  local content = {id = battleID}
  NetMessageMgr:SendMsg(NetAPIList.contention_logbattle_detail_req.Code, content, GameUIStarCraftDetail.LogBattleDetailCallback, true, nil)
end
function GameUIStarCraftDetail.LogBattleDetailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_logbattle_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_logbattle_detail_ack.Code and content.report then
    DebugOut("GameUIStarCraftDetail.LogBattleDetailCallback content")
    DebugTable(content)
    DebugOut("mBattleList")
    DebugTable(GameUIStarCraftDetail.mBattleList)
    do
      local window_type = "challenge_win"
      if GameUIStarCraftDetail.mBattleList[GameUIStarCraftDetail.mBattleList.ClickedIndex].mType == 100 then
        window_type = "challenge_lose"
      else
        window_type = "challenge_win"
        local index = 1
        GameUIBattleResult:UpdateAwardItem3(window_type, 2, -1, 0, 0)
        for i = 1, #content.reward_list do
          if content.reward_list[i].no ~= 0 then
            GameUIBattleResult:UpdateAwardItem3(window_type, index, content.reward_list[i].item_type, content.reward_list[i].no, content.reward_list[i].number)
            index = index + 1
          end
        end
      end
      if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      GameUIBattleResult:SetFightReport(content.report, nil, nil)
      local lastGameState = GameStateManager:GetCurrentGameState()
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.report)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(lastGameState)
        GameUIStarCraftDetail:RefreshBattleReport()
        local flash = GameUIStarCraftDetail.flash_obj
        if flash then
          flash:InvokeASCallback("_root", "showBattleReportPanel")
          GameUIStarCraftDetail.IsShowBattleReportWindow = true
        end
        GameUIBattleResult:LoadFlashObject()
        GameUIBattleResult:AnimationMoveIn(window_type, false)
        lastGameState:AddObject(GameUIBattleResult)
      end)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      return true
    end
  end
  return false
end
function GameUIStarCraftDetail:SetKeyPointBuffList()
  local buffList = GameUIStarCraftMap:GetBuffListByPointID(GameUIStarCraftDetail.mCurPointID)
  local descList = ""
  local valueList = ""
  local frameList = ""
  local typeList = ""
  local data = {}
  for i, v in ipairs(buffList) do
    local desc = v:GetDisplayerString()
    local value, key, preStr
    local item = {}
    if v.mID < 100 then
      preStr = "+ "
    else
      preStr = "- "
    end
    if v:GetType() == 1 then
      value = preStr .. v.mBuffDetailItems[1].value .. "%"
    elseif v:GetType() == 5 then
      preStr = "+ "
      value = preStr .. v.mBuffDetailItems[1].limit_value .. "/" .. v.mBuffDetailItems[1].key .. "S"
    else
      local floatValue = v.mBuffDetailItems[1].value * 60
      if floatValue >= 1 then
        value = preStr .. GameUtils.numberConversionFloat(floatValue) .. "/H"
      else
        floatValue = floatValue * 24
        value = preStr .. GameUtils.numberConversionFloat(floatValue) .. "/D"
      end
    end
    local frame = v:GetDisplayFrame()
    typeList = typeList .. v:GetType() .. "\001"
    descList = descList .. desc .. "\001"
    valueList = valueList .. value .. "\001"
    frameList = frameList .. frame .. "\001"
    item.type = v:GetType()
    item.value = value
    item.desc = desc
    item.frame = frame
    data[#data + 1] = item
  end
  DebugOut("typeList in normal = ", typeList)
  DebugTable(buffList)
  GameUIStarCraftDetail.buffListData = data
  GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setKeyPointDetailInfoBuff", #GameUIStarCraftDetail.buffListData)
end
function GameUIStarCraftDetail:SetEventKeyPointData(event)
  local curEvent = event
  local awardNum = curEvent:GetAwardNum()
  local descList = ""
  local valueList = ""
  local frameList = ""
  local typeList = ""
  local buffList = GameUIStarCraftMap:GetBuffListByPointID(GameUIStarCraftDetail.mCurPointID)
  local data = {}
  for i, v in ipairs(buffList) do
    local desc = v:GetDisplayerString()
    local value, key, preStr
    local item = {}
    if v.mID < 100 then
      preStr = "+ "
    else
      preStr = "- "
    end
    if v:GetType() == 1 then
      value = preStr .. v.mBuffDetailItems[1].value .. "%"
    elseif v:GetType() == 5 then
      preStr = "+ "
      value = preStr .. v.mBuffDetailItems[1].limit_value .. "/" .. v.mBuffDetailItems[1].key .. "S"
    else
      local floatValue = v.mBuffDetailItems[1].value * 60
      if floatValue >= 1 then
        value = preStr .. GameUtils.numberConversionFloat(floatValue) .. "/H"
      else
        floatValue = floatValue * 24
        value = preStr .. GameUtils.numberConversionFloat(floatValue) .. "/D"
      end
    end
    local frame = v:GetDisplayFrame()
    typeList = typeList .. v:GetType() .. "\001"
    descList = descList .. desc .. "\001"
    valueList = valueList .. value .. "\001"
    frameList = frameList .. frame .. "\001"
    item.type = v:GetType()
    item.value = value
    item.desc = desc
    item.frame = frame
    data[#data + 1] = item
  end
  local typeToBuffType = 1
  if curEvent:IsWhiteHole() then
    typeToBuffType = 0
  end
  for i = 1, awardNum do
    local desc = curEvent:GetAwardDesc(i)
    local valueStr = curEvent:GetAwardValueStr(i)
    local frame = curEvent:GetAwardFrame(i)
    local item = {}
    typeList = typeList .. typeToBuffType .. "\001"
    descList = descList .. desc .. "\001"
    valueList = valueList .. valueStr .. "\001"
    frameList = frameList .. frame .. "\001"
    i = i + 1
    item.type = typeToBuffType
    item.value = valueStr
    item.desc = desc
    item.frame = frame
    data[#data + 1] = item
  end
  DebugOut("typeList in Event = ", typeList)
  GameUIStarCraftDetail.buffListData = data
  GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setKeyPointDetailInfoBuff", #GameUIStarCraftDetail.buffListData)
end
function GameUIStarCraftDetail:UpdateGarrisonItem(id)
  local itemKey = tonumber(id)
  local itemInfo = GameUIStarCraftDetail.mOccupierList[itemKey + GameUIStarCraftDetail.mCurrentDisplayIndex - 1]
  if itemInfo == nil then
    GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setKeyPointDetailInfoGarrisonItem", itemKey, "", "empty")
  else
    GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setKeyPointDetailInfoGarrisonItem", itemKey, GameUtils:GetUserDisplayName(itemInfo.mName), itemInfo.sexFrame)
  end
end
function GameUIStarCraftDetail:RefreshGarrisons(pageIndex, ocuppieIndex)
  local flash = GameUIStarCraftDetail.flash_obj
  if not flash then
    return
  end
  local ocuppoerStartIndex = pageIndex * GASSIONS_PER_PAGE - GASSIONS_PER_PAGE + 1
  local ocuppoerEndIndex = pageIndex * GASSIONS_PER_PAGE
  local occupierName = ""
  local occupierSex = ""
  local occupierIsKing = ""
  for i = ocuppoerStartIndex, ocuppoerEndIndex do
    local itemInfo = GameUIStarCraftDetail.mOccupierList[i]
    if itemInfo == nil then
      occupierSex = occupierSex .. "empty" .. "\001"
      occupierName = occupierName .. " " .. "\001"
      occupierIsKing = occupierIsKing .. "false" .. "\001"
    else
      occupierName = occupierName .. GameUtils:GetUserDisplayName(itemInfo.mName) .. "\001"
      occupierSex = occupierSex .. itemInfo.sexFrame .. "\001"
      occupierIsKing = occupierIsKing .. tostring(itemInfo.mIsKing) .. "\001"
    end
    i = i + 1
  end
  flash:InvokeASCallback("_root", "setOcuppierList", ocuppieIndex, occupierName, occupierSex, occupierIsKing)
end
function GameUIStarCraftDetail:NextGarrisons()
  local flash = GameUIStarCraftDetail.flash_obj
  if not flash then
    return
  end
  local nextOcupperStartIndex = (GameUIStarCraftDetail.mCurrentPageIndex + 1) * GASSIONS_PER_PAGE - GASSIONS_PER_PAGE + 1
  local itemInfo = GameUIStarCraftDetail.mOccupierList[nextOcupperStartIndex]
  if itemInfo then
    GameUIStarCraftDetail.mCurrentPageIndex = GameUIStarCraftDetail.mCurrentPageIndex + 1
    GameUIStarCraftDetail:RefreshGarrisons(GameUIStarCraftDetail.mCurrentPageIndex, OcuppieMCIndex.Index_Right)
    flash:InvokeASCallback("_root", "nextOcupper")
  else
    local nextPageInServer = math.ceil(GameUIStarCraftDetail.mCurrentPageIndex / 4) + 1
    GameUIStarCraftDetail:RequestNextOcuppoers(GameUIStarCraftDetail.mCurPointID, nextPageInServer)
  end
  GameUIStarCraftDetail:SetOcuppierArrowVisible()
end
function GameUIStarCraftDetail:PreGarrisons()
  local flash = GameUIStarCraftDetail.flash_obj
  if not flash then
    return
  end
  local prevOcupperStartIndex = (GameUIStarCraftDetail.mCurrentPageIndex - 1) * GASSIONS_PER_PAGE - GASSIONS_PER_PAGE + 1
  local itemInfo = GameUIStarCraftDetail.mOccupierList[prevOcupperStartIndex]
  if itemInfo then
    GameUIStarCraftDetail.mCurrentPageIndex = GameUIStarCraftDetail.mCurrentPageIndex - 1
    GameUIStarCraftDetail:RefreshGarrisons(GameUIStarCraftDetail.mCurrentPageIndex, OcuppieMCIndex.Index_Left)
    flash:InvokeASCallback("_root", "prevOcupper")
  end
  GameUIStarCraftDetail:SetOcuppierArrowVisible()
end
function GameUIStarCraftDetail:SetOcuppierArrowVisible()
  local flash = GameUIStarCraftDetail.flash_obj
  if not flash then
    return
  end
  DebugOut("mCurrentPageIndex = ", GameUIStarCraftDetail.mCurrentPageIndex)
  DebugOut("mTotalPages = ", GameUIStarCraftDetail.mTotalPages)
  DebugOut("mCurrent table = ")
  DebugTable(GameUIStarCraftDetail.mOccupierList)
  if GameUIStarCraftDetail.mCurrentPageIndex >= GameUIStarCraftDetail.mTotalPages then
    flash:InvokeASCallback("_root", "setNextGassionVisible", false)
  else
    flash:InvokeASCallback("_root", "setNextGassionVisible", true)
  end
  if GameUIStarCraftDetail.mCurrentPageIndex > 1 then
    flash:InvokeASCallback("_root", "setPrevGassionVisible", true)
  else
    flash:InvokeASCallback("_root", "setPrevGassionVisible", false)
  end
end
function GameUIStarCraftDetail.teleportHandler()
  GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "hideKeyPointDetail")
  GameUIStarCraftMap:OnFSCommand("teleportClicked", GameUIStarCraftDetail.mCurPointID)
end
function GameUIStarCraftDetail:ShowPlayerInfo()
  local frame = GameUIStarCraftMap:GetDetailInfoColorFrameByPointID(GameUIStarCraftDetail.mCurPointID)
  local flash = GameUIStarCraftDetail.flash_obj
  if flash then
    DebugOut("GameUIStarCraftDetail.mCurrentOccupierIndex = ", GameUIStarCraftDetail.mCurrentOccupierIndex)
    DebugOut("total player = ", #GameUIStarCraftDetail.mOccupierList)
    flash:InvokeASCallback("_root", "showPlayerInfoPanel", frame)
    if GameUIStarCraftDetail.mCurrentOccupierIndex <= 1 then
      flash:InvokeASCallback("_root", "showPlayerLeftArrow", false)
    else
      flash:InvokeASCallback("_root", "showPlayerLeftArrow", true)
    end
    if GameUIStarCraftDetail.mCurrentOccupierIndex >= #GameUIStarCraftDetail.mOccupierList then
      flash:InvokeASCallback("_root", "showPlayerRightArrow", false)
    else
      flash:InvokeASCallback("_root", "showPlayerRightArrow", true)
    end
  end
end
function GameUIStarCraftDetail:ClosePlayerInfo()
  local flash = GameUIStarCraftDetail.flash_obj
  if flash then
    flash:InvokeASCallback("_root", "closePlayerInfo")
  end
end
function GameUIStarCraftDetail:NextPlayer()
  if GameUIStarCraftDetail.mCurrentOccupierIndex == #GameUIStarCraftDetail.mOccupierList then
    return
  end
  GameUIStarCraftDetail.mCurrentOccupierIndex = GameUIStarCraftDetail.mCurrentOccupierIndex + 1
  local flash = GameUIStarCraftDetail.flash_obj
  if GameUIStarCraftDetail.mCurrentOccupierIndex >= #GameUIStarCraftDetail.mOccupierList then
    GameUIStarCraftDetail.mCurrentOccupierIndex = #GameUIStarCraftDetail.mOccupierList
    if flash then
      flash:InvokeASCallback("_root", "showPlayerRightArrow", false)
    end
  end
  if GameUIStarCraftDetail.mCurrentOccupierIndex > 1 and flash then
    flash:InvokeASCallback("_root", "showPlayerLeftArrow", true)
  end
  GameUIStarCraftDetail:RequestPlayerInfo()
end
function GameUIStarCraftDetail:PrePlayer()
  if GameUIStarCraftDetail.mCurrentOccupierIndex == 1 then
    return
  end
  GameUIStarCraftDetail.mCurrentOccupierIndex = GameUIStarCraftDetail.mCurrentOccupierIndex - 1
  local flash = GameUIStarCraftDetail.flash_obj
  if GameUIStarCraftDetail.mCurrentOccupierIndex <= 1 then
    GameUIStarCraftDetail.mCurrentOccupierIndex = 1
    if flash then
      flash:InvokeASCallback("_root", "showPlayerLeftArrow", false)
    end
  end
  if GameUIStarCraftDetail.mCurrentOccupierIndex < #GameUIStarCraftDetail.mOccupierList and flash then
    flash:InvokeASCallback("_root", "showPlayerRightArrow", true)
  end
  GameUIStarCraftDetail:RequestPlayerInfo()
end
function GameUIStarCraftDetail:RequestPlayerInfo()
  local player = GameUIStarCraftDetail.mOccupierList[GameUIStarCraftDetail.mCurrentOccupierIndex]
  local content = {
    user_id = player.mID
  }
  DebugOut("player info id = ", player.mID)
  NetMessageMgr:SendMsg(NetAPIList.contention_occupier_req.Code, content, GameUIStarCraftDetail.RequestPlayerInfoCallback, true, nil)
end
function GameUIStarCraftDetail.RequestPlayerInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_occupier_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_occupier_ack.Code then
    for i, v in ipairs(GameUIStarCraftDetail.mOccupierList) do
      if i == GameUIStarCraftDetail.mCurrentOccupierIndex then
        GameUIStarCraftDetail.mOccupierList[i].mHP = content.hp
        GameUIStarCraftDetail.mOccupierList[i].mFleetMatrix = content.matrices
        GameUIStarCraftDetail.mOccupierList[i].mFleetLevel = content.fleet_levels
        GameUIStarCraftDetail.mOccupierList[i].mTotalHP = content.hp_limit
        GameUIStarCraftDetail.mOccupierList[i].mInfluence = content.influence
        GameUIStarCraftDetail.mOccupierList[i].mForce = content.force
        GameUIStarCraftDetail.mOccupierList[i].mLevel = content.level
        GameUIStarCraftDetail.mOccupierList[i].mSex = content.sex
        if GameUIStarCraftDetail.mOccupierList[i].sexFrame == nil then
          GameUIStarCraftDetail.mOccupierList[i].sexFrame = GameDataAccessHelper:GetFleetAvatar(tonumber(content.icon), 1, content.sex)
        end
      end
    end
    if not GameUIStarCraftDetail.mPlayerInfoPanelOpened then
      GameUIStarCraftDetail:ShowPlayerInfo()
      GameUIStarCraftDetail.mPlayerInfoPanelOpened = true
    end
    GameUIStarCraftDetail:RefreshPlayerInfoPanel()
    return true
  end
  return false
end
function GameUIStarCraftDetail:RefreshPlayerInfoPanel()
  local player = GameUIStarCraftDetail.mOccupierList[GameUIStarCraftDetail.mCurrentOccupierIndex]
  player.mTotalHP = player.mTotalHP or 100
  local percentHp = player.mHP / player.mTotalHP
  local percetText = player.mHP .. "/" .. player.mTotalHP
  local flash = GameUIStarCraftDetail.flash_obj
  if flash then
    flash:InvokeASCallback("_root", "clearPlayerFleet")
  end
  function sortFunc(a, b)
    return a.cell_id < b.cell_id
  end
  table.sort(player.mFleetMatrix, sortFunc)
  table.sort(player.mFleetLevel, sortFunc)
  DebugOut("after sort:")
  DebugTable(player.mFleetMatrix)
  DebugOut(player)
  for i, v in ipairs(player.mFleetMatrix) do
    if flash then
      local iconFrame = ""
      if v.fleet_identity == 1 then
        iconFrame = player.sexFrame
      else
        iconFrame = GameDataAccessHelper:GetFleetAvatar(v.fleet_identity, player.mFleetLevel[i].level, player.mSex)
      end
      DebugOut("player avatar icon frame = ", iconFrame)
      flash:InvokeASCallback("_root", "setPlayerFleetInfo", i, GameUIStarCraftMap:GetDetailInfoColorFrameByPointID(GameUIStarCraftDetail.mCurPointID), iconFrame)
    end
  end
  local disabledFleetNum = 0
  if percentHp <= 0.2 then
    disabledFleetNum = 4
  elseif percentHp <= 0.4 then
    disabledFleetNum = 3
  elseif percentHp <= 0.6 then
    disabledFleetNum = 2
  elseif percentHp <= 0.8 then
    disabledFleetNum = 1
  end
  local leftFleets = #player.mFleetMatrix - disabledFleetNum
  if leftFleets < 1 then
    leftFleets = 1
  end
  local lastIndex = 5
  local firstIndex = leftFleets + 1
  while lastIndex >= firstIndex do
    flash:InvokeASCallback("_root", "setPlayerFleetState", lastIndex, "gray")
    lastIndex = lastIndex - 1
  end
  if flash then
    local deadNum = disabledFleetNum
    if disabledFleetNum >= #player.mFleetMatrix then
      deadNum = #player.mFleetMatrix - 1
    end
    local serverName = GameUIStarCraftMap:GetServerDisplayNameByPointID(GameUIStarCraftDetail.mCurPointID)
    flash:InvokeASCallback("_root", "setPlayerInfo", player.mName, player.mLevel, player.sexFrame, serverName, GameUtils.numberAddComma(player.mForce), GameUtils.numberAddComma(player.mInfluence), deadNum)
  end
  local percent = math.floor(percentHp * 100)
  DebugOut("percent hp = ", percent)
  if percent <= 0 then
    percent = -1
  elseif percent >= 100 then
    percent = 99
  end
  flash:InvokeASCallback("_root", "setPlayerHP", 100 - percent, percetText)
end
function GameUIStarCraftDetail:RequestBattleReport()
  local content = {
    user_id = GameGlobalData:GetData("userinfo").player_id
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_logs_req.Code, content, GameUIStarCraftDetail.RequestBattleReportCallback, true, nil)
end
function GameUIStarCraftDetail:RequestRank()
  local content = {
    id = GameUIStarCraftDetail.mRank.CurrentRank
  }
  NetMessageMgr:SendMsg(NetAPIList.contention_rank_req.Code, content, GameUIStarCraftDetail.RequestRankCallback, true, nil)
end
function GameUIStarCraftDetail:RequestReward()
  local content = {id = 1}
  NetMessageMgr:SendMsg(NetAPIList.contention_award_req.Code, content, GameUIStarCraftDetail.RequestRewardCallback, true, nil)
end
function GameUIStarCraftDetail.RequestRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_award_ack.Code then
    if content ~= nil then
      DebugOut("RequestRewardCallback content.awards")
      DebugTable(content.awards)
      GameUIStarCraftDetail.mReward.RankReward = content.awards
      for i = 1, #content.awards do
        if content.awards[i].id == 0 then
          table.remove(GameUIStarCraftDetail.mReward.RankReward, i)
        end
      end
    end
    table.sort(GameUIStarCraftDetail.mReward.RankReward, function(a, b)
      return a.id < b.id
    end)
    DebugOut("after sort")
    DebugTable(GameUIStarCraftDetail.mReward.RankReward)
    GameUIStarCraftDetail:RefreshReward()
    return true
  end
  return false
end
function GameUIStarCraftDetail:GetSubRewardItem(row, column)
  return GameUIStarCraftDetail.mReward.RankReward[row].awards[column]
end
function GameUIStarCraftDetail:GetKingRewardItem(idx)
  if GameUIStarCraftDetail.mReward.KingReward and GameUIStarCraftDetail.mReward.KingReward[1] and GameUIStarCraftDetail.mReward.KingReward[1].awards and GameUIStarCraftDetail.mReward.KingReward[1].awards[idx] then
    return GameUIStarCraftDetail.mReward.KingReward[1].awards[idx]
  else
    return nil
  end
end
function GameUIStarCraftDetail:ShowItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
function GameUIStarCraftDetail:RefreshReward()
  if GameUIStarCraftDetail.mReward.CurrentReward == 1 then
    if GameUIStarCraftDetail.mReward.RankReward ~= nil then
      local flash = GameUIStarCraftDetail.flash_obj
      if flash then
        flash:InvokeASCallback("_root", "clearRewardItem")
        local itemCount = #GameUIStarCraftDetail.mReward.RankReward
        flash:InvokeASCallback("_root", "initRewardListItem")
        for i = 1, itemCount do
          flash:InvokeASCallback("_root", "addRewardItem", i)
        end
        flash:InvokeASCallback("_root", "setRewardArrowVisible")
      end
    end
  elseif GameUIStarCraftDetail.mReward.CurrentReward == 2 and GameUIStarCraftDetail.mReward.KingReward ~= nil then
    local flash = GameUIStarCraftDetail.flash_obj
    if flash then
      flash:InvokeASCallback("_root", "clearKingRewardItem")
      if GameUIStarCraftDetail.mReward.KingReward and GameUIStarCraftDetail.mReward.KingReward[1] and GameUIStarCraftDetail.mReward.KingReward[1].awards then
        local itemCount = #GameUIStarCraftDetail.mReward.KingReward[1].awards
        flash:InvokeASCallback("_root", "initKingRewardListItem")
        for i = 1, itemCount do
          flash:InvokeASCallback("_root", "addKingRewardItem", i)
        end
        flash:InvokeASCallback("_root", "setKingRewardArrowVisible")
      end
    end
  end
end
function GameUIStarCraftDetail.RequestBattleReportCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_logs_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_logs_ack.Code then
    if content ~= nil then
      GameUIStarCraftDetail.mBattleList = {}
      for i, v in ipairs(content.logs) do
        local battleReport = {}
        battleReport.mType = v.log_type
        battleReport.mID = v.id
        battleReport.args = v.params
        battleReport.ext = v.ext
        battleReport.time = v.time
        table.insert(GameUIStarCraftDetail.mBattleList, battleReport)
      end
    end
    GameUIStarCraftDetail:RefreshBattleReport()
    return true
  end
  return false
end
function GameUIStarCraftDetail.RequestRankCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_rank_req.Code then
    if content.code ~= nil then
      GameUIStarCraftDetail:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_rank_ack.Code then
    if content ~= nil then
      GameUIStarCraftDetail.mRank.CurrentRank = 1
      GameUIStarCraftDetail.mRank.WorldRank = content.rank[1].rank_list
      GameUIStarCraftDetail.mRank.ServerRank = content.rank[2].rank_list
      GameUIStarCraftDetail:SetOwnRank()
    end
    GameUIStarCraftDetail:RefreshRank()
    return true
  end
  return false
end
function GameUIStarCraftDetail:SetOwnRank()
  local ownIndex
  if GameUIStarCraftDetail.mRank.CurrentRank == 1 then
    local worldRankLength = #GameUIStarCraftDetail.mRank.WorldRank
    local userID = GameGlobalData:GetData("userinfo").player_id
    for i = 1, worldRankLength do
      if GameUIStarCraftDetail.mRank.WorldRank[i].user_id == userID then
        GameUIStarCraftDetail.mRank.ownWorldRank = GameUIStarCraftDetail.mRank.WorldRank[i]
      end
    end
    if worldRankLength == 101 then
      table.remove(GameUIStarCraftDetail.mRank.WorldRank, 101)
    end
  end
  if GameUIStarCraftDetail.mRank.CurrentRank == 2 then
    local serverRankLength = #GameUIStarCraftDetail.mRank.ServerRank
    local userID = GameGlobalData:GetData("userinfo").player_id
    for i = 1, serverRankLength do
      if GameUIStarCraftDetail.mRank.ServerRank[i].user_id == userID then
        GameUIStarCraftDetail.mRank.ownServerRank = GameUIStarCraftDetail.mRank.ServerRank[i]
      end
    end
    if serverRankLength == 101 then
      table.remove(GameUIStarCraftDetail.mRank.ServerRank, 101)
    end
  end
  local userName = GameGlobalData:GetData("userinfo").name
  local serverName = GameUIStarCraftMap:GetSelfServerName() or GameUtils:GetActiveServerInfo().name
  local rank = -1
  local point = 0
  local userID = GameGlobalData:GetData("userinfo").player_id
  local isKing = false
  if GameUIStarCraftDetail.mRank.CurrentRank == 1 then
    if GameUIStarCraftDetail.mRank.ownWorldRank.team == nil then
      GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "SetOwnRank", isKing, tonumber(rank), GameUtils:CutOutUsernameByLastDot(userName), tostring(serverName), tostring(point))
    end
    if GameUIStarCraftDetail.mRank.ownWorldRank.team ~= nil and GameUIStarCraftDetail.flash_obj then
      if nil ~= GameUIStarCraftDetail.mRank.ownWorldRank.king and 1 == tonumber(GameUIStarCraftDetail.mRank.ownWorldRank.king) then
        isKing = true
      end
      GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "SetOwnRank", isKing, tonumber(GameUIStarCraftDetail.mRank.ownWorldRank.rank), GameUtils:CutOutUsernameByLastDot(GameUIStarCraftDetail.mRank.ownWorldRank.user_name), tostring(GameUIStarCraftDetail.mRank.ownWorldRank.team), tostring(GameUIStarCraftDetail.mRank.ownWorldRank.point))
    end
  end
  if GameUIStarCraftDetail.mRank.CurrentRank == 2 then
    if GameUIStarCraftDetail.mRank.ownServerRank.team == nil then
      GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "SetOwnRank", isKing, tonumber(rank), GameUtils:CutOutUsernameByLastDot(userName), tostring(serverName), tostring(point))
    end
    if GameUIStarCraftDetail.mRank.ownServerRank.team ~= nil and GameUIStarCraftDetail.flash_obj then
      if nil ~= GameUIStarCraftDetail.mRank.ownServerRank.king and 1 == tonumber(GameUIStarCraftDetail.mRank.ownServerRank.king) then
        isKing = true
      end
      GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "SetOwnRank", isKing, tonumber(GameUIStarCraftDetail.mRank.ownServerRank.rank), GameUtils:CutOutUsernameByLastDot(GameUIStarCraftDetail.mRank.ownServerRank.user_name), tostring(GameUIStarCraftDetail.mRank.ownServerRank.team), tostring(GameUIStarCraftDetail.mRank.ownServerRank.point))
    end
  end
end
function GameUIStarCraftDetail:RefreshBattleReport()
  if GameUIStarCraftDetail.mBattleList ~= nil then
    local flash = GameUIStarCraftDetail.flash_obj
    if flash then
      flash:InvokeASCallback("_root", "clearBattleItem")
      local itemCount = #GameUIStarCraftDetail.mBattleList
      flash:InvokeASCallback("_root", "initBattleListItem")
      for i = 1, itemCount do
        flash:InvokeASCallback("_root", "addBattleItem", i)
      end
    end
  end
end
function GameUIStarCraftDetail:RefreshRank()
  local data = {}
  data.serverRankText = GameLoader:GetGameText("LC_MENU_TC_RANK_SERVER_TITLE")
  data.campRankText = GameLoader:GetGameText("LC_MENU_PANDORA_WAR_CAMP_RANK")
  data.serverText = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_SERVER")
  data.campText = GameLoader:GetGameText("LC_MENU_PANDORA_WAR_CAMP")
  if GameUIStarCraftDetail.mRank.CurrentRank == 1 then
    if GameUIStarCraftDetail.mRank.WorldRank ~= nil then
      local flash = GameUIStarCraftDetail.flash_obj
      if flash then
        flash:InvokeASCallback("_root", "clearRankItem")
        local itemCount = #GameUIStarCraftDetail.mRank.WorldRank
        flash:InvokeASCallback("_root", "initRankListItem", GameGlobalData.starCraftData.type, data)
        for i = 1, itemCount do
          flash:InvokeASCallback("_root", "addRankItem", i)
        end
        flash:InvokeASCallback("_root", "setRemainArrowVisible")
      end
    end
  elseif GameUIStarCraftDetail.mRank.CurrentRank == 2 and GameUIStarCraftDetail.mRank.ServerRank ~= nil then
    local flash = GameUIStarCraftDetail.flash_obj
    if flash then
      flash:InvokeASCallback("_root", "clearRankItem")
      local itemCount = #GameUIStarCraftDetail.mRank.ServerRank
      flash:InvokeASCallback("_root", "initRankListItem", GameGlobalData.starCraftData.type, data)
      for i = 1, itemCount do
        flash:InvokeASCallback("_root", "addRankItem", i)
      end
      flash:InvokeASCallback("_root", "setRemainArrowVisible")
    end
  end
end
function GameUIStarCraftDetail:UpdateBattleItem(id)
  local itemKey = tonumber(id)
  if itemKey > #GameUIStarCraftDetail.mBattleList then
    return
  end
  local itemInfo = GameUIStarCraftDetail.mBattleList[itemKey]
  local itemLoclizationID = "LC_MENU_TC_REPORT_" .. itemInfo.mType
  if itemInfo.mType == 1 then
    itemLoclizationID = "LC_MENU_TC_REPORT_KILL_" .. itemInfo.args[2]
  elseif itemInfo.mType == 2 then
    itemLoclizationID = "LC_MENU_TC_REPORT_END_KILL_" .. itemInfo.args[3]
  elseif itemInfo.mType == 3 then
    itemLoclizationID = "LC_MENU_TC_REPORT_END_KILL_B_" .. itemInfo.args[2]
  elseif itemInfo.mType == 4 then
    itemLoclizationID = "LC_MENU_TC_REPORT_END_KIL_A_" .. itemInfo.args[2]
  end
  DebugOut("itemLoclizationID = ", itemLoclizationID)
  local itemStr = GameLoader:GetGameText(itemLoclizationID)
  DebugOut("battle txt str = ", itemStr)
  for i, v in ipairs(itemInfo.args) do
    local value = v
    if LocalizationReplaceOrder[itemInfo.mType][i] == "<judian>" then
      value = GameUIStarCraftMap:GetKeyPointName(tonumber(v))
    end
    if itemStr ~= nil or itemStr ~= "" then
      itemStr = string.gsub(itemStr, LocalizationReplaceOrder[itemInfo.mType][i], value)
    end
  end
  local timeStr = GameUtils:formatTimeToDateStr(itemInfo.time)
  GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setBattleItem", itemKey, itemInfo.mType, timeStr, itemStr)
end
function GameUIStarCraftDetail:RequestKingReward()
  local content = {}
  NetMessageMgr:SendMsg(NetAPIList.contention_king_info_req.Code, content, GameUIStarCraftDetail.RequestKingRewardCallback, true, nil)
end
function GameUIStarCraftDetail.RequestKingRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_king_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_king_info_ack.Code then
    if content ~= nil then
      GameUIStarCraftDetail.mKingRewardLeftTimeFetchTime = os.time()
      GameUIStarCraftDetail.mKingRewardLeftTime = tonumber(content.end_time)
      GameUIStarCraftDetail.mReward.KingReward[1] = {}
      GameUIStarCraftDetail.mReward.KingReward[1].awards = content.award
      GameUIStarCraftDetail:RefreshReward()
    end
    return true
  end
  return false
end
function GameUIStarCraftDetail:UpdateKingRewardLeftTime()
  if GameUIStarCraftDetail.mIsRewardPanelShow and -1 ~= GameUIStarCraftDetail.mKingRewardLeftTime and -1 ~= GameUIStarCraftDetail.mKingRewardLeftTimeFetchTime then
    local tmpLeftTime = GameUIStarCraftDetail.mKingRewardLeftTime - (os.time() - GameUIStarCraftDetail.mKingRewardLeftTimeFetchTime)
    if tmpLeftTime < 0 then
      tmpLeftTime = 0
    end
    if GameUIStarCraftDetail.mCurKingRewardLeftTime ~= tmpLeftTime then
      GameUIStarCraftDetail.mCurKingRewardLeftTime = tmpLeftTime
      local timeStr = GameUtils:formatTimeString(tmpLeftTime)
      if GameUIStarCraftDetail.flash_obj then
        GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setKingRewardLeftTime", timeStr)
      end
      if 0 == GameUIStarCraftDetail.mCurKingRewardLeftTime then
        local waitCallback = function()
          DebugOut("Request king reward data. ")
          GameUIStarCraftDetail.mKingRewardLeftTimeFetchTime = -1
          GameUIStarCraftDetail:RequestKingReward()
          return nil
        end
        GameTimer:Add(waitCallback, 2000)
      end
    end
  end
end
function GameUIStarCraftDetail:ShowCollectMenu()
  local flash = GameUIStarCraftDetail.flash_obj
  if flash then
    flash:InvokeASCallback("_root", "showCollectResMenu")
    GameUIStarCraftDetail.IsShowCollectMenu = true
  end
end
function GameUIStarCraftDetail:RequestCollectResMenu()
  NetMessageMgr:SendMsg(NetAPIList.contention_income_req.Code, nil, GameUIStarCraftDetail.RequestCollectResMenuCallback, true, nil)
end
function GameUIStarCraftDetail.RequestCollectResMenuCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.contention_income_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.contention_income_ack.Code then
    if content ~= nil then
      GameUIStarCraftDetail:RefreshCollectResMenu(content)
      GameUIStarCraftDetail:ShowCollectMenu()
    end
    return true
  end
  return false
end
function GameUIStarCraftDetail:RefreshCollectResMenu(resContent)
  if resContent then
    GameUIStarCraftDetail.mCollectResList = resContent.income_list
    local flash = GameUIStarCraftDetail.flash_obj
    if flash then
      flash:InvokeASCallback("_root", "clearCollectResItem")
      local itemCount = #GameUIStarCraftDetail.mCollectResList
      flash:InvokeASCallback("_root", "initCollectResItem")
      for i = 1, itemCount do
        flash:InvokeASCallback("_root", "addCollectResItem", i)
      end
      flash:InvokeASCallback("_root", "setCollectResArrowVisible")
    end
  end
end
function GameUIStarCraftDetail:MakeGameItemFormCollectResItem(itemInfo)
  if itemInfo then
    local gameItem = {}
    if GameHelper:IsResource(itemInfo.type) then
      gameItem.item_type = itemInfo.type
      gameItem.number = 1
      gameItem.no = 1
      gameItem.level = 0
    else
      gameItem.item_type = "item"
      gameItem.number = tonumber(itemInfo.type)
      gameItem.no = 1
      gameItem.level = 0
    end
    return gameItem
  end
  return nil
end
function GameUIStarCraftDetail:UpdateCollectResItem(index)
  local itemKey = tonumber(index)
  local itemInfo = GameUIStarCraftDetail.mCollectResList[itemKey]
  if itemInfo then
    local gameItem = GameUIStarCraftDetail:MakeGameItemFormCollectResItem(itemInfo)
    local itemIconFrmae = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(gameItem, nil, nil)
    local itemName = GameHelper:GetAwardNameText(gameItem.item_type, gameItem.number)
    GameUIStarCraftDetail.flash_obj:InvokeASCallback("_root", "setCollectResItem", itemKey, itemIconFrmae, itemName, itemInfo.player, itemInfo.alliance, itemInfo.server)
  end
end
return GameUIStarCraftDetail
