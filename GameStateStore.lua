local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
local GameGoodsBuyBox = LuaObjectManager:GetLuaObject("GameGoodsBuyBox")
local GameStateStore = GameStateManager.GameStateStore
GameStateStore.CurItemDetail = nil
function GameStateStore:InitGameState()
end
function GameStateStore:OnFocusGain(previousState)
  DebugStore("GameStateStore:OnFocusGain", previousState)
  self.m_preState = previousState
  self:AddObject(GameGoodsList)
end
function GameStateStore:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameGoodsList)
end
function GameStateStore:GetPreState()
  return self.m_preState
end
function GameStateStore:ShowBuyConfirmWin(item)
  GameStateStore.CurItemDetail = item
  if GameStateStore.CurItemDetail == nil then
    return
  end
  self:AddObject(GameGoodsBuyBox)
end
function GameStateStore:HideBuyConfirmWin()
  self:EraseObject(GameGoodsBuyBox)
  GameStateStore.CurItemDetail = nil
end
function GameStateStore:SetCurItemDetail(item)
  GameStateStore.CurItemDetail = item
  if GameStateStore.CurItemDetail == nil then
    return
  end
  GameStateManager:GetCurrentGameState():AddObject(GameGoodsBuyBox)
end
function GameStateStore:HideBuyConfirmWinInSevenDayBenefit(...)
  GameStateManager:GetCurrentGameState():EraseObject(GameGoodsBuyBox)
  GameStateStore.CurItemDetail = nil
end
