local GameUIScratchGacha = LuaObjectManager:GetLuaObject("GameUIScratchGacha")
local GameStateScratchGacha = GameStateManager.GameStateScratchGacha
function GameStateScratchGacha:InitGameState()
end
function GameStateScratchGacha:OnFocusGain(previousState)
  self.m_preState = previousState
  self:AddObject(GameUIScratchGacha)
end
function GameStateScratchGacha:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameUIScratchGacha)
end
function GameStateScratchGacha:GetPreState()
  return self.m_preState
end
