local matrix = {
  _TYPE = "module",
  _NAME = "matrix",
  _VERSION = "0.2.11.20120416"
}
local matrix_meta = {}
function matrix:new(rows, columns, value)
  if type(rows) == "table" then
    if type(rows[1]) ~= "table" then
      return ...
    end
    return ...
  end
  local mtx = {}
  local value = value or 0
  if columns == "I" then
    for i = 1, rows do
      mtx[i] = {}
      for j = 1, rows do
        if i == j then
          mtx[i][j] = 1
        else
          mtx[i][j] = 0
        end
      end
    end
  else
    for i = 1, rows do
      mtx[i] = {}
      for j = 1, columns do
        mtx[i][j] = value
      end
    end
  end
  return ...
end
setmetatable(matrix, {
  __call = function(...)
