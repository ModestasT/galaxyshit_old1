local GameUILargeMapRank = LuaObjectManager:GetLuaObject("GameUILargeMapRank")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameStateLargeMapRank = GameStateManager.GameStateLargeMapRank
local GameStateLargeMap = GameStateManager.GameStateLargeMap
GameUILargeMapRank.baseData = nil
GameUILargeMapRank.curTab = 1
GameUILargeMapRank.rankData = nil
function GameUILargeMapRank:Show()
  GameStateManager:SetCurrentGameState(GameStateLargeMapRank)
end
function GameUILargeMapRank:OnInitGame()
end
function GameUILargeMapRank:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "InitFlashObject")
  self:GetFlashObject():InvokeASCallback("_root", "SetTab", GameUILargeMapRank.curTab)
  self:GetFlashObject():InvokeASCallback("_root", "MoveIn")
  GameUILargeMapRank:ReqRankData()
end
function GameUILargeMapRank:OnEraseFromGameState()
  DebugOut("GameUILargeMapRank:OnEraseFromGameState")
  self:UnloadFlashObject()
  GameUILargeMapRank.baseData = nil
  GameUILargeMapRank.curTab = 1
  GameUILargeMapRank.rankData = nil
end
function GameUILargeMapRank:OnFSCommand(cmd, arg)
  if cmd == "close" then
    GameStateManager:SetCurrentGameState(GameStateLargeMap)
  elseif cmd == "TouchTab" then
    GameUILargeMapRank.curTab = tonumber(arg)
    GameUILargeMapRank:ReqRankData()
  elseif cmd == "update_rank_item" then
    local itemData = GameUILargeMapRank.rankData.rankList[tonumber(arg)]
    if GameUILargeMapRank:GetFlashObject() then
      GameUILargeMapRank:GetFlashObject():InvokeASCallback("_root", "UpdateRankItemData", tonumber(arg), itemData)
    end
  elseif cmd == "TouchIconMy" then
    local index = tonumber(arg)
    local item = GameUILargeMapRank.rankData.my.rewards[index].model
    if item and item.item_type == "item" then
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif item and item.item_type == "fleet" then
      local fleetID = item.number
      ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
    end
  elseif cmd == "touchIcon" then
    local rankIndex, index = unpack(LuaUtils:string_split(arg, "|"))
    local rankItem = GameUILargeMapRank.rankData.rankList[tonumber(rankIndex)]
    local item = rankItem.rewards[index].model
    if item and item.item_type == "item" then
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif item and item.item_type == "fleet" then
      local fleetID = item.number
      ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
    end
  end
end
function GameUILargeMapRank:ReqRankData()
  local param = {}
  param.rank_type = GameUILargeMapRank.curTab
  NetMessageMgr:SendMsg(NetAPIList.large_map_rank_req.Code, param, GameUILargeMapRank.LargeMapRankDataCallBack, true, nil)
end
function GameUILargeMapRank.LargeMapRankDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_rank_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_rank_ack.Code then
    GameUILargeMapRank:GenerateRankData(content)
    GameUILargeMapRank:SetRankData(GameUILargeMapRank.rankData)
    return true
  end
  return false
end
function GameUILargeMapRank:GenerateRankData(content)
  local data = {}
  data.rankType = content.rank_type
  data.explainTitle = GameLoader:GetGameText("LC_MENU_MAP_RANK_DESC_" .. content.rank_type)
  data.explainText = GameLoader:GetGameText("LC_MENU_MAP_RANK_HELP_" .. content.rank_type)
  data.rewardTime = content.reward_time
  data.rewardBaseTime = os.time()
  data.rewardTimeStr = GameLoader:GetGameText("LC_MENU_MAP_RANK_COUNTDOWN") .. "\n" .. GameUtils:formatTimeString(data.rewardTime)
  data.my = {}
  data.my.rankValueStr = GameLoader:GetGameText("LC_MENU_MAP_RANK_ACHIEVEMNET_" .. content.rank_type) .. "<font color='#3300ee'>" .. content.my_rank_info.rank_value .. "</font>"
  if content.rank_type == 1 or content.rank_type == 3 then
    data.my.rankStr = GameLoader:GetGameText("LC_MENU_MAP_RANK_LEGION_RANKING") .. "<font color='#3300ee'>" .. content.my_rank_info.rank .. "</font>"
  else
    data.my.rankStr = GameLoader:GetGameText("LC_MENU_MAP_RANK_SINGLE") .. "<font color='#3300ee'>" .. content.my_rank_info.rank .. "</font>"
  end
  data.my.rewards = {}
  for k, v in pairs(content.my_rank_info.rewards) do
    local item = {}
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    item.displayCount = GameUtils.numberConversion(math.floor(item.count))
    item.iconframe = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    item.iconPic = GameHelper:GetItemInfo(v).icon_pic or "undefined"
    item.model = v
    data.my.rewards[#data.my.rewards + 1] = item
  end
  data.rankList = {}
  for k, v in pairs(content.rank_list) do
    local item = {}
    item.id = v.id
    item.rankCountryName = v.alliance_name
    item.rankPlayerName = v.player_name
    item.rank = v.rank
    item.rankType = content.rank_type
    item.rewards = {}
    for k1, v1 in pairs(v.rewards) do
      local tmp = {}
      tmp.count = GameHelper:GetAwardCount(v1.item_type, v1.number, v1.no)
      tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
      tmp.iconframe = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v1)
      tmp.iconPic = GameHelper:GetItemInfo(v1).icon_pic or "undefined"
      tmp.model = v1
      item.rewards[#item.rewards + 1] = tmp
    end
    data.rankList[#data.rankList + 1] = item
  end
  GameUILargeMapRank.rankData = data
end
function GameUILargeMapRank:SetRankData(data)
  if GameUILargeMapRank:GetFlashObject() then
    GameUILargeMapRank:GetFlashObject():InvokeASCallback("_root", "SetRankData", data)
  end
end
function GameUILargeMapRank:UpdateRewardTime()
  if GameUILargeMapRank.rankData then
    local leftTime = GameUILargeMapRank.rankData.rewardTime - (os.time() - GameUILargeMapRank.rankData.rewardBaseTime)
    if leftTime < 0 then
      leftTime = 0
    end
    GameUILargeMapRank.rankData.rewardTimeStr = GameLoader:GetGameText("LC_MENU_MAP_RANK_COUNTDOWN") .. "\n" .. GameUtils:formatTimeString(leftTime)
    GameUILargeMapRank:GetFlashObject():InvokeASCallback("_root", "SetRewardTime", GameUILargeMapRank.rankData.rewardTimeStr)
    if leftTime == 0 then
      GameUILargeMapRank:ReqRankData()
    end
  end
end
function GameUILargeMapRank:Update(dt)
  local flashObj = GameUILargeMapRank:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "OnUpdate", dt)
    GameUILargeMapRank:UpdateRewardTime()
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUILargeMapRank.OnAndroidBack()
    GameUILargeMapRank:OnFSCommand("close")
  end
end
