local GameUIMasterAchievementTips = LuaObjectManager:GetLuaObject("MasterAchievementTips")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local isMasterAchievementTipShowed = false
local tipsShowTime = 0
function GameUIMasterAchievementTips:OnAddToGameState(game_state)
  DebugOut("GameUIMasterAchievementTips:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function GameUIMasterAchievementTips:OnEraseFromGameState(game_state)
  DebugOut("GameUIMasterAchievementTips:OnEraseFromGameState")
  self:UnloadFlashObject()
end
function GameUIMasterAchievementTips:Update(dt)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
  if isMasterAchievementTipShowed == true and os.time() - tipsShowTime > 1 then
    tipsShowTime = 0
    isMasterAchievementTipShowed = false
    GameUIMasterAchievementTips:HideBox("masterAchievementTips")
  end
end
function GameUIMasterAchievementTips:SetAppendState(state)
  self.m_appendState = state
end
function GameUIMasterAchievementTips.GetAppendState()
  if GameUIMasterAchievementTips.m_appendState then
    return GameUIMasterAchievementTips.m_appendState
  else
    return (...), GameStateManager
  end
end
function GameUIMasterAchievementTips:HideBox(boxType)
  if not self:GetFlashObject() then
    return
  end
  DebugOut("GameUIMasterAchievementTips:HideBox", boxType)
  if boxType == "masterAchievementTips" then
    self:GetFlashObject():InvokeASCallback("_root", "HideMasterAchievementTips")
  end
end
function GameUIMasterAchievementTips:OnFSCommand(cmd, arg)
  if cmd == "boxHiden" then
    self.currentBox = ""
    self.GetAppendState():EraseObject(self)
  elseif cmd == "MasterAchievementTipShowed" then
    isMasterAchievementTipShowed = true
    tipsShowTime = os.time()
  end
end
function GameUIMasterAchievementTips:ShowMasterAchievementTips(cnt, textTitleArr, textAttrArr)
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self.currentBox = "masterAchievementTips"
  GameUIMasterAchievementTips:GetFlashObject():InvokeASCallback("_root", "showMasterAchievementTips", cnt, textTitleArr, textAttrArr)
end
