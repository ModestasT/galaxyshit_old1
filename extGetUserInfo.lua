function ext.T4FGetUserID()
  local userInfo = GameGlobalData:GetUserInfo()
  if userInfo then
    return userInfo.player_id
  end
  return ""
end
function ext.T4FGetUserName()
  local userInfo = GameGlobalData:GetUserInfo()
  if userInfo then
    return userInfo.name
  end
  return ""
end
function ext.T4FGetServerID()
  local serverInfo = GameUtils:GetActiveServerInfo()
  if serverInfo then
    return serverInfo.id
  end
  return GameUtils:GetActiveServerInfo().id or ""
end
function ext.T4FGetServerName()
  return ""
end
function ext.T4FGetAccount()
  local acc_info = GameUtils:GetLoginInfo().AccountInfo or {}
  local _passport = acc_info.passport or ""
  return _passport
end
ext.RegisterNotification = ext.RegisterNotification or function(...)
  return
end
