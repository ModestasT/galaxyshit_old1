local rightMenu_170 = GameData.RightMenu.rightMenu_170
rightMenu_170.fleets = {
  MODULE_NAME = "fleets",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1001102
}
rightMenu_170.bag = {
  MODULE_NAME = "bag",
  PLAYER_REQ_LEVEL = 5,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.hire = {
  MODULE_NAME = "hire",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002012
}
rightMenu_170.arena = {
  MODULE_NAME = "arena",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002012
}
rightMenu_170.friends = {
  MODULE_NAME = "friends",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1003005
}
rightMenu_170.setting = {
  MODULE_NAME = "setting",
  PLAYER_REQ_LEVEL = 1,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.wc = {
  MODULE_NAME = "wc",
  PLAYER_REQ_LEVEL = 1,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.slot = {
  MODULE_NAME = "slot",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002005
}
rightMenu_170.alliance = {
  MODULE_NAME = "alliance",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1003005
}
rightMenu_170.enhance = {
  MODULE_NAME = "enhance",
  PLAYER_REQ_LEVEL = 4,
  BUILDING_REQ = "engineering_bay",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu_170.chat = {
  MODULE_NAME = "chat",
  PLAYER_REQ_LEVEL = 10,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.combat_speed = {
  MODULE_NAME = "combat_speed",
  PLAYER_REQ_LEVEL = 10,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.arena = {
  MODULE_NAME = "arena",
  PLAYER_REQ_LEVEL = 13,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1002010
}
rightMenu_170.rush = {
  MODULE_NAME = "rush",
  PLAYER_REQ_LEVEL = 16,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.equip_evolution = {
  MODULE_NAME = "equip_evolution",
  PLAYER_REQ_LEVEL = 10,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.elite_challenge = {
  MODULE_NAME = "elite_challenge",
  PLAYER_REQ_LEVEL = 10,
  BUILDING_REQ = "commander_academy ",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1001011
}
rightMenu_170.infinite_cosmos = {
  MODULE_NAME = "infinite_cosmos",
  PLAYER_REQ_LEVEL = 21,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.task = {
  MODULE_NAME = "task",
  PLAYER_REQ_LEVEL = 22,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.tech = {
  MODULE_NAME = "tech",
  PLAYER_REQ_LEVEL = 25,
  BUILDING_REQ = "tech_lab",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu_170.colonial = {
  MODULE_NAME = "colonial",
  PLAYER_REQ_LEVEL = 24,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.wd = {
  MODULE_NAME = "wd",
  PLAYER_REQ_LEVEL = 55,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.tc = {
  MODULE_NAME = "tc",
  PLAYER_REQ_LEVEL = 32,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.mining = {
  MODULE_NAME = "mining",
  PLAYER_REQ_LEVEL = 0,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1008009
}
rightMenu_170.remodel = {
  MODULE_NAME = "remodel",
  PLAYER_REQ_LEVEL = 15,
  BUILDING_REQ = "factory",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu_170.krypton = {
  MODULE_NAME = "krypton",
  PLAYER_REQ_LEVEL = 35,
  BUILDING_REQ = "krypton_center",
  BUILDING_REQ_LEVEL = 1,
  BATTLE_ID = 0
}
rightMenu_170.worldboss = {
  MODULE_NAME = "worldboss",
  PLAYER_REQ_LEVEL = 32,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.centalsystem = {
  MODULE_NAME = "centalsystem",
  PLAYER_REQ_LEVEL = 38,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.ac = {
  MODULE_NAME = "ac",
  PLAYER_REQ_LEVEL = 40,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.prime_wve = {
  MODULE_NAME = "prime_wve",
  PLAYER_REQ_LEVEL = 40,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.crusade = {
  MODULE_NAME = "crusade",
  PLAYER_REQ_LEVEL = 45,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.krypton_refine = {
  MODULE_NAME = "krypton_refine",
  PLAYER_REQ_LEVEL = 65,
  BUILDING_REQ = "krypton_center",
  BUILDING_REQ_LEVEL = 14,
  BATTLE_ID = 0
}
rightMenu_170.offline = {
  MODULE_NAME = "offline",
  PLAYER_REQ_LEVEL = 1000,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.reform = {
  MODULE_NAME = "reform",
  PLAYER_REQ_LEVEL = 8,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 1001011
}
rightMenu_170.starwar = {
  MODULE_NAME = "starwar",
  PLAYER_REQ_LEVEL = 90,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.artifact = {
  MODULE_NAME = "artifact",
  PLAYER_REQ_LEVEL = 90,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.empire_colosseum = {
  MODULE_NAME = "empire_colosseum",
  PLAYER_REQ_LEVEL = 90,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.enchant = {
  MODULE_NAME = "enchant",
  PLAYER_REQ_LEVEL = 40,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
rightMenu_170.large_map = {
  MODULE_NAME = "large_map",
  PLAYER_REQ_LEVEL = 40,
  BUILDING_REQ = "undefined",
  BUILDING_REQ_LEVEL = 0,
  BATTLE_ID = 0
}
