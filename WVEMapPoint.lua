WVEMapPoint = luaClass(nil)
function WVEMapPoint:ctor(id, pointType, uiType, coordinates, name)
  self.mID = id
  self.mType = pointType
  self.mUIType = uiType
  self.mCoordinates = {
    mX = coordinates[1],
    mY = coordinates[2]
  }
  self.mOccupyState = nil
  self.mOccupyNum = nil
  self.mName = name
end
function WVEMapPoint:GetCoords()
  return self.mCoordinates
end
