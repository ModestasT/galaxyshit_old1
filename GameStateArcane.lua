local GameStateArcane = GameStateManager.GameStateArcane
local GameObjectArcaneMap = LuaObjectManager:GetLuaObject("GameObjectArcaneMap")
local GameObjectArcaneEnhance = LuaObjectManager:GetLuaObject("GameObjectArcaneEnhance")
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
function GameStateArcane:OnFocusGain(previousGameState)
  DebugOut("GameStateArcane:OnFocusGain")
  local netCall = function()
    NetMessageMgr:SendMsg(NetAPIList.ac_info_req.Code, nil, nil, true, netCall)
  end
  netCall()
  GameObjectArcaneMap:LoadFlashObject()
  if previousGameState == GameStateManager.GameStateLab then
    GameObjectArcaneMap:GetFlashObject():InvokeASCallback("_root", "resetAll")
  else
    GameObjectArcaneMap:RestoreBattleMap()
  end
end
function GameStateArcane:OnFocusLost(newState)
  DebugOut("GameStateArcane:OnFocusLost")
  if newState ~= GameStateManager.GameStateBattlePlay and newState ~= GameStateManager.GameStateFormation then
    DebugOut("EraseObject")
    self:EraseObject(GameObjectArcaneEnhance)
    self:EraseObject(GameObjectArcaneMap)
    self:EraseObject(GameObjectBattleMapBG)
    self.ArcaneInfoData = nil
  end
end
function GameStateArcane:SetArcaneInfo(arcaneInfoData)
  local needEnter = false
  if not self.ArcaneInfoData then
    needEnter = true
  end
  self.ArcaneInfoData = arcaneInfoData
  if needEnter then
    GameObjectBattleMapBG:LoadBGMap(1, 1)
    self:AddObject(GameObjectBattleMapBG)
    self:AddObject(GameObjectArcaneMap)
  end
  if self.EnterCallback then
    self.EnterCallback()
    self.EnterCallback = nil
  end
end
function GameStateArcane:GetArcaneInfo()
  return self.ArcaneInfoData
end
function GameStateArcane:GetArcaneBattleID()
  return self.ArcaneInfoData.ac_level * 1000 + self.ArcaneInfoData.ac_step
end
function GameStateArcane:SetEnterCallback(enterCallback)
  self.EnterCallback = enterCallback
end
function GameStateArcane:Quit()
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
end
