local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameObjectWelcome = LuaObjectManager:GetLuaObject("GameObjectWelcome")
local GameStateWD = GameStateManager.GameStateWD
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameObjectFirstPayPush = LuaObjectManager:GetLuaObject("GameObjectFirstPayPush")
GameNotice = {}
local k_NoticeOn = 1
local k_NoticePriority = {
  PayPush = 0,
  Acitve = 1,
  News = 2,
  Vip = 3,
  Level = 4,
  Online = 5,
  Sign = 6,
  WDBegin = 7,
  Welcome = 8,
  WDResult = 9,
  FacebookPop = 10,
  FirstPayPush = 11
}
GameNotice.m_noticeList = {}
GameNotice.m_fetchFunction = {}
GameNotice.m_fetchCallbackCount = 0
function GameNotice:Init()
  self.RegistFetchInfoFunction(self.FetchWDInfo)
  self.RegistFetchInfoFunction(self.FetchWelcomeInfo)
end
function GameNotice:RemoveRechargePushNotice()
  if self.m_noticeList then
    for k, v in pairs(self.m_noticeList) do
      if "PayPush" == v then
        table.remove(self.m_noticeList, k)
        break
      end
    end
  end
end
function GameNotice:IfExistPayPush()
  if self.m_noticeList then
    for k, v in pairs(self.m_noticeList) do
      if "PayPush" == v then
        return true
      end
    end
  end
  return false
end
function GameNotice:RemoveNotice(noticeName)
  if noticeName and self.m_noticeList then
    for k, v in pairs(self.m_noticeList) do
      if noticeName == v then
        table.remove(self.m_noticeList, k)
        break
      end
    end
  end
end
function GameNotice:GetNoticeListCnt()
  if self.m_noticeList then
    return #self.m_noticeList
  else
    return 0
  end
end
function GameNotice:CheckShowNotice()
  if DebugConfig.isDebugNotice then
  end
  if self.m_noticeList == nil or #self.m_noticeList == 0 then
    return
  end
  for i, name in ipairs(self.m_noticeList) do
    if "FacebookPop" == name or "WDResult" == name or "WDBegin" == name then
      GameNotice:RemoveRechargePushNotice()
    end
  end
  local noticeShow
  local priority = -1
  for i, name in ipairs(self.m_noticeList) do
    if k_NoticePriority[name] then
      if priority < k_NoticePriority[name] then
        noticeShow = name
        priority = k_NoticePriority[name]
      end
    elseif priority == -1 then
      noticeShow = name
    end
  end
  DebugOut("Showjsjsjsj:", noticeShow)
  if noticeShow == "Sign" then
  elseif noticeShow == "FacebookPop" then
    if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] == 1 then
      GameNotice:ShowNotice(noticeShow)
    else
      GameNotice:RemoveNotice(noticeShow)
      GameNotice:CheckShowNotice()
      return
    end
  else
    GameNotice:ShowNotice(noticeShow)
    if noticeShow ~= "PayPush" and GameNotice:IfExistPayPush() then
      GameNotice:ShowNotice("PayPush")
    end
  end
  GameUIRechargePush:ClearStatus()
  self.m_noticeList = {}
end
function GameNotice:ShowNotice(noticeName)
  DebugOut("ShowNotice: " .. noticeName)
  if GameStateManager:GetCurrentGameState() ~= GameStateMainPlanet then
    DebugOut("ShowNotice Failed: 1")
    return
  end
  if GameStateGlobalState:IsObjectInState(GameUICommonDialog) or GameStateGlobalState:IsObjectInState(GameQuestMenu) then
    DebugOut("ShowNotice Failed: 2")
    return
  end
  if noticeName == "Sign" then
  elseif noticeName == "Acitve" then
    GameUIActivityNew:ShowNews("activity")
  elseif noticeName == "Vip" then
  elseif noticeName == "Level" then
    GameUIActivity:ShowNews("level")
  elseif noticeName == "Online" then
    GameUIActivity:ShowNews("online")
  elseif noticeName == "WDBegin" then
    GameObjectMainPlanet:BuildingClicked("world_domination")
  elseif noticeName == "WDResult" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_result)
  elseif noticeName == "Welcome" then
    GameObjectWelcome:ShowWelcome()
  elseif noticeName == "PayPush" then
    local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
    GameUIRechargePush:Show()
  elseif noticeName == "NewFisrtPayPush" then
    local GameObjectFirstPayPush = LuaObjectManager:GetLuaObject("GameObjectFirstPayPush")
    GameObjectFirstPayPush:Show()
  elseif noticeName == "FacebookPop" then
    DebugOut("noticeName FacebookPop should shou login")
    if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] == 1 then
      DebugOut("should shou login")
      if Facebook:IsLoginedIn() then
        if not Facebook:GetCanFetchInvitableFriendList() then
          return
        end
        if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] == 1 then
          FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
          FacebookPopUI:GetFacebookInvitableFriendList()
          if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
            local countryStr = "unknow"
          end
        end
      elseif not Facebook:IsBindFacebook() then
        FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
        FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_FIRSTLOGIN_CREDIT)
        if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
          local countryStr = "unknow"
        end
      else
        if not Facebook:GetCanFetchInvitableFriendList() then
          return
        end
        if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FIRSTLOGIN_BIND] == 1 then
          FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_NO_AWARD_FIRSTLOGIN)
          FacebookPopUI.mLogInAwardCallback = nil
          FacebookPopUI:LoginDirectly()
          if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
            local countryStr = "unknow"
          end
        end
      end
    end
  end
end
function GameNotice:AddNotice(noticeName)
  if self.m_noticeList then
    for i, name in ipairs(self.m_noticeList) do
      if name == noticeName then
        return
      end
    end
    DebugOut("AddNotice: " .. noticeName)
    table.insert(self.m_noticeList, noticeName)
  end
end
function GameNotice:FetchInfo()
  DebugOut("GameNotice:FetchInfo")
  DebugTable(self.m_fetchFunction)
  for i, fun in ipairs(self.m_fetchFunction) do
    fun()
  end
end
function GameNotice.FetchWDInfo()
  local playerList = {}
  NetMessageMgr:SendMsg(NetAPIList.alliance_domination_status_req.Code, nil, GameNotice.WDCInfoCallback, false, nil)
end
function GameNotice.FetchWelcomeInfo()
  GameObjectWelcome:SendWelcomeReq()
end
function GameNotice.RegistFetchInfoFunction(funName)
  table.insert(GameNotice.m_fetchFunction, funName)
end
function GameNotice:CheckFetchInfoCallback()
  self.m_fetchCallbackCount = self.m_fetchCallbackCount + 1
  if self.m_fetchCallbackCount >= #self.m_fetchFunction and #self.m_fetchFunction > 0 then
    self.m_fetchFunction = {}
    GameNotice:CheckShowNotice()
  end
end
function GameNotice.WDCInfoCallback(msgtype, content)
  if msgtype == NetAPIList.alliance_domination_status_ack.Code then
    local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
    if content.can_battle then
      GameNotice:AddNotice("WDBegin")
      GameUIRechargePush:WDPopReceived(true)
    else
      GameUIRechargePush:WDPopReceived(false)
    end
    GameNotice:CheckFetchInfoCallback()
    return true
  end
  return false
end
