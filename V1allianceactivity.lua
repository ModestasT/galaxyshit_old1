local activity = GameData.alliance.activity
table.insert(activity, {
  id = 1,
  level_limit = 2,
  icon = "donate",
  actTitle = "LC_MENU_ALLIANCE_DONATE_CONTENT_TITLE",
  actContent = "LC_MENU_ALLIANCE_DONATE_CONTENT"
})
table.insert(activity, {
  id = 2,
  level_limit = 3,
  icon = "expedition",
  actTitle = "LC_MENU_COSMIC_EXPEDITON_CONTENT_TITLE",
  actContent = "LC_MENU_COSMIC_EXPEDITON_CONTENT"
})
table.insert(activity, {
  id = 3,
  level_limit = 2,
  icon = "def",
  actTitle = "LC_MENU_ALIANCE_FORTRESS_CONTENT_TITLE",
  actContent = "LC_MENU_ALIANCE_FORTRESS_CONTENT"
})
table.insert(activity, {
  id = 4,
  level_limit = 1,
  icon = "rebuild",
  actTitle = "LC_MENU_REBUILD_TITLE",
  actContent = "LC_MENU_REBUILD_CONTENT"
})
