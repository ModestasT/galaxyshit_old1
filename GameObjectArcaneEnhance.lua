local GameStateArcane = GameStateManager.GameStateArcane
local GameObjectArcaneEnhance = LuaObjectManager:GetLuaObject("GameObjectArcaneEnhance")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
function GameObjectArcaneEnhance:OnAddToGameState(gameState)
  self:LoadFlashObject()
  self:UpdateArcaneEnhanceData()
  self:GetFlashObject():InvokeASCallback("_root", "moveInAll")
  local main_fleet = GameGlobalData:GetFleetInfo(1)
  local shipFrame = GameDataAccessHelper:GetShip(1, nil, main_fleet.level)
  DebugOut("shipFrame = ", shipFrame)
  self:GetFlashObject():InvokeASCallback("_root", "setFleetFrmae", shipFrame)
  GameGlobalData:RegisterDataChangeCallback("arcane_info", self.OnArcaneInfoChange)
  GameGlobalData:RegisterDataChangeCallback("resource", self.OnArcaneInfoChange)
end
function GameObjectArcaneEnhance:OnEraseFromGameState(gameState)
  self:UnloadFlashObject()
  GameGlobalData:RemoveDataChangeCallback("arcane_info", self.OnArcaneInfoChange)
  GameGlobalData:RemoveDataChangeCallback("resource", self.OnArcaneInfoChange)
end
function GameObjectArcaneEnhance:UpdateArcaneEnhanceData()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  local prelevelEnhanceInfo = GameDataAccessHelper:GetArcaneEnhanceInfoWithEnhanceLevel(arcaneInfo.ac_energy_level - 1)
  local nxtlevelEnhanceInfo = GameDataAccessHelper:GetArcaneEnhanceInfoWithEnhanceLevel(arcaneInfo.ac_energy_level + 1)
  local arcaneEnhanceInfo = GameDataAccessHelper:GetArcaneEnhanceInfoWithEnhanceLevel(arcaneInfo.ac_energy_level)
  local playerLevelInfo = GameGlobalData:GetData("levelinfo")
  local enhanceInfoTable = {}
  if prelevelEnhanceInfo then
    if nxtlevelEnhanceInfo then
      enhanceInfoTable[#enhanceInfoTable + 1] = tostring(prelevelEnhanceInfo.level) .. " -> " .. tostring(arcaneEnhanceInfo.level)
    else
      enhanceInfoTable[#enhanceInfoTable + 1] = tostring(arcaneEnhanceInfo.level)
    end
  else
    enhanceInfoTable[#enhanceInfoTable + 1] = tostring(0) .. " -> " .. tostring(arcaneEnhanceInfo.level)
  end
  local effectTable = {}
  local techcost = 0
  local supplycost = 0
  if prelevelEnhanceInfo then
    if nxtlevelEnhanceInfo then
      effectTable[#effectTable + 1] = GameDataAccessHelper:GetAttributeEffectText(arcaneEnhanceInfo.effectType1)
      effectTable[#effectTable + 1] = tostring(prelevelEnhanceInfo.effectValue1) .. " -> " .. tostring(arcaneEnhanceInfo.effectValue1)
      effectTable[#effectTable + 1] = GameDataAccessHelper:GetAttributeEffectText(arcaneEnhanceInfo.effectType2)
      effectTable[#effectTable + 1] = tostring(prelevelEnhanceInfo.effectValue2) .. " -> " .. tostring(arcaneEnhanceInfo.effectValue2)
    else
      effectTable[#effectTable + 1] = GameDataAccessHelper:GetAttributeEffectText(arcaneEnhanceInfo.effectType1)
      effectTable[#effectTable + 1] = tostring(arcaneEnhanceInfo.effectValue1)
      effectTable[#effectTable + 1] = GameDataAccessHelper:GetAttributeEffectText(arcaneEnhanceInfo.effectType2)
      effectTable[#effectTable + 1] = tostring(arcaneEnhanceInfo.effectValue2)
    end
  else
    effectTable[#effectTable + 1] = GameDataAccessHelper:GetAttributeEffectText(arcaneEnhanceInfo.effectType1)
    effectTable[#effectTable + 1] = tostring(0) .. " -> " .. tostring(arcaneEnhanceInfo.effectValue1)
    effectTable[#effectTable + 1] = GameDataAccessHelper:GetAttributeEffectText(arcaneEnhanceInfo.effectType2)
    effectTable[#effectTable + 1] = tostring(0) .. " -> " .. tostring(arcaneEnhanceInfo.effectValue2)
  end
  enhanceInfoTable[#enhanceInfoTable + 1] = table.concat(effectTable, "\002")
  enhanceInfoTable[#enhanceInfoTable + 1] = tostring(playerLevelInfo.level >= arcaneEnhanceInfo.conditionLevel)
  enhanceInfoTable[#enhanceInfoTable + 1] = GameLoader:GetGameText("LC_MENU_REQUIRE_LEVEL_CHAR") .. " " .. tostring(arcaneEnhanceInfo.conditionLevel)
  table.insert(enhanceInfoTable, tostring(arcaneEnhanceInfo.costTechnique))
  table.insert(enhanceInfoTable, tostring(arcaneEnhanceInfo.costSupply))
  local enhanceInfoDataString = table.concat(enhanceInfoTable, "\001")
  self:SetEnhanceInfo(enhanceInfoDataString)
  local current_energy = -1
  local max_energy = -1
  current_energy = arcaneEnhanceInfo.conditionEnergy - arcaneInfo.ac_energy
  max_energy = arcaneEnhanceInfo.conditionEnergy
  self:SetChargeInfo(current_energy, max_energy)
  self:UpdateTechPointInfo()
  self:UpdateArcaneSupplyInfo()
  self.LastArcaneInfo = arcaneInfo
end
function GameObjectArcaneEnhance:UpdateTechPointInfo()
  local resourceInfo = GameGlobalData:GetData("resource")
  self:GetFlashObject():InvokeASCallback("_root", "setTechPointInfo", GameUtils.numberConversion(resourceInfo.technique))
end
function GameObjectArcaneEnhance:UpdateArcaneSupplyInfo()
  local arcaneInfo = GameStateArcane:GetArcaneInfo()
  self:GetFlashObject():InvokeASCallback("_root", "setArcaneSupplyInfo", GameUtils.numberConversion(arcaneInfo.ac_supply))
end
function GameObjectArcaneEnhance:SetChargeInfo(valueCurrent, valueMax)
  self:GetFlashObject():InvokeASCallback("_root", "setArcaneEnhanceChargeInfo", valueCurrent, valueMax)
end
function GameObjectArcaneEnhance:SetEnhanceInfo(enhanceInfoDataString)
  self:GetFlashObject():InvokeASCallback("_root", "setArcaneEnhanceInfo", enhanceInfoDataString)
end
function GameObjectArcaneEnhance:AnimationEnergyUp(valueup, strike)
  self:GetFlashObject():InvokeASCallback("_root", "playAnimationEnergyUp", strike, valueup)
end
function GameObjectArcaneEnhance:OnFSCommand(cmd, arg)
  if cmd == "onClose" then
    GameStateArcane:EraseObject(self)
  elseif cmd == "onEnhance" then
    NetMessageMgr:SendMsg(NetAPIList.ac_energy_charge_req.Code, nil, self.NetCallbackCharge, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.ac_energy_charge_req.Code, nil, self.NetCallbackCharge, true, nil)
    GameUtils:RecordForMultiTongdui(NetAPIList.ac_energy_charge_req.Code, nil, self.NetCallbackCharge, true, nil)
  end
end
function GameObjectArcaneEnhance.GotoAffair()
  local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
  GameUIKrypton.GotoGetMoney()
end
function GameObjectArcaneEnhance:CheckIsNeedShowDialog(code)
  if code == 9132 or code == 8916 or code == 100125 then
    local text = AlertDataList:GetTextFromErrorCode(code)
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Caption)
    local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    local affairs = GameLoader:GetGameText("LC_MENU_BUILDING_NAME_AFFAIRS_HALL")
    GameUIMessageDialog:SetRightTextButton(cancel)
    GameUIMessageDialog:SetLeftTextButton(affairs, GameObjectArcaneEnhance.GotoAffair)
    GameUIMessageDialog:Display("", text)
  else
    GameUIGlobalScreen:ShowAlert("error", code, nil)
  end
end
function GameObjectArcaneEnhance.NetCallbackCharge(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.ac_energy_charge_req.Code then
    if content.code ~= 0 then
      GameObjectArcaneEnhance:CheckIsNeedShowDialog(content.code)
    end
    return true
  end
  if msgType == NetAPIList.ac_energy_charge_ack.Code then
    GameObjectArcaneEnhance:AnimationEnergyUp(content.increase_energy, content.strike)
    return true
  end
  return false
end
function GameObjectArcaneEnhance.OnArcaneInfoChange()
  GameObjectArcaneEnhance:UpdateArcaneEnhanceData()
end
if AutoUpdate.isAndroidDevice then
  function GameObjectArcaneEnhance.OnAndroidBack()
    GameObjectArcaneEnhance:GetFlashObject():InvokeASCallback("_root", "moveOutAll")
  end
end
