local GameUIAffairHall = LuaObjectManager:GetLuaObject("GameUIAffairHall")
local GameStateAffairInfo = GameStateManager.GameStateAffairInfo
function GameStateAffairInfo:InitGameState()
end
function GameStateAffairInfo:OnFocusGain(previousState)
  self:AddObject(GameUIAffairHall)
  GameStateAffairInfo:RequestInfo()
end
function GameStateAffairInfo:OnFocusLost(newState)
  self:EraseObject(GameUIAffairHall)
end
function GameStateAffairInfo:RequestInfo()
  GameUIAffairHall:RequestInfo()
end
