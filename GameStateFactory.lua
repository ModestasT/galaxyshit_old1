local GameStateFactory = GameStateManager.GameStateFactory
local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
GameStateFactory.CommanderStatusType = {}
GameStateFactory.CommanderStatusType.InService = 1
GameStateFactory.CommanderStatusType.StandBy = 2
function GameStateFactory:InitGameState()
  self:AddObject(GameUIFactory)
end
function GameStateFactory:OnFocusGain()
  GameUIFactory:LoadFlashObject()
  GameUIFactory:GotoInService()
end
function GameStateFactory:OnFocusLost()
  GameUIFactory:UnloadFlashObject()
end
