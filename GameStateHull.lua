local GameStateHull = GameStateManager.GameStateHull
local GameUIHullMain = LuaObjectManager:GetLuaObject("GameUIHullMain")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameCommonBackground = LuaObjectManager:GetLuaObject("GameCommonBackground")
GameStateHull.buildingName = "hull_portal"
function GameStateHull:InitGameState()
end
function GameStateHull:OnFocusGain(pre_gamestate)
  self._prevActiveGameState = pre_gamestate
  GameUIHullMain:LoadFlashObject()
  self:AddObject(GameUIHullMain)
  self:AddObject(GameCommonBackground)
  self:SelectTab(self.Tabtype)
end
function GameStateHull:SetTabtype(tabtype)
  self.Tabtype = tabtype
end
function GameStateHull:InitGameState()
end
function GameStateHull:SelectTab(tab_type)
  if tab_type == "hull_card" then
    GameUIHullMain:UpdateGameData()
  end
  GameUIHullMain:SetCurrentTab(tab_type)
end
function GameStateHull:OnTabChange(tabIndex)
  if tabIndex == 2 then
    self:AddObject(GameFleetInfoBackground)
    GameFleetInfoBackground:SetRootVisible(false)
  else
    self:EraseObject(GameFleetInfoBackground)
  end
end
function GameStateHull:OnFocusLost(next_gamestate)
  GameUIHullMain:ClearLocalData()
  self:EraseObject(GameCommonBackground)
  self:EraseObject(GameUIHullMain)
end
function GameStateHull:Quit()
  GameUIHullMain:ResetCurrentChoosedHulls()
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  self:OnTabChange(0)
end
