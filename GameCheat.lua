GameCheat = {
  m_cheatCode = {
    "00000000",
    "00000001",
    "00000011",
    "00000111",
    "00001111",
    "00011111",
    "00111111"
  },
  m_currentCode = ""
}
if not AutoUpdate.isDeveloper then
  GameCheat = {}
end
function GameCheat:CheatInput(code)
  if not AutoUpdate.isDeveloper or not self.m_currentCode then
    return
  end
  if self.m_currentCode then
    self.m_currentCode = self.m_currentCode .. code
    local haveMatchStartWith = false
    local fullMathIndex
    for i, v in pairs(self.m_cheatCode) do
      if LuaUtils:string_startswith(v, self.m_currentCode) then
        haveMatchStartWith = true
        if v == self.m_currentCode then
          fullMathIndex = i
          break
        end
      end
    end
    if fullMathIndex then
      self:CheatCall(fullMathIndex)
      haveMatchStartWith = false
    end
    if not haveMatchStartWith then
      self.m_currentCode = ""
    end
  end
end
function GameCheat:CheatCall(index)
  if not AutoUpdate.isDeveloper or not self.m_currentCode then
    return
  end
  local OutInfo
  if index == 1 then
    DebugCommand.fps()
    OutInfo = "FPS"
  elseif index == 2 then
    k_DebugFlurryInTip = not k_DebugFlurryInTip
    if k_DebugFlurryInTip then
      OutInfo = "Show Tracking Tip true"
    else
      OutInfo = "Show Tracking Tip false"
    end
  elseif index == 3 then
    DebugCommand.loadbattle()
  elseif index == 4 then
    DebugCommand.freebattle()
  elseif index == 5 then
    SKIP_BATTLE_PLAY_PRELOAD = not SKIP_BATTLE_PLAY_PRELOAD
    if SKIP_BATTLE_PLAY_PRELOAD then
      OutInfo = "SKIP_BATTLE_PLAY_PRELOAD true"
    else
      OutInfo = "SKIP_BATTLE_PLAY_PRELOAD false"
    end
  elseif index == 6 then
    SKIP_BATTLE_BG_PRELOAD = not SKIP_BATTLE_BG_PRELOAD
    if SKIP_BATTLE_BG_PRELOAD then
      OutInfo = "SKIP_BATTLE_BG_PRELOAD true"
    else
      OutInfo = "SKIP_BATTLE_BG_PRELOAD false"
    end
  elseif index == 7 then
    USE_SIMPLE_BATTLE_BG = not USE_SIMPLE_BATTLE_BG
    if USE_SIMPLE_BATTLE_BG then
      OutInfo = "USE_SIMPLE_BATTLE_BG true"
    else
      OutInfo = "USE_SIMPLE_BATTLE_BG false"
    end
  end
  local GameTip = LuaObjectManager:GetLuaObject("GameTip")
  OutInfo = OutInfo or "GameCheat:CheatCall " .. index
  GameTip:Show(OutInfo)
end
