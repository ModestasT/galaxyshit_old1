local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
local GameStateStarSystem = GameStateManager.GameStateStarSystem
GameUIStarSystemPort.uiRecycle = {}
local GameUIStarSystemRecycle = GameUIStarSystemPort.uiRecycle
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameStateStore = GameStateManager.GameStateStore
local DynamicResDownloader = DynamicResDownloader
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local g_curShowType = ""
local _getMedalinfoProp = function(tinfo, strtype)
  assert(tinfo)
  if strtype == "name" then
    local id = math.ceil(tinfo.type / 100000 - 1) * 100000 + tinfo.type % 10
    return ...
  elseif strtype == "info" then
    local id = math.ceil(tinfo.type / 100000 - 1) * 100000 + tinfo.type % 10
    return ...
  elseif strtype == "strlv" then
    return GameUtils:TryGetText("LC_MENU_Level") .. tinfo.level
  elseif strtype == "frame" then
    local id = math.ceil(tinfo.type / 100000 - 1) * 100000 + tinfo.type % 10
    return "medal_" .. id
  elseif strtype == "pic" then
    local id = math.ceil(tinfo.type / 100000 - 1) * 100000 + tinfo.type % 10
    return id .. ".png"
  end
end
function GameUIStarSystemRecycle:GetFlashObject()
  return (...), GameUIStarSystemPort_SUB
end
function GameUIStarSystemRecycle:ShowReset()
  g_curShowType = "reset"
  LuaUtils:BubbleSort(GameUIStarSystemPort.allMedalInfos, function(a, b)
    if a.quality == b.quality then
      if a.rank == b.rank then
        if a.level == b.level then
          return a.exp <= b.exp
        else
          return a.level <= b.level
        end
      else
        return a.rank <= b.rank
      end
    else
      return a.quality <= b.quality
    end
  end)
  local tparam = {}
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    local sum = 0
    for _, vv in pairs(v.reset_material.medals) do
      sum = sum + vv.num
    end
    for _, vv in pairs(v.reset_material.materials) do
      sum = sum + vv.num
    end
    if sum > 0 then
      local t = {}
      t.strname = _getMedalinfoProp(v, "name")
      t.stricon = _getMedalinfoProp(v, "frame")
      t.strpic = _getMedalinfoProp(v, "pic")
      t.strlv = _getMedalinfoProp(v, "strlv")
      t.strbtnname = GameUtils:TryGetText("LC_MENU_MEDAL_RECYCLE_RESET_BUTTON")
      t.nstar = v.rank
      t.quality = v.quality
      t.id = v.id
      table.insert(tparam, t)
    end
  end
  local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "InitResetItems", tparam)
  GameGlobalData:RemoveDataChangeCallback("resource", self.RefreshResource)
end
function GameUIStarSystemRecycle:ShowResolve(tfilter, onlyUpdate)
  g_curShowType = "resolve"
  local isfirst = true
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    if v.isSelected ~= nil then
      isfirst = false
      break
    end
  end
  if isfirst then
    tfilter = {}
  end
  LuaUtils:BubbleSort(GameUIStarSystemPort.allMedalInfos, function(a, b)
    if a.quality == b.quality then
      if a.rank == b.rank then
        if a.level == b.level then
          return a.exp <= b.exp
        else
          return a.level <= b.level
        end
      else
        return a.rank <= b.rank
      end
    else
      return a.quality <= b.quality
    end
  end)
  local tparam = {
    listparam = {}
  }
  for _, v in ipairs(GameUIStarSystemPort.allMedalInfos) do
    if v.quality > 2 then
      local t = {}
      t.stricon = _getMedalinfoProp(v, "frame")
      t.strpic = _getMedalinfoProp(v, "pic")
      t.strlv = _getMedalinfoProp(v, "strlv")
      t.nstar = v.rank
      t.id = v.id
      t.quality = v.quality
      t.strname = _getMedalinfoProp(v, "name")
      if tfilter then
        if tfilter[v.quality] then
          t.isSelected = true
          v.isSelected = true
        else
          t.isSelected = false
          v.isSelected = false
        end
      else
        assert(v.isSelected ~= nil)
        t.isSelected = v.isSelected
      end
      table.insert(tparam.listparam, t)
    end
  end
  local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "InitResolveItems", tparam, onlyUpdate)
  GameUIStarSystemRecycle:OnResolveSelectChange()
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  self.RefreshResource()
end
function GameUIStarSystemRecycle:OnResolveSelectChange()
  local sum = 0
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    if v.isSelected then
      sum = sum + v.decompose_price
    end
  end
  local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
  local gametext = GameUtils:TryGetText("LC_MENU_MEDAL_RECYCLE_DECOMPOSE_CUE", "fenjie ke huode:")
  flash_obj:InvokeASCallback("_root", "SetResolve", gametext, "medal_resource", sum)
end
function GameUIStarSystemRecycle:Show(strshow, aftershowCallback, closeCallback)
  GameUIStarSystemRecycle.closeCallback = closeCallback
  GameUIStarSystemPort:GetAllMedalInfo(function()
    local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
    flash_obj:InvokeASCallback("_root", "ShowRecyle")
    if strshow == "reset" then
      GameUIStarSystemRecycle:ShowReset()
    elseif strshow == "resolve" then
      GameUIStarSystemRecycle:ShowResolve()
    else
      assert(false)
    end
    if aftershowCallback then
      aftershowCallback()
    end
  end)
end
function GameUIStarSystemRecycle:Hide()
  GameTimer:Add(function()
    if GameUIStarSystemRecycle.closeCallback then
      GameUIStarSystemRecycle.closeCallback()
    end
    GameUIStarSystemRecycle.closeCallback = nil
  end, 100)
  GameGlobalData:RemoveDataChangeCallback("resource", self.RefreshResource)
end
function GameUIStarSystemRecycle:onResolveBtnChoice()
  local tparam = {
    1,
    1,
    1
  }
  local tall = {
    0,
    0,
    0
  }
  local tselected = {
    0,
    0,
    0
  }
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    if v.quality > 2 then
      tall[v.quality - 2] = tall[v.quality - 2] + 1
      if v.isSelected then
        tselected[v.quality - 2] = tselected[v.quality - 2] + 1
      end
    end
  end
  for i = 1, 3 do
    if tall[i] == tselected[i] then
      if tall[i] == 0 then
        tparam[i] = -1
      else
        tparam[i] = 1
      end
    else
      tparam[i] = 0
    end
  end
  local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "InitResolveChoice", tparam, false)
end
local function _onResetCallback(msgtype, content)
  if msgtype == NetAPIList.medal_reset_confirm_ack.Code then
    GameUIStarSystemPort:GetAllMedalInfo(function()
      local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
      GameUIStarSystemRecycle:ShowReset()
      GameUIStarSystemRecycle.resetParam.strtitle = GameUtils:TryGetText("LC_MENU_MEDAL_RECYCLE_RESET_SUCCESS")
      local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
      flash_obj:InvokeASCallback("_root", "ShowResetResult", GameUIStarSystemRecycle.resetParam)
      GameUIStarSystemRecycle.resetParam = nil
    end)
    return true
  elseif msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_reset_confirm_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIStarSystemRecycle:onResolveitem(cmd, arg)
  GameUIStarSystemRecycle:onIconitem(cmd, arg)
end
function GameUIStarSystemRecycle:onResolveSelect(cmd, arg)
  local vid, isselect = unpack(LuaUtils:string_split(arg, "|"))
  vid = tonumber(vid)
  isselect = tonumber(isselect)
  local finded = false
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    if v.id == vid then
      if isselect == 0 then
        v.isSelected = false
      else
        v.isSelected = true
      end
      finded = true
      break
    end
  end
  assert(finded, arg)
  GameUIStarSystemRecycle:OnResolveSelectChange()
  GameUIStarSystemRecycle:onResolveBtnChoice()
  GameUIStarSystemRecycle:ShowResolve(nil, true)
end
function GameUIStarSystemRecycle:onResetitem(cmd, arg)
  local vid = tonumber(arg)
  local tinfo
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    if v.id == vid then
      tinfo = v
    end
  end
  assert(tinfo, "bad id " .. vid)
  local tparam = {}
  tparam.strtitle = GameUtils:TryGetText("LC_MENU_MEDAL_RECYCLE_RESET", "chongzhihou ni jiang huode")
  tparam.strinfo = GameUtils:TryGetText("LC_MENU_MEDAL_RECYCLE_RESET_DESC", "chongzhihou jiang fanhuan <number>% cailiao, shifou jixu")
  tparam.strinfo = string.gsub(tparam.strinfo, "<number>", tonumber(tinfo.reset_ratio) * 100)
  tparam.id = vid
  tparam.ticons = {}
  for _, v in pairs(tinfo.reset_material.medals) do
    if v.num > 0 then
      local t = {}
      t.stricon = _getMedalinfoProp(v, "frame")
      t.strpic = _getMedalinfoProp(v, "pic")
      t.id = v.type
      t.strcnt = "X" .. v.num
      t.quality = v.quality
      t.strname = _getMedalinfoProp(v, "name")
      table.insert(tparam.ticons, t)
    end
  end
  for _, v in pairs(tinfo.reset_material.materials) do
    if v.num > 0 then
      local t = {}
      t.stricon = "medal_item_" .. v.type
      t.strpic = v.type .. ".png"
      t.id = v.type
      t.strcnt = "X" .. v.num
      t.quality = v.quality
      t.strname = GameHelper:GetAwardTypeText("medal_item", v.type)
      table.insert(tparam.ticons, t)
    end
  end
  local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "ShowResetPop", tparam)
  GameUIStarSystemRecycle.resetParam = tparam
end
function GameUIStarSystemRecycle:showMedalDetail(tinfo)
  local param = {}
  param.strname = _getMedalinfoProp(tinfo, "name")
  param.strinfo = _getMedalinfoProp(tinfo, "info")
  local gametxt = GameUtils:TryGetText("LC_MENU_MEDAL_MEDAL_LEVEL")
  gametxt = gametxt .. "<n1>/<n2>"
  gametxt = string.gsub(gametxt, "<n1>", tostring(tinfo.level))
  gametxt = string.gsub(gametxt, "<n2>", tostring(30))
  param.strlv1 = gametxt
  gametxt = GameUtils:TryGetText("LC_MENU_MEDAL_MEDAL_RANK")
  gametxt = gametxt .. "<n1>/<n2>"
  gametxt = string.gsub(gametxt, "<n1>", tostring(tinfo.rank))
  gametxt = string.gsub(gametxt, "<n2>", tostring(6))
  param.strlv2 = gametxt
  param.strlv = _getMedalinfoProp(tinfo, "strlv")
  param.stricon = GameHelper:GetAwardTypeIconFrameName("medal_new_icon", tinfo.type)
  param.strpic = GameHelper:GetGameItemDownloadPng("medal_new_icon", tinfo.type)
  param.nstar = tinfo.rank
  param.quality = tinfo.quality
  param.strpropname = GameUtils:TryGetText("LC_MENU_TACTICAL_CENTER_CHARACTER_CHAR", "shuxing")
  param.tp = {}
  for _, vv in pairs(tinfo.attr) do
    local t = {}
    t.nvalue = vv.attr_value
    t.aid = vv.attr_id
    table.insert(param.tp, t)
  end
  for _, vv in pairs(tinfo.attr_rank) do
    local t = {}
    t.nvalue = vv.attr_value
    t.aid = vv.attr_id
    local finded = false
    for _, vvv in pairs(param.tp) do
      if vvv.aid == t.aid then
        finded = true
        vvv.nvalue = vvv.nvalue + vv.attr_value
        break
      end
    end
    if not finded then
      table.insert(param.tp, t)
    end
  end
  for _, vv in pairs(tinfo.attr_add) do
    local t = {}
    t.nvalue = vv.attr_value * (tinfo.level - 1)
    t.aid = vv.attr_id
    local finded = false
    for _, vvv in pairs(param.tp) do
      if vvv.aid == t.aid then
        finded = true
        vvv.nvalue = vvv.nvalue + t.nvalue
        break
      end
    end
    if not finded then
      table.insert(param.tp, t)
    end
  end
  local oldtp = param.tp
  param.tp = {}
  for _, v in pairs(oldtp) do
    if v.nvalue > 0 then
      v.nvalue = GameUtils:GetAttrValue(v.aid, v.nvalue)
      v.strproerty = GameUtils:GetAttrName(v.aid)
      table.insert(param.tp, v)
    end
  end
  local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "ShowResolveDetail", param)
end
function GameUIStarSystemRecycle:onIconitem(cmd, arg)
  local tinfo
  local nid = tonumber(arg)
  assert(nid, arg)
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    if v.id == nid then
      if not v.haveConfig then
        v = GameUtils:MedalHelper_GetMedalConfig(v, v.type, v.level)
      end
      tinfo = v
    end
  end
  assert(tinfo, "bad id " .. nid)
  GameUIStarSystemRecycle:showMedalDetail(tinfo)
end
function _onResetPopIconCallback(msgtype, content)
  if msgtype == NetAPIList.medal_config_info_ack.Code then
    if not content.medal.haveConfig then
      content.medal = GameUtils:MedalHelper_GetMedalConfig(content.medal, content.medal.type, content.medal.level)
    end
    GameUIStarSystemRecycle:showMedalDetail(content.medal)
    return true
  elseif msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_config_info_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIStarSystemRecycle:onResetItemIcons(cmd, arg)
  local targ = LuaUtils:string_split(arg, "|")
  local nid = tonumber(targ[1])
  local nqu = tonumber(targ[2])
  if nid > 10000 then
    local param = {type = nid}
    NetMessageMgr:SendMsg(NetAPIList.medal_config_info_req.Code, param, _onResetPopIconCallback, true, nil)
  else
    local item = {
      item_type = "medal_item",
      number = nid,
      no = 1,
      quality = nqu
    }
    ItemBox:showItemBox("Medal", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
end
function GameUIStarSystemRecycle:onResetCancel(cmd, arg)
end
function GameUIStarSystemRecycle:onResetConfirm(cmd, arg)
  local param = {
    id = tonumber(arg)
  }
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateEquipEnhance then
    NetMessageMgr:SendMsg(NetAPIList.medal_reset_confirm_req.Code, param, GameUIStarSystemPort.medalEquip.OnResetCallback, true, nil)
  else
    NetMessageMgr:SendMsg(NetAPIList.medal_reset_confirm_req.Code, param, _onResetCallback, true, nil)
  end
end
local function _onDecomposeCallback(msgtype, content)
  if msgtype == NetAPIList.medal_decompose_confirm_ack.Code then
    GameUIStarSystemPort:GetAllMedalInfo(function()
      local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
      GameUIStarSystemRecycle:ShowResolve({})
    end)
    return true
  elseif msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_decompose_confirm_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIStarSystemRecycle:onRecycleResolveConfirm(cmd, arg)
  local allID = {}
  local sum = 0
  for _, v in pairs(GameUIStarSystemPort.allMedalInfos) do
    if v.isSelected then
      sum = sum + v.decompose_price
      table.insert(allID, v.id)
    end
  end
  if sum == 0 then
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(GameUtils:TryGetText("LC_MENU_MEDAL_RECYCLE_DECOMPOSE_NULL"))
    return
  end
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetNoButton(nil)
  local function callback()
    local param = {medals = allID}
    NetMessageMgr:SendMsg(NetAPIList.medal_decompose_confirm_req.Code, param, _onDecomposeCallback, true, nil)
  end
  GameUIMessageDialog:SetYesButton(callback)
  local info = GameUtils:TryGetText("LC_MENU_MEDAL_RECYCLE_DECOMPOSE_WARNING", "fenjiehou ke huode <number> ge, shifou queren")
  info = string.gsub(info, "<number>", tonumber(sum))
  GameUIMessageDialog:Display("", info)
end
function GameUIStarSystemRecycle:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  local resnum = resource.medal_resource or 0
  local flash_obj = GameUIStarSystemRecycle:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "RefreshResolveRes", resnum)
  end
end
function GameUIStarSystemRecycle:onRecycleChoice(cmd, arg)
  local targ = LuaUtils:string_split(arg, "|")
  local tfilter = {}
  if targ[1] == "1" then
    tfilter[3] = true
  end
  if targ[2] == "1" then
    tfilter[4] = true
  end
  if targ[3] == "1" then
    tfilter[5] = true
  end
  GameUIStarSystemRecycle:ShowResolve(tfilter, true)
end
