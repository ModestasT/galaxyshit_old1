local GameStateColonization = GameStateManager.GameStateColonization
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
local GameUISlaveSelect = LuaObjectManager:GetLuaObject("GameUISlaveSelect")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
GameUISlaveSelect.PlayerListType = {
  unknown = -1,
  loserList = 2,
  winerList = 3
}
GameUISlaveSelect.playerList_loser = {}
GameUISlaveSelect.playerList_winer = {}
GameUISlaveSelect.curPlayerList = {}
GameUISlaveSelect.curPlayerListID = GameUISlaveSelect.PlayerListType.unknown
GameUISlaveSelect.curFightPlayer = nil
function GameUISlaveSelect:OnInitGame()
  DebugOut("GameUISlaveSelect:OnInitGame()")
  GameGlobalData:RegisterDataChangeCallback("vipinfo", self.onLeveOrVIPChanged)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", self.onLeveOrVIPChanged)
  self.maxareaId = 5
  self.currentareaId = -1
  self.enterAreaId = -1
  self.enterSectionId = -1
  self.curPlayerList = {}
  self.curPlayerListID = GameUISlaveSelect.PlayerListType.unknown
  self.curFightPlayer = nil
  self.enterSection = false
end
function GameUISlaveSelect:Init()
  DebugOut("GameUISlaveSelect:Init")
  self:SetTabSelected(GameUISlaveSelect.PlayerListType.loserList, true)
  local playerList = {}
  table.insert(playerList, GameUISlaveSelect.PlayerListType.loserList)
  table.insert(playerList, GameUISlaveSelect.PlayerListType.winerList)
  local requestParam = {types = playerList}
  NetMessageMgr:SendMsg(NetAPIList.colony_users_req.Code, requestParam, GameUISlaveSelect.FetchListCallback, true, nil)
end
function GameUISlaveSelect:OnAddToGameState()
  DebugOut("GameUISlaveSelect:OnAddToGameState()")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:Init()
  self:MoveIn()
  GameGlobalData:RegisterDataChangeCallback("newChatTotalCount", GameUISlaveSelect.UpdateChatButtonStatus)
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  if chatCount and chatCount > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  end
end
function GameUISlaveSelect:OnEraseFromGameState()
  DebugFestival("GameUISlaveSelect:OnEraseFromGameState()")
  self.currentareaId = -1
  self.enterAreaId = -1
  self.enterSectionId = -1
  self.enterSection = false
  self.playerList_loser = {}
  self.playerList_winer = {}
  self.curPlayerList = {}
  self.curFightPlayer = nil
  self:UnloadFlashObject()
  GameGlobalData:RemoveDataChangeCallback("newChatTotalCount", GameUISlaveSelect.UpdateChatButtonStatus)
end
function GameUISlaveSelect:MoveIn()
  self:UpdateChatMessage("")
  self:SetHeadInfo()
  self:SetExpInfo()
  self:SetTimesInfo()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn")
end
function GameUISlaveSelect:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameUISlaveSelect:UpdateChatMessage(htmlText)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setChatContent", htmlText)
  end
end
function GameUISlaveSelect:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local vipTime = 0
    local vipTimeStr
    if GameVipDetailInfoPanel.mTmpInfoFetchTime and 0 < GameVipDetailInfoPanel.mTmpInfoFetchTime then
      vipTime = GameVipDetailInfoPanel.mTmpInfo.left_time - (os.time() - GameVipDetailInfoPanel.mTmpInfoFetchTime)
    end
    if vipTime > 0 then
      vipTimeStr = GameUtils:formatTimeString(vipTime)
    end
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", vipTimeStr)
    flash_obj:Update(dt)
  end
end
function GameUISlaveSelect:SetExpInfo()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local expStr = ""
    if GameStateColonization.curExpInfo ~= nil then
      expStr = GameUtils.numberConversion(GameStateColonization.curExpInfo.ntf.experience) .. "/" .. GameUtils.numberConversion(GameStateColonization.curExpInfo.ntf.max_experience)
    end
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if levelInfo.level == GameGlobalData.max_level then
      flash_obj:InvokeASCallback("_root", "setExpTitleText", GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_DAILY_MONEY_CHAR"))
    end
    flash_obj:InvokeASCallback("_root", "setExpText", expStr)
  end
  return true
end
function GameUISlaveSelect:SetTimesInfo()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.colonist)
    local titleStr = GameStateColonization:GetActionTitleString(GameStateColonization.TimesType.colonist)
    flash_obj:InvokeASCallback("_root", "setTimesText", titleStr, timesStr)
  end
  return true
end
if AutoUpdate.isAndroidDevice then
  function GameUISlaveSelect.OnAndroidBack()
    GameUISlaveSelect:MoveOut()
  end
end
function GameUISlaveSelect:OnFSCommand(cmd, arg)
  DebugColonial("GameUISlaveSelect command: ", cmd, arg)
  if cmd == "close_menu" then
    self:MoveOut()
  elseif cmd == "move_out_finish" then
    GameStateColonization:backToColonial()
  elseif cmd == "PopupChatWindow" then
    self:HideChatIcon()
    GameStateColonization:AddObject(GameUIChat)
    GameUIChat:SelectChannel("world")
  elseif cmd == "nextAnimOver" or cmd == "prevAnimOver" then
    if cmd == "nextAnimOver" and self.nextAnimStart then
      self.nextAnimStart = false
      self.currentareaId = self.currentareaId + 1
    elseif cmd == "prevAnimOver" and self.prevAnimStart then
      self.prevAnimStart = false
      self.currentareaId = self.currentareaId - 1
    end
    self:RefreshBackground()
  elseif cmd == "showNextPart" or cmd == "showPrevPart" then
    if self.nextAnimStart then
      self.currentareaId = self.currentareaId + 1
      self.nextAnimStart = false
      self:RefreshBackground()
    elseif self.prevAnimStart then
      self.currentareaId = self.currentareaId - 1
      self.prevAnimStart = false
      self:RefreshBackground()
    end
    if cmd == "showNextPart" then
      self.nextAnimStart = true
      if self.currentareaId == self.maxareaId then
        self.nextAnimStart = false
        self:GetFlashObject():InvokeASCallback("_root", "CancelPartsAnim")
      end
    elseif cmd == "showPrevPart" then
      self.prevAnimStart = true
      if self.currentareaId == 1 then
        self.prevAnimStart = false
        self:GetFlashObject():InvokeASCallback("_root", "CancelPartsAnim")
      end
    end
  elseif cmd == "selectSection" then
    local btn_index = tonumber(arg)
    local index = (self.currentareaId - 1) * 4 + btn_index
    DebugColonial(index)
    DebugColonialTable(self.curPlayerList)
    if #self.curPlayerList == 0 or index > #self.curPlayerList then
      return
    else
      self.curFightPlayer = LuaUtils:table_rcopy(self.curPlayerList[index])
      local defid = "0"
      local defpos = 1
      local thirdid = ""
      if self.curPlayerList[index].position == GameStateColonization.IdentityType.slave then
        defid = "" .. self.curFightPlayer.owner_id
        defpos = GameStateColonization.IdentityType.colonist
        thirdid = self.curFightPlayer.player_id
      else
        defid = "" .. self.curFightPlayer.player_id
        defpos = self.curFightPlayer.position
        thirdid = ""
      end
      local requestParam = {
        fight_type = GameStateColonization.FightType.colonial,
        def_id = defid,
        def_pos = defpos,
        third_id = thirdid
      }
      DebugColonialTable(requestParam)
      if GameStateManager:GetCurrentGameState().fightRoundData then
        GameStateManager:GetCurrentGameState().fightRoundData = nil
      end
      NetMessageMgr:SendMsg(NetAPIList.colony_challenge_req.Code, requestParam, GameUISlaveSelect.FightCallback, true, nil)
    end
  elseif cmd == "tab_selected" then
    local index = tonumber(arg)
    local force = false
    if index == 1 then
      self:SetTabSelected(GameUISlaveSelect.PlayerListType.loserList, force)
    elseif index == 2 then
      self:SetTabSelected(GameUISlaveSelect.PlayerListType.winerList, force)
    end
  elseif cmd == "buyCredit" then
    do
      local function purchase_callback()
        local requestParam = {
          action_type = GameStateColonization.TimesType.colonist
        }
        NetMessageMgr:SendMsg(NetAPIList.colony_action_purchase_req.Code, requestParam, GameUISlaveSelect.BuyTimesListCallback, true, nil)
      end
      if GameSettingData.EnablePayRemind ~= nil and GameSettingData.EnablePayRemind then
        do
          local price = 300
          local function price_callback(msgType, content)
            if msgType == NetAPIList.common_ack.Code then
              if content.api == NetAPIList.price_req.Code then
                if content.code ~= 0 then
                  GameUIGlobalScreen:ShowAlert("error", content.code, nil)
                end
                return true
              end
            elseif msgType == NetAPIList.price_ack.Code then
              price = content.price
              local text_content = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_ADD_COLON_COUNT_ALERT")
              text_content = string.format(text_content, price)
              text_content = GameStateColonization:MakeMsgs(text_content, "", "", "", "1", false)
              GameUtils:CreditCostConfirm(text_content, purchase_callback)
              return true
            end
            return false
          end
          local price_requestParam = {
            price_type = "capture_cost",
            type = 0
          }
          NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_requestParam, price_callback, true, nil)
        end
      else
        purchase_callback()
      end
    end
  end
end
function GameUISlaveSelect:HideChatIcon()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "hideChatIcon")
  end
end
function GameUISlaveSelect:ShowChatIcon()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "showChatIcon")
  end
end
function GameUISlaveSelect:SetHeadInfo()
  local userInfo = GameGlobalData:GetData("userinfo")
  local name_display = GameUtils:GetUserDisplayName(userInfo.name)
  local sex = GameGlobalData:GetUserInfo().sex
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex], player_main_fleet.level)
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local identityStr = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_INDEPENDENT_TITLE")
  if GameUIColonial.curColonyInfo ~= nil then
    identityStr = GameStateColonization:GetIdentityStrByPosition(GameUIColonial.curColonyInfo.info.position)
  end
  local flash_obj = GameUISlaveSelect:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setHeadAreaInfo", GameUIColonial.curScreenLayerType, name_display, player_avatar, levelInfo.level, curLevel, identityStr)
  end
end
function GameUISlaveSelect.onLeveOrVIPChanged()
  local flash_obj = GameUISlaveSelect:GetFlashObject()
  if flash_obj then
    GameUISlaveSelect.SetHeadInfo()
  end
end
function GameUISlaveSelect:SetTabSelected(list_id, force)
  if list_id == self.curPlayerListID and not force then
    return
  end
  if list_id == GameUISlaveSelect.PlayerListType.loserList then
    self.curPlayerListID = GameUISlaveSelect.PlayerListType.loserList
    self.curPlayerList = self.playerList_loser
  else
    self.curPlayerListID = GameUISlaveSelect.PlayerListType.winerList
    self.curPlayerList = self.playerList_winer
  end
  self.maxareaId = math.ceil(#self.curPlayerList / 4)
  self.currentareaId = 1
  self:RefreshBackground()
  local flash_obj = GameUISlaveSelect:GetFlashObject()
  if flash_obj then
    local tips = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_ROUL_CHAR")
    if self.curPlayerListID == GameUISlaveSelect.PlayerListType.winerList then
      tips = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_RECENT_PLUNDER_CHAR")
    end
    flash_obj:InvokeASCallback("_root", "SetTipsText", tips)
  end
end
function GameUISlaveSelect:RefreshBackground()
  DebugColonial("RefreshBackground: ", self.currentareaId, self.maxareaId)
  self.currentareaId = math.max(1, self.currentareaId)
  self.currentareaId = math.min(self.maxareaId, self.currentareaId)
  DebugColonial("RefreshBackground: ", self.currentareaId, self.maxareaId)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshSection", self.currentareaId, self.maxareaId)
  local name, avatar, identity, lv, force, alliance, planet = "", "male", "", "", "", "", ""
  if self.currentareaId > 1 then
    name, avatar, identity, lv, force, alliance, planet = self:GetSectionContent(self.currentareaId - 1)
    self:GetFlashObject():InvokeASCallback("_root", "RefreshSlaveSectionContent", 0, name, avatar, identity, lv, force, alliance, planet)
  end
  if self.currentareaId < self.maxareaId then
    name, avatar, identity, lv, force, alliance, planet = self:GetSectionContent(self.currentareaId + 1)
    self:GetFlashObject():InvokeASCallback("_root", "RefreshSlaveSectionContent", 2, name, avatar, identity, lv, force, alliance, planet)
  end
  name, avatar, identity, lv, force, alliance, planet = self:GetSectionContent(self.currentareaId)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshSlaveSectionContent", 1, name, avatar, identity, lv, force, alliance, planet)
end
function GameUISlaveSelect:GetSectionContent(areaId)
  local name, avatar, identity, lv, force, alliance, planet = "", "", "", "", "", "", ""
  if areaId == 0 then
    return name, avatar, identity, lv, force, alliance, planet
  end
  local _start = (areaId - 1) * 4 + 1
  local _end = _start + 3
  DebugColonial("GetSectionContent = ", areaId, _start, _end)
  DebugColonialTable(self.curPlayerList)
  DebugOut("self.curPlayerList")
  DebugTable(self.curPlayerList)
  if _end > #self.curPlayerList then
    _end = #self.curPlayerList
  end
  for i = _start, _end do
    name = name .. GameUtils:GetUserDisplayName(self.curPlayerList[i].name) .. "\001"
    if self.curPlayerList[i].icon ~= 0 and self.curPlayerList[i].icon ~= 1 then
      avatar = avatar .. GameDataAccessHelper:GetFleetAvatar(self.curPlayerList[i].icon, self.curPlayerList[i].main_fleet_level) .. "\001"
    else
      avatar = avatar .. GameUtils:GetPlayerAvatarWithSex(self.curPlayerList[i].sex, self.curPlayerList[i].main_fleet_level) .. "\001"
    end
    lv = lv .. GameLoader:GetGameText("LC_MENU_Level") .. self.curPlayerList[i].level .. "\001"
    force = force .. GameUtils.numberConversion(self.curPlayerList[i].force) .. "\001"
    alliance = alliance .. self.curPlayerList[i].alliance_name .. "\001"
    identity = identity .. GameStateColonization:GetIdentityStrByPosition(self.curPlayerList[i].position) .. "\001"
    planet = planet .. GameStateColonization:GetIconStrByPosition(self.curPlayerList[i]) .. "\001"
  end
  DebugOut("avatar = ", avatar)
  return name, avatar, identity, lv, force, alliance, planet
end
function GameUISlaveSelect.FetchListCallback(msgType, content)
  if msgType == NetAPIList.colony_users_ack.Code then
    DebugColonial("GameUISlaveSelect.FetchListCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    GameUISlaveSelect.playerList_loser = LuaUtils:table_rcopy(content.users_list[1].users)
    GameUISlaveSelect.playerList_winer = LuaUtils:table_rcopy(content.users_list[2].users)
    DebugColonialTable(GameUISlaveSelect.playerList_loser)
    DebugColonialTable(GameUISlaveSelect.playerList_winer)
    local flash_obj = GameUISlaveSelect:GetFlashObject()
    if flash_obj then
      local force = true
      GameUISlaveSelect:SetTabSelected(GameUISlaveSelect.PlayerListType.loserList, force)
    end
    return true
  end
  return false
end
function GameUISlaveSelect.FightCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_challenge_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_challenge_ack.Code then
    DebugColonial("GameUISlaveSelect.FightCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    do
      local window_type = ""
      GameUIBattleResult:SetFightReport(content.report, nil, nil)
      if content.result == 1 then
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        local sex, name, status, lv, force, alliance, icon = GameStateColonization:GetUserInfo(GameUISlaveSelect.curFightPlayer)
        local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_SLAVE_SUCSS_CHAR")
        oriMsg = GameStateColonization:MakeMsgs(oriMsg, "", name, "", "", false)
        icon = GameUISlaveSelect.curFightPlayer.icon
        GameUIBattleResult:updateColonialWinInfo(icon, name, lv, oriMsg)
        window_type = "colonial_win"
        GameUIBattleResult.colonialName = name
      else
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.select_slave
        window_type = "colonial_lose"
      end
      local lastGameState = GameStateManager:GetCurrentGameState()
      if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      GameStateBattlePlay.curBattleType = "SlaveChallenge"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.report)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(lastGameState)
        GameUIBattleResult:LoadFlashObject()
        GameUIBattleResult:AnimationMoveIn(window_type, false, oriMsg, FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_WIN)
        lastGameState:AddObject(GameUIBattleResult)
      end)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      return true
    end
  end
  return false
end
function GameUISlaveSelect.BuyTimesListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    DebugColonial("BuyTimesListCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    if content.api == NetAPIList.colony_action_purchase_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameTip:Show(GameLoader:GetGameText("LC_MENU_PURCHASE_SUCCESS_CHAR"))
      end
      return true
    end
  end
  return false
end
function GameUISlaveSelect.UpdateChatButtonStatus()
  DebugOut("GameUISlaveSelect UpdateChatButtonStatus")
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  DebugOut(chatCount)
  local flash_obj = GameUISlaveSelect:GetFlashObject()
  if flash_obj == nil then
    return
  end
  if chatCount and chatCount > 0 then
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  else
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", false)
  end
end
