local Monsters = GameData.ChapterMonsterAct9.Monsters
Monsters[901001] = {
  ID = 901001,
  durability = 884739,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901002] = {
  ID = 901002,
  durability = 825768,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901003] = {
  ID = 901003,
  durability = 393388,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901004] = {
  ID = 901004,
  durability = 629240,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901005] = {
  ID = 901005,
  durability = 766813,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901006] = {
  ID = 901006,
  durability = 884739,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901007] = {
  ID = 901007,
  durability = 825776,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901008] = {
  ID = 901008,
  durability = 393384,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901009] = {
  ID = 901009,
  durability = 629240,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901010] = {
  ID = 901010,
  durability = 766821,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901011] = {
  ID = 901011,
  durability = 884748,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901012] = {
  ID = 901012,
  durability = 825776,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901013] = {
  ID = 901013,
  durability = 393384,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901014] = {
  ID = 901014,
  durability = 629234,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901015] = {
  ID = 901015,
  durability = 766806,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901016] = {
  ID = 901016,
  durability = 1326958,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901017] = {
  ID = 901017,
  durability = 825776,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901018] = {
  ID = 901018,
  durability = 393380,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901019] = {
  ID = 901019,
  durability = 629234,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901020] = {
  ID = 901020,
  durability = 766806,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901021] = {
  ID = 901021,
  durability = 884739,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901022] = {
  ID = 901022,
  durability = 825776,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901023] = {
  ID = 901023,
  durability = 393384,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901024] = {
  ID = 901024,
  durability = 629234,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901025] = {
  ID = 901025,
  durability = 766813,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901026] = {
  ID = 901026,
  durability = 884748,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901027] = {
  ID = 901027,
  durability = 825768,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901028] = {
  ID = 901028,
  durability = 393384,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901029] = {
  ID = 901029,
  durability = 629240,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901030] = {
  ID = 901030,
  durability = 766813,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901031] = {
  ID = 901031,
  durability = 958476,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901032] = {
  ID = 901032,
  durability = 894580,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901033] = {
  ID = 901033,
  durability = 426152,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901034] = {
  ID = 901034,
  durability = 681656,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901035] = {
  ID = 901035,
  durability = 830719,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901036] = {
  ID = 901036,
  durability = 1437550,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901037] = {
  ID = 901037,
  durability = 894589,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[901038] = {
  ID = 901038,
  durability = 426152,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[901039] = {
  ID = 901039,
  durability = 681656,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[901040] = {
  ID = 901040,
  durability = 830703,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902001] = {
  ID = 902001,
  durability = 958467,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902002] = {
  ID = 902002,
  durability = 894580,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902003] = {
  ID = 902003,
  durability = 426148,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902004] = {
  ID = 902004,
  durability = 681663,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902005] = {
  ID = 902005,
  durability = 830711,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902006] = {
  ID = 902006,
  durability = 958458,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902007] = {
  ID = 902007,
  durability = 894589,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902008] = {
  ID = 902008,
  durability = 426152,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902009] = {
  ID = 902009,
  durability = 681656,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902010] = {
  ID = 902010,
  durability = 830711,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902011] = {
  ID = 902011,
  durability = 958458,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902012] = {
  ID = 902012,
  durability = 894580,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902013] = {
  ID = 902013,
  durability = 426148,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902014] = {
  ID = 902014,
  durability = 681656,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902015] = {
  ID = 902015,
  durability = 830711,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902016] = {
  ID = 902016,
  durability = 1437550,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902017] = {
  ID = 902017,
  durability = 894589,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902018] = {
  ID = 902018,
  durability = 426152,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902019] = {
  ID = 902019,
  durability = 681663,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902020] = {
  ID = 902020,
  durability = 830711,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902021] = {
  ID = 902021,
  durability = 958476,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902022] = {
  ID = 902022,
  durability = 894597,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902023] = {
  ID = 902023,
  durability = 426156,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902024] = {
  ID = 902024,
  durability = 681663,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902025] = {
  ID = 902025,
  durability = 830711,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902026] = {
  ID = 902026,
  durability = 958458,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902027] = {
  ID = 902027,
  durability = 894589,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902028] = {
  ID = 902028,
  durability = 426152,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902029] = {
  ID = 902029,
  durability = 681663,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902030] = {
  ID = 902030,
  durability = 830711,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902031] = {
  ID = 902031,
  durability = 1032186,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902032] = {
  ID = 902032,
  durability = 963393,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902033] = {
  ID = 902033,
  durability = 458924,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902034] = {
  ID = 902034,
  durability = 734098,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902035] = {
  ID = 902035,
  durability = 894609,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902036] = {
  ID = 902036,
  durability = 1548142,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902037] = {
  ID = 902037,
  durability = 963410,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[902038] = {
  ID = 902038,
  durability = 458920,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[902039] = {
  ID = 902039,
  durability = 734085,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[902040] = {
  ID = 902040,
  durability = 894601,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903001] = {
  ID = 903001,
  durability = 1032186,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903002] = {
  ID = 903002,
  durability = 963410,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903003] = {
  ID = 903003,
  durability = 458920,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903004] = {
  ID = 903004,
  durability = 734092,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903005] = {
  ID = 903005,
  durability = 894609,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903006] = {
  ID = 903006,
  durability = 1032204,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903007] = {
  ID = 903007,
  durability = 963393,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903008] = {
  ID = 903008,
  durability = 458920,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903009] = {
  ID = 903009,
  durability = 734098,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903010] = {
  ID = 903010,
  durability = 894609,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903011] = {
  ID = 903011,
  durability = 1032195,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903012] = {
  ID = 903012,
  durability = 963393,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903013] = {
  ID = 903013,
  durability = 458920,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903014] = {
  ID = 903014,
  durability = 734092,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903015] = {
  ID = 903015,
  durability = 894609,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903016] = {
  ID = 903016,
  durability = 1548156,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903017] = {
  ID = 903017,
  durability = 963402,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903018] = {
  ID = 903018,
  durability = 458920,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903019] = {
  ID = 903019,
  durability = 734085,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903020] = {
  ID = 903020,
  durability = 894616,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903021] = {
  ID = 903021,
  durability = 1032204,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903022] = {
  ID = 903022,
  durability = 963393,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903023] = {
  ID = 903023,
  durability = 458916,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903024] = {
  ID = 903024,
  durability = 734092,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903025] = {
  ID = 903025,
  durability = 894601,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903026] = {
  ID = 903026,
  durability = 1032195,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903027] = {
  ID = 903027,
  durability = 963402,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903028] = {
  ID = 903028,
  durability = 458920,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903029] = {
  ID = 903029,
  durability = 734092,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903030] = {
  ID = 903030,
  durability = 894601,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903031] = {
  ID = 903031,
  durability = 1105923,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903032] = {
  ID = 903032,
  durability = 1032214,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903033] = {
  ID = 903033,
  durability = 491688,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903034] = {
  ID = 903034,
  durability = 786520,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903035] = {
  ID = 903035,
  durability = 958498,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903036] = {
  ID = 903036,
  durability = 1658721,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903037] = {
  ID = 903037,
  durability = 1032223,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[903038] = {
  ID = 903038,
  durability = 491688,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[903039] = {
  ID = 903039,
  durability = 786514,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[903040] = {
  ID = 903040,
  durability = 958506,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904001] = {
  ID = 904001,
  durability = 1105923,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904002] = {
  ID = 904002,
  durability = 1032214,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904003] = {
  ID = 904003,
  durability = 491688,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904004] = {
  ID = 904004,
  durability = 786520,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904005] = {
  ID = 904005,
  durability = 958506,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904006] = {
  ID = 904006,
  durability = 1105923,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904007] = {
  ID = 904007,
  durability = 1032214,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904008] = {
  ID = 904008,
  durability = 491692,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904009] = {
  ID = 904009,
  durability = 786514,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904010] = {
  ID = 904010,
  durability = 958498,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904011] = {
  ID = 904011,
  durability = 1105914,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904012] = {
  ID = 904012,
  durability = 1032214,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904013] = {
  ID = 904013,
  durability = 491688,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904014] = {
  ID = 904014,
  durability = 786520,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904015] = {
  ID = 904015,
  durability = 958506,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904016] = {
  ID = 904016,
  durability = 1658721,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904017] = {
  ID = 904017,
  durability = 1032206,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904018] = {
  ID = 904018,
  durability = 491688,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904019] = {
  ID = 904019,
  durability = 786527,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904020] = {
  ID = 904020,
  durability = 958506,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904021] = {
  ID = 904021,
  durability = 1105923,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904022] = {
  ID = 904022,
  durability = 1032223,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904023] = {
  ID = 904023,
  durability = 491692,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904024] = {
  ID = 904024,
  durability = 786514,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904025] = {
  ID = 904025,
  durability = 958506,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904026] = {
  ID = 904026,
  durability = 1105932,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904027] = {
  ID = 904027,
  durability = 1032214,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904028] = {
  ID = 904028,
  durability = 491688,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904029] = {
  ID = 904029,
  durability = 786514,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904030] = {
  ID = 904030,
  durability = 958514,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904031] = {
  ID = 904031,
  durability = 1179660,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904032] = {
  ID = 904032,
  durability = 1101027,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904033] = {
  ID = 904033,
  durability = 524456,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904034] = {
  ID = 904034,
  durability = 838943,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904035] = {
  ID = 904035,
  durability = 1022412,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904036] = {
  ID = 904036,
  durability = 1769326,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904037] = {
  ID = 904037,
  durability = 1101027,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[904038] = {
  ID = 904038,
  durability = 524452,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[904039] = {
  ID = 904039,
  durability = 838943,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[904040] = {
  ID = 904040,
  durability = 1022396,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905001] = {
  ID = 905001,
  durability = 1179651,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905002] = {
  ID = 905002,
  durability = 1101027,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905003] = {
  ID = 905003,
  durability = 524452,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905004] = {
  ID = 905004,
  durability = 838949,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905005] = {
  ID = 905005,
  durability = 1022396,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905006] = {
  ID = 905006,
  durability = 1179651,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905007] = {
  ID = 905007,
  durability = 1101019,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905008] = {
  ID = 905008,
  durability = 524452,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905009] = {
  ID = 905009,
  durability = 838956,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905010] = {
  ID = 905010,
  durability = 1022412,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905011] = {
  ID = 905011,
  durability = 1179642,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905012] = {
  ID = 905012,
  durability = 1101027,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905013] = {
  ID = 905013,
  durability = 524456,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905014] = {
  ID = 905014,
  durability = 838956,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905015] = {
  ID = 905015,
  durability = 1022404,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905016] = {
  ID = 905016,
  durability = 1769340,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905017] = {
  ID = 905017,
  durability = 1101019,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905018] = {
  ID = 905018,
  durability = 524452,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905019] = {
  ID = 905019,
  durability = 838949,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905020] = {
  ID = 905020,
  durability = 1022404,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905021] = {
  ID = 905021,
  durability = 1179642,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905022] = {
  ID = 905022,
  durability = 1101036,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905023] = {
  ID = 905023,
  durability = 524456,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905024] = {
  ID = 905024,
  durability = 838956,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905025] = {
  ID = 905025,
  durability = 1022404,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905026] = {
  ID = 905026,
  durability = 1179660,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905027] = {
  ID = 905027,
  durability = 1101027,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905028] = {
  ID = 905028,
  durability = 524456,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905029] = {
  ID = 905029,
  durability = 838943,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905030] = {
  ID = 905030,
  durability = 1022396,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905031] = {
  ID = 905031,
  durability = 1253370,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905032] = {
  ID = 905032,
  durability = 1169848,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905033] = {
  ID = 905033,
  durability = 557224,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905034] = {
  ID = 905034,
  durability = 891384,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905035] = {
  ID = 905035,
  durability = 1086309,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905036] = {
  ID = 905036,
  durability = 1879932,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905037] = {
  ID = 905037,
  durability = 1169832,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[905038] = {
  ID = 905038,
  durability = 557224,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[905039] = {
  ID = 905039,
  durability = 891384,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[905040] = {
  ID = 905040,
  durability = 1086301,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906001] = {
  ID = 906001,
  durability = 1253370,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906002] = {
  ID = 906002,
  durability = 1169840,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906003] = {
  ID = 906003,
  durability = 557228,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906004] = {
  ID = 906004,
  durability = 891384,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906005] = {
  ID = 906005,
  durability = 1086301,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906006] = {
  ID = 906006,
  durability = 1253379,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906007] = {
  ID = 906007,
  durability = 1169840,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906008] = {
  ID = 906008,
  durability = 557224,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906009] = {
  ID = 906009,
  durability = 891378,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906010] = {
  ID = 906010,
  durability = 1086294,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906011] = {
  ID = 906011,
  durability = 1253388,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906012] = {
  ID = 906012,
  durability = 1169832,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906013] = {
  ID = 906013,
  durability = 557224,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906014] = {
  ID = 906014,
  durability = 891372,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906015] = {
  ID = 906015,
  durability = 1086301,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906016] = {
  ID = 906016,
  durability = 1879932,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906017] = {
  ID = 906017,
  durability = 1169840,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906018] = {
  ID = 906018,
  durability = 557228,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906019] = {
  ID = 906019,
  durability = 891372,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906020] = {
  ID = 906020,
  durability = 1086301,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906021] = {
  ID = 906021,
  durability = 1253379,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906022] = {
  ID = 906022,
  durability = 1169840,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906023] = {
  ID = 906023,
  durability = 557228,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906024] = {
  ID = 906024,
  durability = 891372,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906025] = {
  ID = 906025,
  durability = 1086309,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906026] = {
  ID = 906026,
  durability = 1253379,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906027] = {
  ID = 906027,
  durability = 1169848,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906028] = {
  ID = 906028,
  durability = 557220,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906029] = {
  ID = 906029,
  durability = 891378,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906030] = {
  ID = 906030,
  durability = 1086301,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906031] = {
  ID = 906031,
  durability = 1327107,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906032] = {
  ID = 906032,
  durability = 1238653,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906033] = {
  ID = 906033,
  durability = 589992,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906034] = {
  ID = 906034,
  durability = 943807,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906035] = {
  ID = 906035,
  durability = 1150199,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906036] = {
  ID = 906036,
  durability = 1990510,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906037] = {
  ID = 906037,
  durability = 1238661,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[906038] = {
  ID = 906038,
  durability = 589996,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[906039] = {
  ID = 906039,
  durability = 943813,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[906040] = {
  ID = 906040,
  durability = 1150199,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907001] = {
  ID = 907001,
  durability = 1327107,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907002] = {
  ID = 907002,
  durability = 1238653,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907003] = {
  ID = 907003,
  durability = 589996,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907004] = {
  ID = 907004,
  durability = 943807,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907005] = {
  ID = 907005,
  durability = 1150199,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907006] = {
  ID = 907006,
  durability = 1327116,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907007] = {
  ID = 907007,
  durability = 1238661,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907008] = {
  ID = 907008,
  durability = 589992,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907009] = {
  ID = 907009,
  durability = 943800,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907010] = {
  ID = 907010,
  durability = 1150199,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907011] = {
  ID = 907011,
  durability = 1327107,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907012] = {
  ID = 907012,
  durability = 1238661,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907013] = {
  ID = 907013,
  durability = 589988,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907014] = {
  ID = 907014,
  durability = 943800,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907015] = {
  ID = 907015,
  durability = 1150207,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907016] = {
  ID = 907016,
  durability = 1990497,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907017] = {
  ID = 907017,
  durability = 1238661,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907018] = {
  ID = 907018,
  durability = 589992,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907019] = {
  ID = 907019,
  durability = 943800,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907020] = {
  ID = 907020,
  durability = 1150199,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907021] = {
  ID = 907021,
  durability = 1327116,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907022] = {
  ID = 907022,
  durability = 1238644,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907023] = {
  ID = 907023,
  durability = 589996,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907024] = {
  ID = 907024,
  durability = 943807,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907025] = {
  ID = 907025,
  durability = 1150207,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907026] = {
  ID = 907026,
  durability = 1327116,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907027] = {
  ID = 907027,
  durability = 1238653,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907028] = {
  ID = 907028,
  durability = 589996,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907029] = {
  ID = 907029,
  durability = 943807,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907030] = {
  ID = 907030,
  durability = 1150207,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907031] = {
  ID = 907031,
  durability = 1400835,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907032] = {
  ID = 907032,
  durability = 1307474,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907033] = {
  ID = 907033,
  durability = 622760,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907034] = {
  ID = 907034,
  durability = 996236,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907035] = {
  ID = 907035,
  durability = 1214104,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907036] = {
  ID = 907036,
  durability = 2101102,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907037] = {
  ID = 907037,
  durability = 1307466,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[907038] = {
  ID = 907038,
  durability = 622764,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[907039] = {
  ID = 907039,
  durability = 996236,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[907040] = {
  ID = 907040,
  durability = 1214097,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908001] = {
  ID = 908001,
  durability = 1400835,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908002] = {
  ID = 908002,
  durability = 1307474,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908003] = {
  ID = 908003,
  durability = 622760,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908004] = {
  ID = 908004,
  durability = 996236,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908005] = {
  ID = 908005,
  durability = 1214104,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908006] = {
  ID = 908006,
  durability = 1400835,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908007] = {
  ID = 908007,
  durability = 1307457,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908008] = {
  ID = 908008,
  durability = 622764,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908009] = {
  ID = 908009,
  durability = 996242,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908010] = {
  ID = 908010,
  durability = 1214097,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908011] = {
  ID = 908011,
  durability = 1400835,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908012] = {
  ID = 908012,
  durability = 1307457,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908013] = {
  ID = 908013,
  durability = 622764,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908014] = {
  ID = 908014,
  durability = 996236,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908015] = {
  ID = 908015,
  durability = 1214104,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908016] = {
  ID = 908016,
  durability = 2101102,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908017] = {
  ID = 908017,
  durability = 1307457,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908018] = {
  ID = 908018,
  durability = 622760,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908019] = {
  ID = 908019,
  durability = 996236,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908020] = {
  ID = 908020,
  durability = 1214097,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908021] = {
  ID = 908021,
  durability = 1400835,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908022] = {
  ID = 908022,
  durability = 1307466,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908023] = {
  ID = 908023,
  durability = 622760,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908024] = {
  ID = 908024,
  durability = 996229,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908025] = {
  ID = 908025,
  durability = 1214097,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908026] = {
  ID = 908026,
  durability = 1400835,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908027] = {
  ID = 908027,
  durability = 1307457,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908028] = {
  ID = 908028,
  durability = 622756,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908029] = {
  ID = 908029,
  durability = 996229,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908030] = {
  ID = 908030,
  durability = 1214104,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908031] = {
  ID = 908031,
  durability = 1474554,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908032] = {
  ID = 908032,
  durability = 1376278,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908033] = {
  ID = 908033,
  durability = 655532,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908034] = {
  ID = 908034,
  durability = 1048658,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908035] = {
  ID = 908035,
  durability = 1278002,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908036] = {
  ID = 908036,
  durability = 2211694,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908037] = {
  ID = 908037,
  durability = 1376278,
  Avatar = "head43",
  Ship = "ship40"
}
Monsters[908038] = {
  ID = 908038,
  durability = 655532,
  Avatar = "head42",
  Ship = "ship41"
}
Monsters[908039] = {
  ID = 908039,
  durability = 1048664,
  Avatar = "head47",
  Ship = "ship38"
}
Monsters[908040] = {
  ID = 908040,
  durability = 1277994,
  Avatar = "head47",
  Ship = "ship38"
}
