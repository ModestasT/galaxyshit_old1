local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemPort_SUB = LuaObjectManager:GetLuaObject("GameUIStarSystemPort_SUB")
GameUIStarSystemPort.uiStore = {}
local GameUIStarSystemStore = GameUIStarSystemPort.uiStore
local GameStateStarSystem = GameStateManager.GameStateStarSystem
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local DynamicResDownloader = DynamicResDownloader
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameStateInstance = GameStateManager.GameStateInstance
GameUIStarSystemStore.goodsList = nil
GameUIStarSystemStore.receiveGoodsListTime = -1
GameUIStarSystemStore.nowStoreType = 1
local _serverIDIsRes = function(currency)
  return currency >= 99000001
end
function GameUIStarSystemStore:GetFlashObject()
  return (...), GameUIStarSystemPort_SUB
end
function GameUIStarSystemStore:Show(close_callback)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "ShowStore")
  self:RequestStoreGoodsList(true)
  GameTimer:Add(self._OnTimerTick, 1000)
  GameStateManager:RegisterGameResumeNotification(GameUIStarSystemStore.OnResume, nil)
  self.close_callback = close_callback
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
end
function GameUIStarSystemStore.OnResume()
  local flash_obj = GameUIStarSystemStore:GetFlashObject()
  if not flash_obj then
    return
  end
  GameUIStarSystemStore:RequestStoreGoodsList(true)
end
function GameUIStarSystemStore:Hide()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "HideStore")
  GameStateManager:RemoveGameResumeNotification(GameUIStarSystemStore.OnResume)
  GameGlobalData:RemoveDataChangeCallback("resource", self.RefreshResource)
  GameGlobalData:RemoveDataChangeCallback("item_count", self.RefreshResource)
  if self.close_callback then
    self.close_callback()
    self.close_callback = nil
  end
end
function GameUIStarSystemStore:serverIDToIconFrame(currency)
  if currency == 99000001 then
    return "money"
  elseif currency == 99000002 then
    return "credit"
  elseif currency == 99000003 then
    return "technique"
  elseif currency == 99000004 then
    return "silver"
  elseif currency == 99000005 then
    return "crystal"
  else
    local gameItem = {
      number = currency,
      item_type = "item",
      no = 1
    }
    local strframe = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(gameItem)
    return strframe
  end
end
function GameUIStarSystemStore:BuyNotEnough(strtype, isrefresh)
  local text
  if isrefresh then
    text = GameUtils:TryGetText("LC_MENU_MEDAL_REFRESH_NOTENOUGH", "<item> not enough")
  else
    text = GameUtils:TryGetText("LC_MENU_MEDAL_BUY_NOTENOUGH", "<item> not enough")
  end
  local callback
  if strtype == "credit" then
    text = string.gsub(text, "<item>", GameHelper:GetAwardTypeText("credit"))
    function callback()
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:showVip(false)
    end
  elseif strtype == "medal_resource" then
    text = string.gsub(text, "<item>", GameHelper:GetAwardTypeText("medal_resource"))
    function callback()
      GameUIStarSystemStore:onShopGet()
    end
  else
    assert(false, strtype)
  end
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
  local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
  local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
  GameUIMessageDialog:SetRightTextButton(cancel)
  GameUIMessageDialog:SetLeftGreenButton(affairs, callback)
  GameUIMessageDialog:Display("", text)
end
function GameUIStarSystemStore:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  local lefttype = "credit"
  local leftnum = resource[lefttype] or 0
  local righttype = "medal_resource"
  local rightnum = resource[righttype] or 0
  local flash_obj = GameUIStarSystemStore:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "RefreshResource", lefttype, leftnum, righttype, rightnum)
  end
end
function GameUIStarSystemStore:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
  self:GetFlashObject():Update(dt)
end
if AutoUpdate.isAndroidDevice then
  function GameUIStarSystemStore.OnAndroidBack()
    if GameStateStore.CurItemDetail then
      GameStateStore:HideBuyConfirmWin()
    else
      GameUIStarSystemStore:OnFSCommand("close_menu")
    end
  end
end
function GameUIStarSystemStore:onShopBuy(cmd, arg)
  local itemKey, num = unpack(LuaUtils:string_split(arg, ":"))
  GameUIStarSystemStore:OnBuyItem_12(tonumber(itemKey), tonumber(num))
end
function GameUIStarSystemStore:onShopIcon(cmd, arg)
  local nItemid = tonumber(arg)
  local itemDetail
  for k, v in ipairs(GameUIStarSystemStore.goodsList.items) do
    if v.id == nItemid then
      itemDetail = v
      break
    end
  end
  assert(itemDetail, tostring(nItemid))
  if itemDetail.item.item_type == "item" then
    itemDetail.item.cnt = 1
    ItemBox.showItemParam.hideCost = true
    ItemBox:showItemBox("ChoosableItem", itemDetail.item, itemDetail.item.number, 320, 480, 200, 200, "", nil, "", nil)
  elseif itemDetail.item.item_type == "medal_item" then
    itemDetail.item.quality = itemDetail.quality
    ItemBox:showItemBox("Medal", itemDetail.item, itemDetail.item.number, 320, 480, 200, 200, "", nil, "", nil)
  elseif itemDetail.item.item_type == "medal" then
    local arg = string.format("%d|%d", itemDetail.item.number, itemDetail.quality)
    GameUIStarSystemPort.uiRecycle:onResetItemIcons("", arg)
  end
end
function GameUIStarSystemStore:needUpdateShopItem(cmd, arg)
  self:UpdateGoodsItem(arg)
end
function GameUIStarSystemStore:RequestStoreRefreshPrice()
  local store_refresh_price_content = {
    price_type = "refresh_store_cost",
    type = 0
  }
  local function callback(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgType == NetAPIList.price_ack.Code then
      DebugStoreTable(content)
      local msgBoxContent = string.format(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_BUY_INFO"), content.price)
      local TitleInfo = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_YesNo, TitleInfo, msgBoxContent, GameUIStarSystemStore.doRefreshMysteryStore, nil)
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, store_refresh_price_content, callback, true, nil)
end
function GameUIStarSystemStore:QueryBuyItem(itemIndex)
  DebugStore("item released: ", itemIndex)
  if GameUIStarSystemStore.goodsList ~= nil then
    GameStateStore:ShowBuyConfirmWin(GameUIStarSystemStore.goodsList.items[itemIndex])
  else
    DebugOut("GameUIStarSystemStore.goodsList == nil")
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_CLOSE_INFO"))
  end
end
function GameUIStarSystemStore:TempText(id)
  if id == 1022 or id == 2518 or id == 2320 or id == 2320 or id == 2519 or id == 2311 or id == 2312 or id == 2313 or id == 2314 or id == 2315 then
    return true
  else
    return false
  end
end
function GameUIStarSystemStore:UpdateGoodsItem(Id)
  local itemKey = tonumber(Id)
  local sell_name, sell_frame, sell_pic, sell_num = "", "", "", ""
  local price_name, price_frame, price_oricost, price_discount, price_realcost = "", "", "", "", ""
  local if_time_left = ""
  local buy_times_left = ""
  local itemID = ""
  local canBuy = ""
  local quality = ""
  for k, v in ipairs(GameUIStarSystemStore.goodsList.items) do
    if math.ceil(k / 2) == itemKey then
      v.itemKey = itemKey
      local curItem = v.item
      sell_name = sell_name .. GameHelper:GetAwardNameText(curItem.item_type, curItem.number) .. "\001"
      local count = GameHelper:GetAwardCount(curItem.item_type, curItem.number, curItem.no)
      sell_num = sell_num .. "X" .. count .. "\001"
      local frame = GameHelper:GetAwardTypeIconFrameName(curItem.item_type, curItem.number)
      sell_frame = sell_frame .. frame .. "\001"
      local strpic = GameHelper:GetGameItemDownloadPng(curItem.item_type, curItem.number)
      sell_pic = sell_pic .. strpic .. "\001"
      curItem = v.price
      price_name = price_name .. GameHelper:GetAwardNameText(curItem.item_type, curItem.number) .. "\001"
      local extInfo = {}
      extInfo.itemKey = itemKey
      extInfo.index = itemIndex
      extInfo.frameName = GameHelper:GetAwardTypeIconFrameName(curItem.item_type, curItem.number, curItem.no)
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(curItem, extInfo, GameUIStarSystemStore.donamicDownloadFinishCallback)
      price_frame = price_frame .. GameHelper:GetAwardTypeIconFrameName(curItem.item_type, curItem.number) .. "\001"
      local count = GameHelper:GetAwardCount(curItem.item_type, curItem.number, curItem.no)
      price_oricost = price_oricost .. "X" .. count .. "\001"
      price_discount = price_discount .. math.ceil((1000 - v.discount) / 10) .. "\001"
      price_realcost = price_realcost .. math.ceil(count * v.discount / 1000) .. "\001"
      itemID = itemID .. v.id .. "\001"
      local strremain = "-1"
      if v.buy_times_limit ~= -1 then
        strremain = GameLoader:GetGameText("LC_MENU_SHOP_BUY_TIME_CHAR") .. v.buy_times_limit
      end
      buy_times_left = buy_times_left .. strremain .. "\001"
      if not v.enableBuy then
        canBuy = canBuy .. "0\001"
      else
        canBuy = canBuy .. "1\001"
      end
      if_time_left = v.time_left .. "0\001"
      quality = quality .. v.quality .. "\001"
    end
  end
  local leftOverDesc = GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASED_BUTTON")
  local param = {}
  param.itemKey = itemKey
  param.sell_name = sell_name
  param.sell_frame = sell_frame
  param.sell_pic = sell_pic
  param.sell_num = sell_num
  param.price_frame = price_frame
  param.price_oricost = price_oricost
  param.price_discount = price_discount
  param.price_realcost = price_realcost
  param.if_time_left = if_time_left
  param.buy_times_left = buy_times_left
  param.leftOverDesc = leftOverDesc
  param.itemID = itemID
  param.canBuy = canBuy
  param.quality = quality
  param.strBuyBtn = GameLoader:GetGameText("LC_MENU_LIMIT_PACKAGE_BUY_BUTTON")
  param.has_refresh = false
  self:GetFlashObject():InvokeASCallback("_root", "setItem_new", param)
end
function GameUIStarSystemStore:RefreshGoodsList()
  self:GetFlashObject():InvokeASCallback("_root", "clearListItem")
  if GameUIStarSystemStore.goodsList ~= nil then
    local itemCount = math.ceil(#GameUIStarSystemStore.goodsList.items / 2)
    self:GetFlashObject():InvokeASCallback("_root", "initListItem", "shop_item")
    for i = 1, itemCount do
      self:GetFlashObject():InvokeASCallback("_root", "addListItem", i)
    end
  end
end
function GameUIStarSystemStore:UpdateItemCDTime(itemKey, v1, v2)
  if not GameUIStarSystemStore.goodsList.is_activity then
    self:GetFlashObject():InvokeASCallback("_root", "setItemCDTime", itemKey, "")
    return
  end
  local timeString1 = ""
  local timeString2 = ""
  local index = 0
  if v1 ~= nil and v1.enableBuy and v1.time_left ~= -1 then
    local _lefttime = v1.time_left - (os.time() - GameUIStarSystemStore.receiveGoodsListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    timeString1 = GameUtils:formatTimeString(_lefttime)
  end
  if v2 ~= nil and v2.enableBuy and v2.time_left ~= -1 then
    local _lefttime = v2.time_left - (os.time() - GameUIStarSystemStore.receiveGoodsListTime)
    if _lefttime < 0 then
      _lefttime = 0
    end
    timeString2 = GameUtils:formatTimeString(_lefttime)
  end
  if timeString1 ~= "" or timeString2 ~= "" then
    self:GetFlashObject():InvokeASCallback("_root", "setItemCDTime", itemKey, timeString1, timeString2)
  end
end
function GameUIStarSystemStore.RefreshCallback(msgType, content)
  if msgType == NetAPIList.medal_store_refresh_ack.Code then
    print("RefreshMysteryCallback_refresh_type_ack")
    GameUIStarSystemStore:RequestStoreGoodsList(true)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_store_refresh_req.Code then
    if content.code == 50017 then
      local item = GameUIStarSystemStore.goodsList.reset_price
      GameUIStarSystemStore:BuyNotEnough(item.item_type, true)
    elseif content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  end
  return false
end
function GameUIStarSystemStore.RequestRefreshData()
  if GameGlobalData.isLogin then
    local function dobuy()
      local param = {
        act_id = GameUIStarSystemStore.goodsList.act_id
      }
      NetMessageMgr:SendMsg(NetAPIList.medal_store_refresh_req.Code, param, GameUIStarSystemStore.RefreshCallback, true, nil)
    end
    local data = GameUIStarSystemStore.goodsList
    if data.currency_times <= 0 and data.reset_price.item_type == "credit" then
      local txt = GameUtils:TryGetText("LC_MENU_MEDAL_STORE_PAY_WARNING", " <number> credit refresh?")
      txt = string.gsub(txt, "<number>", data.reset_price.number)
      GameUtils:CreditCostConfirm(txt, dobuy, false, nil)
    else
      dobuy()
    end
  end
end
function GameUIStarSystemStore:onShopGet()
  local endcallback = function()
  end
  local function showcallback()
    local flash_obj = GameUIStarSystemStore:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "SetVisible", "_root.mcshop", true)
    end
  end
  GameUIStarSystemPort.uiRecycle:Show("resolve", showcallback, endcallback)
end
function GameUIStarSystemStore:Update_refresh()
  local data = GameUIStarSystemStore.goodsList
  if data == nil or data.time_left == nil then
    return
  end
  local cd_desc, refresh_type, refresh_icon, refresh_cost
  local cd_time = 0
  cd_time = data.time_left - (os.time() - GameUIStarSystemStore.receiveGoodsListTime) + 2
  if data.time_left > 0 and cd_time < 0 and tonumber(data.currency_times) < tonumber(data.max_times) then
    data.time_left = nil
    GameUIStarSystemStore:RequestStoreGoodsList(true)
    return
  end
  local str_cd_time = ""
  if cd_time > 0 and data.currency_times ~= data.max_times then
    str_cd_time = GameUtils:formatTimeString(cd_time)
  end
  cd_desc = GameLoader:GetGameText("LC_MENU_FREE_REFRESH_CHAR")
  cd_desc = cd_desc .. tostring(data.currency_times) .. "/" .. tostring(data.max_times) .. " " .. str_cd_time
  local refresh_icon = GameHelper:GetAwardTypeIconFrameName(data.reset_price.item_type, data.reset_price.number, data.reset_price.no)
  local refresh_cost = GameHelper:GetAwardCount(data.reset_price.item_type, data.reset_price.number, data.reset_price.no)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "SetNewRefreshData", cd_desc, refresh_icon, refresh_cost, data.currency_times, data.max_times)
end
function GameUIStarSystemStore._OnTimerTick()
  if GameStateStarSystem:IsObjectInState(GameUIStarSystemPort_SUB) then
    if GameUIStarSystemStore.goodsList ~= nil then
      local innerIndex = 0
      local itemKey, v1, v2
      for k, v in ipairs(GameUIStarSystemStore.goodsList.items) do
        itemKey = math.ceil(k / 2)
        if v1 == nil then
          v1 = v
        else
          v2 = v
        end
        if v1 ~= nil and v2 ~= nil then
          GameUIStarSystemStore:UpdateItemCDTime(itemKey, v1, v2)
          v1, v2 = nil, nil
        end
      end
      if v1 ~= nil or v2 ~= nil then
        GameUIStarSystemStore:UpdateItemCDTime(itemKey, v1, v2)
        v1, v2 = nil, nil
      end
    end
    GameUIStarSystemStore:Update_refresh()
    return 1000
  end
  return nil
end
function GameUIStarSystemStore:RequestStoreGoodsList(force_request)
  if GameUIStarSystemStore.goodsList == nil or force_request then
    NetMessageMgr:SendMsg(NetAPIList.enter_medal_store_req.Code, nil, GameUIStarSystemStore.onStoreListCallback, true, nil)
  else
    self:RefreshResource()
    self:RefreshGoodsList()
  end
end
function GameUIStarSystemStore.sortItem()
  local t_a = {}
  local t_b = {}
  local t_c = {}
  for i, v in pairs(GameUIStarSystemStore.goodsList.items) do
    local enableBuy = true
    if v.buy_times_limit < 1 then
      enableBuy = false
      table.insert(t_c, v)
    end
    v.enableBuy = enableBuy
    if enableBuy then
      table.insert(t_a, v)
    end
  end
  if #t_b > 0 or #t_c > 0 then
    GameUIStarSystemStore.goodsList.items = {}
    for _, v in ipairs(t_a) do
      table.insert(GameUIStarSystemStore.goodsList.items, v)
    end
    for _, v in ipairs(t_b) do
      table.insert(GameUIStarSystemStore.goodsList.items, v)
    end
    for _, v in ipairs(t_c) do
      table.insert(GameUIStarSystemStore.goodsList.items, v)
    end
  end
end
function GameUIStarSystemStore.onStoreListCallback(msgType, content)
  if msgType == NetAPIList.enter_medal_store_ack.Code then
    GameUIStarSystemStore.goodsList = content
    GameUIStarSystemStore.sortItem()
    GameUIStarSystemStore.receiveGoodsListTime = os.time()
    GameUIStarSystemStore:RefreshResource()
    GameUIStarSystemStore:RefreshGoodsList()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.enter_medal_store_req.Code then
    DebugOut("common ack error")
    DebugStoreTable(content)
    if content.code == 5 then
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(GameLoader:GetGameText("LC_MENU_MYSTERY_SHOP_CLOSE_INFO"))
      return true
    elseif content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
  end
  return false
end
function GameUIStarSystemStore.donamicDownloadFinishCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUIStarSystemStore:GetFlashObject() then
    local frameName = extInfo.frameName
    DebugOut("GameUIStarSystemStore.donamicDownloadFinishCallback", frameName)
    DebugStore("goto frame", frameName, extInfo.itemKey, extInfo.index)
    GameUIStarSystemStore:GetFlashObject():InvokeASCallback("_root", "SetItemFrame", extInfo.itemKey, extInfo.index, frameName)
  end
  return true
end
function GameUIStarSystemStore.BuyItem_12_Callback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_store_buy_req.Code then
    if content.code ~= 0 then
      if content.code == 50017 then
        local _curitem = GameUIStarSystemStore.param_buy_12.itemDetail.price
        GameUIStarSystemStore:BuyNotEnough(_curitem.item_type, false)
      else
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
    end
    return true
  elseif msgType == NetAPIList.medal_store_buy_ack.Code then
    GameUIStarSystemStore.usedItem = nil
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowRewardBox(content.buy_item)
    GameUIStarSystemStore.goodsList.reset_price = content.reset_price
    local buyitem = GameUIStarSystemStore.param_buy_12.itemDetail
    if 0 < buyitem.buy_times_limit then
      buyitem.buy_times_limit = buyitem.buy_times_limit - 1
      buyitem.enableBuy = false
    end
    GameUIStarSystemStore:UpdateGoodsItem(buyitem.itemKey)
    GameUIStarSystemStore.param_buy_12 = nil
    return true
  end
  return false
end
function GameUIStarSystemStore:OnBuyItem_12(nItemid, ncount)
  local itemDetail
  for k, v in ipairs(GameUIStarSystemStore.goodsList.items) do
    if v.id == nItemid then
      itemDetail = v
      break
    end
  end
  assert(itemDetail, tostring(nItemid))
  local function doBuy()
    GameUIStarSystemStore.param_buy_12 = {itemDetail = itemDetail}
    GameUIStarSystemStore.RequestBuyItem(1, 0)
  end
  local infocost = GameHelper:GetItemInfo(itemDetail.price)
  local infoitem = GameHelper:GetItemInfo(itemDetail.item)
  local textIsCast = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_1")
  local textGameItem = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_2")
  local textToBuy = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_3")
  local textMsg = textIsCast .. textGameItem .. textToBuy .. textGameItem
  local realcnt = math.ceil(infocost.cnt * itemDetail.discount / 1000)
  local textMsgAfterFormat = string.format(textMsg, realcnt, infocost.name, infoitem.cnt, infoitem.name)
  GameUtils:CreditCostConfirm(textMsgAfterFormat, doBuy, false, nil)
end
function GameUIStarSystemStore.RequestBuyItem(count, mstate)
  GameUIStarSystemStore.param_buy_12.buyCount = count
  GameUIStarSystemStore.param_buy_12.mybuycount = count
  local itemDetail = GameUIStarSystemStore.param_buy_12.itemDetail
  local shop_purchase_req_param = {
    act_id = GameUIStarSystemStore.goodsList.act_id,
    item_id = itemDetail.id
  }
  NetMessageMgr:SendMsg(NetAPIList.medal_store_buy_req.Code, shop_purchase_req_param, GameUIStarSystemStore.BuyItem_12_Callback, true, nil)
end
