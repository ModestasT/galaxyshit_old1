local GameStateConfrontation = GameStateManager.GameStateConfrontation
local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
GameStateConfrontation.buildingName = "commander_academy"
function GameStateConfrontation:InitGameState()
end
function GameStateConfrontation:OnFocusGain(previousState)
  self.m_preState = previousState
  GameObjectAdventure:LoadFlashObject()
  self:AddObject(GameUISection)
  self:AddObject(GameObjectAdventure)
  local title = GameLoader:GetGameText("LC_MENU_BUILDING_NAMELG_COMMANDER_ACADEMY")
  GameUISection:SetTitle(title)
  GameObjectAdventure:ShowAfterDialog()
  self:ExecuteStartCallback()
end
function GameStateConfrontation:RegisterStartCallback(func, args)
  self._startCallback = {}
  self._startCallback.func = func
  self._startCallback.args = args
end
function GameStateConfrontation:ExecuteStartCallback()
  if self._startCallback then
    local callback = self._startCallback.func
    local args = self._startCallback.args
    if args and #args > 0 then
      callback(unpack(args))
    else
      callback(args)
    end
    self._startCallback = nil
  end
end
function GameStateConfrontation:OnFocusLost(newState)
  self:EraseObject(GameUISection)
  self:EraseObject(GameObjectAdventure)
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUITechnology) then
    self:EraseObject(GameUITechnology)
  end
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  if GameStateManager:GetCurrentGameState():IsObjectInState(ItemBox) then
    self:EraseObject(ItemBox)
  end
  GameStateConfrontation.m_IsActive = false
end
function GameStateConfrontation:EnterConfrontation()
  local building = GameGlobalData:GetBuildingInfo(self.buildingName)
  if building and building.level > 0 then
    GameStateManager:SetCurrentGameState(GameStateConfrontation)
  elseif building and 0 < building.status then
    GameUIBuilding:DisplayUpgradeDialog(self.buildingName)
  end
end
