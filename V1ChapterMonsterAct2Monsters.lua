local Monsters = GameData.ChapterMonsterAct2.Monsters
Monsters[201001] = {
  ID = 201001,
  vessels = 1,
  durability = 3324,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[201002] = {
  ID = 201002,
  vessels = 2,
  durability = 2718,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201003] = {
  ID = 201003,
  vessels = 3,
  durability = 703,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201004] = {
  ID = 201004,
  vessels = 4,
  durability = 1508,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201005] = {
  ID = 201005,
  vessels = 5,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201006] = {
  ID = 201006,
  vessels = 1,
  durability = 3321,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[201007] = {
  ID = 201007,
  vessels = 2,
  durability = 2718,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201008] = {
  ID = 201008,
  vessels = 3,
  durability = 703,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201009] = {
  ID = 201009,
  vessels = 4,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201010] = {
  ID = 201010,
  vessels = 5,
  durability = 1508,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201011] = {
  ID = 201011,
  vessels = 1,
  durability = 3324,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[201012] = {
  ID = 201012,
  vessels = 2,
  durability = 2716,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201013] = {
  ID = 201013,
  vessels = 3,
  durability = 703,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201014] = {
  ID = 201014,
  vessels = 4,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201015] = {
  ID = 201015,
  vessels = 5,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201016] = {
  ID = 201016,
  vessels = 1,
  durability = 4836,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[201017] = {
  ID = 201017,
  vessels = 2,
  durability = 2716,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201018] = {
  ID = 201018,
  vessels = 3,
  durability = 703,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201019] = {
  ID = 201019,
  vessels = 4,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201020] = {
  ID = 201020,
  vessels = 5,
  durability = 1508,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201021] = {
  ID = 201021,
  vessels = 1,
  durability = 3322,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[201022] = {
  ID = 201022,
  vessels = 2,
  durability = 2718,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201023] = {
  ID = 201023,
  vessels = 3,
  durability = 703,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201024] = {
  ID = 201024,
  vessels = 4,
  durability = 1508,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201025] = {
  ID = 201025,
  vessels = 5,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201026] = {
  ID = 201026,
  vessels = 1,
  durability = 3322,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[201027] = {
  ID = 201027,
  vessels = 2,
  durability = 2718,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201028] = {
  ID = 201028,
  vessels = 3,
  durability = 703,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201029] = {
  ID = 201029,
  vessels = 4,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201030] = {
  ID = 201030,
  vessels = 5,
  durability = 1509,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201031] = {
  ID = 201031,
  vessels = 1,
  durability = 3513,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[201032] = {
  ID = 201032,
  vessels = 2,
  durability = 2871,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201033] = {
  ID = 201033,
  vessels = 3,
  durability = 728,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201034] = {
  ID = 201034,
  vessels = 4,
  durability = 1586,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201035] = {
  ID = 201035,
  vessels = 5,
  durability = 1585,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201036] = {
  ID = 201036,
  vessels = 1,
  durability = 5121,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[201037] = {
  ID = 201037,
  vessels = 2,
  durability = 2870,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201038] = {
  ID = 201038,
  vessels = 3,
  durability = 728,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[201039] = {
  ID = 201039,
  vessels = 4,
  durability = 1585,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[201040] = {
  ID = 201040,
  vessels = 5,
  durability = 1585,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[202001] = {
  ID = 202001,
  vessels = 1,
  durability = 3514,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[202002] = {
  ID = 202002,
  vessels = 2,
  durability = 2870,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202003] = {
  ID = 202003,
  vessels = 3,
  durability = 728,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[202004] = {
  ID = 202004,
  vessels = 4,
  durability = 1586,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202005] = {
  ID = 202005,
  vessels = 5,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202006] = {
  ID = 202006,
  vessels = 1,
  durability = 3516,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[202007] = {
  ID = 202007,
  vessels = 2,
  durability = 2871,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202008] = {
  ID = 202008,
  vessels = 3,
  durability = 728,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[202009] = {
  ID = 202009,
  vessels = 4,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202010] = {
  ID = 202010,
  vessels = 5,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202011] = {
  ID = 202011,
  vessels = 1,
  durability = 3513,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[202012] = {
  ID = 202012,
  vessels = 2,
  durability = 2871,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202013] = {
  ID = 202013,
  vessels = 3,
  durability = 728,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[202014] = {
  ID = 202014,
  vessels = 4,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202015] = {
  ID = 202015,
  vessels = 5,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202016] = {
  ID = 202016,
  vessels = 1,
  durability = 5119,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[202017] = {
  ID = 202017,
  vessels = 2,
  durability = 2870,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202018] = {
  ID = 202018,
  vessels = 3,
  durability = 728,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[202019] = {
  ID = 202019,
  vessels = 4,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202020] = {
  ID = 202020,
  vessels = 5,
  durability = 1586,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202021] = {
  ID = 202021,
  vessels = 1,
  durability = 3513,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[202022] = {
  ID = 202022,
  vessels = 2,
  durability = 2872,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202023] = {
  ID = 202023,
  vessels = 3,
  durability = 728,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[202024] = {
  ID = 202024,
  vessels = 4,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202025] = {
  ID = 202025,
  vessels = 5,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202026] = {
  ID = 202026,
  vessels = 1,
  durability = 3514,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[202027] = {
  ID = 202027,
  vessels = 2,
  durability = 2871,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202028] = {
  ID = 202028,
  vessels = 3,
  durability = 728,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[202029] = {
  ID = 202029,
  vessels = 4,
  durability = 1586,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202030] = {
  ID = 202030,
  vessels = 5,
  durability = 1585,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202031] = {
  ID = 202031,
  vessels = 1,
  durability = 3706,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[202032] = {
  ID = 202032,
  vessels = 2,
  durability = 3025,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202033] = {
  ID = 202033,
  vessels = 3,
  durability = 754,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[202034] = {
  ID = 202034,
  vessels = 4,
  durability = 1662,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202035] = {
  ID = 202035,
  vessels = 5,
  durability = 1663,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[202036] = {
  ID = 202036,
  vessels = 1,
  durability = 5409,
  Avatar = "head22",
  Ship = "ship42"
}
Monsters[202037] = {
  ID = 202037,
  vessels = 2,
  durability = 3025,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[202038] = {
  ID = 202038,
  vessels = 3,
  durability = 754,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[202039] = {
  ID = 202039,
  vessels = 4,
  durability = 1663,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[202040] = {
  ID = 202040,
  vessels = 5,
  durability = 1662,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[203001] = {
  ID = 203001,
  vessels = 1,
  durability = 2458,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[203002] = {
  ID = 203002,
  vessels = 2,
  durability = 1935,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203003] = {
  ID = 203003,
  vessels = 3,
  durability = 754,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[203004] = {
  ID = 203004,
  vessels = 4,
  durability = 981,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203005] = {
  ID = 203005,
  vessels = 5,
  durability = 1208,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203006] = {
  ID = 203006,
  vessels = 1,
  durability = 2458,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[203007] = {
  ID = 203007,
  vessels = 2,
  durability = 1934,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203008] = {
  ID = 203008,
  vessels = 3,
  durability = 754,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[203009] = {
  ID = 203009,
  vessels = 4,
  durability = 981,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203010] = {
  ID = 203010,
  vessels = 5,
  durability = 1208,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203011] = {
  ID = 203011,
  vessels = 1,
  durability = 2457,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[203012] = {
  ID = 203012,
  vessels = 2,
  durability = 1935,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203013] = {
  ID = 203013,
  vessels = 3,
  durability = 754,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[203014] = {
  ID = 203014,
  vessels = 4,
  durability = 981,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203015] = {
  ID = 203015,
  vessels = 5,
  durability = 1208,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203016] = {
  ID = 203016,
  vessels = 1,
  durability = 3536,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[203017] = {
  ID = 203017,
  vessels = 2,
  durability = 1935,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203018] = {
  ID = 203018,
  vessels = 3,
  durability = 754,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[203019] = {
  ID = 203019,
  vessels = 4,
  durability = 981,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203020] = {
  ID = 203020,
  vessels = 5,
  durability = 1208,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203021] = {
  ID = 203021,
  vessels = 1,
  durability = 2457,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[203022] = {
  ID = 203022,
  vessels = 2,
  durability = 1934,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203023] = {
  ID = 203023,
  vessels = 3,
  durability = 754,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[203024] = {
  ID = 203024,
  vessels = 4,
  durability = 981,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203025] = {
  ID = 203025,
  vessels = 5,
  durability = 1208,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203026] = {
  ID = 203026,
  vessels = 1,
  durability = 2457,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[203027] = {
  ID = 203027,
  vessels = 2,
  durability = 1935,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203028] = {
  ID = 203028,
  vessels = 3,
  durability = 754,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[203029] = {
  ID = 203029,
  vessels = 4,
  durability = 981,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203030] = {
  ID = 203030,
  vessels = 5,
  durability = 1208,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203031] = {
  ID = 203031,
  vessels = 1,
  durability = 2579,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[203032] = {
  ID = 203032,
  vessels = 2,
  durability = 2027,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203033] = {
  ID = 203033,
  vessels = 3,
  durability = 780,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[203034] = {
  ID = 203034,
  vessels = 4,
  durability = 1019,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203035] = {
  ID = 203035,
  vessels = 5,
  durability = 1260,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[203036] = {
  ID = 203036,
  vessels = 1,
  durability = 3718,
  Avatar = "head12",
  Ship = "ship42"
}
Monsters[203037] = {
  ID = 203037,
  vessels = 2,
  durability = 2026,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[203038] = {
  ID = 203038,
  vessels = 3,
  durability = 779,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[203039] = {
  ID = 203039,
  vessels = 4,
  durability = 1019,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[203040] = {
  ID = 203040,
  vessels = 5,
  durability = 1260,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204001] = {
  ID = 204001,
  vessels = 1,
  durability = 2458,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[204002] = {
  ID = 204002,
  vessels = 2,
  durability = 1932,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204003] = {
  ID = 204003,
  vessels = 3,
  durability = 779,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204004] = {
  ID = 204004,
  vessels = 4,
  durability = 1020,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204005] = {
  ID = 204005,
  vessels = 5,
  durability = 1259,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204006] = {
  ID = 204006,
  vessels = 1,
  durability = 2458,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[204007] = {
  ID = 204007,
  vessels = 2,
  durability = 1932,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204008] = {
  ID = 204008,
  vessels = 3,
  durability = 779,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204009] = {
  ID = 204009,
  vessels = 4,
  durability = 1020,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204010] = {
  ID = 204010,
  vessels = 5,
  durability = 1259,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204011] = {
  ID = 204011,
  vessels = 1,
  durability = 2459,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[204012] = {
  ID = 204012,
  vessels = 2,
  durability = 1930,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204013] = {
  ID = 204013,
  vessels = 3,
  durability = 779,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204014] = {
  ID = 204014,
  vessels = 4,
  durability = 1019,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204015] = {
  ID = 204015,
  vessels = 5,
  durability = 1260,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204016] = {
  ID = 204016,
  vessels = 1,
  durability = 3538,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[204017] = {
  ID = 204017,
  vessels = 2,
  durability = 1931,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204018] = {
  ID = 204018,
  vessels = 3,
  durability = 779,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204019] = {
  ID = 204019,
  vessels = 4,
  durability = 1019,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204020] = {
  ID = 204020,
  vessels = 5,
  durability = 1259,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204021] = {
  ID = 204021,
  vessels = 1,
  durability = 2459,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[204022] = {
  ID = 204022,
  vessels = 2,
  durability = 1931,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204023] = {
  ID = 204023,
  vessels = 3,
  durability = 779,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204024] = {
  ID = 204024,
  vessels = 4,
  durability = 1020,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204025] = {
  ID = 204025,
  vessels = 5,
  durability = 1260,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204026] = {
  ID = 204026,
  vessels = 1,
  durability = 2459,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[204027] = {
  ID = 204027,
  vessels = 2,
  durability = 1930,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204028] = {
  ID = 204028,
  vessels = 3,
  durability = 779,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204029] = {
  ID = 204029,
  vessels = 4,
  durability = 1019,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204030] = {
  ID = 204030,
  vessels = 5,
  durability = 1259,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204031] = {
  ID = 204031,
  vessels = 1,
  durability = 2575,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[204032] = {
  ID = 204032,
  vessels = 2,
  durability = 2019,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204033] = {
  ID = 204033,
  vessels = 3,
  durability = 805,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204034] = {
  ID = 204034,
  vessels = 4,
  durability = 1058,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204035] = {
  ID = 204035,
  vessels = 5,
  durability = 1311,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204036] = {
  ID = 204036,
  vessels = 1,
  durability = 3711,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[204037] = {
  ID = 204037,
  vessels = 2,
  durability = 2019,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204038] = {
  ID = 204038,
  vessels = 3,
  durability = 805,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[204039] = {
  ID = 204039,
  vessels = 4,
  durability = 1058,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[204040] = {
  ID = 204040,
  vessels = 5,
  durability = 1310,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[205001] = {
  ID = 205001,
  vessels = 1,
  durability = 2573,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[205002] = {
  ID = 205002,
  vessels = 2,
  durability = 2018,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205003] = {
  ID = 205003,
  vessels = 3,
  durability = 805,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205004] = {
  ID = 205004,
  vessels = 4,
  durability = 1058,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205005] = {
  ID = 205005,
  vessels = 5,
  durability = 1310,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205006] = {
  ID = 205006,
  vessels = 1,
  durability = 2573,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[205007] = {
  ID = 205007,
  vessels = 2,
  durability = 2018,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205008] = {
  ID = 205008,
  vessels = 3,
  durability = 805,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205009] = {
  ID = 205009,
  vessels = 4,
  durability = 1057,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205010] = {
  ID = 205010,
  vessels = 5,
  durability = 1310,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205011] = {
  ID = 205011,
  vessels = 1,
  durability = 2574,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[205012] = {
  ID = 205012,
  vessels = 2,
  durability = 2019,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205013] = {
  ID = 205013,
  vessels = 3,
  durability = 805,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205014] = {
  ID = 205014,
  vessels = 4,
  durability = 1058,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205015] = {
  ID = 205015,
  vessels = 5,
  durability = 1310,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205016] = {
  ID = 205016,
  vessels = 1,
  durability = 3710,
  Avatar = "head12",
  Ship = "ship42"
}
Monsters[205017] = {
  ID = 205017,
  vessels = 2,
  durability = 2019,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205018] = {
  ID = 205018,
  vessels = 3,
  durability = 805,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205019] = {
  ID = 205019,
  vessels = 4,
  durability = 1058,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205020] = {
  ID = 205020,
  vessels = 5,
  durability = 1310,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205021] = {
  ID = 205021,
  vessels = 1,
  durability = 2574,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[205022] = {
  ID = 205022,
  vessels = 2,
  durability = 2017,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205023] = {
  ID = 205023,
  vessels = 3,
  durability = 805,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205024] = {
  ID = 205024,
  vessels = 4,
  durability = 1058,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205025] = {
  ID = 205025,
  vessels = 5,
  durability = 1311,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205026] = {
  ID = 205026,
  vessels = 1,
  durability = 2575,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[205027] = {
  ID = 205027,
  vessels = 2,
  durability = 2018,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205028] = {
  ID = 205028,
  vessels = 3,
  durability = 805,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205029] = {
  ID = 205029,
  vessels = 4,
  durability = 1058,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205030] = {
  ID = 205030,
  vessels = 5,
  durability = 1310,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205031] = {
  ID = 205031,
  vessels = 1,
  durability = 2804,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[205032] = {
  ID = 205032,
  vessels = 2,
  durability = 2192,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205033] = {
  ID = 205033,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205034] = {
  ID = 205034,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205035] = {
  ID = 205035,
  vessels = 5,
  durability = 1412,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205036] = {
  ID = 205036,
  vessels = 1,
  durability = 4055,
  Avatar = "head2",
  Ship = "ship43"
}
Monsters[205037] = {
  ID = 205037,
  vessels = 2,
  durability = 2191,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[205038] = {
  ID = 205038,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205039] = {
  ID = 205039,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[205040] = {
  ID = 205040,
  vessels = 5,
  durability = 1413,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206001] = {
  ID = 206001,
  vessels = 1,
  durability = 2805,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[206002] = {
  ID = 206002,
  vessels = 2,
  durability = 2193,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206003] = {
  ID = 206003,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206004] = {
  ID = 206004,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206005] = {
  ID = 206005,
  vessels = 5,
  durability = 1412,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206006] = {
  ID = 206006,
  vessels = 1,
  durability = 2803,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[206007] = {
  ID = 206007,
  vessels = 2,
  durability = 2193,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206008] = {
  ID = 206008,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206009] = {
  ID = 206009,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206010] = {
  ID = 206010,
  vessels = 5,
  durability = 1413,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206011] = {
  ID = 206011,
  vessels = 1,
  durability = 2803,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[206012] = {
  ID = 206012,
  vessels = 2,
  durability = 2193,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206013] = {
  ID = 206013,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206014] = {
  ID = 206014,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206015] = {
  ID = 206015,
  vessels = 5,
  durability = 1413,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206016] = {
  ID = 206016,
  vessels = 1,
  durability = 4055,
  Avatar = "head12",
  Ship = "ship42"
}
Monsters[206017] = {
  ID = 206017,
  vessels = 2,
  durability = 2191,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206018] = {
  ID = 206018,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206019] = {
  ID = 206019,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206020] = {
  ID = 206020,
  vessels = 5,
  durability = 1413,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206021] = {
  ID = 206021,
  vessels = 1,
  durability = 2804,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[206022] = {
  ID = 206022,
  vessels = 2,
  durability = 2192,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206023] = {
  ID = 206023,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206024] = {
  ID = 206024,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206025] = {
  ID = 206025,
  vessels = 5,
  durability = 1413,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206026] = {
  ID = 206026,
  vessels = 1,
  durability = 2804,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[206027] = {
  ID = 206027,
  vessels = 2,
  durability = 2192,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206028] = {
  ID = 206028,
  vessels = 3,
  durability = 856,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206029] = {
  ID = 206029,
  vessels = 4,
  durability = 1134,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206030] = {
  ID = 206030,
  vessels = 5,
  durability = 1413,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206031] = {
  ID = 206031,
  vessels = 1,
  durability = 3035,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[206032] = {
  ID = 206032,
  vessels = 2,
  durability = 2365,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206033] = {
  ID = 206033,
  vessels = 3,
  durability = 907,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206034] = {
  ID = 206034,
  vessels = 4,
  durability = 1211,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206035] = {
  ID = 206035,
  vessels = 5,
  durability = 1515,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206036] = {
  ID = 206036,
  vessels = 1,
  durability = 4401,
  Avatar = "head3",
  Ship = "ship43"
}
Monsters[206037] = {
  ID = 206037,
  vessels = 2,
  durability = 2366,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[206038] = {
  ID = 206038,
  vessels = 3,
  durability = 907,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206039] = {
  ID = 206039,
  vessels = 4,
  durability = 1211,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[206040] = {
  ID = 206040,
  vessels = 5,
  durability = 1515,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207001] = {
  ID = 207001,
  vessels = 1,
  durability = 3036,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[207002] = {
  ID = 207002,
  vessels = 2,
  durability = 2366,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207003] = {
  ID = 207003,
  vessels = 3,
  durability = 907,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207004] = {
  ID = 207004,
  vessels = 4,
  durability = 1211,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207005] = {
  ID = 207005,
  vessels = 5,
  durability = 1515,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207006] = {
  ID = 207006,
  vessels = 1,
  durability = 3034,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[207007] = {
  ID = 207007,
  vessels = 2,
  durability = 2366,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207008] = {
  ID = 207008,
  vessels = 3,
  durability = 908,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207009] = {
  ID = 207009,
  vessels = 4,
  durability = 1212,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207010] = {
  ID = 207010,
  vessels = 5,
  durability = 1515,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207011] = {
  ID = 207011,
  vessels = 1,
  durability = 3035,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[207012] = {
  ID = 207012,
  vessels = 2,
  durability = 2366,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207013] = {
  ID = 207013,
  vessels = 3,
  durability = 907,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207014] = {
  ID = 207014,
  vessels = 4,
  durability = 1211,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207015] = {
  ID = 207015,
  vessels = 5,
  durability = 1516,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207016] = {
  ID = 207016,
  vessels = 1,
  durability = 4401,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[207017] = {
  ID = 207017,
  vessels = 2,
  durability = 2365,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207018] = {
  ID = 207018,
  vessels = 3,
  durability = 908,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207019] = {
  ID = 207019,
  vessels = 4,
  durability = 1211,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207020] = {
  ID = 207020,
  vessels = 5,
  durability = 1516,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207021] = {
  ID = 207021,
  vessels = 1,
  durability = 3035,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[207022] = {
  ID = 207022,
  vessels = 2,
  durability = 2365,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207023] = {
  ID = 207023,
  vessels = 3,
  durability = 907,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207024] = {
  ID = 207024,
  vessels = 4,
  durability = 1211,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207025] = {
  ID = 207025,
  vessels = 5,
  durability = 1515,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207026] = {
  ID = 207026,
  vessels = 1,
  durability = 3035,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[207027] = {
  ID = 207027,
  vessels = 2,
  durability = 2366,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207028] = {
  ID = 207028,
  vessels = 3,
  durability = 907,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207029] = {
  ID = 207029,
  vessels = 4,
  durability = 1211,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207030] = {
  ID = 207030,
  vessels = 5,
  durability = 1515,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207031] = {
  ID = 207031,
  vessels = 1,
  durability = 3265,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[207032] = {
  ID = 207032,
  vessels = 2,
  durability = 2539,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207033] = {
  ID = 207033,
  vessels = 3,
  durability = 958,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207034] = {
  ID = 207034,
  vessels = 4,
  durability = 1288,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207035] = {
  ID = 207035,
  vessels = 5,
  durability = 1618,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207036] = {
  ID = 207036,
  vessels = 1,
  durability = 4748,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[207037] = {
  ID = 207037,
  vessels = 2,
  durability = 2540,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[207038] = {
  ID = 207038,
  vessels = 3,
  durability = 959,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207039] = {
  ID = 207039,
  vessels = 4,
  durability = 1288,
  Avatar = "head25",
  Ship = "ship42"
}
Monsters[207040] = {
  ID = 207040,
  vessels = 5,
  durability = 1618,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[208001] = {
  ID = 208001,
  vessels = 1,
  durability = 3264,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[208002] = {
  ID = 208002,
  vessels = 2,
  durability = 2539,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208003] = {
  ID = 208003,
  vessels = 3,
  durability = 959,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208004] = {
  ID = 208004,
  vessels = 4,
  durability = 1288,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208005] = {
  ID = 208005,
  vessels = 5,
  durability = 1617,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208006] = {
  ID = 208006,
  vessels = 1,
  durability = 3264,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[208007] = {
  ID = 208007,
  vessels = 2,
  durability = 2541,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208008] = {
  ID = 208008,
  vessels = 3,
  durability = 959,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208009] = {
  ID = 208009,
  vessels = 4,
  durability = 1288,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208010] = {
  ID = 208010,
  vessels = 5,
  durability = 1618,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208011] = {
  ID = 208011,
  vessels = 1,
  durability = 3265,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[208012] = {
  ID = 208012,
  vessels = 2,
  durability = 2539,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208013] = {
  ID = 208013,
  vessels = 3,
  durability = 958,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208014] = {
  ID = 208014,
  vessels = 4,
  durability = 1288,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208015] = {
  ID = 208015,
  vessels = 5,
  durability = 1618,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208016] = {
  ID = 208016,
  vessels = 1,
  durability = 4749,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208017] = {
  ID = 208017,
  vessels = 2,
  durability = 2540,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208018] = {
  ID = 208018,
  vessels = 3,
  durability = 958,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208019] = {
  ID = 208019,
  vessels = 4,
  durability = 1288,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208020] = {
  ID = 208020,
  vessels = 5,
  durability = 1618,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208021] = {
  ID = 208021,
  vessels = 1,
  durability = 3265,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[208022] = {
  ID = 208022,
  vessels = 2,
  durability = 2540,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208023] = {
  ID = 208023,
  vessels = 3,
  durability = 959,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208024] = {
  ID = 208024,
  vessels = 4,
  durability = 1288,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208025] = {
  ID = 208025,
  vessels = 5,
  durability = 1618,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208026] = {
  ID = 208026,
  vessels = 1,
  durability = 3266,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[208027] = {
  ID = 208027,
  vessels = 2,
  durability = 2541,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208028] = {
  ID = 208028,
  vessels = 3,
  durability = 959,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208029] = {
  ID = 208029,
  vessels = 4,
  durability = 1288,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208030] = {
  ID = 208030,
  vessels = 5,
  durability = 1618,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208031] = {
  ID = 208031,
  vessels = 1,
  durability = 3495,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[208032] = {
  ID = 208032,
  vessels = 2,
  durability = 2715,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208033] = {
  ID = 208033,
  vessels = 3,
  durability = 1010,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208034] = {
  ID = 208034,
  vessels = 4,
  durability = 1365,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208035] = {
  ID = 208035,
  vessels = 5,
  durability = 1720,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208036] = {
  ID = 208036,
  vessels = 1,
  durability = 5093,
  Avatar = "head9",
  Ship = "ship43"
}
Monsters[208037] = {
  ID = 208037,
  vessels = 2,
  durability = 2714,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[208038] = {
  ID = 208038,
  vessels = 3,
  durability = 1010,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208039] = {
  ID = 208039,
  vessels = 4,
  durability = 1365,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[208040] = {
  ID = 208040,
  vessels = 5,
  durability = 1720,
  Avatar = "head24",
  Ship = "ship5"
}
