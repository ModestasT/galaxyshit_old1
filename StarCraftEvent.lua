require("StarCraftMission.tfl")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
StarCraftEvent = luaClass(nil)
local EVENT_DETAIL_SIMPLE_PARTION = 7200
local EVENT_DISPLAY_STYLE = {
  STYLE_HIDE = 1,
  STYLE_DETAIL = 2,
  STYLE_SIMPLE = 3,
  STYLE_NONE = 4
}
local EVENT_TYPE = {
  EVENT_TYPE_WHITE_HOLE = 1,
  EVENT_TYPE_BOATYARD = 2,
  EVENT_TYPE_MISSILE = 4,
  EVENT_TYPE_MISSION = 1000,
  EVENT_TYPE_SPEED = 2002,
  EVENT_TYPE_DIE_TIME = 2001,
  EVENT_TYPE_NONE = 6
}
function StarCraftEvent:ctor()
  self.mID = -1
  self.mType = -1
  self.mTimeStart = 0
  self.mTimeEnd = 0
  self.mEffectPointList = nil
  self.mAwardsList = nil
  self.mFinished = false
  self.mActive = false
  self.mNeedFetch = false
  self.mDisplayStyle = EVENT_DISPLAY_STYLE.STYLE_NONE
  self.mComingType = 0
  self.mIsUpdating = false
  self.mMissionStateFetchListner = nil
  self.mMissionDetails = {}
end
function StarCraftEvent:InitWithContent(content, timeStart, timeEnd)
  self.mID = content.id
  self.mType = content.type
  self.mTimeStart = timeStart
  self.mTimeEnd = timeEnd
  self.mAwardsList = content.addition_data
  self.mEffectPointList = content.params
  self.mComingType = content.is_show
end
function StarCraftEvent:IsResourceEvent()
  return self.mType >= 0 and self.mType < 1000
end
function StarCraftEvent:IsMissionEvent()
  DebugOut("self.mType = ", self.mType)
  return self.mType >= 1000 and self.mType <= 2000
end
function StarCraftEvent:IsServerEvent()
  return self.mType > 2000 and self.mType < 3000
end
function StarCraftEvent:IsWhiteHole()
  return self.mType == 1
end
function StarCraftEvent:IsBoatyard()
  return self.mType == 2
end
function StarCraftEvent:GetTitleStr()
  local locID = "LC_MENU_TC_EVENT_TITLE_" .. self.mType
  return ...
end
function StarCraftEvent:GetDescStr()
  local locID = "LC_MENU_TC_EVENT_DESC_" .. self.mType
  return ...
end
function StarCraftEvent:GetRemainTime(curTime)
  local remainTime = 0
  if curTime >= self.mTimeStart and curTime < self.mTimeEnd then
    remainTime = self.mTimeEnd - curTime
  elseif curTime < self.mTimeStart then
    remainTime = curTime - self.mTimeStart
  elseif curTime >= self.mTimeEnd then
    remainTime = 0
  end
  return remainTime
end
function StarCraftEvent:GetIconFrame(curTime)
  local displayStyle = self:GetDisplayStyle(curTime)
  if displayStyle == EVENT_DISPLAY_STYLE.STYLE_DETAIL then
    if self:IsMissionEvent() then
      return "event_1000"
    else
      return "event_" .. self.mType
    end
  elseif displayStyle == EVENT_DISPLAY_STYLE.STYLE_SIMPLE then
    if self.mComingType == 1 then
      return "coming_battle"
    elseif self.mComingType == 2 then
      return "coming_resource"
    else
      return nil
    end
  end
  return nil
end
function StarCraftEvent:IsFinished()
  return self.mFinished
end
function StarCraftEvent:GetDisplayStyle(curTime)
  if math.abs(self.mTimeStart - curTime) <= EVENT_DETAIL_SIMPLE_PARTION or curTime >= self.mTimeStart and curTime < self.mTimeEnd then
    return EVENT_DISPLAY_STYLE.STYLE_DETAIL
  end
  if self.mComingType > 0 then
    return EVENT_DISPLAY_STYLE.STYLE_SIMPLE
  end
  return EVENT_DISPLAY_STYLE.STYLE_HIDE
end
function StarCraftEvent:IsInTwoHours(curTime)
  if math.abs(self.mTimeStart - curTime) <= EVENT_DETAIL_SIMPLE_PARTION or curTime >= self.mTimeStart and curTime < self.mTimeEnd then
    return true
  end
  return false
end
function StarCraftEvent:GetAwardDesc(awardIndex)
  if awardIndex > #self.mAwardsList then
    assert(false)
    return nil
  end
  local award = self.mAwardsList[awardIndex]
  if self:IsResourceEvent() then
    if self.mType == EVENT_TYPE.EVENT_TYPE_WHITE_HOLE then
      local descStr = GameLoader:GetGameText("LC_MENU_TC_BUFF_10_DESC")
      local keyLocID = "LC_MENU_" .. string.upper(award.key) .. "_CHAR"
      local key = GameLoader:GetGameText(keyLocID)
      return ...
    elseif self.mType == EVENT_TYPE.EVENT_TYPE_BOATYARD then
      local locID = "LC_MENU_TC_DOCK_BUFF_PARAM_" .. award.key
      return ...
    elseif self.mType == EVENT_TYPE.EVENT_TYPE_MISSILE then
      local locID = "LC_MENU_TC_DEBUFF_TYPE_" .. self.mType
      return ...
    end
  end
end
function StarCraftEvent:GetAwardValueStr(awardIndex)
  if awardIndex > #self.mAwardsList then
    assert(false)
    return nil
  end
  local award = self.mAwardsList[awardIndex]
  local valueStr
  if self:IsResourceEvent() then
    preStr = "+"
    if self.mType == EVENT_TYPE.EVENT_TYPE_WHITE_HOLE then
      valueStr = GameUtils.numberConversion(tonumber(award.value1)) .. "/" .. tonumber(award.value2) / 60 .. "M"
      return valueStr
    elseif self.mType == EVENT_TYPE.EVENT_TYPE_BOATYARD then
      valueStr = preStr .. award.value2 .. "%"
    elseif self.mType == EVENT_TYPE.EVENT_TYPE_MISSILE then
      preStr = "-"
      valueStr = preStr .. GameUtils.numberConversion(tonumber(award.value3)) .. "/" .. tonumber(award.value1) .. "S"
    end
  end
  return valueStr
end
function StarCraftEvent:GetAwardFrame(awardIndex)
  if awardIndex > #self.mAwardsList then
    assert(false)
    return nil
  end
  local award = self.mAwardsList[awardIndex]
  if self:IsWhiteHole() then
    return ...
  elseif self:IsBoatyard() then
    return "tc_" .. award.icon
  elseif self.mType == EVENT_TYPE.EVENT_TYPE_MISSILE then
    return "tc_" .. award.icon
  end
  return "empty"
end
function StarCraftEvent:GetAwardNum()
  return #self.mAwardsList
end
function StarCraftEvent:GetEventTarget()
  if self.mType == EVENT_TYPE.EVENT_TYPE_MISSILE then
    local award = self.mAwardsList[1]
    if award then
      return award.value2
    end
  end
  return -1
end
function StarCraftEvent:GetMissileTime()
  if self.mType == EVENT_TYPE.EVENT_TYPE_MISSILE then
    local award = self.mAwardsList[1]
    if award then
      return award.value1
    end
  end
  return -1
end
function StarCraftEvent:GetMissionIdList()
  return self.mEffectPointList
end
function StarCraftEvent:GetEventEffectPointList()
  return self.mEffectPointList
end
function StarCraftEvent:GenerateMissionDetails()
  self.mMissionDetails = {}
  for i, v in ipairs(self.mEffectPointList) do
    table.insert(self.mMissionDetails, StarCraftMissionTable:GetMissionByID(v))
  end
end
function StarCraftEvent:GetSubMission(missionIndex)
  if missionIndex > #self.mMissionDetails then
    assert(false)
  end
  return self.mMissionDetails[missionIndex]
end
function StarCraftEvent:GetSubMissionNum()
  return #self.mMissionDetails
end
