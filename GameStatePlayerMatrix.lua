local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateRecruit = GameStateManager.GameStateRecruit
local GameStateTacticsCenter = GameStateManager.GameStateTacticsCenter
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameObjectPlayerMatrix = LuaObjectManager:GetLuaObject("GameObjectPlayerMatrix")
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerVessels = GameObjectDragger:NewInstance("VesselsDragger.tfs", false)
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateDaily = GameStateManager.GameStateDaily
function GameObjectDraggerVessels:AnimationBigger()
  self:GetFlashObject():InvokeASCallback("_root", "animationBigger")
end
function GameObjectDraggerVessels:AnimationSmaller()
  self:GetFlashObject():InvokeASCallback("_root", "animationSmaller")
end
function GameObjectDraggerVessels:OnAddToGameState(parent)
  local commander_data = GameGlobalData:GetFleetInfo(self.DraggedFleetID)
  local vessels_image = GameDataAccessHelper:GetCommanderVesselsImage(self.DraggedFleetID, commander_data.level, false)
  local commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.DraggedFleetID, commander_data.level)
  local vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.DraggedFleetID, commander_data.level)
  local commander_sex = GameDataAccessHelper:GetCommanderSex(self.DraggedFleetID)
  DebugOut("extra_id:", self.ExtraId)
  if self.ExtraId ~= -1 then
    vessels_image = GameDataAccessHelper:GetCommanderVesselsImage(self.ExtraId, commander_data.level, false)
    commander_avatar = GameDataAccessHelper:GetFleetAvatar(self.ExtraId, commander_data.level)
    vessels_type = GameDataAccessHelper:GetCommanderVesselsType(self.ExtraId, commander_data.level)
    commander_sex = GameDataAccessHelper:GetCommanderSex(self.ExtraId)
  end
  if commander_sex == 1 then
    commander_sex = "man"
  elseif commander_sex == 0 then
    commander_sex = "woman"
  else
    commander_sex = "unknown"
  end
  local commanderID = commander_data.identity
  local commander_color = GameDataAccessHelper:GetCommanderColorFrame(self.ExtraId, commander_data.level)
  DebugOut("GameObjectDraggerVessels", commander_color)
  DebugOut("add to game state", vessels_image, commander_avatar, vessels_type, commanderID, commander_color, commander_sex)
  self:GetFlashObject():InvokeASCallback("_root", "setVesselsData", vessels_image, commander_avatar, vessels_type, commanderID, commander_color, commander_sex, true)
  self:AnimationBigger()
end
function GameObjectDraggerVessels:OnEraseFromGameState(parent)
  self.IsDraggedFleetInFreelist = false
  self.DraggedFleetGridIndex = -1
  self.DraggedFleetID = -1
  self.ExtraId = -1
  self.IsReleasedInFreeList = false
  self.ReleasedGridIndex = -1
  self.MoveBackDestGridIndex = -1
  self.IsMoveBackToFreeList = false
  self.MoveBackFleetID = -1
end
function GameObjectDraggerVessels:SetAutoMoveTo(grid_type, grid_index, fleet_id)
  if grid_type == "rest" or grid_type == "battle" then
    GameObjectDraggerVessels.DragToGridType = grid_type
    GameObjectDraggerVessels.DragToGridIndex = grid_index
    local dest_pos = GameObjectPlayerMatrix:GetCommanderGridPos(grid_type, grid_index)
    self:SetDestPosition(dest_pos._x, dest_pos._y)
    self:StartAutoMove()
  else
    assert(false)
  end
end
function GameObjectDraggerVessels:OnReachedDestPos()
  if GameObjectDraggerVessels.DragToGridType and GameObjectDraggerVessels.DragToGridIndex then
    GameObjectPlayerMatrix:OnLocatedCommander(GameObjectDraggerVessels)
    GameStatePlayerMatrix:EraseObject(self)
  else
    assert(false)
  end
end
function GameStatePlayerMatrix:QuitState()
  if prev_state == GameStateBattleMap then
    GameStateBattleMap:ReEnter(nil)
  else
    GameStateManager:SetCurrentGameState(self.prev_state)
  end
end
function GameStatePlayerMatrix:InitGameState()
  GameStatePlayerMatrix:AddObject(GameObjectPlayerMatrix)
end
function GameStatePlayerMatrix:OnFocusGain(prev_state)
  if GameStatePlayerMatrix.mPreparePrestigeBack then
    GameStatePlayerMatrix:RestoreDataForBackFromPrestige()
    GameStatePlayerMatrix.mPreparePrestigeBack = false
  elseif prev_state ~= GameStateRecruit and prev_state ~= GameStateTacticsCenter then
    self.prev_state = prev_state
  end
  self:AddObject(GameObjectPlayerMatrix)
  if not GameObjectDraggerVessels:GetFlashObject() then
    GameObjectDraggerVessels:LoadFlashObject()
  end
end
function GameStatePlayerMatrix:OnFocusLost()
  self:EraseObject(GameObjectPlayerMatrix)
  GameObjectDraggerVessels:UnloadFlashObject()
end
function GameStatePlayerMatrix:BeginDragCommanderVessels(commander_id, initPosX, initPosY, mouseX, mouseY, extra_id)
  DebugOut("BeginDragCommanderVessels - ", commander_id, ",", initPosX, ",", initPosY, ",", mouseX, ",", mouseY, ",", extra_id)
  GameObjectDraggerVessels.DraggedFleetID = commander_id
  GameObjectDraggerVessels.ExtraId = commander_id
  if commander_id == 1 and extra_id ~= nil then
    GameObjectDraggerVessels.ExtraId = extra_id
  end
  GameObjectDraggerVessels:BeginDrag(-70, -70, mouseX, mouseY)
  GameObjectDraggerVessels:SetDestPosition(initPosX, initPosY)
  GameObjectPlayerMatrix:OnBeginDragCommander(GameObjectDraggerVessels)
  self:AddObject(GameObjectDraggerVessels)
end
function GameStatePlayerMatrix:OnTouchPressed(x, y)
  if GameObjectDraggerVessels:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStatePlayerMatrix:OnTouchReleased(x, y)
  if GameObjectDraggerVessels:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
  local dragedFleetID = GameObjectDraggerVessels.DraggedFleetID
  if dragedFleetID and dragedFleetID ~= -1 then
    do
      local located_type, located_pos = GameObjectPlayerMatrix:GetCommanderLocatedInfo()
      located_pos = tonumber(located_pos)
      local from_grid_type = GameObjectDraggerVessels.DraggedFleetGridType
      local from_grid_index = GameObjectDraggerVessels.DraggedFleetGridIndex
      DebugOutBattle("from_grid_type - ", GameObjectDraggerVessels.DraggedFleetGridType)
      DebugOutBattle("from_grid_index - ", GameObjectDraggerVessels.DraggedFleetGridIndex)
      if located_type and located_pos then
        if located_type == "rest" then
          local located_commander = GameObjectPlayerMatrix:GetCommanderIDWithGrid(located_type, located_pos)
          GameObjectDraggerVessels.ReleasedGridIndex = located_pos
          if dragedFleetID == 1 then
            GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
            GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif located_commander > 0 then
            do
              local ret, major, adjutant = GameObjectPlayerMatrix:CheckAdjutant(GameObjectDraggerVessels.DraggedFleetGridType, located_type, dragedFleetID, GameObjectDraggerVessels.DraggedFleetGridIndex, located_pos)
              if 0 == ret then
                DebugOut("OK")
                GameObjectDraggerVessels:SetAutoMoveTo(located_type, located_pos)
              elseif -1 == ret then
                DebugOut("Out of max count adjutant can battle.")
                GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
                GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
              elseif 1 == ret then
                DebugOut(tostring(dragedFleetID) .. " is " .. tostring(major) .. "'s adjutant")
                do
                  local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
                  local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant)
                  local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(major)
                  tip = string.gsub(tip, "<npc2_name>", adjutantName)
                  tip = string.gsub(tip, "<npc1_name>", majorName)
                  local function releaseAdjutantResultCallback(success)
                    DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                    if success then
                      GameObjectPlayerMatrix:UpdateBattleCommanderByFleetId(major)
                      GameObjectDraggerVessels:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
                    else
                      GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                    end
                  end
                  local function okCallback()
                    GameStatePlayerMatrix:RequestReleaseAdjutant(major, releaseAdjutantResultCallback)
                  end
                  local function cancelCallback()
                    GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                  end
                  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
                end
              else
                GameObjectDraggerVessels:SetAutoMoveTo(located_type, located_pos)
              end
            end
          else
            GameObjectDraggerVessels:SetAutoMoveTo(located_type, located_pos)
          end
        elseif located_type == "battle" then
          local located_commander = GameObjectPlayerMatrix:GetCommanderIDWithGrid(located_type, located_pos)
          local commanderCount = GameObjectPlayerMatrix:CountBattleCommander()
          local validCommanderCount = GameGlobalData:GetData("matrix").count
          local commanderCountCheck = commanderCount < validCommanderCount or commanderCount == validCommanderCount and located_commander > 0
          DebugOut("located_commander " .. tostring(located_commander))
          if located_commander == 1 and GameObjectDraggerVessels.DraggedFleetGridType == "rest" then
            GameTip:Show(GameLoader:GetGameText("LC_MENU_LOADING_HINT_9"), 3000)
            GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
          elseif GameObjectDraggerVessels.DraggedFleetGridType == "rest" and not commanderCountCheck then
            GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
            local tip = GameLoader:GetGameText("LC_ALERT_fleet_count_over_max_new")
            local function okCallback()
              GameHelper:SetEnterPage(GameHelper.PagePrestige)
              GameStateManager:SetCurrentGameState(GameStateDaily)
            end
            local cancelCallback = function()
            end
            GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
          else
            do
              local ret, major, adjutant = GameObjectPlayerMatrix:CheckAdjutant(GameObjectDraggerVessels.DraggedFleetGridType, located_type, dragedFleetID, GameObjectDraggerVessels.DraggedFleetGridIndex, located_pos)
              if 0 == ret then
                DebugOut("OK")
                GameObjectDraggerVessels:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
              elseif -1 == ret then
                DebugOut("Out of max count adjutant can battle.")
                GameTip:Show(GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_4"), 3000)
                GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
              elseif 1 == ret then
                DebugOut(tostring(adjutant) .. " is " .. tostring(major) .. "'s adjutant")
                do
                  local tip = GameLoader:GetGameText("LC_MENU_BRIDGE_INFO_3")
                  DebugOut("tip " .. tip)
                  local adjutantName = GameDataAccessHelper:GetFleetLevelDisplayName(adjutant)
                  local majorName = GameDataAccessHelper:GetFleetLevelDisplayName(major)
                  tip = string.gsub(tip, "<npc2_name>", adjutantName)
                  tip = string.gsub(tip, "<npc1_name>", majorName)
                  local function releaseAdjutantResultCallback(success)
                    DebugOut("releaseAdjutantResultCallback " .. tostring(success))
                    if success then
                      GameObjectPlayerMatrix:UpdateBattleCommanderByFleetId(major)
                      GameObjectDraggerVessels:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
                    else
                      GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                    end
                  end
                  local function okCallback()
                    GameStatePlayerMatrix:RequestReleaseAdjutant(major, releaseAdjutantResultCallback)
                  end
                  local function cancelCallback()
                    GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
                  end
                  GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
                end
              else
                GameObjectDraggerVessels:SetAutoMoveTo(located_type, located_pos, dragedFleetID)
              end
            end
          end
        else
          assert(false)
        end
      else
        GameObjectDraggerVessels:SetAutoMoveTo(from_grid_type, from_grid_index, dragedFleetID)
      end
      GameObjectDraggerVessels:AnimationSmaller()
    end
  end
end
function GameStatePlayerMatrix:OnTouchMoved(x, y)
  if GameObjectDraggerVessels:IsAutoMove() then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
  local dragedFleetID = GameObjectDraggerVessels.DraggedFleetID
  if dragedFleetID and dragedFleetID ~= -1 then
    GameObjectDraggerVessels:UpdateDrag()
  end
end
GameStatePlayerMatrix.ReleaseAdjutantResultCallback = nil
function GameStatePlayerMatrix:RequestReleaseAdjutant(major, callback)
  DebugOut("RequestReleaseAdjutant " .. tostring(major))
  local req = {major = major}
  GameStatePlayerMatrix.ReleaseAdjutantResultCallback = callback
  NetMessageMgr:SendMsg(NetAPIList.release_adjutant_req.Code, req, self.RequestReleaseAdjutantCallback, true, nil)
end
function GameStatePlayerMatrix.RequestReleaseAdjutantCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.release_adjutant_req.Code then
    if 0 == content.code then
      if GameStatePlayerMatrix.ReleaseAdjutantResultCallback then
        GameStatePlayerMatrix.ReleaseAdjutantResultCallback(true)
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      if GameStatePlayerMatrix.ReleaseAdjutantResultCallback then
        GameStatePlayerMatrix.ReleaseAdjutantResultCallback(false)
      end
    end
    return true
  end
  return false
end
GameStatePlayerMatrix.mPreparePrestigeBack = false
GameStatePlayerMatrix.mBackupData = nil
function GameStatePlayerMatrix:PreparePrestigeBack()
  GameStatePlayerMatrix.mPreparePrestigeBack = true
end
function GameStatePlayerMatrix:BackupDataForGotoPrestige()
  GameStatePlayerMatrix.mBackupData = {
    mStateData = {},
    mUIData = {}
  }
  GameStatePlayerMatrix.mBackupData.mStateData.mPreState = GameStatePlayerMatrix.prev_state
end
function GameStatePlayerMatrix:RestoreDataForBackFromPrestige()
  if GameStatePlayerMatrix.mBackupData then
    GameStatePlayerMatrix.prev_state = GameStatePlayerMatrix.mBackupData.mStateData.mPreState
    GameStatePlayerMatrix.mBackupData = nil
  end
end
