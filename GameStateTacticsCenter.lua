module("GameStateManager", package.seeall)
local GameStateTacticsCenter = GameStateManager.GameStateTacticsCenter
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local QuestTutorialTacticsCenter = TutorialQuestManager.QuestTutorialTacticsCenter
local GameObjectTacticsCenter = LuaObjectManager:GetLuaObject("GameObjectTacticsCenter")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
function GameStateTacticsCenter:InitGameState()
  DebugOut("GameStateTacticsCenter:InitGameState")
  RefineDataManager:RegisterAllNtfMesgHandler()
end
function GameStateTacticsCenter:OnFocusGain(preState)
  self.preState = preState
  self:AddObject(GameObjectTacticsCenter)
  GameObjectTacticsCenter:LoadFlashObject()
  if not QuestTutorialTacticsCenter:IsActive() then
    GameObjectTacticsCenter:enter()
  end
end
function GameStateTacticsCenter:OnFocusLost()
  DebugOut("GameStateTacticsCenter:OnFocusLost")
  self:EraseObject(GameObjectTacticsCenter)
end
function GameStateTacticsCenter:quit()
  GameStateManager:SetCurrentGameState(self.preState)
  if GameStateTacticsCenter.battle_failed then
    GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
    local param = {cli_data = "art"}
    NetMessageMgr:SendMsg(NetAPIList.battle_rate_req.Code, param, nil, false, nil)
    GameStateTacticsCenter.battle_failed = nil
  end
end
