local spell = GameData.effectImage.spell
table.insert(spell, {id = 1, spellname = "normal2"})
table.insert(spell, {id = 2, spellname = "laser"})
table.insert(spell, {id = 3, spellname = "gamma"})
table.insert(spell, {id = 4, spellname = "gravity"})
table.insert(spell, {id = 5, spellname = "indra"})
table.insert(spell, {
  id = 6,
  spellname = "gammaBlaster"
})
table.insert(spell, {
  id = 7,
  spellname = "photonMissile"
})
table.insert(spell, {id = 8, spellname = "gravityA"})
table.insert(spell, {id = 9, spellname = "gravityB"})
table.insert(spell, {id = 10, spellname = "gravityC"})
table.insert(spell, {id = 11, spellname = "indraB"})
table.insert(spell, {
  id = 12,
  spellname = "indraBarrageB"
})
table.insert(spell, {
  id = 13,
  spellname = "gammaBlasterA"
})
table.insert(spell, {
  id = 14,
  spellname = "gammaBlasterB"
})
table.insert(spell, {
  id = 15,
  spellname = "gammaBlasterC"
})
table.insert(spell, {
  id = 16,
  spellname = "gammaBlasterD"
})
table.insert(spell, {id = 17, spellname = "castBuff"})
table.insert(spell, {id = 18, spellname = "locked2"})
table.insert(spell, {id = 19, spellname = "ice"})
table.insert(spell, {
  id = 20,
  spellname = "normal_new2"
})
table.insert(spell, {id = 21, spellname = "disturb"})
table.insert(spell, {
  id = 22,
  spellname = "gammaBlasterE"
})
table.insert(spell, {id = 23, spellname = "gravityD"})
table.insert(spell, {
  id = 24,
  spellname = "gammaBlasterG"
})
table.insert(spell, {
  id = 25,
  spellname = "indra_cross_new"
})
