AutoUpdateInBackground = {
  nextDownloadTimer = -1,
  nextDownloadHeroTimer = -1,
  totleDownloadFile = 0,
  serverVersion = -1,
  localVersion = 0,
  localAppVersion = 0,
  localFileList = {},
  fileCorrupted = false,
  hasPendingDownload = false,
  hasPendingHeroDownload = false,
  localHeroFileList = {},
  totalDownloadHeroFile = 0,
  isDownloadingHeroFile = false,
  heroFileCorrupted = false,
  localHeroVersion = 0,
  downloadList = {},
  currentDownloadIndex = -1,
  forceDonwload = false,
  registedFileList = {}
}
local crc_cache = {}
local crc_lastframe = 0
local crc_cnt = 0
local function cmp_crc(filename, thecrc, forcecmp)
  if filename == nil then
    return true
  end
  forcecmp = true
  if crc_cache[filename] then
    return crc_cache[filename] == thecrc
  end
  if not forcecmp then
    if crc_lastframe ~= GameStateManager.m_frameCounter then
      crc_lastframe = GameStateManager.m_frameCounter
      crc_cnt = 0
    end
    if crc_cnt > 8 then
      return true
    end
  end
  local crc = ext.crc32.crc32(filename)
  crc_cache[filename] = crc
  crc_cnt = crc_cnt + 1
  return crc == thecrc
end
forceDownloadList = {
  ["data2/LAZY_LOAD_DOWN_ship48.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship20.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship54.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship1001.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship51.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship66.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship16.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship61.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship58.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship64.png"] = true,
  ["data2/LAZY_LOAD_DOWN_ship75.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head11.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head11.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head18.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head51.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head51.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head51.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head1001.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head1001.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head1001.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head48.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head48.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head48.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head63.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head63.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head63.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head33.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head33.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head33.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head58.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head58.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head58.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head55.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head55.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head55.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head61.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head61.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head61.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_49_49_head72.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_100_120_head72.png"] = true,
  ["data2/LAZY_LOAD_DOWN_avata_256_256_head72.png"] = true
}
function AutoUpdateInBackground.getTimeHourStr()
  local time = os.time()
  local timeStr = os.date("!%H", time)
  return timeStr or "Unknow"
end
function AutoUpdateInBackground:IsFileUpdatedToServer(filename, debug)
  assert(string.sub(filename, 1, 6) == "data2/")
  local fileInfo = self.localFileList[filename]
  if not self.registedFileList[filename] and fileInfo then
    ext.UpdateAutoUpdateFile(filename, fileInfo.crc, fileInfo.version, fileInfo.serverVersion)
    self.registedFileList[filename] = true
  end
  local result = fileInfo and fileInfo.version == fileInfo.serverVersion and cmp_crc(filename, fileInfo.crc)
  if not result then
    self:AddToDonwloadList(filename, nil, nil)
  end
  return result
end
function AutoUpdateInBackground:IsBuffResUpdatedToServer(filename)
  assert(string.sub(filename, 1, 6) == "data2/")
  local hrFileListName = "hrfl.tfl"
  if ext.IsArm64 and ext.IsArm64() then
    hrFileListName = "hrfl_64.tfl"
  end
  if not hrFileList then
    dofile(hrFileListName)
  end
  if hrFileList then
    local fileInfo = hrFileList[filename]
    if fileInfo and fileInfo.crc ~= "" then
      if not self.registedFileList[filename] then
        ext.UpdateAutoUpdateFile(filename, fileInfo.crc, 0, 0)
        self.registedFileList[filename] = true
      end
      if not cmp_crc(filename, fileInfo.crc) then
        self:AddToDonwloadList(filename)
        return false
      end
    end
    return true
  end
end
function AutoUpdateInBackground:IsHeroFileUpdatedToServer(filename, callback, extInfo)
  assert(string.sub(filename, 1, 6) == "data2/")
  if LuaUtils:table_size(self.localHeroFileList or {}) == 0 then
    ext.dofile("lHRfl.tfl")
    DebugOut("CheckLocalHeroFile1")
    if SavedLocalHeroFileList ~= nil then
      self.localHeroFileList = SavedLocalHeroFileList
      self.localHeroVersion = SavedLocalHeroFileList.version
    end
    self:GetUpdateHeroFileList()
  end
  local fileInfo = self.localHeroFileList[filename]
  if fileInfo and fileInfo.crc ~= "" then
    if not self.registedFileList[filename] then
      ext.UpdateAutoUpdateFile(filename, fileInfo.crc, 0, 0)
      self.registedFileList[filename] = true
    end
    if not cmp_crc(filename, fileInfo.crc) then
      self:AddToDonwloadList(filename, callback, extInfo)
      return false, "crc"
    end
    return true, "ok"
  else
    self:AddToDonwloadList(filename, callback, extInfo)
    return false, "file"
  end
end
function AutoUpdateInBackground:CheckUpdateFileList()
  DebugOut("CheckUpdateFileList")
  DebugOut(ext.IsReachableWifi())
  self:CheckDownloadingURL()
end
function AutoUpdateInBackground:CheckDownloadingURL()
  local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
  ext.http.request({
    AutoUpdate.gameGateway .. "servers/update_addr",
    param = {background = 1},
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      DebugOut("errstr = ", errstr)
      DebugOut("statusCode = ", statusCode)
      if statusCode ~= 200 then
        DebugOut("BG autoUpdate getURL error ", statusCode)
      elseif content then
        if content.error and content.error ~= "" then
          DebugOut("BG autoUpdate getURL error ", statusCode)
        else
          AutoUpdateInBackground.url = content.url .. "/"
          DebugOut("BG autoUpdate URL ", AutoUpdateInBackground.url)
          self:CheckNewUpdateVersionFile()
        end
      end
    end
  })
end
function AutoUpdateInBackground:CheckNewUpdateVersionFile()
  local fullUrlOfVer = AutoUpdateInBackground.url .. "bgUpdateVer.txt"
  ext.http.request({
    fullUrlOfVer,
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        DebugOut("BG autoUpdate getVer error", errstr, statusCode)
        return
      end
      local _, _, filetag, serverVersion, AppVer1, AppVer2, AppVer3 = string.find(content, [[
(%a+)
(%d+)
(%d+).(%d+).(%d+)]])
      self.localAppVersion = tonumber(AppVer3) + 100 * tonumber(AppVer2) + 10000 * tonumber(AppVer1)
      if filetag ~= "tag" then
        DebugOut("BG autoUpdate wrong tag")
        return
      end
      self.serverVersion = serverVersion
      if ext.IsReachableWifi() then
      end
    end
  })
end
function AutoUpdateInBackground:CheckLocalFile()
  if AutoUpdate.isLzma3to1Support then
    ext.dofile("lBGfl_lzma_3to1.tfl")
  elseif AutoUpdate.isLzmaSupport then
    ext.dofile("lBGfl_lzma.tfl")
  else
    ext.dofile("lBGfl.tfl")
  end
  DebugOut("CheckLocalFile")
  if SavedLocalFileList ~= nil then
    self.localFileList = SavedLocalFileList
    self.localVersion = SavedLocalFileList.version
    self.NeedLocalCrcCheck = true
    self.CheckedLocalList = {}
    self.CheckLocalList = LuaUtils:table_copy(self.localFileList)
  end
  SavedLocalFileList = nil
  self:GetUpdateFileList()
end
function AutoUpdateInBackground:OnUpdate(dt)
end
function AutoUpdateInBackground:CheckLocalHeroFile()
  ext.dofile("lHRfl.tfl")
  DebugOut("CheckLocalHeroFile2")
  if SavedLocalHeroFileList ~= nil then
    self.localHeroFileList = SavedLocalHeroFileList
    self.localHeroVersion = SavedLocalHeroFileList.version
    self.NeedCrcCheck = true
    self.checkList = LuaUtils:table_copy(self.localHeroFileList)
    self.CheckedCrcList = {}
  end
  SavedLocalHeroFileList = nil
  self:GetUpdateHeroFileList()
end
function AutoUpdateInBackground:GetUpdateFileList()
  if not self.fileCorrupted and self.localVersion == self.serverVersion then
    DebugOut("there is no resource need update in bg")
    return
  end
  local bgFileName = "bgfl.tfl"
  if ext.IsArm64 and ext.IsArm64() then
    bgFileName = "bgfl_64.tfl"
  end
  if AutoUpdate.isLzmaSupport then
    bgFileName = "bgfl_lzma.tfl"
    if ext.IsArm64 and ext.IsArm64() then
      bgFileName = "bgfl_lzma_64.tfl"
    end
  end
  if AutoUpdate.isLzma3to1Support then
    bgFileName = "bgfl_lzma_3to1.tfl"
    if ext.IsArm64 and ext.IsArm64() then
      bgFileName = "bgfl_lzma_3to1_64.tfl"
    end
  end
  ext.http.requestDownload({
    AutoUpdateInBackground.url .. bgFileName,
    method = "GET",
    localpath = bgFileName,
    callback = function(statusCode, filename, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        DebugOut("BG AutoUpdate get bgfl.tfl error", errstr, statusCode)
        return
      end
      if filename ~= nil then
        local ret, err = dofile(filename)
        if err then
          DebugOut("BG AutoUpdate wrong bgfl.tfl")
          return
        end
        for k, v in pairs(bgFileList) do
          if type(v) == "table" then
            local localFile = self.localFileList[k]
            if localFile == nil then
              self.localFileList[k] = {
                version = 0,
                serverVersion = 0,
                crc = ""
              }
              localFile = self.localFileList[k]
            end
            if localFile.crc ~= v.crc then
              localFile.version = 0
            end
            localFile.crc = v.crc
            localFile.serverVersion = v.serverversion
          end
        end
        bgFileList = nil
        for k, v in pairs(self.localFileList) do
          if type(v) == "table" and v.serverVersion > v.version then
            self.hasPendingDownload = true
            math.randomseed(ext.getSysTime())
            self.nextDownloadTimer = math.random(5000, 30000)
            break
          end
        end
      end
    end
  })
end
function AutoUpdateInBackground:GetUpdateHeroFileList()
  local hrFileListName = "hrfl.tfl"
  if ext.IsArm64 and ext.IsArm64() then
    hrFileListName = "hrfl_64.tfl"
  end
  if AutoUpdateInBackground.url == nil then
    return
  end
  ext.http.requestDownload({
    AutoUpdateInBackground.url .. hrFileListName,
    method = "GET",
    localpath = "hrfl.tfl",
    callback = function(statusCode, filename, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        DebugOut("BG AutoUpdate get hrfl.tfl error", errstr, statusCode)
        return
      end
      if filename ~= nil then
        local ret, err = dofile(filename)
        if err then
          DebugOut("Hero AutoUpdate wrong hrfl.tfl")
          return
        end
        self.downloadableServerFileList = hrFileList
        DebugOut("hrFileList = ")
        DebugTable(hrFileList)
        for k, v in pairs(hrFileList) do
          if type(v) == "table" then
            local localFile = self.localHeroFileList[k]
            if localFile == nil and forceDownloadList[k] then
              self.localHeroFileList[k] = {crc = ""}
              localFile = self.localHeroFileList[k]
            end
            if localFile and localFile.crc ~= v.crc then
              localFile.crc = ""
            end
          end
        end
        hrFileList = nil
        DebugOut("self.localHeroFileList = ")
        DebugTable(self.localHeroFileList)
        for k, v in pairs(self.localHeroFileList) do
          if type(v) == "table" and forceDownloadList[k] and v.crc == "" then
            self.forceDonwload = true
            AutoUpdateInBackground:AddToDonwloadList(k)
          end
        end
        DebugOut("self.totalDownloadHeroFile = ", self.totalDownloadHeroFile)
      end
    end
  })
end
function AutoUpdateInBackground:AddToDonwloadList(filename, callback, extendInfo)
  DebugOut("add file to download list = ", filename)
  for i, v in ipairs(self.downloadList) do
    if v.filename == filename then
      local callbackItem = {}
      if type(extendInfo) == "table" then
        callbackItem.extInfo = LuaUtils:table_rcopy(extendInfo)
      else
        callbackItem.extInfo = nil
      end
      if type(callback) ~= "function" then
        callbackItem.callbackFun = nil
      else
        callbackItem.callbackFun = callback
      end
      table.insert(v.callback, callbackItem)
      if i > self.currentDownloadIndex + 1 and i > 4 then
        if self.currentDownloadIndex > 0 then
          local st = LuaUtils:table_rcopy(v)
          table.remove(self.downloadList, i)
          table.insert(self.downloadList, self.currentDownloadIndex + 1, st)
        else
          local st = LuaUtils:table_rcopy(v)
          table.remove(self.downloadList, i)
          table.insert(self.downloadList, 1, st)
        end
      end
      return
    end
  end
  local downloadTask = {}
  downloadTask.filename = filename
  downloadTask.lastTryDownloadTimestamp = -1
  downloadTask.callback = {}
  local callbackItem = {}
  if type(extendInfo) == "table" then
    callbackItem.extInfo = LuaUtils:table_rcopy(extendInfo)
  else
    callbackItem.extInfo = nil
  end
  if type(callback) ~= "function" then
    callbackItem.callbackFun = nil
  else
    callbackItem.callbackFun = callback
  end
  table.insert(downloadTask.callback, callbackItem)
  DebugOutPutTable(downloadTask, "AddToDonwloadList")
  table.insert(self.downloadList, downloadTask)
end
function AutoUpdateInBackground:StartDownloadRes(filename)
  local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
  if filename ~= nil then
    ext.http.requestDownload({
      AutoUpdateInBackground.url .. filename,
      method = "GET",
      localpath = filename,
      callback = function(statusCode, filename, errstr)
        if errstr ~= nil or statusCode ~= 200 then
          DebugStore("Failed get res file ", statusCode, errstr, resUrl)
          table.insert(self.downloadList, self.downloadList[self.currentDownloadIndex])
          table.remove(self.downloadList, self.currentDownloadIndex)
          self.currentDownloadIndex = -1
          return
        end
        DebugOut("filename = ", filename)
        local crcresult = ext.crc32.crc32(filename)
        local tmpName = string.match(filename, ".+/([^/]*%w+)$")
        tmpName = "data2/" .. tmpName
        DebugOut("tmpName = ", tmpName)
        if forceDownloadList[tmpName] then
          self.localHeroFileList[tmpName] = self.localHeroFileList[tmpName] or {}
          self.localHeroFileList[tmpName].crc = crcresult
          self.needSaveHero = true
        else
          self.localHeroFileList[tmpName] = self.localHeroFileList[tmpName] or {}
          self.localHeroFileList[tmpName].crc = crcresult
          self.needSaveHero = true
        end
        self.localHeroFileList[tmpName].t = os.time()
        ext.UpdateAutoUpdateFile(filename, crcresult, 0, 0)
        local downloadItem = self.downloadList[self.currentDownloadIndex]
        if downloadItem.callback then
          for i, v in ipairs(downloadItem.callback) do
            if v.callbackFun ~= nil then
              v.callbackFun(v.extInfo)
            end
          end
        end
        table.remove(self.downloadList, self.currentDownloadIndex)
        DebugOut("after download cur list in self.downloadList = ", self.currentDownloadIndex)
        DebugTable(self.downloadList)
        self.currentDownloadIndex = -1
        DebugStore("download finished ,", AutoUpdateInBackground.url .. tmpName)
      end
    })
  end
end
function AutoUpdateInBackground:UpdateDownloadList(dt)
  if self.currentDownloadIndex ~= -1 then
    return
  end
  if not self.url then
    return
  end
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateBattlePlay then
    return
  end
  if #self.downloadList <= 0 then
    if self.forceDonwload then
      self.forceDonwload = false
      self.localHeroVersion = self.serverVersion
      self.localHeroFileList.version = self.serverversion
    end
    self.currentDownloadIndex = -1
    return
  end
  local downloadTask = {}
  for i, v in ipairs(self.downloadList) do
    if v.lastTryDownloadTimestamp == -1 or os.time() - v.lastTryDownloadTimestamp > 600 then
      self.currentDownloadIndex = i
      v.lastTryDownloadTimestamp = os.time()
      downloadTask = v
      break
    end
  end
  if self.currentDownloadIndex == -1 then
  end
  AutoUpdateInBackground:StartDownloadRes(downloadTask.filename)
end
function AutoUpdateInBackground:Update(dt)
  self:UpdateDownloadList(dt)
  self:UpdateBgFile(dt)
end
function AutoUpdateInBackground:UpdateHeroFile(dt)
  if not GameGlobalData:GetUserInfo() then
    return
  end
  if self.nextDownloadHeroTimer == -1 then
    return
  end
  self.nextDownloadHeroTimer = self.nextDownloadHeroTimer - dt
  if self.nextDownloadHeroTimer > 0 then
    return
  end
  if self.isDownloadingHeroFile then
    return
  end
  self.nextDownloadHeroTimer = -1
  local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
  for k, v in pairs(self.localHeroFileList) do
    if type(v) == "table" and v.serverVersion > v.version then
      DebugOut("try to download hero file ", k)
      DebugOut("cur total file =  ", self.totalDownloadHeroFile)
      DebugTable(v)
      self.isDownloadingHeroFile = true
      self.currentHeroTask = v
      ext.http.requestDownload({
        AutoUpdateInBackground.url .. k,
        localpath = k,
        callback = function(statusCode, filename, errstr)
          self.isDownloadingHeroFile = false
          if errstr ~= nil or statusCode ~= 200 then
            if filename then
              local trimedFileName = string.match(filename, ".+/([^/]*%w+)$")
              DebugOut("AutoUpdateIn Hero download file error :" .. trimedFileName)
            end
            DebugOut("Error MSG:", errstr, statusCode)
            return
          end
          if filename ~= nil then
            self.nextDownloadHeroTimer = 5000 + math.random(1000, 12000)
            if not cmp_crc(filename, self.currentHeroTask.crc) then
              local trimedFileName = string.match(filename, ".+/([^/]*%w+)$")
              DebugOut("AutoUpdateIn Hero download file crc not match:" .. trimedFileName)
              return
            else
              self.currentHeroTask.version = self.currentHeroTask.serverVersion
              DebugOut("file:" .. filename .. " updated to version:" .. self.currentHeroTask.serverVersion)
              ext.UpdateAutoUpdateFile(filename, self.currentHeroTask.crc, self.currentHeroTask.version, self.currentHeroTask.serverVersion)
              self.needSaveHero = true
            end
          end
        end
      })
      return
    end
  end
  self.localHeroVersion = self.serverVersion
  self.localHeroFileList.version = self.serverVersion
end
function AutoUpdateInBackground:UpdateBgFile(dt)
  if not self.hasPendingDownload then
    return
  end
  if not GameGlobalData:GetUserInfo() then
    return
  end
  if self.nextDownloadTimer == -1 then
    return
  end
  self.nextDownloadTimer = self.nextDownloadTimer - dt
  if self.nextDownloadTimer > 0 then
    return
  end
  self.nextDownloadTimer = -1
  for k, v in pairs(self.localFileList) do
    if type(v) == "table" and v.serverVersion > v.version then
      DebugOut("try to download file ", k)
      DebugTable(v)
      self.hasPendingDownload = true
      self.currentTask = v
      DebugOut("self.currentTask = ")
      DebugTable(self.currentTask)
      ext.http.requestDownload({
        AutoUpdateInBackground.url .. k,
        localpath = k,
        callback = function(statusCode, filename, errstr)
          self.nextDownloadTimer = 5000 + math.random(1000, 12000)
          local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
          if errstr ~= nil or statusCode ~= 200 then
            if filename then
              local trimedFileName = string.match(filename, ".+/([^/]*%w+)$")
              DebugOut("AutoUpdateInBG download file error :" .. trimedFileName)
            end
            DebugOut("Error MSG:", errstr, statusCode)
            return
          end
          if filename ~= nil then
            if not cmp_crc(filename, self.currentTask.crc) then
              local trimedFileName = string.match(filename, ".+/([^/]*%w+)$")
              DebugOut("AutoUpdateInBG download file crc not match:" .. trimedFileName)
              return
            else
              self.totleDownloadFile = self.totleDownloadFile + 1
              self.currentTask.version = self.currentTask.serverVersion
              DebugOut("file:" .. filename .. " updated to version:" .. self.currentTask.serverVersion)
              ext.UpdateAutoUpdateFile(filename, self.currentTask.crc, self.currentTask.version, self.currentTask.serverVersion)
              self.needSave = true
              BattleEffectManager:OnImageDownload(filename)
            end
          end
        end
      })
      return
    end
  end
  self.localVersion = self.serverVersion
  self.localFileList.version = self.serverVersion
  self.hasPendingDownload = false
  self.fileCorrupted = false
end
function AutoUpdateInBackground:SaveBGFileList()
  DebugOut("AutoUpdateInBackground:SaveBGFileList", self.needSave)
  if self.needSave then
    self.needSave = false
    if AutoUpdate.isLzma3to1Support then
      ext.SaveGameData("lBGfl_lzma_3to1.tfl", GameUtils:formatSaveString(self.localFileList, "SavedLocalFileList"))
    elseif AutoUpdate.isLzmaSupport then
      ext.SaveGameData("lBGfl_lzma.tfl", GameUtils:formatSaveString(self.localFileList, "SavedLocalFileList"))
    else
      ext.SaveGameData("lBGfl.tfl", GameUtils:formatSaveString(self.localFileList, "SavedLocalFileList"))
    end
  end
end
local crashDevice
function AutoUpdateInBackground:SaveHRFileList()
  if crashDevice == nil then
    do
      local deviceName = ext.GetDeviceFullName()
      if deviceName == "iPhone3GS" or deviceName == "iPod4" or deviceName == "iPad1" or deviceName == "iPad2" or deviceName == "iPhone4S" or deviceName == "iPhone4" or deviceName == "iPod5" or deviceName == "iPadMini" or deviceName == "iPadMini2" or deviceName == "iPhone5S" or deviceName == "iPhone5" then
        crashDevice = true
      else
        crashDevice = false
        do break end
        if crashDevice then
          local len = 0
          for k, v in pairs(AutoUpdateInBackground.localHeroFileList) do
            len = len + 1
          end
          if len > 400 then
            local path = ""
            for k, v in pairs(AutoUpdateInBackground.localHeroFileList) do
              local needremove = false
              local curtime = os.time()
              if v.t == nil then
                v.t = curtime
              end
              needremove = string.find(k, "ship") == nil
              needremove = needremove and string.find(k, "newEnhance_bg") == nil
              if needremove then
                path = string.format("%s%s", ext.GetDocPath(), k)
                os.remove(path)
                AutoUpdateInBackground.localHeroFileList[k] = nil
              end
            end
            GameDataAccessHelper.ShipImageCache = {}
            crc_cache = {}
          end
        end
      end
    end
  end
  if self.needSaveHero then
    DebugOut("AutoUpdateInBackground:SaveHRFileList", self.needSaveHero)
    self.needSaveHero = false
    if self.localHeroFileList then
      ext.SaveGameData("lHRfl.tfl", GameUtils:formatSaveString(self.localHeroFileList, "SavedLocalHeroFileList"))
    end
  end
end
