WVEEnemyInfoPanel = luaClass(nil)
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
function WVEEnemyInfoPanel:ctor(wveGameManeger)
  self.mGameManger = wveGameManeger
  self.ITEM_COLUMN = 2
end
function WVEEnemyInfoPanel:OnFSCommand(cmd, arg)
  local flashObj = self.mGameManger:GetFlashObject()
  if cmd == "updateEnemyItemInfo" then
    local itemIndex = tonumber(arg)
    self:SetEnemyItemInfo(itemIndex)
  elseif cmd == "ShowEnemyInfoPanel" then
    NetMessageMgr:SendMsg(NetAPIList.prime_enemy_info_req.Code, nil, self.RequestEnemyInfoCallback, true, nil)
  elseif cmd == "HideEnemyInfoWindow" then
    if flashObj then
      flashObj:InvokeASCallback("_root", "HideEnemyInfoWindow")
      self.IsShowEnemyInfoWindow = false
    end
  else
    return false
  end
  return true
end
function WVEEnemyInfoPanel:SetEnemyItemInfo(itemIndex)
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    return
  end
  DebugOut("SetEnemyItemInfo:", itemIndex, self.enemyItemNum)
  if itemIndex == 1 then
    flashObj:InvokeASCallback("_root", "SetEnemyTitleInfo", itemIndex, GameLoader:GetGameText("LC_MENU_WVE_PRIMUS_TITLE"))
  elseif itemIndex <= self.enemyItemNum + 1 then
    self:SetEnemyDetailItem(itemIndex)
  elseif itemIndex == self.enemyItemNum + 2 then
    flashObj:InvokeASCallback("_root", "SetEnemyTitleInfo", itemIndex, GameLoader:GetGameText("LC_MENU_DETAIL_BUTTON"))
  else
    self:SetEnemyDescItem(itemIndex)
  end
end
function WVEEnemyInfoPanel:SetEnemyDescItem(itemIndex)
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    return
  end
  local dataIndex = itemIndex - 2 - self.enemyItemNum
  local itemData = self.enemyInfo[dataIndex]
  flashObj:InvokeASCallback("_root", "SetEnemyDetailInfo", itemIndex, itemData.isBoss == 1, itemData.monsters, itemData.desc)
end
function WVEEnemyInfoPanel:SetEnemyDetailItem(itemIndex)
  local flashObj = WVEGameManeger:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "HideEnemyItemInfo", itemIndex)
  local startIndex = (itemIndex - 2) * self.ITEM_COLUMN + 1
  local endIndex = startIndex + self.ITEM_COLUMN - 1
  if endIndex > #self.enemyInfo then
    endIndex = #self.enemyInfo
  end
  local column = 1
  for i = startIndex, endIndex do
    local itemData = self.enemyInfo[i]
    local name = itemData.name
    flashObj:InvokeASCallback("_root", "SetEnemyItemInfo", itemIndex, column, itemData.isBoss == 1, itemData.amount, itemData.monsters, name)
    column = column + 1
  end
end
function WVEEnemyInfoPanel.RequestEnemyInfoCallback(msgType, content)
  DebugOut("RequestEnemyInfoCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.prime_enemy_info_ack.Code then
    WVEGameManeger.mWVEEnemyInfoPanel:SetEnemyListInfo(content.prime_monsters)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.prime_enemy_info_req.Code then
    return true
  end
  return false
end
function WVEEnemyInfoPanel:GenerateTestData()
  local data = {}
  for i = 1, 2 do
    local item = {}
    item.position = 0
    item.amount = 100
    item.monsters = {}
    item.desc_id = 1
    table.insert(data, item)
  end
  for i = 1, 6 do
    local item = {}
    item.position = 1
    item.amount = 999
    item.monsters = {}
    item.desc_id = 1
    for j = 1, 4 do
      table.insert(item.monsters, 501001)
    end
    table.insert(data, item)
  end
  return data
end
function WVEEnemyInfoPanel:SetEnemyListInfo(content)
  local flashObj = WVEGameManeger:GetFlashObject()
  if flashObj and content then
    self:RecordEnemyInfo(content)
    flashObj:InvokeASCallback("_root", "ShowEnemyInfoPanel")
    flashObj:InvokeASCallback("_root", "initEnemyInfoListBox", self.enemyItemNum + #self.enemyInfo + 2)
    self.IsShowEnemyInfoWindow = true
  end
end
function WVEEnemyInfoPanel:RecordEnemyInfo(content)
  self.enemyInfo = {}
  for k, v in pairs(content) do
    local item = {}
    item.isBoss = tonumber(v.position)
    item.amount = v.amount
    item.desc = GameLoader:GetGameText("LC_BATTLE_PRIME_MOBS_" .. tostring(v.desc_id) .. "_DESC")
    item.monsters = {}
    item.name = GameLoader:GetGameText("LC_BATTLE_PRIME_MOBS_" .. tostring(v.desc_id))
    for kM, vM in pairs(v.monsters) do
      if tonumber(vM) ~= 0 then
        DebugOut("RecordEnemyInfo:", vM)
        DebugOut(GameDataAccessHelper:GetMonsterShip(tonumber(vM), false))
        table.insert(item.monsters, GameDataAccessHelper:GetMonsterShip(tonumber(vM), false))
      end
    end
    table.insert(self.enemyInfo, item)
  end
  self.enemyItemNum = math.ceil(#self.enemyInfo / self.ITEM_COLUMN)
end
