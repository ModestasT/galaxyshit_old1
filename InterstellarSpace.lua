local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local InterstellarAdventure = LuaObjectManager:GetLuaObject("InterstellarAdventure")
InterstellarSpace = luaClass(nil)
function InterstellarSpace:ctor(spaceId, spaceLootList, adventureCd, adventureCostType, adventureCostValue)
  self.mLootsNum = 0
  self.mLootsEarned = 0
  self.mAwardsList = nil
  self.mRestAwardsIndexList = nil
  self.mState = EInterstellarSpaceState.SPACE_STATE_LOCKED
  self.mID = spaceId
  self.mName = GameLoader:GetGameText("LC_MENU_INTERSTELLAR_HUNT_PLANET_" .. spaceId)
  self.mAdventuresCD = adventureCd
  self.mAdventuresCDFetchTime = os.time()
  self.mCurrentAdventureCostType = adventureCostType
  self.mCurrentAdventureCostValue = adventureCostValue
end
function InterstellarSpace:Init(spaceLootList, curFocusSpaceID)
  self.mLootsNum = 0
  self.mLootsEarned = 0
  self.mAwardsList = {}
  self.mRestAwardsIndexList = {}
  for i, v in ipairs(spaceLootList) do
    if v.award_status == 1 then
      self.mLootsEarned = self.mLootsEarned + 1
    else
      table.insert(self.mRestAwardsIndexList, i)
    end
    self.mLootsNum = self.mLootsNum + 1
    local awardItem = InterstellarAdventureAward.new(v.award, v.award_status, v.big_or_norm)
    awardItem:GenerateDisplayData()
    table.insert(self.mAwardsList, awardItem)
  end
  if self.mID == InterstellarAdventure.mCurrentSpaceID then
    if self.mLootsNum == self.mLootsEarned and self.mLootsNum > 0 then
      self.mState = EInterstellarSpaceState.SPACE_STATE_FINISH
    else
      self.mState = EInterstellarSpaceState.SPACE_STATE_FOCUS
    end
  elseif self.mLootsNum == self.mLootsEarned and self.mLootsNum > 0 then
    self.mState = EInterstellarSpaceState.SPACE_STATE_FINISH
  else
    self.mState = EInterstellarSpaceState.SPACE_STATE_LOCKED
  end
end
function InterstellarSpace:RefreshSpace(newCDs, anewCostType, newCostValue)
  self.mLootsNum = 0
  self.mLootsEarned = 0
  self.mRestAwardsIndexList = {}
  self.mAdventuresCD = newCDs
  self.mAdventuresCDFetchTime = os.time()
  self.mCurrentAdventureCostType = anewCostType
  self.mCurrentAdventureCostValue = newCostValue
  for i, v in ipairs(self.mAwardsList) do
    if v.mIsGetted == 1 then
      self.mLootsEarned = self.mLootsEarned + 1
    else
      table.insert(self.mRestAwardsIndexList, i)
    end
    self.mLootsNum = self.mLootsNum + 1
  end
  if self.mID == InterstellarAdventure.mCurrentSpaceID then
    if self.mLootsNum == self.mLootsEarned and self.mLootsNum > 0 then
      self.mState = EInterstellarSpaceState.SPACE_STATE_FINISH
    else
      self.mState = EInterstellarSpaceState.SPACE_STATE_FOCUS
    end
  elseif self.mLootsNum == self.mLootsEarned and self.mLootsNum > 0 then
    self.mState = EInterstellarSpaceState.SPACE_STATE_FINISH
  else
    self.mState = EInterstellarSpaceState.SPACE_STATE_LOCKED
  end
end
function InterstellarSpace:GetAwards(awardIndex)
  local award = self.mAwardsList[awardIndex]
  if award then
    award.mIsGetted = 1
  end
end
function InterstellarSpace:IsAllAwardsGet()
  if self.mState == EInterstellarSpaceState.SPACE_STATE_FINISH then
    return true
  end
  return false
end
function InterstellarSpace:IsFinalAwards()
  if self.mLootsNum > 0 and self.mLootsEarned and self.mLootsNum - self.mLootsEarned == 1 then
    return true
  end
  return false
end
function InterstellarSpace:GetCostDisplayData()
  local costDesc = ""
  local iconFrame = ""
  if self.mCurrentAdventureCostType == "free" then
    costDesc = GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_FREE_CHAR")
    iconFrame = "FREE"
  elseif GameHelper:IsResource(self.mCurrentAdventureCostType) then
    local gameItem = GameUtils:MakeGameitem(self.mCurrentAdventureCostType, nil, self.mCurrentAdventureCostValue, nil)
    iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(gameItem, nil, nil)
    costDesc = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(gameItem.item_type, gameItem.number, gameItem.no))
  else
    local curCoinID = InterstellarAdventure.mCurrentCoinID
    local gameItem = GameUtils:MakeGameitem("item", curCoinID, self.mCurrentAdventureCostValue, nil)
    iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(gameItem, nil, nil)
    costDesc = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(gameItem.item_type, gameItem.number, gameItem.no))
  end
  return costDesc, iconFrame
end
function InterstellarSpace:GetCDDisplayData()
  local timeNum = self.mAdventuresCD - (os.time() - self.mAdventuresCDFetchTime)
  if timeNum < 3600 then
    return GameUtils:formatTimeStringWithoutHour(timeNum), nil
  else
    return GameUtils:formatTimeString(timeNum), nil
  end
end
function InterstellarSpace:GetCurrentDisplayData()
  if self:IsAllAwardsGet() then
    return "", ""
  elseif self.mAdventuresCD <= 0 or os.time() - self.mAdventuresCDFetchTime >= self.mAdventuresCD then
    return (...), self
  else
    return (...), self
  end
end
function InterstellarSpace:GenerateAllAwardsDisplayData()
  for i, v in ipairs(self.mAwardsList) do
    v:GenerateDisplayData()
  end
end
function InterstellarSpace:GetAwardsList()
  return self.mAwardsList
end
function InterstellarSpace:GetRestAwardIndexList()
  return self.mRestAwardsIndexList
end
function InterstellarSpace:GetIndexInRestList(awardIndex)
  for i, v in ipairs(self.mRestAwardsIndexList) do
    if v == awardIndex then
      return i
    end
  end
  return -1
end
function InterstellarSpace:RefeshSpace()
end
