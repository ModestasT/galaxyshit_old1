local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
local GameStateLargeMap = GameStateManager.GameStateLargeMap
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUILargeMapAlliance = LuaObjectManager:GetLuaObject("GameUILargeMapAlliance")
local GameUILargeMapUI = LuaObjectManager:GetLuaObject("GameUILargeMapUI")
local GameUILargeMapRank = LuaObjectManager:GetLuaObject("GameUILargeMapRank")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStateLargeMapAlliance = GameStateManager.GameStateLargeMapAlliance
local GameStateLargeMapRank = GameStateManager.GameStateLargeMapRank
require("data1/Bit.tfl")
require("data1/GameLargeMapUILayer.tfl")
GameUILargeMap.UIList = {}
GameUILargeMap.UIGroupList = {}
GameUILargeMap.BGList = {}
GameUILargeMap.StarList = {}
GameUILargeMap.baseData = nil
GameUILargeMap.ShipList = {}
GameUILargeMap.PlayerData = {}
GameUILargeMap.MyPlayerData = nil
GameUILargeMap.pendingBlock = {}
GameUILargeMap.needUpdateBlock = {}
GameUILargeMap.nowUpdateBlock = {}
GameUILargeMap.occuptBlock = {}
GameUILargeMap.massLines = {}
GameUILargeMap.massWarning = {}
GameUILargeMap.CurShowUPRow = 0
GameUILargeMap.CurShowDownRow = 0
GameUILargeMap.CurShowLeftCol = 0
GameUILargeMap.CurShowRightCol = 0
GameUILargeMap.refreshCount = 8
GameUILargeMap.draging = false
GameUILargeMap.firstTimeEnterChat = true
GameUILargeMap.onAddStateCallBack = nil
GameUILargeMap.MyBlockVivionData = nil
GameUILargeMap.MyFlyPoints = nil
GameUILargeMap.MyVisionData = {
  VisibleIDS = {},
  SoonVisibleIDS = {},
  SoonFadeIDS = {},
  SoonInID = nil,
  SoonFadeID = nil
}
GameUILargeMap.OddLineArray = {
  {
    -266,
    -265,
    -264,
    -263,
    -262
  },
  {
    -200,
    -199,
    -198,
    -197,
    -196,
    -195
  },
  {
    -135,
    -134,
    -133,
    -132,
    -131,
    -130,
    -129
  },
  {
    -69,
    -68,
    -67,
    -66,
    -65,
    -64,
    -63,
    -62
  },
  {
    -4,
    -3,
    -2,
    -1,
    0,
    1,
    2,
    3,
    4
  },
  {
    63,
    64,
    65,
    66,
    67,
    68,
    69,
    70
  },
  {
    129,
    130,
    131,
    132,
    133,
    134,
    135
  },
  {
    196,
    197,
    198,
    199,
    200,
    201
  },
  {
    262,
    263,
    264,
    265,
    266
  }
}
GameUILargeMap.EvenLineArray = {
  {
    -266,
    -265,
    -264,
    -263,
    -262
  },
  {
    -201,
    -200,
    -199,
    -198,
    -197,
    -196
  },
  {
    -135,
    -134,
    -133,
    -132,
    -131,
    -130,
    -129
  },
  {
    -70,
    -69,
    -68,
    -67,
    -66,
    -65,
    -64,
    -63
  },
  {
    -4,
    -3,
    -2,
    -1,
    0,
    1,
    2,
    3,
    4
  },
  {
    62,
    63,
    64,
    65,
    66,
    67,
    68,
    69
  },
  {
    129,
    130,
    131,
    132,
    133,
    134,
    135
  },
  {
    195,
    196,
    197,
    198,
    199,
    200
  },
  {
    262,
    263,
    264,
    265,
    266
  }
}
function GameUILargeMap:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("battle_supply", self._ActionTimesChangeCallback)
end
function GameUILargeMap:OnAddToGameState()
  DebugOut("GameUILargeMap:OnAddToGameState")
  self:LoadFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "initFlashObj")
  GameUILargeMap:ReqMainMapData()
  if GameUILargeMap.onAddStateCallBack then
    GameUILargeMap.onAddStateCallBack()
    GameUILargeMap.onAddStateCallBack = nil
  end
end
function GameUILargeMap:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameUILargeMap.UIList = {}
  GameUILargeMap.UIGroupList = {}
  GameUILargeMap.BGList = {}
  GameUILargeMap.StarList = {}
  GameUILargeMap.baseData = nil
  GameUILargeMap.ShipList = {}
  GameUILargeMap.PlayerData = {}
  GameUILargeMap.MyPlayerData = nil
  GameUILargeMap.pendingBlock = {}
  GameUILargeMap.needUpdateBlock = {}
  GameUILargeMap.occuptBlock = {}
  GameUILargeMap.massLines = {}
  GameUILargeMap.massWarning = {}
  GameUILargeMap.CurShowUPRow = 0
  GameUILargeMap.CurShowDownRow = 0
  GameUILargeMap.CurShowLeftCol = 0
  GameUILargeMap.CurShowRightCol = 0
  GameUILargeMap.draging = false
  GameUILargeMap.refreshCount = 8
  GameUILargeMap.MyFlyPoints = nil
  GameUILargeMap.MyVisionData = {
    VisibleIDS = {},
    SoonVisibleIDS = {},
    SoonFadeIDS = {},
    SoonInID = nil,
    SoonFadeID = nil
  }
  collectgarbage("collect")
end
function GameUILargeMap.LargeMapMyinfoNtf(content)
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap.MyPlayerData = content
    GameUILargeMap.MyPlayerData.troopBaseTime = os.time()
    GameUILargeMap:updateExcuAndStatus()
    GameUILargeMap:UpdateMoneyUI()
    GameUILargeMap:UpdateSelfPos()
    if GameUILargeMap:GetFlashObject() then
      GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "UpdateLeaderPosUI", GameUILargeMap.MyPlayerData)
    end
  end
  if GameUILargeMapUI:GetFlashObject() then
    GameUILargeMapUI.baseData = content
  end
  if GameUILargeMapAlliance:GetFlashObject() then
    GameUILargeMapAlliance.baseData = content
    GameUILargeMapAlliance:RefreshUI()
  end
end
function GameUILargeMap.LargeMapRouteNtf(content)
  if not GameUILargeMap:GetFlashObject() or GameUILargeMap.MyPlayerData == nil then
    return
  end
  for k, v in pairs(content.route_list) do
    if v.type > 0 and v.type ~= 5 and v.move_points[1] ~= nil and v.cur_time < v.time then
      GameUILargeMap:AddShip(v.player_id, v.move_points[1], v.route_type, v.trip_type)
      GameUILargeMap:AddMoveEvent(v.player_id, v.time, v.cur_time, v.move_points, v.route_type, v.start_show_time, v.end_show_time)
      if v.player_id == GameUILargeMap.MyPlayerData.player_id or v.player_id == GameUILargeMap.MyPlayerData.troop_id then
        GameUILargeMap.MyFlyPoints = v.move_points
      end
    elseif v.type == 5 then
      GameUILargeMap:RemoveShip(v.player_id)
      GameUILargeMap:RemoveMoveEvent(v.move_points, v.player_id)
      if v.player_id == GameUILargeMap.MyPlayerData.player_id or v.player_id == GameUILargeMap.MyPlayerData.troop_id then
        GameUILargeMap.MyFlyPoints = nil
      end
    end
  end
end
function GameUILargeMap.LargeMapBlockNtf(content)
  DebugOut("GameUILargeMap.LargeMapBlockNtf")
  if not GameUILargeMap:GetFlashObject() or GameUILargeMap.MyPlayerData == nil then
    return
  end
  GameUILargeMap:GenerateBlockData(content, true)
end
function GameUILargeMap.LargeMapEventNtf(content)
  if not GameUILargeMap:GetFlashObject() or GameUILargeMap.MyPlayerData == nil then
    return
  end
  for k, v in ipairs(content.event_list) do
    GameUILargeMap:OnEventFunc(v)
  end
end
function GameUILargeMap.LargeMapLineNtf(content)
  if GameUILargeMap:GetFlashObject() then
    for k, v in ipairs(content.lines) do
      GameUILargeMap:OnLines(v)
    end
  end
end
function GameUILargeMap.LargeMapVisionNtf(content)
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GenerateVisionData(content)
  end
end
function GameUILargeMap.LargeMapVisionChangeNtf(content)
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GenerateVisionChangeData(content)
  end
end
function GameUILargeMap:OnFSCommand(cmd, arg)
  if cmd == "close" then
    if GameStateLargeMap.m_preState ~= GameStateBattlePlay and GameStateLargeMap.m_preState ~= GameStateLargeMapAlliance and GameStateLargeMap.m_preState ~= GameStateLargeMapRank then
      GameStateManager:SetCurrentGameState(GameStateLargeMap.m_preState)
    else
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
    end
  end
  if cmd == "updateUIMC" then
    table.insert(GameUILargeMap.needUpdateBlock, tonumber(arg))
  elseif cmd == "updateUIMCNow" then
    local item = GameUILargeMap.UIList["id_" .. arg]
    if item and (item.side_status > 0 or 0 < item.status or 0 < #item.Players) then
      table.insert(GameUILargeMap.nowUpdateBlock, item)
    end
  elseif cmd == "ReadyDrag" then
    GameUILargeMap.draging = true
    GameUILargeMap.refreshCount = 8
  elseif cmd == "updateBgMC" then
    local item = GameUILargeMap.BGList[tonumber(arg) + 1]
    self:GetFlashObject():InvokeASCallback("_root", "UpdateBgMC", item)
  elseif cmd == "TouchBlock" then
    local param = {}
    param.blockId = tonumber(arg)
    param.enabldShow = GameUILargeMap.MyBlockVivionData["id" .. arg] ~= nil or GameUILargeMap.MyVisionData.VisibleIDS["id" .. arg] ~= nil
    GameUILargeMapUI:Show(GameUILargeMap.MyPlayerData, param, GameUILargeMapUI.TYPE_BLOCK_DETAIL)
  elseif cmd == "ShipArrive" then
    local shipid, blockId, arriveStatus, ro = unpack(LuaUtils:string_split(arg, ","))
    local ship = GameUILargeMap.ShipList[tonumber(shipid)]
    if ship then
      ship.pos = tonumber(blockId)
    end
    if arriveStatus == "end" then
      GameUILargeMap:SavePlayerRotation(shipid, blockId, tonumber(ro))
    end
    if shipid == GameUILargeMap.MyPlayerData.player_id or shipid == GameUILargeMap.MyPlayerData.troop_id then
      GameUILargeMap.MyPlayerData.pos = tonumber(blockId)
      GameUILargeMap:UpdateSelfPos()
      if GameUILargeMap.MyFlyPoints then
        for k, v in ipairs(GameUILargeMap.MyFlyPoints) do
          if v == tonumber(blockId) and k < #GameUILargeMap.MyFlyPoints then
            GameUILargeMap.MyVisionData.SoonFadeID = tonumber(blockId)
            GameUILargeMap.MyVisionData.SoonInID = GameUILargeMap.MyFlyPoints[k + 1]
            break
          else
            GameUILargeMap.MyVisionData.SoonFadeID = nil
            GameUILargeMap.MyVisionData.SoonInID = nil
            GameUILargeMap.MyVisionData.SoonFadeIDS = {}
            GameUILargeMap.MyVisionData.SoonVisibleIDS = {}
          end
        end
      end
      GameUILargeMap:CalculateMyVision()
    end
  elseif cmd == "refrshBlock" then
    if GameUILargeMap.baseData == nil then
      return
    end
    GameUILargeMap.draging = false
    GameUILargeMap.nowUpdateBlock = {}
    local upRow, downRow, leftCol, rightCol = unpack(LuaUtils:string_split(arg, ","))
    upRow = math.max(1, tonumber(upRow) - 5)
    downRow = math.min(GameUILargeMap.baseData.rowCount, tonumber(downRow) + 5)
    leftCol = math.max(1, tonumber(leftCol) - 8)
    rightCol = math.min(GameUILargeMap.baseData.colCount, tonumber(rightCol) + 8)
    GameUILargeMap:ReqUIBlockData(tonumber(upRow), tonumber(downRow), tonumber(leftCol), tonumber(rightCol), false)
    GameUILargeMap:RemoveUnUseUI(upRow, downRow, leftCol, rightCol)
  elseif cmd == "updateBlockPlayer" then
    local item = GameUILargeMap.PlayerData[tonumber(arg)]
    self:GetFlashObject():InvokeASCallback("_root", "UpdatePlayerItem", item)
  elseif cmd == "GetMoreExe" then
  elseif cmd == "LocalSelf" then
    if GameUILargeMap.MyPlayerData then
      self:GetFlashObject():InvokeASCallback("_root", "SetCenterPosition", GameUILargeMap.MyPlayerData.pos)
    end
  elseif cmd == "EnterCountry" then
    GameUILargeMapAlliance:ShowByType(GameUILargeMap.MyPlayerData, 1)
  elseif cmd == "MassBtn" then
    if GameUILargeMap.MyPlayerData.troop_id ~= "" then
      GameUILargeMapUI:Show(GameUILargeMap.MyPlayerData, nil, GameUILargeMapUI.TYPE_MASS_MEMBERS)
    else
      GameUILargeMapUI:Show(GameUILargeMap.MyPlayerData, nil, GameUILargeMapUI.TYPE_MASS_FLEETS)
    end
  elseif cmd == "Repair" then
  elseif cmd == "BuyEnergy" then
    GameUILargeMap:onBuyEnergy()
  elseif cmd == "LocalLeader" then
    GameUILargeMap:onLocalLeader()
  elseif cmd == "EnterRank" then
    GameUILargeMapRank:Show()
  elseif cmd == "Popup_Chat_Windows" then
    GameUILargeMap:HideChatIcon()
    GameStateManager:GetCurrentGameState():AddObject(GameUIChat)
    if GameUILargeMap.firstTimeEnterChat == true then
      GameUIChat:SelectChannel("largemap_world")
    elseif GameUIChat.activeChannel == "battle" or GameUIChat.activeChannel == "alliance" then
      GameUIChat:SelectChannel("largemap_world")
    else
      DebugOut("jsjkasd:", GameUIChat.activeChannel)
      if GameUIChat.activeChannel then
        GameUIChat:SelectChannel(GameUIChat.activeChannel)
      else
        GameUIChat:SelectChannel("largemap_world")
      end
    end
    GameUILargeMap.firstTimeEnterChat = false
  end
end
function GameUILargeMap.LargeMapBaseCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_base_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_base_ack.Code then
    GameUILargeMap.MyPlayerData = content.my_info
    GameUILargeMap.MyPlayerData.troopBaseTime = os.time()
    GameUILargeMap:updateExcuAndStatus()
    GameUILargeMap:GenerateBaseData(content)
    GameUILargeMap:CalculateMyVision()
    GameUILargeMap:InitUI()
    GameUILargeMap:InitBg()
    GameUILargeMap:InitStar()
    GameUILargeMap:UpdateMoneyUI()
    GameUILargeMap:SetPositionForCenter()
    GameUILargeMap:UpdateBg()
    GameUILargeMap:MoveInHead()
    GameUILargeMap:ForceUpdateUIWithoutUpdate()
    GameUILargeMap:UpdateSelfPos()
    return true
  end
  return false
end
function GameUILargeMap:HideChatIcon()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "MoveOutChatIcon")
  end
end
function GameUILargeMap:ForceUpdateUIWithoutUpdate()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "ForceUpdateUIWithoutUpdate")
  end
end
function GameUILargeMap:MoveInHead()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "moveInHead")
  end
end
function GameUILargeMap:RemoveUnUseUI(upRow, downRow, leftCol, rightCol)
  GameUILargeMap.CurShowUPRow = 0
  GameUILargeMap.CurShowDownRow = 0
  GameUILargeMap.CurShowLeftCol = 0
  GameUILargeMap.CurShowRightCol = 0
  if GameUILargeMap.CurShowDownRow == 0 or GameUILargeMap.CurShowRightCol == 0 then
    return
  end
  if upRow < GameUILargeMap.CurShowUPRow then
    for i = downRow + 1, GameUILargeMap.CurShowDownRow do
      for j = GameUILargeMap.CurShowLeftCol, GameUILargeMap.CurShowRightCol do
        local id = (i - 1) * GameUILargeMap.baseData.col_count + j - 1
        GameUILargeMap.UIList["id_" .. id] = nil
      end
    end
  elseif upRow > GameUILargeMap.CurShowUPRow then
    for i = GameUILargeMap.CurShowUPRow, upRow - 1 do
      for j = GameUILargeMap.CurShowLeftCol, GameUILargeMap.CurShowRightCol do
        local id = (i - 1) * GameUILargeMap.baseData.col_count + j - 1
        GameUILargeMap.UIList["id_" .. id] = nil
      end
    end
  end
  if leftCol < GameUILargeMap.CurShowLeftCol then
    for i = GameUILargeMap.CurShowUPRow, GameUILargeMap.CurShowDownRow do
      for j = rightCol + 1, GameUILargeMap.CurShowRightCol do
        local id = (i - 1) * GameUILargeMap.baseData.col_count + j - 1
        GameUILargeMap.UIList["id_" .. id] = nil
      end
    end
  elseif leftCol > GameUILargeMap.CurShowLeftCol then
    for i = GameUILargeMap.CurShowUPRow, GameUILargeMap.CurShowDownRow do
      for j = GameUILargeMap.CurShowLeftCol, leftCol - 1 do
        local id = (i - 1) * GameUILargeMap.baseData.col_count + j - 1
        GameUILargeMap.UIList["id" .. id] = nil
      end
    end
  end
  GameUILargeMap.CurShowUPRow = upRow
  GameUILargeMap.CurShowDownRow = downRow
  GameUILargeMap.CurShowLeftCol = leftCol
  GameUILargeMap.CurShowRightCol = rightCol
  collectgarbage("collect")
end
function GameUILargeMap:GenerateBaseData(content)
  local data = {}
  data.colCount = content.col_count
  data.rowCount = content.row_count
  data.centerId = content.center_id
  GameUILargeMap.baseData = data
  GameUILargeMap.buildList = content.build_list
  GameUILargeMap.BGList = content.bg_list
  for k, v in ipairs(content.nebula_list) do
    local item = {}
    item.frame = v.frame
    item.id = v.id
    item.px = v.pos_x
    item.py = v.pos_y
    GameUILargeMap.StarList[#GameUILargeMap.StarList + 1] = item
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetBgList", GameUILargeMap.BGList)
  end
end
function GameUILargeMap:InitUI()
  if self:GetFlashObject() and GameUILargeMap.baseData then
    self:GetFlashObject():InvokeASCallback("_root", "initUI", GameUILargeMap.baseData, GameUILargeMap.buildList)
    self:GetFlashObject():InvokeASCallback("_root", "UpdateLeaderPosUI", GameUILargeMap.MyPlayerData)
  end
end
function GameUILargeMap:UpdateUI()
  if self:GetFlashObject() and GameUILargeMap.baseData then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateUI")
  end
end
function GameUILargeMap:InitBg()
  if self:GetFlashObject() and GameUILargeMap.BGList then
    self:GetFlashObject():InvokeASCallback("_root", "initBg")
  end
end
function GameUILargeMap:UpdateBg()
  if self:GetFlashObject() and GameUILargeMap.BGList then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateBg")
  end
end
function GameUILargeMap:InitStar()
  if self:GetFlashObject() and GameUILargeMap.StarList then
    self:GetFlashObject():InvokeASCallback("_root", "initStar", GameUILargeMap.StarList)
  end
end
function GameUILargeMap:UpdateStar()
  if self:GetFlashObject() and GameUILargeMap.StarList then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateStar")
  end
end
function GameUILargeMap:SetPositionForCenter()
  if GameUILargeMap.baseData then
    self:GetFlashObject():InvokeASCallback("_root", "SetCenterPosition", GameUILargeMap.baseData.centerId)
  end
end
function GameUILargeMap:AddShip(shipId, BlockId, routeType, tripType)
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "AddShip", shipId, BlockId, routeType, tripType)
  end
end
function GameUILargeMap:RemoveShip(shipId)
  if GameUILargeMap:GetFlashObject() then
    local isSelf = false
    if shipId == GameUILargeMap.MyPlayerData.player_id or shipId == GameUILargeMap.MyPlayerData.troop_id then
      isSelf = true
    end
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "RemoveShip", shipId, isSelf)
  end
end
function GameUILargeMap:AddMoveEvent(shipId, useTime, curTime, points, routeType, startShowTime, endShowTime)
  if GameUILargeMap:GetFlashObject() then
    local isSelf = false
    if shipId == GameUILargeMap.MyPlayerData.player_id or shipId == GameUILargeMap.MyPlayerData.troop_id then
      isSelf = true
    end
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "AddShipPath", shipId, useTime, curTime, points, routeType, startShowTime, endShowTime, isSelf)
  end
end
function GameUILargeMap:RemoveMoveEvent(points, shipId)
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "RemoveShipPath", points, shipId)
  end
end
function GameUILargeMap:UpdateMyLocal()
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "UpdateMyLocal", GameUILargeMap.MyPlayerData)
  end
end
function GameUILargeMap:RefreshUIMC(id)
  GameUILargeMap:OnFSCommand("updateUIMC", id)
end
function GameUILargeMap:RequestEnemyMatrix(blockId)
  local param = {block = blockId}
  NetMessageMgr:SendMsg(NetAPIList.large_map_enemy_req.Code, param, GameUILargeMap.enemyMatrixCallback, true, nil)
end
function GameUILargeMap.enemyMatrixCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_enemy_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_enemy_ack.Code then
    GameStateManager.GameStateFormation.enemyForce = content.force
    GameStateManager.GameStateFormation.isNeedRequestMatrixInfo = false
    GameStateManager.GameStateFormation.enemyMatrixData = content.matrix
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateFormation)
    return true
  end
  return false
end
function GameUILargeMap:onBattle(eventid)
  local function _callback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_battle_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    elseif msgtype == NetAPIList.large_map_battle_ack.Code then
      local battleplay = GameStateManager.GameStateBattlePlay
      GameStateBattlePlay.curBattleType = "largemap"
      if not GameUILargeMap.isPlaying and GameObjectBattleReplay.GroupBattleReportArr[1] and 0 < #GameObjectBattleReplay.GroupBattleReportArr[1].headers then
        battleplay:InitBattle(battleplay.MODE_PLAY, GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].report)
        GameStateBattlePlay:RegisterOverCallback(function()
          GameUIBattleResult:LoadFlashObject()
          GameUIBattleResult:SetFightReport(GameObjectBattleReplay.GroupBattleReportArr, nil, nil, true)
          local data = {}
          local lang = "en"
          if GameSettingData and GameSettingData.Save_Lang then
            lang = GameSettingData.Save_Lang
            if string.find(lang, "ru") == 1 then
              lang = "ru"
            elseif GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese() then
              lang = "zh"
            end
          end
          data.lang = lang
          data.isChinese = GameUtils:IsChinese()
          data.isWin = content.is_win
          local str = GameLoader:GetGameText("LC_MENU_MAP_WINNER")
          data.WinDesc = string.gsub(str, "<number1>", content.win_name)
          GameUIBattleResult:SetLargeMapResult(data)
          local function callback()
            GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
          end
          GameUILargeMap.onAddStateCallBack = callback
          GameStateManager:SetCurrentGameState(GameStateLargeMap)
          GameUILargeMap.isPlaying = nil
        end, nil)
        GameUILargeMap.isPlaying = true
        GameStateManager:SetCurrentGameState(battleplay)
      end
      return true
    end
    return false
  end
  local param = {}
  param.event_id = eventid
  NetMessageMgr:SendMsg(NetAPIList.large_map_battle_req.Code, param, _callback, true, nil)
end
function GameUILargeMap:ReqMainMapData()
  NetMessageMgr:SendMsg(NetAPIList.large_map_base_req.Code, nil, GameUILargeMap.LargeMapBaseCallBack, true, nil)
end
function GameUILargeMap.LargeMapBlockCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_block_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_block_ack.Code then
    GameUILargeMap:GenerateBlockData(content, false)
    return true
  end
  return false
end
function GameUILargeMap:GenerateBlockData(content, frameUpdate)
  if GameUILargeMap.MyPlayerData == nil then
    return
  end
  local data = {}
  for k, v in pairs(content.block_list) do
    local OldItem = GameUILargeMap.UIList["id_" .. v.id]
    local item = {}
    item.sa = bit:_and(2, v.side_status) > 0
    item.sa1 = 0 < bit:_and(16, v.side_status)
    item.sb = 0 < bit:_and(8, v.side_status)
    item.sb1 = 0 < bit:_and(1, v.side_status)
    item.sc = 0 < bit:_and(4, v.side_status)
    item.sc1 = 0 < bit:_and(32, v.side_status)
    item.side_status = v.side_status
    item.id = v.id
    item.show = v.show
    item.type = v.type
    item.buildType = v.build_type
    item.owner = v.show and v.owner or 0
    item.color = v.color
    item.isMy = v.owner == GameUILargeMap.MyPlayerData.owner_code and v.owner ~= 0
    item.status = v.status
    item.fleetType = v.fleet_type
    item.occupy_color = v.occupy_color
    item.occupy_owner = v.occupy_owner
    item.isMyOccupy = v.occupy_owner == GameUILargeMap.MyPlayerData.owner_code and v.occupy_owner ~= 0
    item.rotation = v.rotation
    item.timePercent = 0
    item.totalTime = v.total_time
    item.dstTime = v.left_time + os.time()
    item.showRed = 0 < bit:_and(2, v.status)
    item.showMass = false
    if item.showRed and 0 < v.total_time then
      item.timePercent = math.floor(v.left_time / v.total_time * 100)
    elseif item.showRed and v.total_time == 0 then
      item.timePercent = 0
    end
    if 0 < item.timePercent then
      GameUILargeMap.occuptBlock[item.id] = item
    elseif GameUILargeMap.occuptBlock[item.id] then
      GameUILargeMap.occuptBlock[item.id] = nil
    end
    GameUILargeMap.UIList["id_" .. item.id] = item
    data["id_" .. item.id] = item
    if 0 < item.color then
      if GameUILargeMap.UIGroupList[v.owner] and LuaUtils:table_size(GameUILargeMap.UIGroupList[v.owner]) == 0 then
        GameUILargeMap.UIGroupList[v.owner][item.id] = item
      else
        GameUILargeMap.UIGroupList[v.owner] = {}
        GameUILargeMap.UIGroupList[v.owner][item.id] = item
      end
    end
    if OldItem and (v.status ~= OldItem.status or v.owner ~= OldItem.owner or v.side_status ~= OldItem.side_status or item.totalTime ~= OldItem.totalTime or item.fleetType ~= OldItem.fleetType or item.show ~= OldItem.show) or not OldItem and (v.side_status > 0 or 0 < v.status or 0 < item.fleetType or v.show) or frameUpdate then
      table.insert(GameUILargeMap.needUpdateBlock, tonumber(v.id))
    end
  end
  GameUILargeMap:SetUIListData(GameUILargeMap.UIList)
end
function GameUILargeMap:SetUIListData(data)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetUIListData", data)
  end
end
function GameUILargeMap:OnPendingBlock()
  for k, v in pairs(GameUILargeMap.pendingBlock) do
    local item = GameUILargeMap.UIList["id_" .. v]
    if item then
      item.color = 0
    end
    GameUILargeMap:RefreshUIMC(v)
  end
end
function GameUILargeMap:UpdateUIByFrame()
  local nowItem = table.remove(GameUILargeMap.nowUpdateBlock, 1)
  if nowItem then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateUIMC", nowItem)
  end
  if GameUILargeMap.draging then
    return
  end
  if #GameUILargeMap.needUpdateBlock > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "SetUpdateList", GameUILargeMap.needUpdateBlock)
    GameUILargeMap.needUpdateBlock = {}
  end
end
function GameUILargeMap:ReqUIBlockData(upRow, downRow, leftCol, rightCol, forceWait)
  local param = {}
  param.col_left = leftCol
  param.col_right = rightCol
  param.row_up = upRow
  param.row_down = downRow
  NetMessageMgr:SendMsg(NetAPIList.large_map_block_req.Code, param, GameUILargeMap.LargeMapBlockCallBack, forceWait, nil)
end
function GameUILargeMap:GetSameWonerColor(owner)
  for k, v in pairs(GameUILargeMap.UIGroupList[owner] or {}) do
    if v.color > 0 then
      return v.color
    end
  end
  return 0
end
function GameUILargeMap:SelectColor(color)
  local value = 0
  if 0 < bit:_and(color, 1) then
    value = 1
  elseif 0 < bit:_and(color, 2) then
    value = 2
  elseif 0 < bit:_and(color, 4) then
    value = 4
  elseif 0 < bit:_and(color, 8) then
    value = 8
  elseif 0 < bit:_and(color, 16) then
    value = 16
  end
  if value == 0 then
    DebugOut("dont find color:", color)
  end
  return value
end
function GameUILargeMap:CheckBlockId(srcid, id)
  if id < 0 then
    return false
  end
  local item = GameUILargeMap.UIList["id_" .. id]
  if item and item.show == false then
    return false
  end
  local ouoff = (math.floor(srcid / GameUILargeMap.baseData.colCount) + 1) % 2 == 0
  if srcid % GameUILargeMap.baseData.colCount == 0 then
    if ouoff then
      if math.abs(id - srcid) == GameUILargeMap.baseData.colCount or id - srcid == 1 then
        return true
      else
        return false
      end
    elseif srcid - id == 1 then
      return false
    else
      return true
    end
  elseif (srcid + 1) % GameUILargeMap.baseData.colCount == 0 then
    if ouoff then
      if id - srcid == 1 then
        return false
      else
        return true
      end
    elseif srcid - id == 1 or math.abs(id - srcid) == GameUILargeMap.baseData.colCount then
      return true
    else
      return false
    end
  end
  return true
end
function GameUILargeMap:GetNearBlockID(Item)
  local srcId = Item.id
  local oddoff = (math.floor(srcId / GameUILargeMap.baseData.colCount) + 1) % 2 ~= 0 and 1 or 0
  local ouoff = (math.floor(srcId / GameUILargeMap.baseData.colCount) + 1) % 2 == 0 and 1 or 0
  local list = {}
  local id1 = srcId + GameUILargeMap.baseData.colCount - ouoff
  local id2 = srcId + GameUILargeMap.baseData.colCount + 1 - ouoff
  local id3 = srcId - 1
  local id4 = srcId - GameUILargeMap.baseData.colCount - 1 + oddoff
  local id5 = srcId - GameUILargeMap.baseData.colCount + oddoff
  local id6 = srcId + 1
  if GameUILargeMap:CheckBlockId(srcId, id1) then
    if id1 ~= (Item.father and Item.father.id or nil) then
      local item = {}
      item.id = id1
      item.father = Item
      table.insert(list, item)
    end
  end
  if GameUILargeMap:CheckBlockId(srcId, id2) then
    if id2 ~= (Item.father and Item.father.id or nil) then
      local item = {}
      item.id = id2
      item.father = Item
      table.insert(list, item)
    end
  end
  if GameUILargeMap:CheckBlockId(srcId, id3) then
    if id3 ~= (Item.father and Item.father.id or nil) then
      local item = {}
      item.id = id3
      item.father = Item
      table.insert(list, item)
    end
  end
  if GameUILargeMap:CheckBlockId(srcId, id4) then
    if id4 ~= (Item.father and Item.father.id or nil) then
      local item = {}
      item.id = id4
      item.father = Item
      table.insert(list, item)
    end
  end
  if GameUILargeMap:CheckBlockId(srcId, id5) then
    if id5 ~= (Item.father and Item.father.id or nil) then
      local item = {}
      item.id = id5
      item.father = Item
      table.insert(list, item)
    end
  end
  if GameUILargeMap:CheckBlockId(srcId, id6) then
    if id6 ~= (Item.father and Item.father.id or nil) then
      local item = {}
      item.id = id6
      item.father = Item
      table.insert(list, item)
    end
  end
  return list
end
function GameUILargeMap:GenerateVisionChangeData(content)
  for k, v in pairs(content.add_ids) do
    GameUILargeMap.MyBlockVivionData["id" .. v] = v
    table.insert(GameUILargeMap.needUpdateBlock, v)
  end
  for k, v in pairs(content.sub_ids) do
    GameUILargeMap.MyBlockVivionData["id" .. v] = nil
    table.insert(GameUILargeMap.needUpdateBlock, v)
  end
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetVisionData", GameUILargeMap.MyBlockVivionData)
  end
end
function GameUILargeMap:GenerateVisionData(content)
  local data = {}
  for k, v in pairs(content.visible_ids) do
    data["id" .. v] = v
    if GameUILargeMap.MyBlockVivionData and not GameUILargeMap.MyBlockVivionData["id" .. v] then
      table.insert(GameUILargeMap.needUpdateBlock, v)
    end
  end
  GameUILargeMap.MyBlockVivionData = data
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetVisionData", data)
  end
end
function GameUILargeMap:CalculateMyVision()
  if GameUILargeMap.MyPlayerData then
    GameUILargeMap:CalculateSoonVision()
    local fadeList = {}
    local showList = {}
    local lastVisibleIDs = LuaUtils:table_copy(GameUILargeMap.MyVisionData.VisibleIDS)
    GameUILargeMap.MyVisionData.VisibleIDS = GameUILargeMap:CalculateVision(GameUILargeMap.MyPlayerData.pos)
    for k, v in pairs(lastVisibleIDs or {}) do
      if not GameUILargeMap.MyVisionData.VisibleIDS[k] and not GameUILargeMap.MyBlockVivionData[k] then
        table.insert(GameUILargeMap.needUpdateBlock, v)
        fadeList[k] = v
      end
    end
    for k, v in pairs(GameUILargeMap.MyVisionData.VisibleIDS or {}) do
      if not lastVisibleIDs[k] and not GameUILargeMap.MyBlockVivionData[k] then
        table.insert(GameUILargeMap.needUpdateBlock, v)
        showList[k] = v
      end
    end
    if GameUILargeMap:GetFlashObject() then
      GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetMyVisionData", GameUILargeMap.MyVisionData, fadeList, showList)
    end
  end
end
function GameUILargeMap:CalculateVision(srcID)
  local result = {}
  local ouoff = (math.floor(srcID / GameUILargeMap.baseData.colCount) + 1) % 2 == 0
  if ouoff then
    for k, v in ipairs(GameUILargeMap.EvenLineArray) do
      for j, v2 in ipairs(v) do
        local id = srcID + v2
        result["id" .. id] = id
      end
    end
  else
    for k, v in ipairs(GameUILargeMap.OddLineArray) do
      for j, v2 in ipairs(v) do
        local id = srcID + v2
        result["id" .. id] = id
      end
    end
  end
  return result
end
function GameUILargeMap:CalculateSoonVision()
  if GameUILargeMap.MyVisionData.SoonInID then
    local list1 = GameUILargeMap:CalculateVision(GameUILargeMap.MyVisionData.SoonInID)
    local list2 = GameUILargeMap:CalculateVision(GameUILargeMap.MyVisionData.SoonFadeID)
    for k, v in pairs(list1) do
      if list2[k] then
        list1[k] = nil
        list2[k] = nil
      end
    end
    GameUILargeMap.MyVisionData.SoonVisibleIDS = list1
    GameUILargeMap.MyVisionData.SoonFadeIDS = list2
  end
end
function GameUILargeMap:GetSearchStartItemPath(Item, pathList)
  if Item.father then
    table.insert(pathList, 1, Item.father.id)
    GameUILargeMap:GetSearchStartItemPath(Item.father, pathList)
  else
    return
  end
end
function GameUILargeMap:GetSearchEndItemPath(Item, pathList)
  if Item.father then
    DebugTable(pathList)
    table.insert(pathList, Item.father.id)
    GameUILargeMap:GetSearchEndItemPath(Item.father, pathList)
  else
    return
  end
end
function GameUILargeMap:SearchPathPoints(srcId, dstId)
  local pointList = {}
  local srcList = {}
  local dstList = {}
  local itemsrc = {}
  itemsrc.id = srcId
  itemsrc.father = nil
  local itemdst = {}
  itemdst.id = dstId
  itemdst.father = nil
  srcList[srcId] = itemsrc
  dstList[dstId] = itemdst
  local findItem = false
  local findID
  while true do
    local newSrcList = {}
    local newDstList = {}
    for k, v in pairs(srcList) do
      local item = v
      local nearlist = GameUILargeMap:GetNearBlockID(item)
      for k1, v1 in pairs(nearlist) do
        if srcList[v1.id] == nil and newSrcList[v1.id] == nil then
          newSrcList[v1.id] = v1
          if dstList[v1.id] then
            findItem = true
            findID = v1.id
            break
          end
        end
      end
      if findItem then
        break
      end
    end
    srcList = newSrcList
    if findItem then
      break
    end
    for k, v in pairs(dstList) do
      local item = v
      local nearlist = GameUILargeMap:GetNearBlockID(item)
      for k1, v1 in pairs(nearlist) do
        if dstList[v1.id] == nil and newDstList[v1.id] == nil then
          newDstList[v1.id] = v1
          if srcList[v1.id] then
            findItem = true
            findID = v1.id
            break
          end
        end
      end
      if findItem then
        break
      end
    end
    dstList = newDstList
    if findItem then
      break
    end
  end
  if findID then
    DebugOut(findID)
    DebugTable(srcList[findID])
    DebugTable(dstList[findID])
    GameUILargeMap:GetSearchStartItemPath(srcList[findID], pointList)
    table.insert(pointList, findID)
    GameUILargeMap:GetSearchEndItemPath(dstList[findID], pointList)
  end
  DebugTable(pointList)
  return pointList
end
function GameUILargeMap:ReqRouteData(ty, targetId)
  local param = {}
  param.target_id = targetId
  param.type = ty
  NetMessageMgr:SendMsg(NetAPIList.large_map_route_req.Code, param, GameUILargeMap.LargeMapRouteCallBack, true, nil)
end
function GameUILargeMap.LargeMapRouteCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_route_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_route_ack.Code then
    local item = GameUILargeMap.UIList["id_" .. content.move_points[#content.move_points]]
    GameUILargeMap:GenerateRouteData(content)
    return true
  end
  return false
end
function GameUILargeMap:GenerateRouteData(content)
  if content.type > 0 and content.type ~= 5 and 0 < #content.move_points then
    GameUILargeMap:AddShip(content.player_id, content.move_points[1], content.route_type, content.trip_type)
    GameUILargeMap:AddMoveEvent(content.player_id, content.time, content.cur_time, content.move_points, content.route_type, content.start_show_time, content.end_show_time)
  elseif content.type == 5 then
    GameUILargeMap:RemoveShip(content.player_id)
    GameUILargeMap:RemoveMoveEvent(content.move_points, content.player_id)
  end
end
function GameUILargeMap:OnEventFunc(event)
  if event.event_type == 1 then
  elseif event.event_type == 2 then
    GameUILargeMap:onBattle(event.event_id)
  elseif event.event_type == 3 then
  elseif event.event_type == 5 then
    GameUILargeMap:PlayOccupyFinishAnim(event.target_id)
  elseif event.event_type == 13 then
    GameUILargeMap:PlayOccupyFinishAnim(event.target_id)
  end
end
function GameUILargeMap:OnLines(line)
  if GameUILargeMap:GetFlashObject() then
    if line.operate_type ~= 2 then
      self:AddMassLine(line)
    else
      self:RemoveMassLine(line)
      if GameUILargeMapUI:GetFlashObject() and GameUILargeMapUI.curShowType == GameUILargeMapUI.TYPE_MASS_MEMBERS and line.line_type == 2 then
        GameUILargeMapUI:OnFSCommand("close", "")
      end
    end
  end
end
function GameUILargeMap:AddMassLine(lineInfo)
  if nil == GameUILargeMap.massWarning[lineInfo.end_id] then
    GameUILargeMap.massWarning[lineInfo.end_id] = {}
  end
  local found = false
  for _, v in pairs(GameUILargeMap.massWarning[lineInfo.end_id]) do
    if v == lineInfo.id then
      found = true
      break
    end
  end
  if not found then
    table.insert(GameUILargeMap.massWarning[lineInfo.end_id], lineInfo.id)
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "AddLine", lineInfo.id, lineInfo.start_id, lineInfo.end_id, lineInfo.line_type)
  else
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "UpdateLine", lineInfo.id, lineInfo.start_id, lineInfo.end_id, lineInfo.line_type)
  end
  GameUILargeMap.massLines[lineInfo.id] = lineInfo
end
function GameUILargeMap:RemoveMassLine(lineInfo)
  GameUILargeMap.massLines[lineInfo.id] = nil
  if nil == GameUILargeMap.massWarning[lineInfo.end_id] then
    return
  end
  for i, v in pairs(GameUILargeMap.massWarning[lineInfo.end_id]) do
    if v == lineInfo.id then
      table.remove(GameUILargeMap.massWarning[lineInfo.end_id], i)
      GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "RemoveLine", lineInfo.id, lineInfo.start_id, lineInfo.end_id, lineInfo.line_type)
      break
    end
  end
end
function GameUILargeMap:ClearMassLines()
  if GameUILargeMap:GetFlashObject() then
    for k, v in pairs(GameUILargeMap.massLines) do
      GameUILargeMap:RemoveMassLine(v)
    end
    GameUILargeMap.massLines = {}
  end
end
function GameUILargeMap:ClearBlockVision()
  if GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetVisionData", {})
  end
  for k, v in ipairs(GameUILargeMap.MyBlockVivionData or {}) do
    table.insert(GameUILargeMap.needUpdateBlock, v)
  end
  GameUILargeMap.MyBlockVivionData = {}
end
function GameUILargeMap:PlayOccupyFinishAnim(id)
  local item = GameUILargeMap.occuptBlock[id]
  if item and GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "PlayOccupyFinishAnim", item)
    GameUILargeMap.occuptBlock[id] = nil
  end
end
function GameUILargeMap:RemoveShipFormBlock(id, playerID)
  local item = GameUILargeMap.UIList["id_" .. id]
  if item then
    for k, v in pairs(item.Players) do
      if v.playerId == playerID then
        table.remove(item.Players, k)
        break
      end
    end
    GameUILargeMap:RefreshUIMC(id)
  end
end
function GameUILargeMap:UpdateBlockOccupyTime(id)
  if id == nil then
    for k, v in pairs(GameUILargeMap.occuptBlock) do
      GameUILargeMap:UpdateBlockOccupyTime(k)
    end
  else
    local item = GameUILargeMap.occuptBlock[id]
    if item ~= nil then
      if item.showRed then
        item.timePercent = math.floor((item.dstTime - os.time()) / item.totalTime * 100)
      end
      if item.timePercent < 0 then
        item.timePercent = 0
      end
      GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "UpdateBlockPercent", item)
    end
  end
end
function GameUILargeMap:onLocalLeader()
  if GameUILargeMap.MyPlayerData.troop_id ~= "" then
    local param = {}
    param.troop_id = GameUILargeMap.MyPlayerData.troop_id
    NetMessageMgr:SendMsg(NetAPIList.large_map_local_leader_req.Code, param, GameUILargeMap.LargeMapLocalLeaderCallBack, true, nil)
  end
end
function GameUILargeMap.LargeMapLocalLeaderCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.large_map_local_leader_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.large_map_local_leader_ack.Code then
    if GameUILargeMap:GetFlashObject() then
      GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetCenterPosition", content.leader_pos)
    end
    return true
  end
  return false
end
function GameUILargeMap:Update(dt)
  local flashObj = GameUILargeMap:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "OnUpdate", dt)
    GameUILargeMap:UpdateUIByFrame()
    GameUILargeMap:UpdateBlockOccupyTime()
    GameUILargeMap:UpdateTroopTime()
  end
end
function GameUILargeMap:UpdateMoneyUI()
  if GameUILargeMap.MyPlayerData and GameUILargeMap:GetFlashObject() then
    local exploit_money = GameUtils:formatNumber(GameUILargeMap.MyPlayerData.exploit_money)
    local common_money = GameUtils:formatNumber(GameUILargeMap.MyPlayerData.common_money)
    local owner_block = GameUILargeMap.MyPlayerData.alliance_info.block_count
    local showBlock = GameUILargeMap.MyPlayerData.alliance_info.id ~= "" and GameUILargeMap.MyPlayerData.alliance_info.id ~= "0"
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetMoneyUI", exploit_money, common_money, owner_block, showBlock)
  end
end
function GameUILargeMap:UpdateTroopTime()
  if GameUILargeMap.MyPlayerData and GameUILargeMap:GetFlashObject() and GameUILargeMap.MyPlayerData.troop_id ~= "" and GameUILargeMap.MyPlayerData.troop_id ~= "0" then
    local leftTime = GameUILargeMap.MyPlayerData.troop_time - (os.time() - GameUILargeMap.MyPlayerData.troopBaseTime)
    if leftTime < 0 then
      leftTime = -1
    end
    local str = GameUtils:formatTimeString(leftTime)
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetMassTime", str)
  end
end
function GameUILargeMap:UpdateSelfPos()
  if GameUILargeMap.MyPlayerData and GameUILargeMap:GetFlashObject() then
    GameUILargeMap:GetFlashObject():InvokeASCallback("_root", "SetSelfPos", GameUILargeMap.MyPlayerData.pos)
  end
end
function GameUILargeMap:SavePlayerRotation(playerID, blockId, rotation)
  local param = {}
  param.player_id = playerID
  param.arrive_id = blockId
  param.rotation = rotation
  NetMessageMgr:SendMsg(NetAPIList.large_map_ship_arrive_req.Code, param, nil, false, nil)
end
function GameUILargeMap.serverCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code then
    return true
  end
  if msgtype == NetAPIList.supply_info_ack.Code then
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUILargeMap.OnAndroidBack()
    GameUILargeMap:OnFSCommand("close")
  end
end
