local deviceName = ext.GetDeviceFullName()
local deviceRealName = ""
isLowMemDevice = false
g_deviceName = deviceName
g_isMemoryOver2GDevice = false
if deviceName == "iPhone3GS" or deviceName == "iPod4" or deviceName == "iPad1" or deviceName == "iPad2" or deviceName == "iPad3" or deviceName == "iPad4" or deviceName == "iPhone4S" or deviceName == "iPhone4" or deviceName == "iPod5" or deviceName == "iPadMini" or deviceName == "iPhone5S" or deviceName == "iPhone5" or deviceName == "iPhone6" or deviceName == "android" and ext.GetAndroidDeviceAvailMem() < 209715200 and ext.GetAndroidDeviceTotalMem() < 536870912 then
  isLowMemDevice = true
end
print("currentDeviceName ", deviceName, "isLowMemDevice ", isLowMemDevice)
local deviceVersion = ext.GetDeviceVersion()
if string.find(deviceVersion, "iPhone8") or string.find(deviceVersion, "iPhone9") or string.find(deviceVersion, "iPad6") then
  g_isMemoryOver2GDevice = true
end
if AutoUpdate.isAndroidDevice then
  print("Device Real Name: ", deviceRealName)
  ext.__debugLog("android AvailMem():" .. ext.GetAndroidDeviceAvailMem() .. "   " .. "TotalMem()" .. ":" .. ext.GetAndroidDeviceTotalMem())
  if ext.GetAndroidDeviceAvailMem() > 419430400 then
    g_isMemoryOver2GDevice = true
  end
end
print("Device isMemoryOver2GDevice ", g_isMemoryOver2GDevice)
GL2_IS_ASK_FOR_ALL_STORE_LIST = true
LIMITE_BUFF_COUNT = false
SKIP_BATTLE_PLAY_PRELOAD = false
SKIP_BATTLE_BG_PRELOAD = false
USE_SIMPLE_BATTLE_BG = false
FREE_LAZY_LOAD_IMAGE_WHEN_OBJECT_ERASE_FROM_STATE = true
USE_DEFAULT_EFFECT = false
ENABLE_BATTLE_LOADING_STEP = true
ENABLE_BATTLE_MULTIPLE_UPDATE = true
if isLowMemDevice then
  SKIP_BATTLE_BG_PRELOAD = true
  SKIP_BATTLE_PLAY_PRELOAD = true
  USE_SIMPLE_BATTLE_BG = true
  FREE_LAZY_LOAD_IMAGE_WHEN_OBJECT_ERASE_FROM_STATE = true
end
if deviceName == "android" and ext.GetAndroidDeviceTotalMem() < 576716800 then
  SKIP_BATTLE_BG_PRELOAD = true
  SKIP_BATTLE_PLAY_PRELOAD = true
  USE_SIMPLE_BATTLE_BG = true
end
