local material = GameData.medal_enhance.material
table.insert(material, {
  type = 901,
  exp = 15,
  price = 3,
  quality = 1
})
table.insert(material, {
  type = 902,
  exp = 150,
  price = 30,
  quality = 2
})
table.insert(material, {
  type = 903,
  exp = 750,
  price = 150,
  quality = 3
})
table.insert(material, {
  type = 9001,
  exp = 0,
  price = 1,
  quality = 2
})
table.insert(material, {
  type = 9002,
  exp = 0,
  price = 6,
  quality = 2
})
