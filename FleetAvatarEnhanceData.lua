FleetAvatarEnhanceData = luaClass(nil)
function FleetAvatarEnhanceData:ctor(fleetID)
  self.mFleetCard = {}
  self.mRecoverCard = {}
  self.mFleetAttri = {}
  self.mFleetAvatarID = fleetID
  self.mCurLevel = -1
  self.mNextLevel = -1
  self.mGrowUpLevelRequire = -1
  self.mGrowUpProbability = -1
  self.mGrowUpRequireList = nil
  self.mRecoverList = nil
end
function FleetAvatarEnhanceData:GenerateAvatarCardByConfig(configData)
  DebugOut("GenerateAvatarAttriDataByConfig = ")
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(self.mFleetAvatarID)
  local commander_name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(self.mFleetAvatarID))
  local vessels_name = GameLoader:GetGameText("LC_FLEET_" .. GameDataAccessHelper:GetCommanderVesselsType(self.mFleetAvatarID))
  local avatar_name = GameDataAccessHelper:GetFleetAvatar(self.mFleetAvatarID, self.mCurLevel)
  local vessels_image_name = GameDataAccessHelper:GetCommanderVesselsImage(self.mFleetAvatarID, self.mCurLevel)
  local commander_ability = GameDataAccessHelper:GetCommanderAbility(self.mFleetAvatarID)
  local data_table = {}
  data_table[#data_table + 1] = commander_name
  data_table[#data_table + 1] = GameUIRecruitMain.SelectCommanderColor(self.mFleetAvatarID)
  data_table[#data_table + 1] = avatar_name
  data_table[#data_table + 1] = vessels_image_name
  data_table[#data_table + 1] = "TYPE_" .. tostring(basic_info.vessels)
  data_table[#data_table + 1] = vessels_name
  data_table[#data_table + 1] = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID)
  data_table[#data_table + 1] = commander_ability.Damage_Assess
  data_table[#data_table + 1] = commander_ability.Damage_Rate
  data_table[#data_table + 1] = commander_ability.Defence_Assess
  data_table[#data_table + 1] = commander_ability.Defence_Rate
  data_table[#data_table + 1] = commander_ability.Assist_Assess
  data_table[#data_table + 1] = commander_ability.Assist_Rate
  data_table[#data_table + 1] = GameUtils:GetFleetRankFrame(commander_ability.Rank, FleetAvatarEnhanceData.DownloadRankImageCallback)
  local data_string = table.concat(data_table, "\001")
end
function FleetAvatarEnhanceData:GenerateAvatarCardByServerData(netContent)
  DebugOut("GenerateAvatarCardByServerData")
  DebugTable(netContent)
  self.mGrowUpLevelRequire = netContent.level_limited
  self.mGrowUpProbability = netContent.probability / 100
  self.mGrowUpProbabilityStep = netContent.probability_step / 100
  self.mFleetCard.mCurFleetCard = netContent.fleets[1]
  self.mFleetCard.mNextFleetCard = netContent.fleets[2]
  if self.mFleetCard.mNextFleetCard == nil then
    self.mFleetCard.mNextFleetCard = self.mFleetCard.mCurFleetCard
  end
  self.mCurLevel = self.mFleetCard.mCurFleetCard.level
  self.mNextLevel = self.mFleetCard.mNextFleetCard.level
end
function FleetAvatarEnhanceData:GenerateAvatarAttriDataByConfig(configData)
end
function FleetAvatarEnhanceData:GenerateAvatarAttriDataByServerData(netContent)
  self.mFleetAttri.mCurLevelAttriValue = {}
  self.mFleetAttri.mNextLevelAttriValue = {}
  self.mFleetAttri.mAttriIDs = {}
  self.mFleetAttri.mAttriNames = {}
  self.mFleetAttri.mCurLevelSkillID = -1
  self.mFleetAttri.mNextLevelSkillID = -1
  for i, v in ipairs(netContent.changed_attrs) do
    if v.id == 30 then
      self.mFleetAttri.mSkillAttriName = "jineng"
      self.mFleetAttri.mCurLevelSkillID = v.value
      self.mFleetAttri.mNextLevelSkillID = v.next_level_value
    else
      local textIndex = 0
      if v.id >= 200 then
        textIndex = v.id - 100
      elseif v.id >= 100 then
        textIndex = v.id - 100
      end
      local attriName = GameLoader:GetGameText("LC_MENU_Equip_param_" .. textIndex)
      if textIndex == 9 then
        v.value = v.value / 10
        v.next_level_value = v.next_level_value / 10
      elseif textIndex == 10 then
        v.value = (v.value + 1500) / 10
        v.next_level_value = (v.next_level_value + 1500) / 10
      elseif textIndex == 11 then
        v.value = v.value / 10
        v.next_level_value = v.next_level_value / 10
      elseif textIndex == 12 then
        v.value = v.value / 10
        v.next_level_value = v.next_level_value / 10
      elseif textIndex == 13 then
        v.value = v.value / 10
        v.next_level_value = v.next_level_value / 10
      elseif textIndex == 14 then
        v.value = v.value / 10
        v.next_level_value = v.next_level_value / 10
      elseif textIndex == 17 then
        v.value = v.value / 10
        v.next_level_value = v.next_level_value / 10
      end
      DebugOut("attriName = ", attriName)
      table.insert(self.mFleetAttri.mAttriIDs, textIndex)
      table.insert(self.mFleetAttri.mAttriNames, attriName)
      table.insert(self.mFleetAttri.mCurLevelAttriValue, v.value)
      table.insert(self.mFleetAttri.mNextLevelAttriValue, v.next_level_value)
    end
  end
  if self.mFleetAttri.mCurLevelSkillID > 0 then
    table.insert(self.mFleetAttri.mAttriIDs, 30)
    table.insert(self.mFleetAttri.mAttriNames, self.mFleetAttri.mSkillAttriName)
    table.insert(self.mFleetAttri.mCurLevelAttriValue, self.mFleetAttri.mCurLevelSkillID)
    table.insert(self.mFleetAttri.mNextLevelAttriValue, self.mFleetAttri.mNextLevelSkillID)
  end
end
function FleetAvatarEnhanceData:SetGrowUpRequireList(requireList)
  self.mGrowUpRequireList = requireList
end
function FleetAvatarEnhanceData:SetRecoverList(recoverList)
  self.mRecoverList = recoverList
end
function FleetAvatarEnhanceData:SetRecoverCard(netContent)
  self.mRecoverCard.mCurFleetCard = netContent.fleets[1]
  self.mRecoverCard.mNextFleetCard = netContent.fleets[2]
end
function FleetAvatarEnhanceData:GetGrowUpRequireList()
  return self.mGrowUpRequireList
end
function FleetAvatarEnhanceData:GetRecoverList()
  return self.mRecoverList
end
function FleetAvatarEnhanceData:GetRecoverCard()
  return self.mRecoverCard
end
function FleetAvatarEnhanceData:GetFleetSpellDiffData()
  return self.mFleetAttri.mCurLevelSkillID, self.mFleetAttri.mNextLevelSkillID
end
function FleetAvatarEnhanceData:GetFleetAttriDiffData()
  return self.mFleetAttri
end
function FleetAvatarEnhanceData:GetFleetAttriDataStr()
  if self.mFleetAttri.mAttriNamesStr == nil then
    self.mFleetAttri.mAttriNamesStr = ""
    for i, v in ipairs(self.mFleetAttri.mAttriNames) do
      self.mFleetAttri.mAttriNamesStr = self.mFleetAttri.mAttriNamesStr .. v .. "\001"
    end
  end
  if self.mFleetAttri.mCurLevelAttriValueStr == nil then
    self.mFleetAttri.mCurLevelAttriValueStr = ""
    for i, v in ipairs(self.mFleetAttri.mCurLevelAttriValue) do
      self.mFleetAttri.mCurLevelAttriValueStr = self.mFleetAttri.mCurLevelAttriValueStr .. v .. "\001"
    end
  end
  if self.mFleetAttri.mNextLevelAttriValueStr == nil then
    self.mFleetAttri.mNextLevelAttriValueStr = ""
    for i, v in ipairs(self.mFleetAttri.mNextLevelAttriValue) do
      self.mFleetAttri.mNextLevelAttriValueStr = self.mFleetAttri.mNextLevelAttriValueStr .. v .. "\001"
    end
  end
  return self.mFleetAttri.mAttriNamesStr, self.mFleetAttri.mCurLevelAttriValueStr, self.mFleetAttri.mNextLevelAttriValueStr
end
function FleetAvatarEnhanceData:GetFleetCardData()
  return self.mFleetCard
end
function FleetAvatarEnhanceData:GetGrowupLevelRequire()
  return self.mGrowUpLevelRequire
end
function FleetAvatarEnhanceData:GetGrowupProbability()
  return self.mGrowUpProbability, self.mGrowUpProbabilityStep
end
function FleetAvatarEnhanceData:SetGrowupProbability(probability)
  self.mGrowUpProbability = probability
end
function FleetAvatarEnhanceData:IsMaxLevel()
  return self.mCurLevel == self.mNextLevel
end
function FleetAvatarEnhanceData:IsMinLevel()
  return self.mCurLevel <= 0
end
function FleetAvatarEnhanceData:IsPercentAttri(textIndex)
  if textIndex == 9 or textIndex == 10 or textIndex == 11 or textIndex == 12 or textIndex == 13 or textIndex == 14 or textIndex == 17 then
    return true
  end
  return false
end
