local GameCommonGoodsList = LuaObjectManager:GetLuaObject("GameCommonGoodsList")
local GameCommonBuyBox = LuaObjectManager:GetLuaObject("GameCommonBuyBox")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUICommonEvent = LuaObjectManager:GetLuaObject("GameUICommonEvent")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateStore = GameStateManager.GameStateStore
GameCommonBuyBox.iconDisplayed = false
GameCommonBuyBox.setEmptyAlready = false
GameCommonBuyBox.isRquestPriceForConfirming = false
GameCommonBuyBox.isRequestPriceForUpdatePriceUI = false
GameCommonBuyBox.quickBuyCountRecord = 0
GameCommonBuyBox.currentBuyCount = 0
function GameCommonBuyBox:OnInitGame()
end
function GameCommonBuyBox:Init()
  if GameCommonGoodsList.storeType == GameCommonGoodsList.TYPE.wdc then
    self:DisplayDetailWdc()
  else
    self:DisplayDetail()
  end
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshResource)
end
function GameCommonBuyBox:RefreshResource()
  if GameCommonGoodsList.storeType == GameCommonGoodsList.TYPE.wdc then
    GameCommonBuyBox:DisplayDetailWdc()
  else
    GameCommonBuyBox:DisplayDetail()
  end
end
function GameCommonBuyBox:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameCommonBuyBox.iconDisplayed = false
  GameCommonBuyBox.setEmptyAlready = false
  GameCommonBuyBox.isSelectHero = GameCommonGoodsList.isSelectHero
  local flash_obj = self:GetFlashObject()
  if GameCommonBuyBox.isSelectHero then
    flash_obj:InvokeASCallback("_root", "hideCommonBox")
  else
    flash_obj:InvokeASCallback("_root", "hideHeroInfoWindow")
  end
  self:Init()
  if not GameCommonBuyBox.isSelectHero and self:IsCurStoreItemQuickBuyable() then
    self:GetFlashObject():InvokeASCallback("_root", "luacall_setUIVisible", 0)
    self.RequestQuickbuyableItemsDefaultCount()
  else
    self:MoveIn()
  end
end
function GameCommonBuyBox:IsCurStoreItemQuickBuyable()
  return GameCommonGoodsList.CurItemDetail.quick_buy > 0
end
function GameCommonBuyBox:OnEraseFromGameState()
  GameGlobalData:RemoveDataChangeCallback("item_count", self.RefreshResource)
  self.IsShowMoreInfoWindow = nil
  self:UnloadFlashObject()
end
function GameCommonBuyBox:MoveIn()
  local flash_obj = self:GetFlashObject()
  if GameCommonBuyBox.isSelectHero then
    GameCommonBuyBox:ShowHeroDetailWindow(GameCommonGoodsList.CurItemDetail.fleet_id, nil, GameCommonGoodsList.CurItemDetail.fleet_level)
  else
    flash_obj:InvokeASCallback("_root", "animationMoveIn")
  end
end
function GameCommonBuyBox:MoveOut()
  local flash_obj = self:GetFlashObject()
  if GameCommonBuyBox.isSelectHero then
    flash_obj:InvokeASCallback("_root", "heroInfoWindowMoveOut")
  else
    flash_obj:InvokeASCallback("_root", "animationMoveOut")
  end
end
function GameCommonBuyBox:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  if GameCommonGoodsList.isSelectHero then
    flash_obj:InvokeASCallback("_root", "onUpdateFrameHeroInfo", dt)
  else
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
  flash_obj:InvokeASCallback("_root", "onUpdateFrameDesc", dt)
  GameCommonBuyBox:UpdateDisplayIcon()
end
function GameCommonBuyBox:UpdateDisplayIcon()
  local v = GameCommonGoodsList.CurItemDetail
  if v == nil then
    return
  end
  if GameCommonBuyBox.iconDisplayed == false then
    local frame = "empty"
    if DynamicResDownloader:IsDynamicStuff(v.item_id, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(v.item_id .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        frame = "item_" .. v.item_id
      else
        frame = "temp"
      end
    else
      frame = "item_" .. v.item_id
    end
    if frame == "temp" then
      if GameCommonBuyBox.setEmptyAlready == false then
        if not GameCommonBuyBox.isSelectHero then
          self:GetFlashObject():InvokeASCallback("_root", "setIcon", frame)
        end
        GameCommonBuyBox.setEmptyAlready = true
      end
    else
      if not GameCommonBuyBox.isSelectHero then
        self:GetFlashObject():InvokeASCallback("_root", "setIcon", frame)
      end
      GameCommonBuyBox.iconDisplayed = true
    end
  end
end
function GameCommonBuyBox:DisplayDetailWdc()
  local v = GameCommonGoodsList.CurItemDetail
  DebugOut("display detail")
  DebugTable(v)
  local resource = GameGlobalData:GetData("item_count")
  local resourceCount = GameGlobalData:GetData("resource")
  local item_1 = 0
  local item_2 = 0
  local item_3 = 0
  if GameHelper:IsResource(GameCommonGoodsList.id1) then
    item_1 = resourceCount[GameCommonGoodsList.id1]
  end
  if GameHelper:IsResource(GameCommonGoodsList.id2) then
    item_2 = resourceCount[GameCommonGoodsList.id2]
  end
  if GameHelper:IsResource(GameCommonGoodsList.id3) then
    item_3 = resourceCount[GameCommonGoodsList.id3]
  end
  DebugOut("items = " .. item_1 .. "," .. item_2 .. ", " .. item_3)
  local text1 = 0
  local text2 = 0
  local text3 = 0
  local jump_shop = false
  for k, _v in ipairs(v.items) do
    if GameCommonBuyBox:GetResourceNameByid(tonumber(_v.item_id)) == GameCommonGoodsList.id1 then
      text1 = "" .. item_1
      if tonumber(_v.item_no) <= tonumber(item_1) then
        text1 = "<font color='#FFCC00'>" .. text1 .. "</font>/" .. v.price1
      else
        text1 = "<font color='#FF0000'>" .. text1 .. "</font>/" .. v.price1
        if _v.is_jump2store then
          jump_shop = true
        end
      end
    elseif GameCommonBuyBox:GetResourceNameByid(tonumber(_v.item_id)) == GameCommonGoodsList.id2 then
      text2 = "" .. item_2
      if tonumber(_v.item_no) <= tonumber(item_2) then
        text2 = "<font color='#FFCC00'>" .. text2 .. "</font>/" .. v.price2
      else
        text2 = "<font color='#FF0000'>" .. text2 .. "</font>/" .. v.price2
        if _v.is_jump2store then
          jump_shop = true
        end
      end
    elseif GameCommonBuyBox:GetResourceNameByid(tonumber(_v.item_id)) == GameCommonGoodsList.id3 then
      text3 = "" .. item_3
      if tonumber(_v.item_no) <= tonumber(item_3) then
        text3 = "<font color='#FFCC00'>" .. text3 .. "</font>/" .. v.price3
      else
        text3 = "<font color='#FF0000'>" .. text3 .. "</font>/" .. v.price3
        if _v.is_jump2store then
          jump_shop = true
        end
      end
    end
  end
  local isCanBuy = true
  if v.day_buy_left == 0 or v.all_buy_left == 0 then
    DebugOut("1111")
    isCanBuy = false
  end
  local dayLeftText = GameLoader:GetGameText("LC_MENU_SHOP_DAY_BUY_TIME_CHAR") .. v.day_buy_left
  local allLeftText = v.all_buy_left
  self:GetFlashObject():InvokeASCallback("_root", "setState", "p2")
  if jump_shop then
    self:GetFlashObject():InvokeASCallback("_root", "setIsShowBuy", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setIsShowBuy", false)
  end
  if GameCommonBuyBox.isSelectHero then
    self:GetFlashObject():InvokeASCallback("_root", "setHeroInfoWindowItem", nil, nil, v.price1, v.price2, v.price3, GameCommonGoodsList.frame1, GameCommonGoodsList.frame2, GameCommonGoodsList.frame3, text1, text2, text3, isCanBuy, dayLeftText, allLeftText)
  else
    local isQuickBuyable = GameCommonBuyBox.IsCurStoreItemQuickBuyable()
    self:GetFlashObject():InvokeASCallback("_root", "setItem", v.name, v.desc, v.price1, v.price2, v.price3, GameCommonGoodsList.frame1, GameCommonGoodsList.frame2, GameCommonGoodsList.frame3, text1, text2, text3, isQuickBuyable, isCanBuy, dayLeftText, allLeftText)
  end
end
function GameCommonBuyBox:GetResourceNameByid(id)
  local name = ""
  if id == 99000004 then
    name = "silver"
  else
    return id
  end
  return name
end
function GameCommonBuyBox:GetResourceidByName(name)
  local id = 0
  if name == "silver" then
    id = 99000004
  end
  return id
end
function GameCommonBuyBox:DisplayDetail()
  local v = GameCommonGoodsList.CurItemDetail
  DebugOut("display detaildxxx")
  DebugTable(v)
  local resource = GameGlobalData:GetData("item_count")
  local resourceCount = GameGlobalData:GetData("resource")
  local item_1 = 0
  local item_2 = 0
  local item_3 = 0
  if GameHelper:IsResource(GameCommonGoodsList.id1) then
    item_1 = resourceCount[GameCommonGoodsList.id1]
  end
  if GameHelper:IsResource(GameCommonGoodsList.id2) then
    item_2 = resourceCount[GameCommonGoodsList.id2]
  end
  if GameHelper:IsResource(GameCommonGoodsList.id3) then
    item_3 = resourceCount[GameCommonGoodsList.id3]
  end
  for i, v in ipairs(resource.items) do
    if v.item_id == GameCommonGoodsList.id1 then
      item_1 = item_1 + v.item_no
    elseif v.item_id == GameCommonGoodsList.id2 then
      item_2 = item_2 + v.item_no
    elseif v.item_id == GameCommonGoodsList.id3 then
      item_3 = item_3 + v.item_no
    end
  end
  DebugOut("items = " .. item_1 .. "," .. item_2 .. ", " .. item_3)
  local text1 = 0
  local text2 = 0
  local text3 = 0
  local jump_shop = false
  for k, _v in ipairs(v.items) do
    if tonumber(_v.item_id) == GameCommonGoodsList.id1 then
      text1 = "" .. item_1
      if tonumber(_v.item_no) <= tonumber(item_1) then
        text1 = "<font color='#FFCC00'>" .. text1 .. "</font>/" .. v.price1
      else
        text1 = "<font color='#FF0000'>" .. text1 .. "</font>/" .. v.price1
        if _v.is_jump2store then
          jump_shop = true
        end
      end
    elseif tonumber(_v.item_id) == GameCommonGoodsList.id2 then
      text2 = "" .. item_2
      if tonumber(_v.item_no) <= tonumber(item_2) then
        text2 = "<font color='#FFCC00'>" .. text2 .. "</font>/" .. v.price2
      else
        text2 = "<font color='#FF0000'>" .. text2 .. "</font>/" .. v.price2
        if _v.is_jump2store then
          jump_shop = true
        end
      end
    elseif tonumber(_v.item_id) == GameCommonGoodsList.id3 then
      text3 = "" .. item_3
      if tonumber(_v.item_no) <= tonumber(item_3) then
        text3 = "<font color='#FFCC00'>" .. text3 .. "</font>/" .. v.price3
      else
        text3 = "<font color='#FF0000'>" .. text3 .. "</font>/" .. v.price3
        if _v.is_jump2store then
          jump_shop = true
        end
      end
    end
  end
  local isCanBuy = true
  local dayLeftText = ""
  local allLeftText = ""
  self:GetFlashObject():InvokeASCallback("_root", "setState", "p1")
  if jump_shop then
    self:GetFlashObject():InvokeASCallback("_root", "setIsShowBuy", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setIsShowBuy", false)
  end
  if GameCommonBuyBox.isSelectHero then
    self:GetFlashObject():InvokeASCallback("_root", "setHeroInfoWindowItem", nil, nil, v.price1, v.price2, v.price3, GameCommonGoodsList.frame1, GameCommonGoodsList.frame2, GameCommonGoodsList.frame3, text1, text2, text3, isCanBuy, dayLeftText, allLeftText)
  else
    local choosable_items = GameCommonGoodsList.choosable_items
    local isQuickBuyable = GameCommonBuyBox.IsCurStoreItemQuickBuyable()
    self:GetFlashObject():InvokeASCallback("_root", "setItem", v.name, v.desc, v.price1, v.price2, v.price3, GameCommonGoodsList.frame1, GameCommonGoodsList.frame2, GameCommonGoodsList.frame3, text1, text2, text3, isQuickBuyable, isCanBuy, dayLeftText, allLeftText, choosable_items)
    GameCommonGoodsList.choosable_items = nil
  end
end
function GameCommonBuyBox:OnFSCommand(cmd, arg)
  DebugOut("GameCommonBuyBox:OnFSCommand " .. cmd)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "BuyReleased" then
    if GameCommonBuyBox.IsShowMoreInfoWindow then
      DebugOut("zm shield BuyReleased message.")
      self:GetFlashObject():InvokeASCallback("_root", "heroInfoWindowMoveOut")
    elseif not self.isSelectHero and self:IsCurStoreItemQuickBuyable() and 1 ~= tonumber(arg) then
      local buyCount = tonumber(arg)
      self.isRquestPriceForConfirming = true
      self.RequestQuickBuyCast(buyCount, true)
      self.quickBuyCountRecord = buyCount
    else
      self.RequestBuyItem(1)
    end
  elseif cmd == "close_menu" then
    self:MoveOut()
    GameCommonGoodsList.isShowCurItemDatail = false
  elseif cmd == "close_heroInfoWindow" then
    self:GetFlashObject():InvokeASCallback("_root", "heroInfoWindowMoveOut")
  elseif cmd == "OnClickShowMoreInfo" then
    self:RecordShowWhichInfoWindow()
  elseif cmd == "boxHiden" then
    GameCommonGoodsList.isShowCurItemDatail = false
    GameCommonGoodsList:HideBuyConfirmWin()
  elseif cmd == "BuyjumpReleased" then
    if GameCommonBuyBox.IsShowMoreInfoWindow then
      DebugOut("zm shield BuyReleased message.")
      self:GetFlashObject():InvokeASCallback("_root", "heroInfoWindowMoveOut")
    else
      GameStateManager:SetCurrentGameState(GameStateStore)
    end
  elseif "onQuickBuyNumberChanged" == cmd then
    local buyCount = tonumber(arg)
    GameCommonBuyBox.onQuickBuyNumberChanged(buyCount)
  end
end
function GameCommonBuyBox.onQuickBuyNumberChanged(buyCount)
  DebugOut("GameCommonBuyBox.onQuickBuyNumberChanged " .. buyCount)
  GameCommonBuyBox.isRequestPriceForUpdatePriceUI = true
  GameCommonBuyBox.RequestQuickBuyCast(buyCount, false)
end
function GameCommonBuyBox.RequestQuickbuyableItemsDefaultCount()
  DebugOut("GameCommonBuyBox.RequestQuickbuyableItemsDefaultCount")
  local reqParam = {
    store_type = GameCommonBuyBox.GetCommonEventTypeNow(),
    id = GameCommonGoodsList.CurItemDetail.id
  }
  DebugTable(reqParam)
  NetMessageMgr:SendMsg(NetAPIList.activity_store_count_req.Code, reqParam, GameCommonBuyBox.NetRequestCallback, true, nil)
end
function GameCommonBuyBox.RequestQuickBuyCast(count, isBlock)
  DebugOut("GameCommonBuyBox.RequestQuickBuyCast")
  local reqParam = {
    store_type = GameCommonBuyBox.GetCommonEventTypeNow(),
    id = GameCommonGoodsList.CurItemDetail.id,
    buy_times = count
  }
  DebugTable(reqParam)
  NetMessageMgr:SendMsg(NetAPIList.activity_store_price_req.Code, reqParam, GameCommonBuyBox.NetRequestCallback, isBlock, nil)
end
function GameCommonBuyBox.NetRequestCallback(msgType, content)
  if msgtype == NetAPIList.common_ack.Code and (NetAPIList.activity_store_count_req.code == content.api or NetAPIList.activity_store_price_req.code == content.api) then
    DebugOut("exception")
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.activity_store_count_ack.Code then
    GameCommonBuyBox.OnQuickBuyDefaultCountCallback(content)
    return true
  elseif msgType == NetAPIList.activity_store_price_ack.Code then
    GameCommonBuyBox.OnQuickBuyPriceCallback(content)
    return true
  end
  return false
end
function GameCommonBuyBox.OnQuickBuyDefaultCountCallback(content)
  GameCommonBuyBox:GetFlashObject():InvokeASCallback("_root", "luacall_setUIVisible", 1)
  GameCommonBuyBox:MoveIn()
  GameCommonBuyBox:GetFlashObject():InvokeASCallback("_root", "luacall_initTheQuickBuyNumber", content.times, content.max_times)
  GameCommonBuyBox.UpdateQuickBuyUIPrice(content.all_cost)
  if 0 == content.times then
    GameCommonBuyBox.onQuickBuyNumberChanged(1)
  end
end
function GameCommonBuyBox.OnQuickBuyPriceCallback(content)
  for i, cost_attr in ipairs(content.all_cost) do
    local cost = tonumber(cost_attr.cost)
    assert(cost and cost >= 0 and "if this assertion faild, the server must be error")
  end
  if GameCommonBuyBox.isRquestPriceForConfirming then
    GameCommonBuyBox.OpenQuickBuyConfirmingUI(GameCommonBuyBox.quickBuyCountRecord, content.all_cost)
    GameCommonBuyBox.isRquestPriceForConfirming = false
  end
  if GameCommonBuyBox.isRequestPriceForUpdatePriceUI then
    GameCommonBuyBox.UpdateQuickBuyUIPrice(content.all_cost)
    GameCommonBuyBox.isRequestPriceForUpdatePriceUI = false
  end
end
function GameCommonBuyBox.getGameItemText(item_type, item_id)
  return ...
end
function GameCommonBuyBox.createComfirmingText(count, cost_attr_array)
  local comfirmingTextArray = {}
  local textIsCast = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_1")
  table.insert(comfirmingTextArray, textIsCast)
  for i, cost_attr in ipairs(cost_attr_array) do
    local textGameItem = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_2")
    local itemName = GameCommonBuyBox.getGameItemText("", cost_attr.item_id)
    textGameItem = string.format(textGameItem, cost_attr.cost, itemName)
    table.insert(comfirmingTextArray, textGameItem .. ",")
  end
  local textToBuy = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_3")
  table.insert(comfirmingTextArray, textToBuy)
  local textGameItem = GameLoader:GetGameText("LC_MENU_BUY_MORE_ITEMS_2")
  local curItemName = GameCommonBuyBox.getGameItemText(GameCommonGoodsList.CurItemDetail.item_type, GameCommonGoodsList.CurItemDetail.item_id)
  textGameItem = string.format(textGameItem, count * GameCommonGoodsList.CurItemDetail.item_no, curItemName)
  table.insert(comfirmingTextArray, textGameItem)
  return ...
end
function GameCommonBuyBox.OpenQuickBuyConfirmingUI(count, cost_attr_array)
  local function buyCallback()
    GameCommonBuyBox.RequestBuyItem(count)
  end
  local confirmingText = GameCommonBuyBox.createComfirmingText(count, cost_attr_array)
  GameUtils:CreditCostConfirm(confirmingText, buyCallback, true, nil)
end
function GameCommonBuyBox.UpdateQuickBuyUIPrice(cost_attr_array)
  DebugOut("GameCommonBuyBox.UpdateQuickBuyUIPrice")
  DebugTable(cost_attr_array)
  function isCastTheResource(item_id)
    for i, cast_arrt in ipairs(cost_attr_array) do
      if item_id == cast_arrt.item_id then
        return true
      end
    end
    return false
  end
  function getResourceCastCount(item_id)
    for i, cast_arrt in ipairs(cost_attr_array) do
      if item_id == cast_arrt.item_id then
        return cast_arrt.cost or 99999999
      end
    end
    return 0
  end
  local priceArray = {
    "",
    "",
    ""
  }
  local textArray = {
    "",
    "",
    ""
  }
  local idArray = {}
  local id1 = GameCommonBuyBox:GetResourceidByName(GameCommonGoodsList.id1)
  local id2 = GameCommonBuyBox:GetResourceidByName(GameCommonGoodsList.id2)
  local id3 = GameCommonBuyBox:GetResourceidByName(GameCommonGoodsList.id3)
  if GameCommonGoodsList.storeType == GameCommonGoodsList.TYPE.wdc then
    idArray = {
      id1,
      id2,
      id3
    }
  else
    idArray = {
      GameCommonGoodsList.id1,
      GameCommonGoodsList.id2,
      GameCommonGoodsList.id3
    }
  end
  DebugTable(idArray)
  for i = 1, 3 do
    local item_id = idArray[i]
    if isCastTheResource(item_id) then
      local cast = getResourceCastCount(item_id)
      if 0 == cast then
        priceArray[i] = "not0"
      else
        priceArray[i] = "" .. cast
      end
      textArray[i] = GameCommonBuyBox.createPriceText(item_id, cast)
    else
      priceArray[i] = "0"
    end
  end
  DebugTable(priceArray)
  DebugTable(textArray)
  GameCommonBuyBox:GetFlashObject():InvokeASCallback("_root", "luacall_updatePrice", priceArray[1], priceArray[2], priceArray[3], textArray[1], textArray[2], textArray[3])
end
function GameCommonBuyBox.createPriceText(item_id, castCount)
  local text = ""
  local amount = GameCommonBuyBox.getItemAmountInBag(item_id) or 0
  if castCount <= amount then
    text = "<font color='#FFCC00'>" .. amount .. "</font>/" .. castCount
  else
    text = "<font color='#FF0000'>" .. amount .. "</font>/" .. castCount
  end
  return text
end
function GameCommonBuyBox.getItemAmountInBag(item_id)
  local resource = GameGlobalData:GetData("item_count")
  local resourceCount = GameGlobalData:GetData("resource")
  local name = GameCommonBuyBox:GetResourceNameByid(item_id)
  local count = resourceCount[name]
  for i, item in ipairs(resource.items) do
    if item_id == item.item_id then
      count = item.item_no or 99999999
    end
  end
  return count
end
function GameCommonBuyBox:RecordShowWhichInfoWindow()
  if not GameCommonBuyBox.IsShowMoreInfoWindow then
    GameCommonBuyBox.IsShowMoreInfoWindow = true
    DebugOut("zm IsShowMoreInfoWindow:", GameCommonBuyBox.IsShowMoreInfoWindow)
    return
  end
  self.IsShowMoreInfoWindow = not self.IsShowMoreInfoWindow
  DebugOut("zm IsShowMoreInfoWindow:", self.IsShowMoreInfoWindow)
end
function GameCommonBuyBox.GetCommonEventTypeNow()
  local reqType = GameUICommonEvent.NetMegType.FESTIVAL
  if GameUICommonEvent.curDetailType == GameUICommonEvent.DetailType.WD_EVENT then
    reqType = GameUICommonEvent.NetMegType.WD_EVENT
  end
  if GameCommonGoodsList.storeType == GameCommonGoodsList.TYPE.wdc then
    reqType = GameCommonGoodsList.TYPE.wdc
  end
  return reqType
end
function GameCommonBuyBox.RequestBuyItem(count)
  GameCommonBuyBox.currentBuyCount = count
  local storeType = GameCommonBuyBox.GetCommonEventTypeNow()
  local shop_purchase_req_param = {
    store_type = storeType,
    id = GameCommonGoodsList.CurItemDetail.id,
    buy_times = count
  }
  NetMessageMgr:SendMsg(NetAPIList.activity_stores_buy_req.Code, shop_purchase_req_param, GameCommonBuyBox.BuyItemCallback, true, nil)
end
function GameCommonBuyBox.BuyItemCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.activity_stores_buy_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameCommonGoodsList:updateCommonGoodsListInfo(GameCommonGoodsList.CurItemDetail.id, GameCommonBuyBox.currentBuyCount)
        GameCommonBuyBox:MoveOut()
        GameCommonGoodsList:TrySendGoodsListReq()
      end
    end
    return true
  end
  return false
end
function GameCommonBuyBox:TryQueryFleetDetailReq(fleetID, fleetLevel)
  DebugOut("query fleet detail req", fleetID)
  self:GetFlashObject():InvokeASCallback("_root", "hideHeroInfoWindow")
  local tmpLevel = 0
  if fleetLevel ~= nil then
    tmpLevel = fleetLevel
  end
  local req_content = {
    fleet_id = fleetID,
    level = tmpLevel,
    type = 0,
    req_type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.fleet_info_req.Code, req_content, GameCommonBuyBox.GetCommanderDetailCallBack, true, nil)
end
function GameCommonBuyBox.GetCommanderDetailCallBack(msgType, content)
  DebugOut("detail call back")
  DebugOut(msgType)
  DebugTable(content)
  if msgType == NetAPIList.fleet_info_ack.Code then
    GameGlobalData:SetFleetDetail(content.fleet_id, content.fleet_info.fleets[1])
    GameCommonBuyBox:ShowHeroDetailWindow(content.fleet_id)
    return true
  end
  return false
end
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
function GameCommonBuyBox:ShowHeroDetailWindow(fleetID, fleetData, fleetLevel)
  if fleetData == nil then
    fleetData = GameGlobalData:GetFleetDetail(fleetID)
    if fleetData == nil then
      GameCommonBuyBox:TryQueryFleetDetailReq(fleetID, fleetLevel)
      return
    end
  end
  DebugOut("GameGlobalData.GlobalData.fleetDetails")
  DebugTable(GameGlobalData.GlobalData.fleetDetails)
  DebugOut("----------show commander detail")
  if not GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    GameStateManager:GetCurrentGameState():AddObject(self)
    self:GetFlashObject():InvokeASCallback("_root", "heroInfoWindowMoveIn")
  end
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetID, fleetData.level)
  local ability = GameDataAccessHelper:GetCommanderAbility(fleetID, fleetData.level)
  local name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(fleetID, fleetData.level))
  local userinfo = GameGlobalData:GetUserInfo()
  if fleetID == 1 then
    name = GameUtils:GetUserDisplayName(userinfo.name)
  end
  local vessleType = GameDataAccessHelper:GetCommanderVesselsType(fleetID, fleetData.level)
  local vessleTypeName = GameLoader:GetGameText("LC_FLEET_" .. vessleType)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleetID, fleetData.level)
  local level = GameGlobalData:GetData("levelinfo").level
  local sex = GameDataAccessHelper:GetCommanderSex(fleetID)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  local fleetInfoTitle = fleetID .. "\001" .. iconFrame .. "\001" .. name .. "\001" .. level .. "\001" .. tostring(fleetData.force) .. "\001" .. vessleType .. "\001" .. vessleTypeName .. "\001" .. GameUtils:GetFleetRankFrame(ability.Rank, GameCommonBuyBox.DownloadRankImageCallback) .. "\001" .. sex .. "\001"
  local skill = basic_info.SPELL_ID
  local skillName = GameDataAccessHelper:GetSkillNameText(skill)
  local skillDesc = GameDataAccessHelper:GetSkillDesc(skill)
  local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  local skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill))
  local skillIconFram = GameDataAccessHelper:GetSkillIcon(skill)
  DebugOut("Skill Id", skill)
  local fleetSkill = skillIconFram .. "\001" .. skillName .. "\001" .. skillDesc .. "\001" .. skillRangeType .. "\001" .. skillRangetypeName .. "\001"
  local propertyName = ""
  for i = 1, 21 do
    local isGift = false
    for j = 1, #fleetData.gift do
      if fleetData.gift[j].gift == i then
        isGift = true
        break
      end
    end
    if isGift then
      if i == 18 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 21) .. "</font>" .. "\001"
      elseif i == 19 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 22) .. "</font>" .. "\001"
      elseif i == 20 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 23) .. "</font>" .. "\001"
      elseif i == 21 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 24) .. "</font>" .. "\001"
      else
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "</font>" .. "\001"
      end
    elseif i == 18 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 21) .. "\001"
    elseif i == 19 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 22) .. "\001"
    elseif i == 20 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 23) .. "\001"
    elseif i == 21 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 24) .. "\001"
    else
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "\001"
    end
  end
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleetID, fleetData.level)
  local phAttack = fleetData.ph_attack
  local enAttack = fleetData.en_attack
  if tonumber(fleetVessels) == 4 then
    phAttack = " \226\128\148 "
  else
    enAttack = " \226\128\148 "
  end
  DebugTable(fleetData)
  local crit_lv = "" .. fleetData.crit_lv / 10 .. "%"
  local crit_damage = "" .. (fleetData.crit_damage + 1500) / 10 .. "%"
  local anti_crit = "" .. fleetData.anti_crit / 10 .. "%"
  local intercept = "" .. fleetData.intercept / 10 .. "%"
  local penetration = "" .. fleetData.penetration / 10 .. "%"
  local motility = "" .. fleetData.motility / 10 .. "%"
  local hit_rate = "" .. fleetData.hit_rate / 10 .. "%"
  local damage_deepens = "" .. fleetData.damage_deepens / 10 .. "%"
  local damage_reduction = "" .. fleetData.damage_reduction / 10 .. "%"
  local skill_damage_deepens = "" .. fleetData.skill_damage_deepens / 10 .. "%"
  local skill_damage_reduction = "" .. fleetData.skill_damage_reduction / 10 .. "%"
  local propertyValue = ""
  for i = 1, 21 do
    local isGift = false
    for j = 1, #fleetData.gift do
      if fleetData.gift[j].gift == i then
        isGift = true
        break
      end
    end
    local pre = ""
    local suf = ""
    if isGift then
      pre = "<font color='#FC9901'>"
      suf = "</font>"
    end
    if i == 1 then
      propertyValue = propertyValue .. pre .. fleetData.max_durability .. suf .. "\001"
    elseif i == 2 then
      propertyValue = propertyValue .. pre .. fleetData.shield .. suf .. "\001"
    elseif i == 3 then
      propertyValue = propertyValue .. pre .. phAttack .. suf .. "\001"
    elseif i == 4 then
      propertyValue = propertyValue .. pre .. fleetData.ph_armor .. suf .. "\001"
    elseif i == 5 then
      propertyValue = propertyValue .. pre .. fleetData.sp_attack .. suf .. "\001"
    elseif i == 6 then
      propertyValue = propertyValue .. pre .. fleetData.sp_armor .. suf .. "\001"
    elseif i == 7 then
      propertyValue = propertyValue .. pre .. enAttack .. suf .. "\001"
    elseif i == 8 then
      propertyValue = propertyValue .. pre .. fleetData.en_armor .. suf .. "\001"
    elseif i == 9 then
      propertyValue = propertyValue .. pre .. crit_lv .. suf .. "\001"
    elseif i == 10 then
      propertyValue = propertyValue .. pre .. crit_damage .. suf .. "\001"
    elseif i == 11 then
      propertyValue = propertyValue .. pre .. anti_crit .. suf .. "\001"
    elseif i == 12 then
      propertyValue = propertyValue .. pre .. motility .. suf .. "\001"
    elseif i == 13 then
      propertyValue = propertyValue .. pre .. penetration .. suf .. "\001"
    elseif i == 14 then
      propertyValue = propertyValue .. pre .. intercept .. suf .. "\001"
    elseif i == 15 then
      propertyValue = propertyValue .. pre .. fleetData.fce .. suf .. "\001"
    elseif i == 16 then
      propertyValue = propertyValue .. pre .. fleetData.accumulator .. suf .. "\001"
    elseif i == 17 then
      propertyValue = propertyValue .. pre .. hit_rate .. suf .. "\001"
    elseif i == 18 then
      propertyValue = propertyValue .. pre .. damage_deepens .. suf .. "\001"
    elseif i == 19 then
      propertyValue = propertyValue .. pre .. damage_reduction .. suf .. "\001"
    elseif i == 20 then
      propertyValue = propertyValue .. pre .. skill_damage_deepens .. suf .. "\001"
    elseif i == 21 then
      propertyValue = propertyValue .. pre .. skill_damage_reduction .. suf .. "\001"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "showHeroDetailWindow", fleetInfoTitle, fleetSkill, propertyName, propertyValue)
end
function GameCommonBuyBox.DownloadRankImageCallback(extInfo)
  if GameCommonBuyBox:GetFlashObject() then
    GameCommonBuyBox:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id)
  end
end
