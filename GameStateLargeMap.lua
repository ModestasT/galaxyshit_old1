local GameStateLargeMap = GameStateManager.GameStateLargeMap
local GameUILargeMapUI = LuaObjectManager:GetLuaObject("GameUILargeMapUI")
local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
function GameStateLargeMap:InitGameState()
  self.m_preState = nil
end
function GameStateLargeMap:OnFocusGain(previousState)
  self.m_preState = previousState
  self:AddObject(GameUILargeMap)
end
function GameStateLargeMap:OnFocusLost(newState)
  DebugOutBattlePlay("GameStateLargeMap:OnFocusLost")
  self:EraseObject(GameUILargeMap)
  GameUILargeMapUI:Hide()
  self.m_preState = nil
end
