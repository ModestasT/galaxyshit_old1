WVEInfoPanelDie = luaClass(nil)
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
function WVEInfoPanelDie:ctor(wveGameManager)
  self.mGameManger = wveGameManager
end
function WVEInfoPanelDie:OnFSCommand(cmd, arg)
  if cmd == "ClickDiePanelRebuildBtn" then
    DebugOut("ClickDiePanelRebuildBtn")
    self:RequestReviveYesNo()
  elseif cmd == "ClickDiePanelReplayBtn" then
    DebugOut("ClickDiePanelReplayBtn")
    self:RequestLastReport()
  else
    return false
  end
  return true
end
function WVEInfoPanelDie:RequestLastReport()
  local rankData = GameGlobalData:GetData("prime_rank_info")
  DebugOut("RequestLastReport:1")
  DebugTable(rankData)
  if not rankData then
    return
  end
  local selfRank = {}
  for k, v in pairs(rankData) do
    if tonumber(v.type) == 2 then
      if v.self_rank then
        selfRank = v.self_rank
        break
      end
    elseif tonumber(v.type) == 1 and v.self_rank then
      selfRank = v.self_rank
      break
    end
  end
  DebugOut("RequestLastReport:2")
  DebugTable(selfRank)
  if selfRank then
    local requestContent = {}
    requestContent.server_id = selfRank.server_id
    requestContent.user_id = selfRank.user_id
    DebugOut("RequestLastReport:3")
    DebugTable(requestContent)
    NetMessageMgr:SendMsg(NetAPIList.prime_last_report_req.Code, requestContent, self.RequestLastReportCallback, true, nil)
  end
end
function WVEInfoPanelDie.RequestLastReportCallback(msgType, content)
  DebugOut("RequestLastReportCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.battle_fight_report_ack.Code then
    if content and content.histories and content.histories[1] then
      GameStateBattlePlay.curBattleType = "WVEInfo"
      GameStateManager.GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.histories[1].report, nil, nil)
      GameStateBattlePlay:RegisterOverCallback(function()
        WVEGameManeger:BackToGame()
      end, nil)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_REPLAY_NOT_EXIST"))
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.battle_fight_report_ack then
    return true
  end
  return false
end
function WVEInfoPanelDie:RequestReviveYesNo()
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(self.RequestRevive, false)
  local titleText = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local contentText = string.format(GameLoader:GetGameText("LC_MENU_TC_REBUILD_IMMEDIATELY_CONFIRM_ALERT"), WVEGameManeger.revive_credit)
  GameUIMessageDialog:Display(titleText, contentText)
end
function WVEInfoPanelDie.RequestRevive()
  local userResource = GameGlobalData:GetData("resource")
  if userResource and userResource.credit < WVEGameManeger.revive_credit then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_NOT_ENOUGH_CREDIT"))
  else
    NetMessageMgr:SendMsg(NetAPIList.revive_self_req.Code, nil, WVEGameManeger.mWVEInfoPanelDie.RequestReviveCallback, true, nil)
  end
end
function WVEInfoPanelDie.RequestReviveCallback(msgType, content)
  DebugOut("RequestReviveCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.revive_self_req.Code then
    DebugOut("RequestReviveCallback:true")
    return true
  end
  return false
end
function WVEInfoPanelDie:Show(dieInfo)
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  self.mLastFrameTime = -1
  flashObj:InvokeASCallback("_root", "DieMC_Show", dieInfo)
end
function WVEInfoPanelDie:Hide()
  local flashObj = self.mGameManger:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "DieMC_Hide")
end
function WVEInfoPanelDie:OnUpdate(dt)
  local playerMySelf = self.mGameManger.mWVEPlayerManager:GetPlayerMyself()
  if playerMySelf.mState == EWVEPlayerState.STATE_PLAYER_DIE then
    local tmpLeftTime = 0
    local rebuildPercent = 1
    if 0 < playerMySelf.mDieWaitTime then
      tmpLeftTime = playerMySelf.mDieWaitTime - (os.time() - playerMySelf.mDieTime)
      rebuildPercent = 100 - tmpLeftTime / playerMySelf.mDieWaitTime * 100
      if tmpLeftTime < 0 then
        tmpLeftTime = 0
      end
    else
      rebuildPercent = 1
      tmpLeftTime = 0
    end
    if self.mLastFrameTime == tmpLeftTime then
      return
    end
    self.mLastFrameTime = tmpLeftTime
    local timeStr = GameUtils:formatTimeString(tmpLeftTime)
    local dieInfo = {}
    dieInfo.timeStr = timeStr
    dieInfo.rebuildPercent = rebuildPercent
    local flashObj = self.mGameManger:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "DieMC_SetUpDieInfo", dieInfo)
      flashObj:InvokeASCallback("_root", "MainMC_SetUpDieInfo", dieInfo)
    end
  end
end
