module("GameObjectTacticsCenter", package.seeall)
require("TacticsCenterDataManager.tfl")
local GameObjectTacticsCenter = LuaObjectManager:GetLuaObject("GameObjectTacticsCenter")
GameObjectTacticsCenter._vesselOpenNow = nil
GameObjectTacticsCenter._equipIdSelectNow = nil
GameObjectTacticsCenter._forceRecord = 0
GameObjectTacticsCenter.slotRedPointData = nil
local QuestTutorialTacticsCenter = TutorialQuestManager.QuestTutorialTacticsCenter
local QuestTutorialTacticsRefine = TutorialQuestManager.QuestTutorialTacticsRefine
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local SubDebugging = require("data1/GameUITacticsCenterRefine.tfl")
local TacticsCenterDataManager = TacticsCenterDataManager.TacticsCenterDataManager
local tcdm = TacticsCenterDataManager
local GameUtils = GameUtils
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local __equals = function(a, b)
  if nil == a then
    DebugOut("warning, you pass a nil to __equals first param, you really want?")
  end
  if nil == b then
    DebugOut("warning, you pass a nil to __equals second param, you really want?")
  end
  return a == b
end
local __table_find_if = function(table, predicate)
  for k, v in pairs(table) do
    if predicate(v) then
      return k, v
    end
  end
  return nil
end
function GameObjectTacticsCenter:OnInitGame()
end
function GameObjectTacticsCenter:OnFSCommand(cmd, arg)
  if "close_menu" == cmd then
    self:_handleFSCloseMenu(arg)
  elseif "press_slot_icon" == cmd then
    self:_handleFSPressSlotIcon(arg)
  elseif "press_slot_debugging" == cmd then
    self:_handleFSPressSlotDebugging(arg)
    tcdm:setShowState(tcdm.ShowLayerState.tacticsRefine)
  elseif "equip_recovery" == cmd then
    self:_handleFSEquipRecovery(arg)
  elseif "equip_on" == cmd then
    self:_handleFSEquipOn(arg)
  elseif "equip_off" == cmd then
    self:_handleFSEquipOff(arg)
  elseif "equip_exchange" == cmd then
    self:_handleFSEquipExchange(arg)
  elseif "select_equip" == cmd then
    self:_handleFSSelectEquip(arg)
    if QuestTutorialTacticsCenter:IsActive() then
      self:_invokeAS("_root", "hideTutorialShot")
    end
  elseif "closed_equip_info" == cmd then
    self:_handleFSClosedEquipInfo()
  elseif "EquipmentUnselectOver" == cmd then
    self:_setUIEquipInfoAndListVisible(false)
  elseif "release_btn_help" == cmd then
    self:_showUIHelpInfo()
  elseif "GoToMasterPage" == cmd then
    GameUIMaster.CurrentScope = {
      [1] = -1
    }
    GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.ModuleMaster
    GameUIMaster.CurrentPageType = GameUIMaster.MasterPage.centrol_grade
    GameUIMaster.CurMatrixId = FleetMatrix.cur_index
    GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
  end
  SubDebugging:OnFSCommand(cmd, arg)
end
function GameObjectTacticsCenter:OnAddToGameState()
  DebugOut("GameObjectTacticsCenter:OnAddToGameState")
  self:TutorialCheckShot()
  tcdm:clearData()
  self._datachangeListener = tcdm:createADataChangeListener()
  function self._datachangeListener:onDebuggingSave(equip)
    GameObjectTacticsCenter:_updateUISlot(equip.vessel)
    GameObjectTacticsCenter:dataDrive_UpdateEquipInfoAndBtn()
    GameObjectTacticsCenter:dataDrive_UpdateAllSlotHighlightState()
  end
  tcdm:addDataChangeListener(self._datachangeListener)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameObjectTacticsCenter.fleetInfoCallback)
  GameGlobalData:RegisterDataChangeCallback("resource", GameUITacticsCenterRefine.RefreshGlobalData)
  GameGlobalData:RegisterDataChangeCallback("item_count", GameUITacticsCenterRefine.RefreshGlobalData)
  self:_invokeAS("_root", "luafs_setRefineUIVisible", false)
  self:_invokeAS("_root", "luafs_setRootVisible", false)
  self:_setUIEquipInfoAndListVisible(false)
  tcdm:setShowState(tcdm.ShowLayerState.tacticsCenter)
end
function GameObjectTacticsCenter:TutorialCheckShot(...)
  self:_invokeAS("_root", "_hideAllTutorial")
  if QuestTutorialTacticsCenter:IsActive() then
    local function callback()
      NetMessageMgr:SendMsg(NetAPIList.tactical_guide_req.Code, nil, GameObjectTacticsCenter._onNet, true, nil)
      self:_invokeAS("_root", "luafs_showSlotButtonTutorial", TacticsCenterDataManager.EvesselType.qi_jian)
    end
    GameUICommonDialog:PlayStory({1100087, 1100088}, callback)
  end
end
function GameObjectTacticsCenter:TutorialCheckRefinebutton(...)
  self:_invokeAS("_root", "_hideAllTutorial")
  if QuestTutorialTacticsCenter:IsActive() then
    self:_invokeAS("_root", "luafs_showSlotButtonTutorial", TacticsCenterDataManager.EvesselType.qi_jian)
  end
  if QuestTutorialTacticsRefine:IsActive() then
    local showTutorialRefineVessel = TacticsCenterDataManager:getHasEquipAndMin()
    if showTutorialRefineVessel ~= 0 then
      self:_invokeAS("_root", "luafs_showRefineButtonTutorial", showTutorialRefineVessel)
    end
  end
end
function GameObjectTacticsCenter.fleetInfoCallback()
  DebugOut("GameObjectTacticsCenter.fleetInfoCallback")
  local newForce = GameObjectTacticsCenter.getFleetsForce()
  local forceChange = newForce - GameObjectTacticsCenter._forceRecord
  DebugOut("newForce")
  DebugOut(newForce .. " " .. GameObjectTacticsCenter._forceRecord)
  if forceChange < 0 then
    GameObjectTacticsCenter:_runUIForceDownAnimation(math.abs(forceChange))
  elseif forceChange > 0 then
    GameObjectTacticsCenter:_runUIForceUpAnimation(forceChange)
  end
  GameObjectTacticsCenter:_updateUIForce()
end
function GameObjectTacticsCenter:OnEraseFromGameState()
  DebugOut("GameObjectTacticsCenter:OnEraseFromGameState")
  GameObjectTacticsCenter.slotRedPointData = nil
  tcdm:remvoeDataChangeListener(self._datachangeListener)
  self:UnloadFlashObject()
end
function GameObjectTacticsCenter:_handleFSClosedEquipInfo()
  self:TutorialCheckRefinebutton()
  self:_closeEquipInfoAndListUI()
end
function GameObjectTacticsCenter:_handleFSSelectEquip(arg)
  DebugOut("GameObjectTacticsCenter:_handleFSSelectEquip")
  DebugOut(arg)
  if tonumber(arg) == 0 then
    return
  end
  DebugOut(type(arg))
  local equipId = "" .. arg
  self._equipIdSelectNow = equipId
  self:dataDrive_UpdateEquipInfoAndBtn()
  self:dataDrive_UpdateEquipInListHighlightState()
end
function GameObjectTacticsCenter:_handleFSCloseMenu(arg)
  GameStateManager.GameStateTacticsCenter:quit()
end
function GameObjectTacticsCenter:_handleFSPressSlotIcon(arg)
  DebugOut("GameObjectTacticsCenter:_handleFSPressSlotIcon")
  DebugOut(arg)
  local vessel = tonumber(arg)
  DebugOut(vessel)
  assert(vessel)
  local slotState = TacticsCenterDataManager:getSlotState(vessel)
  if __equals(tcdm.ESlotState.equiped_, slotState) or __equals(tcdm.ESlotState.none_, slotState) then
    local _vesselOpenBefore = self._vesselOpenNow
    self._vesselOpenNow = vessel
    if _vesselOpenBefore ~= nil then
      self:_updateEquipInfoAndListUI(self._vesselOpenNow)
    else
      self:_setUIEquipInfoAndListVisible(true)
      self:_openEquipInfoAndListUI(self._vesselOpenNow)
    end
    self:dataDrive_UpdateAllSlotHighlightState()
  elseif __equals(tcdm.ESlotState.lock_, slotState) then
    self:_tipUISlotUnavailableReasonOrPopUnlockConfirming()
  else
    error("logic error")
  end
end
function GameObjectTacticsCenter:_handleFSPressSlotDebugging(arg)
  DebugOut("GameObjectTacticsCenter:_handleFSPressSlotDebugging")
  DebugOut(arg)
  local vessel = tonumber(arg)
  local slotState = TacticsCenterDataManager:getSlotState(vessel)
  if __equals(tcdm.ESlotState.equiped_, slotState) then
    local equip = TacticsCenterDataManager:getEquipInSlot(vessel)
    assert(equip)
    self:_openDebuggingUI(vessel, equip.id)
  elseif __equals(tcdm.ESlotState.none_, slotState) then
  elseif __equals(tcdm.ESlotState.lock_, slotState) then
    self:_tipUISlotUnavailableReasonOrPopUnlockConfirming()
  else
    error("logic error")
  end
end
function GameObjectTacticsCenter:_handleFSEquipRecovery(arg)
  assert(self._equipIdSelectNow)
  self:_requestForEquipRecoverAward(self._equipIdSelectNow)
end
function GameObjectTacticsCenter:_handleFSEquipOn(arg)
  if QuestTutorialTacticsCenter:IsActive() then
    local function callback()
      GameObjectTacticsCenter:_handleFSClosedEquipInfo()
    end
    QuestTutorialTacticsCenter:SetFinish(true, false)
    QuestTutorialTacticsRefine:SetActive(true, false)
    GameUICommonDialog:PlayStory({1100089}, callback)
  end
  self:_requestForEquipOn(self._vesselOpenNow, self._equipIdSelectNow)
end
function GameObjectTacticsCenter:_handleFSEquipOff(arg)
  self:_requestForEquipOff(self._vesselOpenNow)
end
function GameObjectTacticsCenter:_handleFSEquipExchange(arg)
  self:_requestForEquipOn(self._vesselOpenNow, self._equipIdSelectNow)
end
function GameObjectTacticsCenter:_requestForEnter()
  DebugOut("GameObjectTacticsCenter:_requestForEnter")
  NetMessageMgr:SendMsg(NetAPIList.enter_tactical_req.Code, nil, GameObjectTacticsCenter._onNet, true, nil)
end
function GameObjectTacticsCenter:_requestForEquipOn(vessel, equipId)
  local param = {
    vessel = vessel or 1,
    equip_id = equipId
  }
  NetMessageMgr:SendMsg(NetAPIList.tactical_equip_on_req.Code, param, GameObjectTacticsCenter._onNet, true, nil)
end
function GameObjectTacticsCenter:_requestForEquipOff(vessel)
  local param = {
    vessel = vessel or 1
  }
  NetMessageMgr:SendMsg(NetAPIList.tactical_equip_off_req.Code, param, GameObjectTacticsCenter._onNet, true, nil)
end
function GameObjectTacticsCenter:_requestForUnlockNextSlot()
  DebugOut("GameObjectTacticsCenter:_requestForUnlockNextSlot")
  NetMessageMgr:SendMsg(NetAPIList.tactical_unlock_slot_req.Code, nil, GameObjectTacticsCenter._onNet, true, nil)
end
function GameObjectTacticsCenter:_requestForEquipRecoverAward(equipId)
  local param = {equip_id = equipId}
  NetMessageMgr:SendMsg(NetAPIList.tactical_delete_awards_req.Code, param, GameObjectTacticsCenter._onNet, true, nil)
end
function GameObjectTacticsCenter:_requestForEquipRecover(equipId)
  local param = {equip_id = equipId}
  NetMessageMgr:SendMsg(NetAPIList.tactical_equip_delete_req.Code, param, GameObjectTacticsCenter._onNet, true, nil)
end
function GameObjectTacticsCenter:enter()
  DebugOut("GameObjectTacticsCenter:enter")
  self:_requestForEnter()
end
function GameObjectTacticsCenter._onNet(msgType, content)
  DebugOut("GameObjectTacticsCenter._onNet")
  DebugOut(msgType)
  DebugTable(content)
  if content.code and 0 ~= content.code and (__equals(msgType, NetAPIList.tactical_equip_on_ack.Code) or __equals(msgType, NetAPIList.tactical_equip_off_ack.Code) or __equals(msgType, NetAPIList.tactical_unlock_slot_ack.Code) or __equals(msgType, NetAPIList.tactical_equip_delete_ack.Code) or __equals(msgType, NetAPIList.tactical_guide_req.Code)) then
    GameObjectTacticsCenter:_commonNetErrorHandle(content)
  elseif __equals(msgType, NetAPIList.enter_tactical_ack.Code) then
    GameObjectTacticsCenter.slotRedPointData = {}
    for k, v in ipairs(content.slots) do
      local data = {}
      data.vessel = v.vessel
      data.red_point = v.red_point
      table.insert(GameObjectTacticsCenter.slotRedPointData, data)
    end
    GameObjectTacticsCenter:_handleEnterTacticalACK(content)
  elseif __equals(msgType, NetAPIList.tactical_equip_on_ack.Code) then
    GameObjectTacticsCenter:_handleEquipOnACK(content)
    GameObjectTacticsCenter:ReRequestData()
  elseif __equals(msgType, NetAPIList.tactical_equip_off_ack.Code) then
    GameObjectTacticsCenter:_handleEquipOffACK(content)
    GameObjectTacticsCenter:ReRequestData()
  elseif __equals(msgType, NetAPIList.tactical_unlock_slot_ack.Code) then
    GameObjectTacticsCenter:_handleUnlockSlotACK(content)
    GameObjectTacticsCenter:ReRequestData()
  elseif __equals(msgType, NetAPIList.tactical_equip_delete_ack.Code) then
    GameObjectTacticsCenter:_handleEquipDeleteACK(content)
    GameObjectTacticsCenter:ReRequestData()
  elseif __equals(msgType, NetAPIList.common_ack.Code) and __equals(content.api, NetAPIList.tactical_guide_req.Code) then
    GameObjectTacticsCenter:enter()
    return true
  elseif __equals(msgType, NetAPIList.tactical_delete_awards_ack.Code) then
    GameObjectTacticsCenter:_handleEquipDeleteAward(content)
  else
    return false
  end
  return true
end
function GameObjectTacticsCenter:_handleEquipDeleteAward(content)
  DebugOut("_handleEquipDeleteAward = ")
  DebugTable(content)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(function()
    GameObjectTacticsCenter:_requestForEquipRecover(self._equipIdSelectNow)
  end)
  GameUIMessageDialog:SetNoButton(function()
  end)
  local infoTable = {}
  for _, item in ipairs(content.awards) do
    local itemText = GameHelper:GetAwardText(item.item_type, item.number, item.no)
    table.insert(infoTable, itemText)
  end
  local infoStr = table.concat(infoTable, " ")
  local textInfo = string.format(GameLoader:GetGameText("LC_MENU_TACTICAL_CENTER_DECOMPOSING_INFO"), infoStr)
  local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  GameUIMessageDialog:Display(textTitle, textInfo)
end
function GameObjectTacticsCenter:_commonNetErrorHandle(content)
  if content.code ~= 0 then
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
  end
end
function GameObjectTacticsCenter:_handleEquipDeleteACK(content)
  local equipDeleted = tcdm:deleteAUnusedEquip(content.equip_id)
  assert(equipDeleted and "logic error")
  local infoTable = {}
  for _, gameItem in ipairs(equipDeleted.decompose) do
    local itemText = GameHelper:GetAwardText(gameItem.item_type, gameItem.number, gameItem.no)
    table.insert(infoTable, itemText)
  end
  local infoStr = table.concat(infoTable, " ")
  local nowfoStr = string.format(GameLoader:GetGameText("LC_MENU_ITEM_REWARDS_ALERT"), infoStr)
  self._equipIdSelectNow = nil
  self:_updateUIEquipList(self._vesselOpenNow)
  self:dataDrive_UpdateEquipInfoAndBtn()
  self:dataDrive_UpdateEquipInListHighlightState()
end
function GameObjectTacticsCenter:ReRequestData()
  NetMessageMgr:SendMsg(NetAPIList.enter_tactical_req.Code, nil, GameObjectTacticsCenter.ReConstructData, false, nil)
end
function GameObjectTacticsCenter.ReConstructData(msgType, content)
  if msgType == NetAPIList.enter_tactical_ack.Code then
    GameObjectTacticsCenter.slotRedPointData = {}
    for k, v in ipairs(content.slots) do
      local data = {}
      data.vessel = v.vessel
      data.red_point = v.red_point
      table.insert(GameObjectTacticsCenter.slotRedPointData, data)
    end
    TacticsCenterDataManager:initDataUseEnterTacticalACK(content)
    GameObjectTacticsCenter:RefreshRedPoint()
  end
  return false
end
function GameObjectTacticsCenter:_handleEnterTacticalACK(content)
  DebugOut("GameObjectTacticsCenter:_handleEnterTacticalACK")
  TacticsCenterDataManager:initDataUseEnterTacticalACK(content)
  self:_invokeAS("_root", "luafs_setRootVisible", true)
  self:_moveUIIn()
  self:_updateUIMain()
  self:TutorialCheckRefinebutton()
  self:RefreshRedPoint()
end
function GameObjectTacticsCenter:RefreshRedPoint()
  self:_invokeAS("_root", "setSlotFixRedPoint", false)
  if RefineDataManager.used_times and RefineDataManager.used_times > 0 then
    self:_invokeAS("_root", "setSlotFixRedPoint", true)
  else
    self:_invokeAS("_root", "setSlotFixRedPoint", false)
  end
  if self.slotRedPointData then
    for k, v in ipairs(self.slotRedPointData) do
      if 0 < v.red_point then
        self:_invokeAS("_root", "setSlotEquipRedPoint", v.vessel, true)
      else
        self:_invokeAS("_root", "setSlotEquipRedPoint", v.vessel, false)
      end
    end
  end
end
function GameObjectTacticsCenter:_handleEquipOnACK(content)
  if __equals(0, content.code) then
    if __equals("0", content.old_equip_id) then
      TacticsCenterDataManager:equipEquip(content.vessel, content.new_equip_id)
    else
      TacticsCenterDataManager:exchangeEquip(content.vessel, content.new_equip_id)
    end
    self._equipIdSelectNow = content.new_equip_id
    self:_updateUIMain()
    self:_updateUIEquipList(self._vesselOpenNow)
    self:dataDrive_UpdateEquipInfoAndBtn()
    self:dataDrive_UpdateEquipInListHighlightState()
    self:dataDrive_UpdateAllSlotHighlightState()
  else
  end
end
function GameObjectTacticsCenter:_handleEquipOffACK(content)
  if __equals(0, content.code) then
    TacticsCenterDataManager:unequipEquip(content.vessel)
    self._equipIdSelectNow = nil
    self:_updateUIMain()
    self:_updateUIEquipList(self._vesselOpenNow)
    self:dataDrive_UpdateEquipInfoAndBtn()
    self:dataDrive_UpdateEquipInListHighlightState()
    self:dataDrive_UpdateAllSlotHighlightState()
  else
  end
end
function GameObjectTacticsCenter:_handleUnlockSlotACK(content)
  DebugOut("GameObjectTacticsCenter:_handleUnlockSlotACK")
  DebugOut(content)
  if __equals(0, content.code) then
    TacticsCenterDataManager:updateUnlockSlotConditionAndCast(content.next)
    self:_updateUIMain()
    self:dataDrive_UpdateAllSlotHighlightState()
  else
  end
end
function GameObjectTacticsCenter:_invokeAS(...)
