local Camera = {}
local matrix = require("Matrix.tfl")
Camera.eyepos = matrix({
  {0},
  {0},
  {0}
})
Camera.lookat = matrix({
  {0},
  {0},
  {3}
})
Camera.fov = 60
Camera.near = 0.3
Camera.far = 1000
Camera.pixelRect = {1024, 768}
Camera.worldToCameraMatrix = nil
Camera.projectionMatrix = nil
Camera.worldToCameraMatrix_inv = nil
Camera.projectionMatrix_inv = nil
local deg2fov = function(x)
  return x * 3.1415926 / 180
end
local function v3_normalize(v3)
  local len = matrix.len(v3)
  v3[1][1] = v3[1][1] / len
  v3[2][1] = v3[2][1] / len
  v3[3][1] = v3[3][1] / len
end
local function _calcVP()
  local zAxis = Camera.eyepos - Camera.lookat
  v3_normalize(zAxis)
  local up = matrix({
    {0},
    {1},
    {0}
  })
  local xAxis = matrix.cross(zAxis, up)
  v3_normalize(xAxis)
  local yAxis = matrix.cross(xAxis, zAxis)
  v3_normalize(yAxis)
  local viewmat = matrix({
    {
      0,
      0,
      0,
      0
    },
    {
      0,
      0,
      0,
      0
    },
    {
      0,
      0,
      0,
      0
    },
    {
      0,
      0,
      0,
      1
    }
  })
  viewmat[1][1] = xAxis[1][1]
  viewmat[2][1] = xAxis[2][1]
  viewmat[3][1] = xAxis[3][1]
  viewmat[1][2] = yAxis[1][1]
  viewmat[2][2] = yAxis[2][1]
  viewmat[3][2] = yAxis[3][1]
  viewmat[1][3] = zAxis[1][1]
  viewmat[2][3] = zAxis[2][1]
  viewmat[3][3] = zAxis[3][1]
  local tmpmat = matrix({
    {
      1,
      0,
      0,
      0
    },
    {
      0,
      1,
      0,
      0
    },
    {
      0,
      0,
      1,
      0
    },
    {
      0,
      0,
      0,
      1
    }
  })
  tmpmat[1][4] = -Camera.eyepos[1][1]
  tmpmat[2][4] = -Camera.eyepos[2][1]
  tmpmat[3][4] = -Camera.eyepos[3][1]
  Camera.worldToCameraMatrix = viewmat * tmpmat
  local aspect = Camera.pixelRect[1] / Camera.pixelRect[2]
  local fov_v = deg2fov(Camera.fov)
  local tan_fov_v = math.tan(fov_v * 0.5)
  local tan_fov_h = tan_fov_v * aspect
  local projmat = matrix({
    {
      0,
      0,
      0,
      0
    },
    {
      0,
      0,
      0,
      0
    },
    {
      0,
      0,
      0,
      0
    },
    {
      0,
      0,
      0,
      0
    }
  })
  projmat[1][1] = 1 / (aspect * tan_fov_v)
  projmat[2][2] = 1 / tan_fov_v
  projmat[4][3] = -1
  projmat[3][3] = -(Camera.far + Camera.near) / (Camera.far - Camera.near)
  projmat[3][4] = -2 * Camera.far * Camera.near / (Camera.far - Camera.near)
  Camera.projectionMatrix = projmat
  Camera.worldToCameraMatrix_inv = matrix.invert(Camera.worldToCameraMatrix)
  Camera.projectionMatrix_inv = matrix.invert(Camera.projectionMatrix)
end
function Camera.SetParam(eyepos, lookat, pixelRect)
  assert(#eyepos == 3)
  assert(type(eyepos[1]) == "number")
  assert(type(eyepos[2]) == "number")
  assert(type(eyepos[3]) == "number")
  assert(#lookat == 3)
  assert(type(lookat[1]) == "number")
  assert(type(lookat[2]) == "number")
  assert(type(lookat[3]) == "number")
  Camera.eyepos[1][1] = eyepos[1]
  Camera.eyepos[2][1] = eyepos[2]
  Camera.eyepos[3][1] = eyepos[3]
  Camera.lookat[1][1] = lookat[1]
  Camera.lookat[2][1] = lookat[2]
  Camera.lookat[3][1] = lookat[3]
  assert(#pixelRect == 2)
  assert(type(pixelRect[1]) == "number")
  assert(type(pixelRect[2]) == "number")
  Camera.pixelRect = pixelRect
  _calcVP()
end
function Camera.WorldToScreenPoint(worldpos)
  assert(#worldpos == 3)
  assert(type(worldpos[1]) == "number")
  assert(type(worldpos[2]) == "number")
  assert(type(worldpos[3]) == "number")
  assert(Camera.worldToCameraMatrix, "use SetParam")
  local v4 = matrix({
    {
      worldpos[1]
    },
    {
      worldpos[2]
    },
    {
      worldpos[3]
    },
    {1}
  })
  local spos = Camera.projectionMatrix * Camera.worldToCameraMatrix * v4
  local ret = {}
  ret[1] = Camera.pixelRect[1] * 0.5 * (1 + spos[1][1] / spos[4][1])
  ret[2] = Camera.pixelRect[2] * 0.5 * (1 + spos[2][1] / spos[4][1])
  ret[3] = worldpos[3] - Camera.near
  return ret
end
function Camera.ScreenToWorldPoint(screenpos)
  assert(#screenpos == 3)
  assert(type(screenpos[1]) == "number")
  assert(type(screenpos[2]) == "number")
  assert(type(screenpos[3]) == "number")
  assert(Camera.worldToCameraMatrix, "use SetParam")
  local v4 = matrix({
    {
      screenpos[1]
    },
    {
      screenpos[2]
    },
    {
      screenpos[3]
    },
    {1}
  })
  local w = 1
  v4[1][1] = v4[1][1] / (Camera.pixelRect[1] * 0.5) - 1
  v4[2][1] = v4[2][1] / (Camera.pixelRect[2] * 0.5) - 1
  v4[3][1] = v4[3][1] + Camera.near
  local spos = Camera.worldToCameraMatrix_inv * Camera.projectionMatrix_inv * v4
  local s = screenpos[3] / spos[3][1]
  return {
    spos[1][1] * s,
    spos[2][1] * s,
    spos[3][1] * s
  }
end
return Camera
