WVEMapPath = luaClass(nil)
function WVEMapPath:ctor(id, point1, point2, pointCoords1, pointCoords2, length)
  self.mID = id
  self.mPointID_1 = point1
  self.mPointID_2 = point2
  self.mPointCoords_1 = pointCoords1
  self.mPointCoords_2 = pointCoords2
  self.mPathLength = length
end
function WVEMapPath:GetAnotherPointID(pointID)
  if self.mPointID_1 == pointID then
    return self.mPointID_2
  end
  return self.mPointID_1
end
