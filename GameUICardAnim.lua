local GameUICardAnim = LuaObjectManager:GetLuaObject("GameUICardAnim")
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIStarSystemGacha = GameUIStarSystemPort.gacha
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
GameUICardAnim.isNeedShowTip = false
local CardFileToDownload = {
  [1] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_12.png",
  [2] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_11.png",
  [3] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_10.png",
  [4] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_13.png",
  [5] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_14.png",
  [6] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_15.png",
  [7] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_16.png",
  [8] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_17.png",
  [9] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_18.png",
  [10] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_19.png",
  [11] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_20.png",
  [12] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_21.png",
  [13] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_2.png",
  [14] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_22.png",
  [15] = "LAZY_LOAD_DOWN_SPELL_PentaBatter_hurt_23.png"
}
function GameUICardAnim:ShowAnim(showType, data)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  for k, v in ipairs(CardFileToDownload) do
    AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/" .. v)
  end
  local flashObj = self:GetFlashObject()
  flashObj:lockInput()
  if showType == "buyTen" then
    flashObj:InvokeASCallback("_root", "SetTenRewardInfo", data)
    flashObj:InvokeASCallback("_root", "showConfirmTip", 10, self.isNeedShowTip)
    if GameSettingData.EnableSkipStarSystemAnim then
      flashObj:InvokeASCallback("_root", "ShowWithoutAnim")
    else
      flashObj:InvokeASCallback("_root", "beginShow")
    end
  elseif showType == "buyOne" then
    flashObj:InvokeASCallback("_root", "SetOneRewardInfo", data)
    flashObj:InvokeASCallback("_root", "showConfirmTip", 1, self.isNeedShowTip)
    if GameSettingData.EnableSkipStarSystemAnim then
      flashObj:InvokeASCallback("_root", "ShowWithoutAnim")
    else
      flashObj:InvokeASCallback("_root", "beginShow")
    end
  end
end
function GameUICardAnim:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function GameUICardAnim:SetIsNeedShowTip(isShow)
  self.isNeedShowTip = isShow
end
function GameUICardAnim:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUICardAnim:OnFSCommand(cmd, arg)
  if cmd == "buy_ten" then
    GameUIStarSystemGacha:OnFSCommand(cmd, arg)
  elseif cmd == "buy_one" then
    GameUIStarSystemGacha:OnFSCommand(cmd, arg)
  elseif cmd == "ConfirmQuit" then
    GameUICardAnim.isNeedShowTip = false
    local GameStateStarSystem = GameStateManager.GameStateStarSystem
    GameStateStarSystem:EraseObject(GameUICardAnim)
  elseif cmd == "ShowResultDetail" then
    local itemType, index = unpack(LuaUtils:string_split(arg, ":"))
    local type = ""
    local e_quality = 0
    local reward = GameUIStarSystemGacha.curResultList[tonumber(index)]
    type = reward.item_reward.number
    if reward.type == 0 then
      if type > 999 then
        e_quality = 2
      else
        e_quality = type % 10
      end
      local item = {
        item_type = itemType,
        number = type,
        no = 1,
        quality = e_quality
      }
      ItemBox:showItemBox("Medal", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    else
      ItemBox:realShowMedalDetail(reward.medal)
    end
  elseif cmd == "ShowDetail" then
    local itemType, index = unpack(LuaUtils:string_split(arg, ":"))
    local consume = {}
    local type = ""
    local e_quality = 0
    if itemType == "medal_item" then
      type = GameUIStarSystemGacha.itemList[tonumber(index)].item_reward.number
      if type > 999 then
        e_quality = 2
      else
        e_quality = type % 10
      end
      local item = {
        item_type = itemType,
        number = type,
        no = 1,
        quality = e_quality
      }
      ItemBox:showItemBox("Medal", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif itemType == "medal" then
      ItemBox:realShowMedalDetail(GameUIStarSystemGacha.medalList[tonumber(index)].medal)
    end
  end
end
