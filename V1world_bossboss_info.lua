local boss_info = GameData.world_boss.boss_info
boss_info[5300001] = {
  ID = 5300001,
  LEVEL = 1,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300002] = {
  ID = 5300002,
  LEVEL = 2,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300003] = {
  ID = 5300003,
  LEVEL = 3,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300004] = {
  ID = 5300004,
  LEVEL = 4,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300005] = {
  ID = 5300005,
  LEVEL = 14,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300006] = {
  ID = 5300006,
  LEVEL = 27,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300007] = {
  ID = 5300007,
  LEVEL = 40,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300008] = {
  ID = 5300008,
  LEVEL = 50,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300009] = {
  ID = 5300009,
  LEVEL = 60,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300102] = {
  ID = 5300102,
  LEVEL = 4,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300103] = {
  ID = 5300103,
  LEVEL = 5,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300104] = {
  ID = 5300104,
  LEVEL = 6,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300105] = {
  ID = 5300105,
  LEVEL = 7,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300106] = {
  ID = 5300106,
  LEVEL = 8,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300107] = {
  ID = 5300107,
  LEVEL = 9,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300108] = {
  ID = 5300108,
  LEVEL = 10,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300109] = {
  ID = 5300109,
  LEVEL = 11,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300110] = {
  ID = 5300110,
  LEVEL = 12,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300111] = {
  ID = 5300111,
  LEVEL = 13,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300112] = {
  ID = 5300112,
  LEVEL = 14,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300113] = {
  ID = 5300113,
  LEVEL = 15,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300114] = {
  ID = 5300114,
  LEVEL = 16,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300115] = {
  ID = 5300115,
  LEVEL = 17,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300116] = {
  ID = 5300116,
  LEVEL = 18,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300117] = {
  ID = 5300117,
  LEVEL = 19,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300118] = {
  ID = 5300118,
  LEVEL = 20,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300119] = {
  ID = 5300119,
  LEVEL = 21,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300120] = {
  ID = 5300120,
  LEVEL = 22,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300121] = {
  ID = 5300121,
  LEVEL = 23,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300122] = {
  ID = 5300122,
  LEVEL = 24,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300123] = {
  ID = 5300123,
  LEVEL = 25,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300124] = {
  ID = 5300124,
  LEVEL = 26,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300125] = {
  ID = 5300125,
  LEVEL = 27,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300126] = {
  ID = 5300126,
  LEVEL = 28,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300127] = {
  ID = 5300127,
  LEVEL = 29,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300128] = {
  ID = 5300128,
  LEVEL = 30,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300129] = {
  ID = 5300129,
  LEVEL = 31,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300130] = {
  ID = 5300130,
  LEVEL = 32,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300131] = {
  ID = 5300131,
  LEVEL = 33,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300132] = {
  ID = 5300132,
  LEVEL = 34,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300133] = {
  ID = 5300133,
  LEVEL = 35,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300134] = {
  ID = 5300134,
  LEVEL = 36,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300135] = {
  ID = 5300135,
  LEVEL = 37,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300136] = {
  ID = 5300136,
  LEVEL = 38,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300137] = {
  ID = 5300137,
  LEVEL = 39,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300138] = {
  ID = 5300138,
  LEVEL = 40,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300139] = {
  ID = 5300139,
  LEVEL = 41,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300140] = {
  ID = 5300140,
  LEVEL = 42,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300141] = {
  ID = 5300141,
  LEVEL = 43,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300142] = {
  ID = 5300142,
  LEVEL = 44,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300143] = {
  ID = 5300143,
  LEVEL = 45,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300144] = {
  ID = 5300144,
  LEVEL = 46,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300145] = {
  ID = 5300145,
  LEVEL = 47,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300146] = {
  ID = 5300146,
  LEVEL = 48,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300147] = {
  ID = 5300147,
  LEVEL = 49,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300148] = {
  ID = 5300148,
  LEVEL = 50,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300149] = {
  ID = 5300149,
  LEVEL = 51,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300150] = {
  ID = 5300150,
  LEVEL = 52,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300151] = {
  ID = 5300151,
  LEVEL = 53,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300152] = {
  ID = 5300152,
  LEVEL = 54,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300153] = {
  ID = 5300153,
  LEVEL = 55,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300154] = {
  ID = 5300154,
  LEVEL = 56,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300155] = {
  ID = 5300155,
  LEVEL = 57,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300156] = {
  ID = 5300156,
  LEVEL = 58,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300157] = {
  ID = 5300157,
  LEVEL = 59,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300158] = {
  ID = 5300158,
  LEVEL = 60,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300159] = {
  ID = 5300159,
  LEVEL = 61,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300160] = {
  ID = 5300160,
  LEVEL = 62,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300161] = {
  ID = 5300161,
  LEVEL = 63,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300162] = {
  ID = 5300162,
  LEVEL = 64,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300163] = {
  ID = 5300163,
  LEVEL = 65,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300164] = {
  ID = 5300164,
  LEVEL = 66,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300165] = {
  ID = 5300165,
  LEVEL = 67,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300166] = {
  ID = 5300166,
  LEVEL = 68,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300167] = {
  ID = 5300167,
  LEVEL = 69,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300168] = {
  ID = 5300168,
  LEVEL = 70,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300169] = {
  ID = 5300169,
  LEVEL = 71,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300170] = {
  ID = 5300170,
  LEVEL = 72,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300171] = {
  ID = 5300171,
  LEVEL = 73,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300172] = {
  ID = 5300172,
  LEVEL = 74,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300173] = {
  ID = 5300173,
  LEVEL = 75,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300174] = {
  ID = 5300174,
  LEVEL = 76,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300175] = {
  ID = 5300175,
  LEVEL = 77,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300176] = {
  ID = 5300176,
  LEVEL = 78,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300177] = {
  ID = 5300177,
  LEVEL = 79,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300178] = {
  ID = 5300178,
  LEVEL = 80,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300179] = {
  ID = 5300179,
  LEVEL = 81,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300180] = {
  ID = 5300180,
  LEVEL = 82,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300181] = {
  ID = 5300181,
  LEVEL = 83,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300182] = {
  ID = 5300182,
  LEVEL = 84,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300183] = {
  ID = 5300183,
  LEVEL = 85,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300184] = {
  ID = 5300184,
  LEVEL = 86,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300185] = {
  ID = 5300185,
  LEVEL = 87,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300186] = {
  ID = 5300186,
  LEVEL = 88,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300187] = {
  ID = 5300187,
  LEVEL = 89,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300188] = {
  ID = 5300188,
  LEVEL = 90,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300189] = {
  ID = 5300189,
  LEVEL = 91,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300190] = {
  ID = 5300190,
  LEVEL = 92,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300191] = {
  ID = 5300191,
  LEVEL = 93,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300192] = {
  ID = 5300192,
  LEVEL = 94,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300193] = {
  ID = 5300193,
  LEVEL = 95,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300194] = {
  ID = 5300194,
  LEVEL = 96,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300195] = {
  ID = 5300195,
  LEVEL = 97,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300196] = {
  ID = 5300196,
  LEVEL = 98,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300197] = {
  ID = 5300197,
  LEVEL = 99,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300198] = {
  ID = 5300198,
  LEVEL = 100,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300199] = {
  ID = 5300199,
  LEVEL = 101,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300200] = {
  ID = 5300200,
  LEVEL = 102,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300201] = {
  ID = 5300201,
  LEVEL = 103,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300202] = {
  ID = 5300202,
  LEVEL = 104,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300203] = {
  ID = 5300203,
  LEVEL = 105,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300204] = {
  ID = 5300204,
  LEVEL = 106,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300205] = {
  ID = 5300205,
  LEVEL = 107,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300206] = {
  ID = 5300206,
  LEVEL = 108,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300207] = {
  ID = 5300207,
  LEVEL = 109,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300208] = {
  ID = 5300208,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300220] = {
  ID = 5300220,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300210] = {
  ID = 5300210,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300211] = {
  ID = 5300211,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300212] = {
  ID = 5300212,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300213] = {
  ID = 5300213,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300214] = {
  ID = 5300214,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300215] = {
  ID = 5300215,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300216] = {
  ID = 5300216,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300217] = {
  ID = 5300217,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300218] = {
  ID = 5300218,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300219] = {
  ID = 5300219,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300220] = {
  ID = 5300220,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300221] = {
  ID = 5300221,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300222] = {
  ID = 5300222,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300223] = {
  ID = 5300223,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300224] = {
  ID = 5300224,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300225] = {
  ID = 5300225,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300226] = {
  ID = 5300226,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300227] = {
  ID = 5300227,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300228] = {
  ID = 5300228,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300229] = {
  ID = 5300229,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300230] = {
  ID = 5300230,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300231] = {
  ID = 5300231,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300232] = {
  ID = 5300232,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300233] = {
  ID = 5300233,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300234] = {
  ID = 5300234,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300235] = {
  ID = 5300235,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300236] = {
  ID = 5300236,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300237] = {
  ID = 5300237,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300238] = {
  ID = 5300238,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300239] = {
  ID = 5300239,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300240] = {
  ID = 5300240,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300241] = {
  ID = 5300241,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300242] = {
  ID = 5300242,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300243] = {
  ID = 5300243,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300244] = {
  ID = 5300244,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300245] = {
  ID = 5300245,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300246] = {
  ID = 5300246,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
boss_info[5300247] = {
  ID = 5300247,
  LEVEL = 110,
  vessels = 6,
  Avatar = "head38",
  Ship = "ship36"
}
