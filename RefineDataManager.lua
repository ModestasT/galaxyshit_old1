local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local GameUITacticsCenterRefine = require("data1/GameUITacticsCenterRefine.tfl")
function RefineDataManager:clearData(...)
  self.lock_num = 0
  self.unlock_lock_attr = {}
  self.lock_attr_price = {}
end
function RefineDataManager:InitData(content)
  self.lock_num = content.lock_num
  self.lock_max = content.lock_max
  self.unlock_lock_attr = content.unlock_lock_attr
  self.lock_attr_price = content.lock_attr_price
  self.last_refines = content.last_refines
  self.need_item = content.need_item
  self.used_times = content.used_times
  self.max_times = content.max_times
  self.item_lock = content.lock_item
  self.item_lock_cnt = content.lock_attr_cost
  self._usedCount = 0
  self:RegisterAllNtfMesgHandler()
end
function RefineDataManager:GetLockNum()
  return self.lock_num
end
function RefineDataManager:GetLockMax(...)
  return self.lock_max
end
function RefineDataManager:GetUnlockCondition(...)
  return self.unlock_lock_attr
end
function RefineDataManager:GetLockAttrPrice_item(nextId)
  assert(self.item_lock_cnt[nextId], "not find id " .. nextId)
  return self.item_lock_cnt[nextId]
end
function RefineDataManager:GetLockAttrPrice(nextId)
  if not self.lock_attr_price[nextId] then
    DebugOut("error lock_attr_price has no value ")
  end
  return self.lock_attr_price[nextId]
end
function RefineDataManager:GetLastRefine()
  return self.last_refines
end
function RefineDataManager:GetUsedTimes()
  return self.used_times
end
function RefineDataManager:GetMaxTimes(...)
  return self.max_times
end
function RefineDataManager:GetNeedItem(...)
  return self.need_item
end
function RefineDataManager:GetNextRefreshTime(...)
  return self.next_refresh
end
function RefineDataManager:SetNextRefreshTime(value)
  self.next_refresh = value
end
function RefineDataManager:GetIsOpenTacticsCenter(...)
  return self._isOpenTaticsCenter
end
function RefineDataManager:SetUsedCount(number)
  self._usedCount = number
end
function RefineDataManager:GetUsedCount()
  return self._usedCount
end
function RefineDataManager:EquipToItem(id)
  local item = {}
  item.item_type = "item"
  item.number = id
  item.no = 0
  item.level = 0
  DebugOut("item:")
  DebugTable(item)
  return item
end
function RefineDataManager:RegisterAllNtfMesgHandler(...)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.tactical_resource_ntf.Code, RefineDataManager.TacticalResourceNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.tactical_status_ntf.Code, RefineDataManager.isOpenTacticsCenterNtf)
end
function RefineDataManager:RemoveAllNtfMesgHandler(...)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.tactical_resource_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.tactical_status_ntf.Code)
end
function RefineDataManager.isOpenTacticsCenterNtf(content)
  DebugOut("isOpenTacticsCenterNtf:")
  DebugOut(content.status)
  RefineDataManager._isOpenTaticsCenter = content.status
end
function RefineDataManager.TacticalResourceNtf(content)
  DebugOut("TacticalResourceNtf:")
  DebugTable(content)
  RefineDataManager.used_times = content.used_times
  RefineDataManager.max_times = content.max_times
  RefineDataManager.next_refresh = content.next_refresh + 5
  GameUITacticsCenterRefine:_updateRefineCountToAsTime()
  local GameObjectTacticsCenter = LuaObjectManager:GetLuaObject("GameObjectTacticsCenter")
  GameObjectTacticsCenter:RefreshRedPoint()
end
