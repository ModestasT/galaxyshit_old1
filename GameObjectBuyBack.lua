local GameObjectBuyBack = LuaObjectManager:GetLuaObject("GameObjectBuyBack")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
function GameObjectBuyBack:show()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self.buybackIdx = nil
  GameStateManager:GetCurrentGameState():AddObject(GameObjectBuyBack)
  self:RequestPopBack()
end
function GameObjectBuyBack:OnEraseFromGameState(state)
  self:UnloadFlashObject()
end
function GameObjectBuyBack:RequestPopBack()
  NetMessageMgr:SendMsg(NetAPIList.shop_purchase_back_list_req.Code, nil, self.purchaseBackListCallback, true)
end
function GameObjectBuyBack.purchaseBackListCallback(msgType, content)
  if msgType == NetAPIList.shop_purchase_back_list_ack.Code then
    GameObjectBuyBack.purchaseBackList = content.items
    if not LuaUtils:table_empty(GameObjectBuyBack.purchaseBackList) then
    end
    GameUICollect:CheckTutorialCollect(content.code)
    GameObjectBuyBack:GetFlashObject():InvokeASCallback("_root", "showBuyBack")
    GameObjectBuyBack:RefreshPurchaseBack()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.shop_purchase_back_list_req.Code then
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  end
  return false
end
function GameObjectBuyBack:OnFSCommand(cmd, arg)
  if cmd == "buyback" and not self.buybackIdx then
    self.buybackIdx = tonumber(arg)
    local shop_purchase_back_req_param = {
      idx = self.buybackIdx
    }
    local function netFailedCallback()
      GameObjectBuyBack:RequestPopBack()
    end
    NetMessageMgr:SendMsg(NetAPIList.shop_purchase_back_req.Code, shop_purchase_back_req_param, self.purchasebackCallback, false)
  end
  if cmd == "buyback_moveout_over" then
    self:GetFlashObject():InvokeASCallback("_root", "hideBuyBackEnd")
    GameStateManager:GetCurrentGameState():EraseObject(GameObjectBuyBack)
  end
end
function GameObjectBuyBack.purchasebackCallback(msgType, content)
  if msgType == NetAPIList.shop_purchase_back_ack.Code then
    if content.code == 0 then
      for k, v in pairs(GameObjectBuyBack.purchaseBackList) do
        if v.idx == GameObjectBuyBack.buybackIdx then
          GameObjectBuyBack.purchaseBackList[k] = nil
          GameItemBag:RequestBag(true)
        end
      end
      GameObjectBuyBack.buybackIdx = nil
      GameObjectBuyBack:RefreshPurchaseBack()
    else
      GameObjectBuyBack.buybackIdx = nil
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 709 then
        local text = AlertDataList:GetTextFromErrorCode(130)
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        GameUIKrypton.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      GameObjectBuyBack:RefreshPurchaseBack()
    end
    return true
  end
  return false
end
function GameObjectBuyBack:RefreshPurchaseBack()
  DebugOut("=========>")
  DebugTable(GameObjectBuyBack.purchaseBackList)
  local BuyBackItems = {}
  for k, v in pairs(GameObjectBuyBack.purchaseBackList) do
    local item = {}
    item.cnt = v.cnt
    item.idx = v.idx
    item.strongLv = v.enchant_level
    if DynamicResDownloader:IsDynamicStuff(GameDataAccessHelper:GetItemRealType(v), DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(GameDataAccessHelper:GetItemRealType(v) .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        item.frame = "item_" .. GameDataAccessHelper:GetItemRealType(v)
      else
        item.frame = "temp"
        self:AddDownloadPath(GameDataAccessHelper:GetItemRealType(v), item)
      end
    else
      item.frame = tostring("item_" .. GameDataAccessHelper:GetItemRealType(v))
    end
    table.insert(BuyBackItems, item)
  end
  if GameObjectBuyBack:GetFlashObject() then
    GameObjectBuyBack:GetFlashObject():InvokeASCallback("_root", "setBuyBack", BuyBackItems)
  end
end
function GameObjectBuyBack:AddDownloadPath(item_type, item)
  local resName = item_type .. ".png"
  local extInfo = {}
  extInfo.item_type = item_type
  extInfo.item = item
  extInfo.localPath = "data2/" .. DynamicResDownloader:GetFullName(item_type .. ".png", DynamicResDownloader.resType.PIC)
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
if AutoUpdate.isAndroidDevice then
  function GameObjectBuyBack.OnAndroidBack()
    GameObjectBuyBack:GetFlashObject():InvokeASCallback("_root", "hideBuyBack")
  end
end
