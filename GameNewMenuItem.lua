local GameNewMenuItem = LuaObjectManager:GetLuaObject("GameNewMenuItem")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateCampaignSelection = GameStateManager.GameStateCampaignSelection
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local TutorialQeustArena = TutorialQuestManager.QuestTutorialArena
local QuestTutorialTacticsCenter = TutorialQuestManager.QuestTutorialTacticsCenter
function GameNewMenuItem:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("modules_status", GameNewMenuItem.onGlobalNewItemChange)
end
function GameNewMenuItem.onGlobalNewItemChange()
  if DebugConfig.isSuperMode then
    return
  end
  local content = GameGlobalData:GetData("modules_status")
  DebugOut("GameNewMenuItem.onGlobalNewItemChange")
  DebugTable(content.modules)
  DebugTable(GameNewMenuItem.status)
  GameUIBarRight:FormatRightMenu()
  if GameNewMenuItem.status then
    for k, v in pairs(content.modules) do
      for tk, tv in pairs(GameNewMenuItem.status) do
        if tv.name == v.name and tv.status ~= v.status and v.name ~= "price_off" and v.name ~= "hire" and v.name ~= "bag" and v.name ~= "friends" and v.name ~= "chat" and v.name ~= "rush" and v.name ~= "task" and v.name ~= "colonial" and v.name ~= "wd" and v.name ~= "tc" and v.name ~= "remodel" and v.name ~= "worldboss" and v.name ~= "reform" and v.name ~= "ac" and v.name ~= "lieutenant" and v.name ~= "interference_interfere" and v.name ~= "equip_evolution" and v.name ~= "crusade" and v.name ~= "wc" and v.name ~= "prime_wve" and v.name ~= "infinite_cosmos" and v.name ~= "combat_speed" and v.name ~= "centalsystem" and v.name ~= "glc" and v.name ~= "starwar" and v.name ~= "collection" and v.name ~= "recruit" and v.name ~= "dicoslab" and v.name ~= "spacewar" and v.name ~= "official" and v.name ~= "consortium" and v.name ~= "assistant" and v.name ~= "empire_colosseum" and v.name ~= "large_map" then
          if immanentversion == 2 then
            if tv.name == "prestige" then
            elseif tv.name == "colonial" then
            else
              GameNewMenuItem:Push(tv.name)
            end
          elseif immanentversion == 1 then
            GameNewMenuItem:Push(tv.name)
          end
        end
      end
    end
  end
  GameNewMenuItem.status = content.modules
end
function GameNewMenuItem:Push(key)
  DebugOut("GameNewMenuItem:Push ", key)
  self.newStack = self.newStack or {}
  if LuaUtils:array_find(self.newStack, key) == -1 then
    table.insert(self.newStack, key)
    if LuaUtils:table_size(self.newStack) == 1 then
      self:checkShow()
    end
  end
end
function GameNewMenuItem:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function GameNewMenuItem:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameNewMenuItem:checkShow()
  if self.newStack and not LuaUtils:table_empty(self.newStack) then
    self:LoadFlashObject()
    if not GameStateGlobalState:IsObjectInState(self) then
      GameStateGlobalState:AddObject(self)
    end
    self.isShow = true
    self:ShowMenu(self.newStack[1])
  end
end
function GameNewMenuItem:IsShown()
  return self.isShow
end
function GameNewMenuItem:ShowMenu(key)
  DebugOut("GameNewMenuItem:ShowMenu ", key)
  self:GetFlashObject():InvokeASCallback("_root", "showMenu")
  local titleText, desText = GameLoader:GetGameText("LC_MENU_" .. string.upper(key) .. "_BUTTON"), GameLoader:GetGameText("LC_MENU_UNLOCK_CHAR")
  if key == "remodel" then
    titleText = GameLoader:GetGameText("LC_MENU_REBUILD_INTRANCE_BUTTON")
  end
  self:GetFlashObject():InvokeASCallback("_root", "setMenu", titleText, desText, key)
end
function GameNewMenuItem:OnFSCommand(cmd, arg)
  if cmd == "CloseDialog" then
    DebugTutorial("CloseDialog: ", self.newStack[1])
    if self.isShow then
      self.isShow = false
      table.remove(self.newStack, 1)
      if #self.newStack > 0 then
        self:checkShow()
      else
        GameStateGlobalState:EraseObject(self)
      end
      if self.newStack[1] == "arena" and not TutorialQeustArena:IsFinished() and not TutorialQeustArena:IsActive() then
        TutorialQeustArena:SetActive(true)
      end
    end
    return
  end
  if cmd == "WellPress" then
    DebugTutorial("GameNewMenuItem:OnFSCommand(WellPress): ", self.newStack[1])
    if self.newStack[1] == "prestige" then
      AddFlurryEvent("ClickConfirm_UnlockPrestige", {}, 1)
    elseif self.newStack[1] == "fleets" then
      AddFlurryEvent("ClickConfirm_UnlockEquip", {}, 1)
    elseif self.newStack[1] == "enhance" then
      AddFlurryEvent("ClickConfirm_UnlockEnhance", {}, 1)
    end
  end
end
function GameNewMenuItem:DisplayAllowed()
  local ActiveState = GameStateManager:GetCurrentGameState()
  if ActiveState == GameStateBattlePlay then
    return false
  elseif ActiveState == GameStateArena then
    return false
  elseif ActiveState == GameStateBattleMap and GameStateBattleMap:IsObjectInState(GameUIEvent) then
    return false
  end
  return true
end
if AutoUpdate.isAndroidDevice then
  function GameNewMenuItem.OnAndroidBack()
    GameNewMenuItem:GetFlashObject():InvokeASCallback("_root", "hideMenu")
  end
end
