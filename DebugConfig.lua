DebugConfig = {
  isDebugOut = false,
  isDebugTable = false,
  isDebugValue = false,
  isDebugStack = false,
  isDebugTime = false,
  isDebugState = false,
  isDebugTutorial = false,
  isDebugFlurry = false,
  isDebugActivity = false,
  isDebugNews = false,
  isDebugStore = false,
  isDebugFestival = false,
  isDebugColonial = false,
  isDebugOutBattle = false,
  isDebugBattlePlay = false,
  isDebugBattlePlayBattle = false,
  isDebugWD = false,
  isDebugMem = false,
  isWriteToLogFile = false,
  isWriteTableToFile = false,
  isDebugTip = false,
  isAssertAddObject = false,
  isDebugFPS = false,
  isDebugBattle = false,
  isAutoSkipBattle = false,
  isSkipFirstScene = false,
  isDebugBattllePlayLoad = false,
  isDebugTutorialQuest = false,
  isSuperMode = false,
  isEnterGameDirectly = false,
  isDebugWelcome = false
}
local m_sendingWarning = {}
if not AutoUpdate.isDeveloper then
  DebugConfig = {}
end
function DebugWarning(assertSuccess, info)
  if assertSuccess then
    return
  end
  DebugOut("DebugWarning: " .. info)
  do return end
  if m_sendingWarning[info] ~= nil then
    return
  end
  m_sendingWarning[info] = true
  local warn_info = {
    device_name = ext.GetDeviceName(),
    device_real_name = ext.GetDeviceName(),
    udid = ext.GetIOSOpenUdid(),
    player_id = GameGlobalData:GetData("userinfo") and GameGlobalData:GetData("userinfo").player_id or -1,
    level = GameGlobalData:GetData("levelinfo") and GameGlobalData:GetData("levelinfo").level or 0,
    info_type = "Warning",
    info_code = info
  }
  DebugTable(warn_info)
  NetMessageMgr:SendMsg(NetAPIList.warn_req.Code, warn_info, nil, false, nil)
end
function DebugTraceStack()
  if DebugConfig.isDebugStack then
    print("------------------------------traceback start------------------------------\n")
    print(debug.traceback())
    print("------------------------------traceback end------------------------------\n")
  end
end
function DebugOut(...)
